@echo off

cd c:\Git-Sp\salesforce.web.ui.tests\Scripts
python -m unittest wizard_ui_regression.wizard_regres autoqa

REM Email out the results and clean up reports folder
python c:\Git-Sp\salesforce.web.ui.tests\Scripts\mail_report.py
exit /B

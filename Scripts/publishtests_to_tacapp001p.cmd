@echo off
REM Script for pushing new automation to remote test server

REM Variables
SET LOCAL_SERVER=SND-2779
SET LOCAL_USERS=Users
SET LOCAL_MTAK=mtak
SET LOCAL_GIT=git-sp
SET TEST_SERVER=TACAPP001P
SET LOCAL_SHARE=salesforce.web.ui.tests
SET TEST_SHARE=salesforce.web.ui.tests
SET BACKUP_SHARE=salesforce.web.ui.testsBak

REM Back up cmd script before copying new code
echo ********** Backup Existing Automation **********
echo Source: \\%TEST_SERVER%\%TEST_SHARE%
echo Destination: \\%TEST_SERVER%\%BACKUP_SHARE%
xcopy \\%TEST_SERVER%\%TEST_SHARE%\*.* \\%TEST_SERVER%\%BACKUP_SHARE% /E /Y /EXCLUDE:C:\Users\mtak\ExcludeFromPush\Exclude.txt
REM salesforce.web.ui.tests (file://SND-2779/Users/mtak/git-sp/salesforce.web.ui.tests)

REM Copy new code to remote server
echo ********** Deploy New Automation **********
echo Source: \\%LOCAL_SERVER%\%LOCAL_USERS%\%LOCAL_MTAK%\%LOCAL_GIT%\%LOCAL_SHARE%
REM echo Source:\\SND-2779\Users\mtak\git-sp\salesforce.web.ui.tests
echo Destination: \\%TEST_SERVER%\%TEST_SHARE%
xcopy \\%LOCAL_SERVER%\%LOCAL_SHARE%\*.*  \\%TEST_SERVER%\%TEST_SHARE% /E /Y /c /EXCLUDE:C:\Users\mtak\ExcludeFromPush\Exclude.txt
echo \\%LOCAL_SERVER%\%LOCAL_SHARE%\*.*  \\%TEST_SERVER%\%TEST_SHARE% /E /Y /c /EXCLUDE:C:\Users\mtak\ExcludeFromPush\Exclude.txt
REM Uncomment to close shell on exit
REM exit \B


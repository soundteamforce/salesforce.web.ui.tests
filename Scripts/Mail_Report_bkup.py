import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import os

smtp_server = "SEAQA2APP-1"
email_from = "mtak@soundphysicians.com"
email_to = "mtak@soundphysicians.com,JMoshay@soundphysicians.com,jblagaila@soundphysicians.com," \
           "kadams@soundphysicians.com,edelrosario@soundphysicians.com,tsidhu@soundphysicians.com," \
           "mrigney@soundphysicians.com,jmcgrory@soundphysicians.com,scott.armitage@eightcloud.com," \
           "jimmy.perez@eightcloud.com,brian.mccall@eightcloud.com,bobby.capulong@eightcloud.com," \
           "sdfc-servacct@soundphysicians.com"
subject = "CI Automation Test Run - "
message_body = 'Report from Continuous Integration Automation Test Run\n'
report_path = "C:/CIReports"
run_report_name = "PassResults.txt"
report_files = []
run_report_status = "SUCCESS"
report_fail_status = "FAILURE"
report_fail_match = "FAIL"
run_header_match = "CIVT"

run_result = open(report_path + "/" + run_report_name, "r")
run_status = run_result.readlines()
run_result.close()

for x in run_status:
    if x.find(report_fail_match) >= 0:
        run_report_status = report_fail_status
    if not x.find(run_header_match) >= 0:
        message_body = message_body + "\n" + x

subject = subject + run_report_status

msg = MIMEMultipart()
msg['Subject'] = subject
msg['From'] = email_from
msg['To'] = email_to
text = message_body
msg.attach(MIMEText(text, 'plain'))

listOfFiles = os.listdir(report_path)

for f in listOfFiles:
    report_files.append(f)

for f in report_files:
    file_path = os.path.join(report_path, f)
    attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
    attachment.add_header('Content-Disposition', 'attachment', filename=f)
    msg.attach(attachment)

server = smtplib.SMTP(smtp_server)
server.sendmail(msg["From"], msg["To"].split(","), msg.as_string())

filelist = [f for f in os.listdir(report_path) if f.endswith(".png")]
for f in filelist:
    os.remove(os.path.join(report_path, f))

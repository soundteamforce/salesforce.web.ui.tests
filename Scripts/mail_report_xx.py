import os
from os import sys, path
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import time
from datetime import datetime
import shutil

message_body = 'Test Results from Wizard Web UI Automation Test Run\n\n'
run_report_name = "wizard_results.txt"
report_files = []
run_report_status = "SUCCESS"
report_fail_status = "FAILURE"
report_fail_match = "FAIL"
report_pass_match = "PASS"
run_header_match = "Sound"


fail_count = 0
pass_count = 0
run_result = open('C:\\Users\mtak\git-sp\salesforce.web.ui.tests\Logs\Jul06\Test_Misc.py__07-06_10-39.log', "r")

run_status = run_result.readlines()
run_result.close()

for x in run_status:
    if x.find(report_fail_match) >= 0:
        fail_count += 1
    if x.find(report_pass_match) >= 0:
        pass_count += 1

    if not x.find(run_header_match) >= 0:
        message_body = message_body + x


subject =  str(pass_count) + ' Passed - ' + str(fail_count) + ' Failed'
print(subject)

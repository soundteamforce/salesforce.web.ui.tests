import os
from os import sys, path
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import time
from datetime import datetime
import shutil

smtp_server = "SEAQA2APP-1"
email_from = "mtak@soundphysicians.com"
email_to = "mtak@soundphysicians.com"
subject = "Wizard Web UI Automation Test Run "
message_body = 'Test Results from Wizard Web UI Automation Test Run\n\n'
report_path = "C:/salesforce_reports"
run_report_name = "wizard_results.txt"
report_files = []
run_report_status = "SUCCESS"
report_fail_status = "FAILURE"
report_fail_match = "FAIL"
run_header_match = "Sound"

run_result = open(report_path + "/" + run_report_name, "r")
run_status = run_result.readlines()
run_result.close()
# os.remove(os.path.join(report_path, run_report_name))

for x in run_status:
    if x.find(report_fail_match) >= 0:
        run_report_status = report_fail_status
    if not x.find(run_header_match) >= 0:
        message_body = message_body + "\n" + x

subject = subject + run_report_status


msg = MIMEMultipart()
msg['Subject'] = subject
msg['From'] = email_from
msg['To'] = email_to
text = message_body
msg.attach(MIMEText(text, 'plain'))

listOfFiles = os.listdir(report_path)

# Don't append the screen shots, this can cause the email to be too large to send
for f in listOfFiles:
    if not f.endswith(".png"):
        report_files.append(f)

for f in report_files:
    file_path = os.path.join(report_path, f)
    attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
    attachment.add_header('Content-Disposition', 'attachment', filename=f)
    msg.attach(attachment)

server = smtplib.SMTP(smtp_server)
server.sendmail(msg["From"], msg["To"].split(","), msg.as_string())


# Create directory to preserve test results
filelist = [f for f in os.listdir(report_path)]

date = datetime.today()
# formatted_date = date.timestamp()
formatted_date = time.strftime('%m-%d_%H-%M')
dir_name = "C:/" + str(formatted_date) + "sf_wizard_Results"
os.makedirs(dir_name)

for f in filelist:
    os.remove(os.path.join(report_path, f))
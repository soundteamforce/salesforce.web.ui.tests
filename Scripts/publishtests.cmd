@echo off
REM Script for pushing new automation to remote test server

REM Variables
SET LOCAL_SERVER=SND-2229
SET TEST_SERVER=PWIN10-03
SET LOCAL_SHARE=salesforce.web.ui.tests
SET TEST_SHARE=salesforce.web.ui.tests
SET BACKUP_SHARE=salesforce.web.ui.testsBak

REM Back up cmd script before copying new code
echo ********** Backup Existing Automation **********
echo Source: \\%TEST_SERVER%\%TEST_SHARE%
echo Destination: \\%TEST_SERVER%\%BACKUP_SHARE%
xcopy \\%TEST_SERVER%\%TEST_SHARE%\*.* \\%TEST_SERVER%\%BACKUP_SHARE% /E /Y /H /EXCLUDE://SND-2229/NightlyJobs/Exclude.txt
REM salesforce.web.ui.tests (file://SND-2229/salesforce.web.ui.tests)

REM Copy new code to remote server
echo ********** Deploy New Automation **********
echo Source: \\%LOCAL_SERVER%\%LOCAL_SHARE%
REM echo Source://SND-2229/salesforce.web.ui.tests
echo Destination: \\%TEST_SERVER%\%TEST_SHARE%
xcopy \\%LOCAL_SERVER%\%LOCAL_SHARE%\*.*  \\%TEST_SERVER%\%TEST_SHARE% /E /Y /h /c /EXCLUDE://SND-2229/NightlyJobs/Exclude.txt
echo \\%LOCAL_SERVER%\%LOCAL_SHARE%\*.*  \\%TEST_SERVER%\%TEST_SHARE% /E /Y /h /c /EXCLUDE://SND-2229/NightlyJobs/Exclude.txt
REM Uncomment to close shell on exit
REM exit \B


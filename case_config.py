# This config is needed to run case_bvt
import sys
sys.path.append('../')
from datetime import datetime

now = datetime.now()

today_date = now.strftime('%m/%d/%Y')
today_datetime = now.strftime('%m-%d %H:%M')

log_file_name = "care_bvt"


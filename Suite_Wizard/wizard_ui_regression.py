from os import sys, path
from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search
from Helpers import provider_agreement_helper as pah
from Helpers import pa_wizard_helper as pawh
from Helpers import pa_api_helper as paah
from Helpers import at_api_helper as ath
from Helpers import candidate_helper as canh
from Helpers import contact_helper as conh

# --------------------------------------------------------------------------------------
#   This is Wizard Regression
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_regres(unittest.TestCase):
    test_to_run = [58]  # Tests with guidelines
    #test_to_run = [2,5,6,7,16,19,21,22,23,29,30,31,32,33,34,35,36,37,38]      # Tests with Perm Recruiter
    #test_to_run = [40,41,42,43,44,45]  # These are all the testcases in the regression.

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))

    def tearDown(self):
        cl.log_off(1)
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard Regression  **************")
        cwuc.wd.quit()

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(1 in test_to_run, "test_01_agreement_guideline")
    def test_01_agreement_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter adds Guideline In Provider Employment Agreement"
        lg.log_results("\t TEST CASE: 01\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        if w.add_agreement_guideline():
            pa_pg.delete_terms(1, 0)      # comp_at_num, addnl_at_num to delete

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(2 in test_to_run, "test_02")
    def test_02_update_approved_guideline(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter updates values of Approved Agreement Guideline"
        lg.log_results("\t TEST CASE: 02\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['amount'])
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'}}
        hm.approve_reject_terms(test_terms_dictionary)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')     # Avoid logoff errors


    #
    # ------------## REQUIREMENTS - ('Sarah Baugus', 'CDL') ## ------------------ ##
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(3 in test_to_run, "test_03")
    def test_03_cdl_night_shift_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Staffing Services Adds Night Shift Term In CDL"
        lg.log_results("\t TEST CASE: 03\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.APPCL]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.APPCL['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    #
    # --------## REQUIREMENTS 'Saira Ayub', 'Ashley Rogers' for Both 'Initial CDL')## ------- ##
    @data((tv.prod_usr_echo_recruiter, 'Initial CDL'))
    @unpack
    @unittest.skipUnless(4 in test_to_run, "test_04")
    def test_04_test_shift_pay_and_commencement_bonus_and_work_unit_term(self, dt_login_as_uname,
                                                                         dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Staffing Consultant Adds Swing Shift, Admin " \
                              "Stipend and ICU Shift In Initial CDL"
        lg.log_results("\t TEST CASE: 04\t " + cwuc.test_case_name)

        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.ADMST, tv.ICU]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.SSWING['at_description']},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': tv.ADMST['at_description']},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': tv.ICU['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(5 in test_to_run, "test_05")
    def test_05_test_more_than_13_ats(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter Adds 15 Agreement Terms In Provider Employment Agreement"
        lg.log_results("\t TEST CASE: 05\t " + cwuc.test_case_name)
        total_num_of_terms = 16
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            tv.exe_changes_btn_on = False
            test_terms = [tv.VISA, tv.CLINC, tv.SIGBN, tv.CHFST, tv.HNIGHT, tv.SADMIN, tv.DAY, tv.SSWING, tv.CLSAL,
                          tv.ICU, tv.PTO, tv.PRDBN, tv.RELOC]
            w.add_agreement_term(test_terms)
            tv.exe_changes_btn_on = True
            test_terms = [tv.WLOAD, tv.RETBN]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    #
    # -------------## REQUIREMENTS Multiple Candidate ## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(6 in test_to_run, "test_06")
    def test_06_multiple_candidates(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter Adds Term For Multiple Candidates In Provider Employment " \
                              "Agreement"
        tv.exe_changes_btn_on = False
        lg.log_results("\t TEST CASE: 06\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.WLOAD]
            w.add_agreement_term(test_terms)
            w.change_candidate()
            test_terms_2 = [tv.SIGBN]
            tv.exe_changes_btn_on = True
            w.add_agreement_term(test_terms_2)


    # -------------## REQUIREMENTS ('Jill Albach', 'PEA')## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(7 in test_to_run, "test_07")
    def test_07_non_overlapping_multiple_at_terms_same_type(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter Adds Multiple Non Overlapping Term Of Same Type In Provider " \
                              "Employment Agreement"
        lg.log_results("\t TEST CASE: 07\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            lg.log_results("\t \t TP#62142 - Support - Wizard Automation - Add Multiple AT")
            test_terms = [tv.CLSAL, tv.CLSAL2]
            w.add_agreement_term(test_terms)

    # -------------## REQUIREMENTS - PA and AT MUST be Buy Out  ####
    @data((tv.prod_usr_staffing_admin, 'Buy Out Letter'))
    @unpack
    @unittest.skipUnless(8 in test_to_run, "test_08")
    def test_08_test_buyout_pa_and_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard - Perm Recruiter Adds Buy Out Agreement Term In Buy Out Provider Agreement"
        lg.log_results("\t TEST CASE: 08\t " + cwuc.test_case_name)
        lg.log_results("\t \t TP#62778 - 1 - SPE - Build Buy Out Agreement Terms")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    #
    # ------------ REQUIREMENTS - ('Ashley Rogers', 'Initial CDL')- PA MUST be NON BO, AT - BO -----------#
    @data((tv.prod_usr_echo_recruiter, 'Initial CDL'))
    @unpack
    @unittest.skipUnless(9 in test_to_run, "test_09")
    def test_9_check_non_buyout_pa_and_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        #     This is the test to check the BO option should not exist in NON-BO PA drop-down.
        cwuc.test_case_name = "Wizard  - Buy Out Term doesn't exist under Non Buy Out Provider Agreement"
        lg.log_results("\t TEST CASE: 09\t " + cwuc.test_case_name)
        lg.log_results("\t \t TP#62778 - 2 - SPE - Build Buy Out Agreement Terms")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)


    # *********** REQUIREMENTS -  Admin can change stage TO  and FROM Final Agrmt Sent **************
    # Testing changing to FAS after executing whole process
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(10 in test_to_run, "test_010")
    def test_10_admin_change_pa_stage_to_and_from_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Admin can change stage value TO and FROM Final Agreement Sent"
        lg.log_results("\t TEST CASE: 10\t " + cwuc.test_case_name)
        test_terms = [tv.WRVU]
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.WRVU['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement Sent
            # Change the value of stage FROM FAS to approval requested.
            stg_h.change_pa_stage_value(tv.pa_stage_approvd_value)    # Approved
            lauw_search.login_as_with_search(dt_login_as_uname, 'user') #To avoid the error due to log off in  teardown.


    # ************ REQUIREMENTS -  Admin can change stage TO  and FROM Final Agrmt Execution  ***********
    # Testing change to FAE after PA is created.
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(11 in test_to_run, "test_011")
    def test_11_admin_change_pa_stage_to_and_from_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Admin can change stage value TO and FROM Final Agreement Executed"
        lg.log_results("\t TEST CASE: 11\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            pa_pg.unlock_record()
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # New
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')  #To avoid the error due to log off in
            # teardown.



    # ************ REQUIREMENTS -  non Admin can NOT change stage FROM Final Agrmt Execution to new  ***********
    # Testing change to FAE after PA is created.-
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(12 in test_to_run, "test_12")
    def test_12_non_admin_change_pa_stage_from_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin should not change stage value FROM Final Agreement Executed"
        lg.log_results("\t TEST CASE: 12\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed as a setup by admin
            # admin logoff after changing to FAE, non-admin user will try to update the value FROM FAE
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # change FROM Final Agreement Executed to new
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can change stage value from  Final Agrmt SENT to New  ***********
    # Testing change to FAE after PA is created.
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(13 in test_to_run, "test_13")
    def test_13_non_admin_change_pa_stage_from_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin can change stage value FROM Final Agreement Sent"
        lg.log_results("\t TEST CASE: 13\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement sent as a setup by admin
            # admin logoff after changing to FAE, non-admin user will try to update the value FROM FAE
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # can change FROM FAS to New
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can change stage value TO Final Agrmt SENT  ***********
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(14 in test_to_run, "test_14")
    def test_14_non_admin_can_not_change_pa_stage_to_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin should not change stage value TO Final Agreement Sent"
        lg.log_results("\t TEST CASE: 14\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TEACH]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'TEACH'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement sent
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can NOT change stage value TO Final Agrmt EXECUTED  ***********
    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(15 in test_to_run, "test_15")
    def test_15_non_admin_can_not_change_pa_stage_to_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin should not change stage value TO Final Agreement Executed"
        lg.log_results("\t TEST CASE: 15\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TREAD]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.TREAD['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement executed

    # ************ REQUIREMENTS - Amendment Provider Agreement by Perm Recruiting Director ***********
    @data((tv.prod_usr_perm_recruiter, 'Amendment'))
    @unpack
    @unittest.skipUnless(16 in test_to_run, "test_16")
    def test_16_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiting Director Adds Amendment Provider Agreement"
        lg.log_results("\t TEST CASE: 16\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        tv.exe_changes_btn_on = True
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'):
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
                w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
                pa_pg.navigate_to_provider_agreement_page()
                pa_pg.pa_recordtype_validation(tv.record_type_amnd_ddp)


    # ************ REQUIREMENTS -  Amendment Provider Agreement by People Enhance Support ***********
    @data((tv.prod_usr_people_support_enh, 'Amendment'))
    @unpack
    @unittest.skipUnless(17 in test_to_run, "test_17")
    def test_17_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Enhance Support Adds Amendment Provider Agreement"
        lg.log_results("\t TEST CASE: 17\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TREAD]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'TREAD'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 1)  # logoff
            pa_pg.approve_reject_pa(cv.case_RDO_user, 'Approve')
            cl.log_off()
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    # ************ REQUIREMENTS - Bug 66580 Support - Approval Process not firing when additional terms added
    # ***********
    @data((tv.prod_usr_people_support_enh, 'Compensation Change Letter'))
    @unpack
    @unittest.skipUnless(18 in test_to_run, "test_18")
    def test_18_pa_compensation_change_letter_rate_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Bug 66580 Support - Approval Process Not Firing When Additional Terms Added"
        lg.log_results("\t TEST CASE: 18\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.ORNTN, tv.PDRST]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.HSALL]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.ORNTN['at_description']},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': tv.PDRST['at_description']},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': tv.HSALL['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    # ************ REQUIREMENTS -  System Administrator can add Provider Agreement of Compensation Change Letter type without case number
    @data((tv.prod_usr_perm_recruiter, 'Compensation Change Letter'))
    @unpack
    @unittest.skipUnless(19 in test_to_run, "test_19")
    def test_19_compensation_change_letter_by_administrator(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - System Administrator adds Compensation Change Letter Provider Agreement without case number"
        lg.log_results("\t TEST CASE: 19\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, 'Provider Employment Agreement'):
            w.add_agreement_guideline()
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement executed
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(tv.test_admin_user, dt_pa_record_type):
                test_terms = [tv.APPSU]
                w.add_agreement_term(test_terms)
                sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
                lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    #************ REQUIREMENTS - Compensation Change Letter - Test People Support Enhance user can end terms on wizard
    # page for
    # multiple terms "Wizard  -  " *********** #

    @data((tv.prod_usr_people_support_enh, 'Compensation Change Letter'))
    @unpack
    @unittest.skipUnless(20 in test_to_run, "test_20")
    def test_20_compensation_change_letter_can_end_multiple_ats_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Support Enhance user can end terms on wizard page for multiple terms " \
                              "in Compensation Change Letter."
        lg.log_results("\t TEST CASE: 20\t " + cwuc.test_case_name)
        total_num_of_terms = 11
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.RELOC, tv.ADMST, tv.CHFST, tv.TRAST,  tv.LDPST, tv.STPND, tv.QUABN, tv.WLOAD, tv.HHCBN,
                          tv.ICU, tv.RETBN]
            w.add_agreement_term(test_terms)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.RELOC['at_description']},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': tv.ADMST['at_description']},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': tv.CHFST['at_description']},
                                     3: {'action_taken': 'Approve', 'term_type_to_use': tv.TRAST['at_description']},
                                     4: {'action_taken': 'Approve', 'term_type_to_use': tv.LDPST['at_description']},
                                     5: {'action_taken': 'Approve', 'term_type_to_use': tv.STPND['at_description']},
                                     6: {'action_taken': 'Approve', 'term_type_to_use': tv.QUABN['at_description']},
                                     7: {'action_taken': 'Approve', 'term_type_to_use': 'WORKLOAD'},
                                     8: {'action_taken': 'Approve', 'term_type_to_use': tv.HHCBN['at_description']},
                                     9: {'action_taken': 'Approve', 'term_type_to_use': tv.ICU['at_description']},
                                     10: {'action_taken': 'Approve', 'term_type_to_use': tv.RETBN['at_description']}
                                     }
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(21 in test_to_run, "test_21")
    def test_21_unapproved_term_added_later(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Unapproved Term Is Added After Initial Agreement Term is Approved"
        lg.log_results("\t TEST CASE: 21\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.CLINC]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'CLINC'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.SIGBN]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(22 in test_to_run, "test_22")
    def test_22_declined_terms(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Decline Agreement Terms"
        lg.log_results("\t TEST CASE: 22\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.ORNTN, tv.SIGBN]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Reject', 'term_type_to_use': 'ORIENTATION'},
                                     1: {'action_taken': 'Reject', 'term_type_to_use': 'SIGNING'}}
            hm.approve_reject_terms(test_terms_dictionary)
            pa_pg.validate_term_status(dt_login_as_uname, 2, 'Reject')
            w.validate_visibility_of_terms(test_terms, 'Reject')
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(23 in test_to_run, "test_23")
    def test_23_validate_visibility_of_terms_with_end_date(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Validate The Visibility Of Terms With End Date In Past."
        lg.log_results("\t TEST CASE: 23\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD, tv.SDAY_ENDDATE_45_DAYS_OLD, tv.HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD]
            w.add_agreement_term(test_terms)
            w.validate_visibility_of_terms(test_terms, 'end_date')

    @data((tv.prod_usr_echo_recruiter, 'Initial CDL'))
    @unpack
    @unittest.skipUnless(24 in test_to_run, "test_24")
    def test_24_check_review_status_for_term_added_later(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Check Review Status Updated Correctly For Term Added Later"
        lg.log_results("\t TEST CASE: 24\t " + cwuc.test_case_name)
        lg.log_results("\t TEST CASE: 24\t #69497 Bug- Wizard - AT Not Submitting for Approval")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            pah.validate_review_n_status('Pending', 'Pending Addition')
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(25 in test_to_run, "test_25")
    def test_25_cancel_or_decline_term_after_pa_final_executed_changed_to_declined_cancelled(self, dt_login_as_uname,
                                                                                             dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Term Can Be Approved, Rejected After Final Executed Provider Agreement."
        lg.log_results("\t TEST CASE: 25\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TRAST, tv.WRVU]
            w.add_agreement_term(test_terms)
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement executed
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.TRAST['at_description']},
                                     1: {'action_taken': 'Reject', 'term_type_to_use': tv.WRVU['at_description']}}
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pah.validate_review_n_status_dict(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(26 in test_to_run, "test_26")
    def test_26_cancel_or_decline_term_after_pa_final_executed_changed_to_declined_cancelled(self, dt_login_as_uname,
                                                                                             dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Outstanding Term are Declined Automatically When PA is Declined."
        lg.log_results("\t TEST CASE: 26\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.DAY]
            at_col_name = 'Status__c'
            at_col_value = 'Pending Addition'
            test_terms_dictionary = {0: {'action_taken': 'Decline', 'term_type_to_use': 'DAY'}}
            w.add_agreement_term(test_terms)
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_declined)  # Declined
            pah.validate_review_n_status_dict(test_terms_dictionary)
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)  # To help cleanup process
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            lg.log_results("\t \t Tear down cleans up the agreement terms")

    @data((tv.prod_usr_staffing_admin, 'Initial CDL'), (tv.prod_usr_staffing_admin, 'CDL'))
    @unpack
    @unittest.skipUnless(27 in test_to_run, "test_27")
    def test_27_test_adding_jeopardy_stipend(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Staffing Consultant Adds Jeopardy Stipend In Initial CDL and CDL"
        lg.log_results("\t TEST CASE: 27\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.EJEOP]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'JEOPARDY'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


    @data((tv.prod_usr_echo_recruiter, 'Initial CDL'))
    @unpack
    @unittest.skipUnless(28 in test_to_run, "test_28")
    def test_28_test_adding_jeopardy_stipend(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Echo Recruiter Adds Jeopardy Stipend In Initial CDL  and CDL"
        lg.log_results("\t TEST CASE: 28\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.EJEOP]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'JEOPARDY'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(29 in test_to_run, "test_29")
    def test_add_pea_anesthesia_physician(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Create anesthesia - Physician provider agreement."
        lg.log_results("\t TEST CASE: 29\t " + cwuc.test_case_name)
        # total_num_of_terms = 1
        cwuc.pea_anesthesia_crna = 0
        cwuc.pea_anesthesia_physician = 1
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.SSWING, tv.PRMNT]
        w.add_agreement_term(test_terms)
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.SSWING['at_description']},
                                 1: {'action_taken': 'Approve', 'term_type_to_use': tv.PRMNT['at_description']}}
        hm.approve_reject_terms(test_terms_dictionary)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unittest.skipUnless(30 in test_to_run, "test_30")
    @unpack
    def test_add_pea_anesthesia_crna(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Create anesthesia - CRNA provider agreement."
        lg.log_results("\t TEST CASE: 30\t " + cwuc.test_case_name)
        # total_num_of_terms = 1
        cwuc.pea_anesthesia_crna = 1
        cwuc.pea_anesthesia_physician = 0
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        # test_terms = [tv.EDLOAN]
        test_terms = [tv.CLSAL, tv.EDLOAN]
        w.add_agreement_term(test_terms)
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLSAL['at_description']},
                                 1: {'action_taken': 'Approve', 'term_type_to_use': tv.EDLOAN['at_description']}
                                 }
        hm.approve_reject_terms(test_terms_dictionary)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unittest.skipUnless(31 in test_to_run, "test_31")
    @unpack
    def test_add_compass_pa(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Create Compass Program Provider Agreement."
        lg.log_results("\t TEST CASE: 31\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.CLSAL, tv.SADMIN]
        w.add_agreement_term(test_terms)
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLSAL['at_description']},
                                 1: {'action_taken': 'Approve', 'term_type_to_use': tv.SADMIN['at_description']}
                                 }
        hm.approve_reject_terms(test_terms_dictionary)
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, 'Compass Program Agreement'):
            test_terms = [tv.COMSST]
            w.add_agreement_term(test_terms)
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.COMSST['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unittest.skipUnless(32 in test_to_run, "test_32")
    @unpack
    def test_add_Education_pa(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Create Education Stipend Agreement."
        lg.log_results("\t TEST CASE: 32\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.ACPBN, tv.EDLOAN, tv.CLSAL]
        w.add_agreement_term(test_terms)
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.ACPBN['at_description']},
                                 1: {'action_taken': 'Approve', 'term_type_to_use': tv.EDLOAN['at_description']},
                                 2: {'action_taken': 'Approve', 'term_type_to_use': tv.CLSAL['at_description']}}
        hm.approve_reject_terms(test_terms_dictionary)
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, 'Education Stipend Agreement'):
            test_terms = [tv.RESIST, tv.MEDEDUST]
            w.add_agreement_term(test_terms)
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.RESIST['at_description']},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': tv.MEDEDUST['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unittest.skipUnless(33 in test_to_run, "test_33")
    @unpack
    def test_add_medical_corps_pa(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Create Medical Corps Agreement."
        lg.log_results("\t TEST CASE: 33\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.ICU, tv.CLSAL]
        w.add_agreement_term(test_terms)
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.ICU['at_description']},
                                 1: {'action_taken': 'Approve', 'term_type_to_use': tv.CLSAL['at_description']}}
        hm.approve_reject_terms(test_terms_dictionary)
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, 'Medical Corps Agreement'):
            test_terms = [tv.MEDCORPST]
            w.add_agreement_term(test_terms)
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.MEDCORPST['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(34 in test_to_run, "test_34_perm_recruiter_pa_pea_term_wocl")
    def test_34_perm_recruiter_pa_pea_term_wocl(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "34 Add Weekend On Call Agreement Term - Shift and Hourly. "
        lg.log_results("\t \t #83585 YOZ - Weekend On Call Agreement Term.")
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HWOCL, tv.SWOCL]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(35 in test_to_run, "test_35_perm_recruiter_pa_pea_term_wocl")
    def test_35_perm_recruiter_pa_pea_term_wocl(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "35 Add Level Shift Pay Agreement Term. "
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        lg.log_results("\t \t #82674 TEI - Allow User to Create Level Shift Agreement Terms and Guidelines")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TELE]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(36 in test_to_run, "test_36_physician_term_ml_qtrly_threshold")
    def test_36_physician_term_ml_qtrly_threshold(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "36 Add Moonlight Agreement Term with Quarterly Threshold Interval for Physician"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        lg.log_results("\t \t #84735 PHX - Updates to Moonlighting AT Interval Restrictions")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, None, 'Physician'):
            test_terms = [tv.MLDAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'test_term': tv.MLDAY, 'expected_interval': tv.thrhld_interval_quarterly}}
            pawh.check_at_interval(test_terms_dictionary)
            ath.validate_at_short_code(cwuc.new_pa_name, 'MLDAY')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(37 in test_to_run, "test_37_APP_term_ml_qtrly_sl_critical_care")
    def test_37_APP_term_ml_qtrly_sl_critical_care(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "37 Add Moonlight Agreement Term with Quarterly Interval On Critical Care Service Line for APP"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        lg.log_results("\t \t #84735 PHX - Updates to Moonlighting AT Interval Restrictions")
        clinician_type = 'APP'
        service_line = 'Critical Care'
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, service_line, clinician_type):
            test_terms = [tv.MLICU]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'test_term': tv.MLICU, 'expected_interval': tv.thrhld_interval_quarterly}}
            pawh.check_at_interval(test_terms_dictionary)
            ath.validate_at_short_code(cwuc.new_pa_name, 'MOON')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(38 in test_to_run, "test_38_APP_term_ml_monthly_sl_non_critical_care")
    def test_38_APP_term_ml_monthly_sl_non_critical_care(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "38 Add Moonlight Agreement Term with Monthly Interval On Non Critical Care Service Line for APP"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        lg.log_results("\t \t #84735 PHX - Updates to Moonlighting AT Interval Restrictions")
        clinician_type = 'APP'
        service_line = ['Telemedicine', 'Partner Standard', 'TeleICU', 'EM','Hospital Medicine', 'Hospitalist', 'Anesthesia']
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, service_line, clinician_type):
            test_terms = [tv.MLDAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'test_term': tv.MLDAY, 'expected_interval': tv.thrhld_interval_monthly}}
            pawh.check_at_interval(test_terms_dictionary)

    @data((tv.prod_usr_people_support_enh, tv.pa_type_ccl))
    @unpack
    @unittest.skipUnless(39 in test_to_run, "Clinical_Case_agreement_term_all_description_type")
    def test_39_clinical_case_retro_ag_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#83451 - People support enhanced user Kathleen Aukland adds 4 Clinical Case agreement terms with all description type"
        lg.log_results("\t TEST CASE: 39\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.RETRO, tv.APPL, tv.BHAPL, tv.CASE]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.RETRO['at_description']},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': tv.APPL['at_description']},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': tv.BHAPL['at_description']},
                                     3: {'action_taken': 'Approve', 'term_type_to_use': tv.CASE['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    @data((tv.prod_usr_echo_recruiter, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(40 in test_to_run, "Echo recruiter adds term to CDL PA with existing cancelled term")
    def test_bug_86684_CDL_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "\t TEST CASE: 40\t Echo recruiter adds Stipend term to CDL PA with existing cancelled Stipend term"
        lg.log_results("\t \tBUG 86684 - Echo unable to Add Stipend Agreement Terms")
        lg.log_results("\t TEST CASE: 40\t " + cwuc.test_case_name)
        total_num_of_terms = 2
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT, tv.CHFST]
            at_col_name = 'Cancellation__c'
            at_col_value = True
            w.add_agreement_term(test_terms)
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_echo_recruiter, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(41 in test_to_run,
                         "Echo recruiter adds Stipend term to CDL PA with existing Pending Removal Hourly Pay term")
    def test_bug_86684_CDL_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Echo recruiter adds Stipend term to CDL PA with existing Pending Removal Hourly Pay term"
        lg.log_results("\t \tBUG 86684 - Echo unable to Add Stipend Agreement Terms")
        lg.log_results("\t TEST CASE: 41\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            at_col_name = 'Status__c'
            at_col_value = 'Pending Removal'
            w.add_agreement_term(test_terms)
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.ADMST]
            w.add_agreement_term(test_terms)
            at_col_name = 'Status__c'  # **** Helping cleanup process ***
            at_col_value = 'Pending Addition'
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)

    @data((tv.prod_usr_echo_recruiter, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(42 in test_to_run,
                         "Echo recruiter adds Stipend term to CDL PA with existing Confirmed Removal Hourly Pay term")
    def test_bug_86684_CDL_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Echo recruiter adds Stipend term to CDL PA with existing Confirmed Removal Hourly Pay term"
        lg.log_results("\t \tBUG 86684 - Echo unable to Add Stipend Agreement Terms")
        lg.log_results("\t TEST CASE: 42\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            at_col_name = 'Status__c'
            at_col_value = 'Confirmed Removal'
            w.add_agreement_term(test_terms)
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.ADMST]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_echo_recruiter, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(43 in test_to_run,
                         "Echo recruiter adds Admin Shift term to CDL PA with existing Declined Clinic Shift Pay term")
    def test_bug_86684_CDL_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Echo recruiter adds Stipend term to CDL PA with existing Declined Hourly Pay term"
        lg.log_results("\t \tBUG 86684 - Echo unable to Add Stipend Agreement Terms")
        lg.log_results("\t TEST CASE: 43\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.CLINC]
            at_col_name = 'Status__c'
            at_col_value = 'Declined'
            w.add_agreement_term(test_terms)
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.SADMIN]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_echo_recruiter, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(44 in test_to_run,
                         "Echo recruiter adds a term to CDL PA with existing Cancelled ICU Shift term")
    def test_bug_86684_CDL_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Echo recruiter adds Compensation Shift Pay term to CDL PA with existing Cancelled ICU Shift term"
        lg.log_results("\t \tBUG 86684 - Echo unable to Add Stipend Agreement Terms")
        lg.log_results("\t TEST CASE: 44\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            at_col_name = 'Status__c'
            at_col_value = 'Cancelled'
            w.add_agreement_term(test_terms)
            ath.update_existing_term(cwuc.new_pa_id, at_col_name, at_col_value)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.ICU]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_echo_recruiter, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(46 in test_to_run, "test_100")
    def test_bug_82449_AT_End_date_should_not_be_removed_after_cancelling_PA(self, dt_login_as_uname,
                                                                             dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Term End Date is not deleted on Cancelling/Declining Ending Provider Agreement"
        lg.log_results("\t \t BUG 82449 - End Dates Removed from Executed CDL")
        lg.log_results("\t TEST CASE: 46\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        lg.log_results(cwuc.new_pa2_name)
        test_terms = [tv.DAY]
        at_col_name = 'Status__c'
        at_col_value = 'Pending Addition'
        w.add_agreement_term(test_terms)
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'}}
        hm.approve_reject_terms(test_terms_dictionary)
        stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed
        pah.validate_review_n_status_dict(test_terms_dictionary)  # Confirmed Addition

        # Cancelling the term by adding another CDL
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()
            pa_pg.cancel_term(dt_login_as_uname, 1, 0)
        pah.validate_review_n_status_dict(test_terms_dictionary)  # Pending Removal

        # User wants to use the term so he cancels the PA
        stg_h.change_pa_stage(tv.pa_stage_value_cancelled, 0)  # Cancelled

        # Validate the stage on first PA and status,cancellation,starting agreement, end date fields on AT
        first_pa_id, first_pa_name = sh.get_existing_pa(cwuc.candidate_id_1)
        lg.log_results("\t \t First PA is: " + first_pa_id + " " + first_pa_name)
        paah.validate_pa_stage_value(first_pa_name, tv.pa_stage_value_fae)  # Final Agreement Executed
        ath.validate_at_details(first_pa_name)
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    #This test needs Workload & Schedule guideline to be created in create entity data with weeks in period
    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(47 in test_to_run, "WorkLoad_and_schedule_agreement_guideline_with_Weeks_in_schedule")
    def test_47_weeks_in_agreement_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#87687  - Perm Recruiter adds Guideline In Provider Employment Agreement with Weeks for Schedule"
        lg.log_results("\t TEST CASE: 47\t " + cwuc.test_case_name)
        cwuc.pea_anesthesia_crna = 1
        cwuc.pea_anesthesia_physician = 0
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        if w.add_agreement_guideline():
            pa_pg.delete_terms(0, 1)  # comp_at_num, addnl_at_num to delete

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea), (tv.prod_usr_people_support_enh, tv.pa_rec_tp_amendment))
    @unpack
    @unittest.skipUnless(48 in test_to_run, "WorkLoad & Schedule Agreement Term Type with Weeks as Period.")
    def test_48_workload_and_schedule_ag_term_weeks(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#87687 - Perm recruiting and People support adds WorkLoad & Schedule Agreement terms with Weeks for Period."
        lg.log_results("\t TEST CASE: 48\t " + cwuc.test_case_name)
        cwuc.pea_anesthesia_crna = 0
        cwuc.pea_anesthesia_physician = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.WLOAD2]
            w.add_agreement_term(test_terms)

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_ica), (tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(49 in test_to_run, "Candidate field automation, Independent Contractor PA type ")
    def test_50_clinical_case_retro_ag_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#90852 - Executed IC Provider Agreement Updates to Candidate/Hub Candidate."
        lg.log_results("\t TEST CASE: 49\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TELE]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'TELE'}}
            hm.approve_reject_terms(test_terms_dictionary)
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed
            canh.verify_candidate_details(cwuc.candidate_name_1, tv.pa_type_ica, cwuc.new_pa_name)

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_esa))
    @unpack
    @unittest.skipUnless(50 in test_to_run, "End dates are mandatory for Medical Education Stipend Agreement Term")
    def test_50_clinical_case_retro_ag_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#91380 - CRNA Education Stipend Agreement Provider Agreement"
        lg.log_results("\t TEST CASE: 50\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, tv.pa_type_pea)
        cwuc.error_msg = "Term End Date is required on Medical Education Stipend records."
        test_terms = [tv.MEDEDUST]   # variable without end dates
        w.add_agreement_term(test_terms)
        test_terms = [tv.MEDEDUST2]   # variable with end dates
        w.add_agreement_term(test_terms)

    # Create guideline without end date for this test in create_entity_data
    @data((tv.prod_usr_people_support_enh, tv.pa_rec_tp_amendment))
    @unpack
    @unittest.skipUnless(51 in test_to_run, "End dates are not mandatory for Compass Stipend, Med Corps Stipend, "
                                            "Residency Stipend and Retention Bonus Guidelines.")
    def test_51_end_dates_not_mandatory(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#93113 - Modify ATG, End Date Not Required for Specific Descriptions"
        lg.log_results("\t TEST CASE: 51\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['end_date'])


    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl),(tv.prod_usr_echo_recruiter, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(52 in test_to_run, "Primary Clinical Leader approves initial CDL PA document "
                                            "when Comp Terms meet Guidelines.")
    def test_52_Clinical_approver_approves_InitialCDL(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#97091 - Clinical Leader Identified - Comp TermsMeet Guidelines"
        lg.log_results("\t TEST CASE: 52\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            w.add_agreement_guideline()
            stg_h.change_pa_stage_value(tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name) # get name of approver
            cl.log_off()                         # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')       # Login as approver and approve PA
            # pa_pg.pa_recordtype_validation(tv.pa_rec_tp_intl_cdl_ddp, tv.pa_stage_value_da)
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl),(tv.prod_usr_echo_recruiter, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(53 in test_to_run, "Primary Clinical Leader approves initial CDL PA document "
                                            "when Comp Terms do not meet Guidelines.")
    def test_53_Clinical_approver_approves_InitialCDL(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#97094 - Clinical Leader Identified - Comp Terms Do not meet Guidelines"
        lg.log_results("\t TEST CASE: 53\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)

            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            # pa_pg.pa_recordtype_validation(tv.pa_rec_tp_intl_cdl_ddp, tv.pa_stage_value_da)
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl), (tv.prod_usr_echo_recruiter, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(54 in test_to_run, "Primary Clinical Leader is Inactive/Frozen. Secondary Clinical Approver "
                                            "approves initial CDL PA document when Comp Terms meet Guidelines.")
    def test_54_Primary_Clinical_approver_frozen(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#97096 - Primary Clinical Approver frozen/Inactive"
        lg.log_results("\t TEST CASE: 54\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            w.add_agreement_guideline()
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            # pa_pg.pa_recordtype_validation(tv.pa_rec_tp_intl_cdl_ddp, tv.pa_stage_value_da)
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl), (tv.prod_usr_echo_recruiter, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(55 in test_to_run, "Primary & Secondary Clinical Leader are Inactive/Frozen. Primary Compensation Approver "
                                            "approves initial CDL PA document when Comp Terms do not meet Guidelines.")
    def test_55_Primary_Secondary_Clinical_approve_inactive(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#97109 - Primary and Secondary Clinical Approvers frozen/Inactive"
        lg.log_results("\t TEST CASE: 55\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)

            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
             # pa_pg.pa_recordtype_validation(tv.pa_rec_tp_intl_cdl_ddp, tv.pa_stage_value_da)
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(56 in test_to_run,
                         "Primary Clinical Leader rejects initial CDL PA document when Comp Terms meet Guidelines.")
    def test_56_Primary_Clinical_approve_rejects_PA(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#97091 - Primary Clinical Approvers rejects Initial CDL."
        lg.log_results("\t TEST CASE: 56\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            w.add_agreement_guideline()
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Reject')  # Login as approver and approve PA
            # pa_pg.pa_recordtype_validation(tv.pa_rec_tp_intl_cdl, tv.pa_stage_value_dr)
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl)

    @data((tv.prod_usr_people_support_enh, tv.pa_type_ccl))
    # @data((tv.prod_usr_perm_recruiter, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(57 in test_to_run,
                                "Bug 110394 - Bad value for Moonlightning Night AT short code in Wizard")
    def test_57_Moonlightning_night_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Bug 110394 - Bad value for Moonlightning Night AT short code in Wizard"
        lg.log_results("\t TEST CASE: 57\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.MNIGHT, tv.MLDAY, tv.MLICU]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'MNIGHT'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'MLDAY'},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': 'MLICU'}}
            hm.approve_reject_terms(test_terms_dictionary)

            sc_des_dict = {tv.MNIGHT['at_description']: tv.MNIGHT['at_short_code'],
                           tv.MLDAY['at_description']: tv.MLDAY['at_short_code'],
                           tv.MLICU['at_description']: tv.MLICU['at_short_code']}

            ath.validate_at_short_code(cwuc.new_pa_name, sc_des_dict)

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(58 in test_to_run, "Clinical Ladder Tier field automation expansion")
    def test_58_clinical_ladder_tier_automation(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "#110455 - Clinical Ladder Tier Designation Automation Enhancements"
        lg.log_results("\t TEST CASE: 58\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Hospital Medicine', 'APP'):
            test_terms = [tv.TELE]
            w.add_agreement_term(test_terms)
            cl.log_off()   # log off as Perm Recruiter

            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed
            conh.verify_clt_value_frm_cont(cwuc.contact_id, 'Tier 1')
            canh.get_clt_val_frm_cand(cwuc.candidate_name_1, 'Tier 1')

            # Update the APP Training Path to Condensed or No Training AND Contract Start Date equals TODAY
            canh.update_can_training_path(cwuc.candidate_id_1, 'Condensed', 3)
            canh.update_ctrct_str_date(cwuc.candidate_id_1, 'today')

            conh.verify_clt_value_frm_cont(cwuc.contact_id, 'Tier 2')
            canh.get_clt_val_frm_cand(cwuc.candidate_name_1, 'Tier 2')

            # Delete the values as part of test teardown
            sh.delete_clt_value_from_cont(cwuc.contact_id)
            canh.update_can_training_path(cwuc.candidate_id_1, '')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(59 in test_to_run, "APP Welcome Email notification, APP Welcome to Sound Physicians Extended")
    def test_59_app_hm_extended(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "110492 - APP Welcome Email notification for HM SSL with Extended Training Path."
        lg.log_results("\t TEST CASE: 59\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Hospital Medicine', 'APP'):
            test_terms = [tv.CLINC]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLINC['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            canh.update_can_training_path(cwuc.candidate_id_1, 'Extended', 9)
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed

            # check for email sent date from PA and Email under Activities
            pa_pg.check_app_wc_email(cwuc.new_pa_name)

            # Delete the values as part of test teardown
            sh.delete_clt_value_from_cont(cwuc.contact_id)
            canh.update_can_training_path(cwuc.candidate_id_1, '', '')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(60 in test_to_run, "APP Welcome Email notification, APP Welcome to Sound Physicians Condensed")
    def test_60_app_hm_extended(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "110492 - APP Welcome Email notification for HM SSL with Condensed Training Path."
        lg.log_results("\t TEST CASE: 60\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Hospital Medicine', 'APP'):
            test_terms = [tv.CLINC]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLINC['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            canh.update_can_training_path(cwuc.candidate_id_1, 'Condensed', 3)
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed

            # check for email sent date from PA and Email under Activities
            pa_pg.check_app_wc_email(cwuc.new_pa_name)

            # Delete the values as part of test teardown
            sh.delete_clt_value_from_cont(cwuc.contact_id)
            canh.update_can_training_path(cwuc.candidate_id_1, '', '')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(61 in test_to_run, "APP Welcome Email notification, APP Welcome to Sound Physicians No "
                                            "Training Required")
    def test_61_app_hm_extended(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "110492 - APP Welcome Email notification for HM SSL with No Training Required."
        lg.log_results("\t TEST CASE: 61\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Hospital Medicine', 'APP'):
            test_terms = [tv.CLINC]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLINC['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            canh.update_can_training_path(cwuc.candidate_id_1, 'No Training Required')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed

            # check for email sent date from PA and Email under Activities
            pa_pg.check_app_wc_email(cwuc.new_pa_name)

            # Delete the values as part of test teardown
            sh.delete_clt_value_from_cont(cwuc.contact_id)
            canh.update_can_training_path(cwuc.candidate_id_1, '', '')

    # This test needs EM as Service Line
    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(62 in test_to_run,
                         "APP Welcome Email notification, APP Welcome to Sound Physicians EM Extended")
    def test_62_app_hm_extended(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "110492 - APP Welcome Email notification for EM SSL"
        lg.log_results("\t TEST CASE: 62\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Emergency Medicine', 'APP'):
            test_terms = [tv.CLINC]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLINC['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            canh.update_can_training_path(cwuc.candidate_id_1, 'Extended', 9)
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed

            # check for email sent date from PA and Email under Activities
            pa_pg.check_app_wc_email(cwuc.new_pa_name)

            # Delete the values as part of test teardown
            sh.delete_clt_value_from_cont(cwuc.contact_id)
            canh.update_can_training_path(cwuc.candidate_id_1, '', '')

    # This test needs Critical Care as Service Line
    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(63 in test_to_run,
                         "APP Welcome Email notification, APP Welcome to Sound Physicians Critical Care")
    def test_63_app_hm_extended(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "110492 - APP Welcome Email notification for Critical care SSL with Training path empty"
        lg.log_results("\t TEST CASE: 63\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Critical Care', 'APP'):
            test_terms = [tv.CLINC]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CLINC['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            canh.update_can_training_path(cwuc.candidate_id_1, '')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement Executed

            # check for email sent date from PA and Email under Activities
            pa_pg.check_app_wc_email(cwuc.new_pa_name)

            # Delete the values as part of test teardown
            sh.delete_clt_value_from_cont(cwuc.contact_id)
            canh.update_can_training_path(cwuc.candidate_id_1, '', '')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(64 in test_to_run, "US 109667 - MIP Bonus Compensation Term")
    def test_64_MIPB_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "US 109667 - MIP Bonus Compensation Term"
        lg.log_results("\t TEST CASE: 64\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.MIPBN]
            w.add_agreement_term(test_terms)
            test_term_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.MIPBN['at_description']}}
            hm.approve_reject_terms(test_term_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(65 in test_to_run, "US #72311 - Delete / Remove Line Item Button in Wizard")
    def test_65_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Perm Recruiter deletes/removes Line Items in Wizard"
        lg.log_results("\t TEST CASe: 65\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.MIPBN, tv.DAY, tv.CHFST]
            terms_to_delete = [tv.DAY, tv.CHFST]
            tv.delete_at_line_item = True
            w.add_agreement_term(test_terms, terms_to_delete)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.MIPBN['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(66 in test_to_run, "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_66_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Primary CRNA Approver approves Initial CDL of CRNA clinician"
        lg.log_results("\t TEST CASe: 66\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'APP'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(67 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_67_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Secondary CRNA Approver approves Initial CDL of CRNA clinician when Primary approver is " \
                              "Inactive/Frozen."
        lg.log_results("\t TEST CASE: 67\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'APP'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(68 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_68_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Primary Compensation Approver approves Initial CDL of CRNA clinician when Primary & " \
                              "Secondary CRNA approver fields are blank."
        lg.log_results("\t TEST CASe: 68\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'APP'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(69 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_69_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Primary Clinical Approver approves Initial CDL presentation of a Physician."
        lg.log_results("\t TEST CASe: 69\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'Physician'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(70 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_70_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Secondary Clinical Approver approves Initial CDL presentation of a Physician when " \
                              "Primary Clinical Approver is Inactive/blank."
        lg.log_results("\t TEST CASe: 70\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'Physician'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(71 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_71_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Primary Compensation Approver approves Initial CDL presentation of a Physician when " \
                              "Primary & Secondary Clinical Approvers are Inactive/blank."
        lg.log_results("\t TEST CASE: 71\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'Physician'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(72 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_72_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Secondary Compensation Approver approves Initial CDL presentation of a Physician when " \
                              "Primary & Secondary Clinical Approvers & Primary comp approvers are Inactive/blank."
        lg.log_results("\t TEST CaSe: 72\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'Physician'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            sh.update_user_active(cv.case_RDO_user, False)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)
            sh.update_user_active(cv.case_RDO_user, True)

    @data((tv.prod_usr_staffing_admin, tv.pa_type_initial_cdl))
    @unpack
    @unittest.skipUnless(73 in test_to_run,
                         "US #111975 - ANE Initial CDL (Presentation) Approval Process for CRNA/Physicians")
    def test_73_delete_at_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Secondary Compensation Approver approves Initial CDL presentation of CRNA when " \
                              "Primary & Secondary Clinical Approvers & Primary comp approvers are Inactive/blank."
        lg.log_results("\t TEST CaSe: 73\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type, 'Anesthesia', 'APP'):
            test_terms = [tv.DAY]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.DAY['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            sh.update_user_active(cv.case_RDO_user, False)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(
                tv.pa_stage_value_dar)  # Send document for approval, change PA stage to 'Document Approval Requested'
            pa_pg.get_pa_approver(cwuc.new_pa_name)  # get name of approver
            cl.log_off()  # Logoff as Staffing Admin
            pa_pg.approve_reject_pa(cwuc.pa_apvr, 'Approve')  # Login as approver and approve PA
            stg_h.validate_pa_record_type(cwuc.new_pa_name, tv.pa_rec_tp_intl_cdl_ddp)
            sh.update_user_active(cv.case_RDO_user, True)

if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)


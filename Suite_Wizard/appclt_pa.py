from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search
from Pages import provider_agreement as pa
from Helpers import browser_utilities as bu

# --------------------------------------------------------------------------------------
#   US 105395 - Enable APP Clinical Ladder Tier PA & Approval Process
#   US 105394 - ACL - Capture APP Productivity Pay (i.e., Clinical Ladder Tier) Comp Terms
#   US 107869 - ACL - Clinical Ladder Tier Comp Guidelines & Terms are Restricted
# --------------------------------------------------------------------------------------

@ddt
class APPCL_pa_tests(unittest.TestCase):
    test_to_run = [6]  # Tests with guidelines

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))

    def tearDown(self):
        cl.log_off(1)
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard Regression  **************")
        cwuc.wd.quit()

    @data((tv.prod_usr_people_support_enh, tv.pa_type_APPCL))
    @unpack
    @unittest.skipUnless(1 in test_to_run, "test_01_add_APPCL_guideline")
    def test_01_add_APPCL_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Support adds APPCL Guideline In Clinical Ladder Tier PA"
        lg.log_results("\t TEST CASE: 01\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        if w.add_agreement_guideline():
            pa_pg.delete_terms(1, 0)      # comp_at_num, addnl_at_num to delete

    @data((tv.prod_usr_people_support_enh, tv.pa_type_APPCL))
    @unpack
    @unittest.skipUnless(2 in test_to_run, "test_02")
    def test_02_update_approved_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Support updates APPCL Guideline In Clinical Ladder Tier PA"
        lg.log_results("\t TEST CASE: 02\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['amount'])
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.APPCL['at_description']}}
        hm.approve_reject_terms(test_terms_dictionary)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')  # Avoid logoff errors

    @data((tv.prod_usr_people_support_enh, tv.pa_type_APPCL))
    @unpack
    @unittest.skipUnless(3 in test_to_run, "test_03_add_APPCL_AT")
    def test_03_add_APPCL_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Support adds APPCL agreement term In Clinical Ladder Tier PA"
        lg.log_results("\t TEST CASE: 03\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.APPCL, tv.APPCL2]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.APPCL['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    @data((tv.prod_usr_people_support_enh, tv.pa_type_APPCL))
    @unpack
    @unittest.skipUnless(4 in test_to_run, "test_4")
    def test_4_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "People Enhance Support Recalls APPCL Provider Agreement"
        lg.log_results("\t TEST CASE: 04\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_pa_update)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.APPCL]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.APPCL['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')

            # Changing stage to Document approval requested to trigger approval process
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 0)

            # Recalling PA from approval
            stg_h.recall_pa_approval()

            # Checking the PA Stage value
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_approvd_value)
            lg.log_results('\t\t People Support Enhanced user could recall APPCL Provider Agreement.')

            # Re-submitting the PA for approval and approve
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 1)
            primary_approvr = sh.get_primary_approver(cwuc.new_pa_name)
            lg.log_results("\t \t Primary approver is " + primary_approvr)
            pa.approve_reject_pa(primary_approvr, 'Approve')
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_value_da)

    # This test needs to have pre-approved term of type APPCL on the position.
    @data((tv.prod_usr_people_support_enh, tv.pa_type_amendment))
    @unpack
    @unittest.skipUnless(5 in test_to_run, "User is not able to add pre-approved APPCL Guidelines to an Amendment PA")
    def test_5_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "User is not able to add pre-approved APPCL Guidelines to an Amendment PA"
        lg.log_results("\t TEST CASE: 05\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_pa_update)
        cl.log_off()
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.APPCL, tv.APPCL2]
        w.check_visibility_of_atg(test_terms, 'at_description')

    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(6 in test_to_run, "User is not able to add pre-approved APPCL Guidelines to a  PEA record")
    def test_6_pa_pea(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "User is not able to add pre-approved APPCL Guidelines to a  PEA record"
        lg.log_results("\t TEST CASE: 06\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.APPCL, tv.APPCL2]
        w.check_visibility_of_atg(test_terms, 'at_description')

    @data((tv.prod_usr_people_support_enh, tv.pa_type_APPCL))
    @unpack
    @unittest.skipUnless(7 in test_to_run, "User can add pre-approved APPCL Guidelines to a APPCL PA record type")
    def test_5_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "User can add pre-approved APPCL Guidelines to a APPCL PA record type"
        lg.log_results("\t TEST CASE: 07\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_pa_update)
        cl.log_off()
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        test_terms = [tv.APPCL, tv.APPCL2]
        exp_pa = [tv.pa_type_APPCL, tv.pa_type_unsp]
        w.check_visibility_of_atg(test_terms, exp_pa)


if __name__ == "__main__":
    unittest.main(module='__main__', argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
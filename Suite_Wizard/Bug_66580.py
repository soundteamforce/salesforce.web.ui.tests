from os import path
import sys
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as v
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Pages import comp_login as cl
from Pages import pa_wizard as w
from Helpers import pa_stage_helpers as stg_h
# from Suites import sf_create_entity_data                                 # <------>
from sc_util import target_process_helper as tph
from Helpers import utilities as u
from Pages import provider_agreement as pa_pg
from Helpers import case_helper as ch
from Helpers import login_as_user_with_search as lauw_search
from Pages import Home as hm
from Resources import case_variables as cv
from Resources import test_variables as tv
from Helpers import salesforce_helper as sh



@ddt
class Test_Bug_66580(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)

        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("")

    def tearDown(self):
        # sh.delete_pa(1)
        lg.log_results('\t End of TEST CASE.')
        if cwuc.run_target_process:
            u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results(txh.teardown_msg)
        # cwuc.wd.quit()


# ************ REQUIREMENTS - Bug 66580 Support - Approval Process not firing when terms are added after executing
# ***********



    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    @unittest.skip("Skipping")
    def test_bug_66580_PEA_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        lg.log_results("BUG 66580 - PEA, add additional term and then compensation term.")
        total_num_of_terms = 2
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.WLOAD]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [v.SSWING]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)

    @data(('Sarah Baugus', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_bug_66580_CDL_comp_addnl_term(self, dt_login_as_uname, dt_pa_record_type):
        lg.log_results("BUG 66580 - CDL, add compensation term and then additional term.")
        total_num_of_terms = 2
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.SSWING]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [v.WLOAD]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            # sh.delete_pa(1)


    @data(('May Luu', 'Compensation Change Letter'))
    @unpack
    # @unittest.skip("Skipping")
    def test_bug_66580_rate_increase_rate_comp_addnl_comp_term(self, dt_login_as_uname, dt_pa_record_type):
        total_num_of_terms = 3
        lg.log_results("BUG 66580 - Compensation Change Letter, add compensation + additional term and then compensation term.")

        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.SDAY, v.VISA]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [v.ICU]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)


if __name__ == "__main__":
    unittest.main(module='__main__', argv=['test_env'], exit=False)
    unittest.TestLoader.sortTestMethodsUsing = None
    unittest.TestLoader().loadTestsFromTestCase(Test_Bug_66580)
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

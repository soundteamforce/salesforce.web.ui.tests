from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search
from Helpers import provider_agreement_helper as pah

# --------------------------------------------------------------------------------------
#   This is Wizard Regression
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_regres(unittest.TestCase):

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.today_datetime_sec)

    def tearDown(self):
        cl.log_off()
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.today_datetime_sec)
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard Regression  **************")
        cwuc.wd.quit()

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_01_agreement_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter adds Guideline In Provider Employment Agreement"
        lg.log_results("\t TEST CASE: 01\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.add_agreement_guideline()
        pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel
        pa_pg.delete_terms(1, 0) # comp_at_num, addnl_at_num to delete
        # lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    # People support enhance - May Luu 10.21., PEA
    @unpack
    # @unittest.skip("Skipping")
    def test_02_update_approved_guideline(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter updates values of Approved Agreement Guideline"
        lg.log_results("\t TEST CASE: 02\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['amount'])
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'}}
        hm.approve_reject_terms(test_terms_dictionary)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


      #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_05_test_more_than_13_ats(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter Adds 16 Agreement Terms In Provider Employment Agreement"
        lg.log_results("\t TEST CASE: 05\t " + cwuc.test_case_name)
        total_num_of_terms = 16
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            tv.exe_changes_btn_on = False
            test_terms = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
                          tv.RELOC, tv.VISA, tv.LEAD, tv.SIGBN, tv.QUABN]
            w.add_agreement_term(test_terms)
            tv.exe_changes_btn_on = True
            test_terms = [tv.WLOAD, tv.RETBN]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    #
    # -------------## REQUIREMENTS Multiple Candidate ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_06_multiple_candidates(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter Adds Term For Multiple Candidates In Provider Employment " \
                              "Agreement"
        tv.exe_changes_btn_on = False
        lg.log_results("\t TEST CASE: 06\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.WLOAD]
            w.add_agreement_term(test_terms)
            w.change_candidate()
            test_terms_2 = [tv.SIGBN]
            tv.exe_changes_btn_on = True
            w.add_agreement_term(test_terms_2)


    #
    # -------------## REQUIREMENTS ('Jill Albach', 'PEA')## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    @unittest.skip("Skipping")
    def test_07_non_overlapping_multiple_at_terms_same_type(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter Adds Multiple Non Overlapping Term Of Same Type In Provider " \
                              "Employment Agreement"
        lg.log_results("\t TEST CASE: 07\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            lg.log_results("\t \t TP#62142 - Support - Wizard Automation - Add Multiple AT")
            test_terms = [tv.CLSAL, tv.CLSAL]
            w.add_agreement_term(test_terms)
            # THE LOOP OF APPROVE_REJECT_TERMS need to update so that same types can be used.
            #
            # test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'CLINICAL'},
            #                          1: {'action_taken': 'Approve', 'term_type_to_use': 'CLINICAL'}}
            # hm.approve_reject_terms(test_terms_dictionary)
            # lauw_search.login_as_with_search(dt_login_as_uname, 'user')  #To avoid the error due to log off in
            # teardown.

    #
    # -------------## REQUIREMENTS - PA and AT MUST be Buy Out  ####
    # @data(('Ashley Rogers', 'Buy Out Letter'), ('Ashley Rogers', 'Buy Out Letter'))
    @data(('Saira Ayub', 'Buy Out Letter'))
    @unpack
    # @unittest.skip("Skipping")
    def test_08_test_buyout_pa_and_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard - Perm Recruiter Adds Buy Out Agreement Term In Buy Out Provider Agreement"
        lg.log_results("\t TEST CASE: 08\t " + cwuc.test_case_name)
        lg.log_results("\t \t TP#62778 - 1 - SPE - Build Buy Out Agreement Terms")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)




    # ************ REQUIREMENTS - Amendment Provider Agreement by Perm Recruiting Director ***********
    @data(('Adreanna Bourassa', 'Amendment'))
    @unpack
    # @unittest.skip("Skipping")
    def test_17_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiting Director Adds Amendment Provider Agreement"
        lg.log_results("\t TEST CASE: 17\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup('Jill Albach', 'Provider Employment Agreement'):
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
                w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
                pa_pg.navigate_to_provider_agreement_page()
                pa_pg.pa_recordtype_validation(tv.record_type_amnd_ddp)


    # ************ REQUIREMENTS -  Amendment Provider Agreement by People Enhance Support ***********
    @data(('Kathleen Aukland', 'Amendment'))
    @unpack
    # @unittest.skip("Skipping")
    def test_18_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Enhance Support Adds Amendment Provider Agreement"
        lg.log_results("\t TEST CASE: 18\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 1)  # logoff
            pa_pg.approve_pa(cv.case_RDO_user)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    # ************ REQUIREMENTS - Bug 66580 Support - Approval Process not firing when additional terms added
    # ***********
    @data(('May Luu', 'Compensation Change Letter'))
    @unpack
    # @unittest.skip("Skipping")
    def test_19_pa_compensation_change_letter_rate_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Bug 66580 Support - Approval Process Not Firing When Additional Terms Added"
        lg.log_results("\t TEST CASE: 19\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        # cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.WLOAD] #
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'SWING'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'WORKLOAD'},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    # ************ REQUIREMENTS -  System Administrator can add Provider Agreement of Rate Increase Letter type without case number
    @data(('Jill Albach', 'Compensation Change Letter'))
    @unpack
    @unittest.skip("Skipping")
    def test_20_compensation_change_letter_by_administrator(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - System Administrator adds Rate Increase letter Provider Agreement without case number"
        lg.log_results("\t TEST CASE: 20\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, 'Provider Employment Agreement'):
            w.add_agreement_guideline()
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement executed
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(tv.test_admin_user, dt_pa_record_type):
                test_terms = [tv.SSWING]
                w.add_agreement_term(test_terms)
                sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
                lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    #************ REQUIREMENTS - Rate Increase letter - Test People Support Enhance user can end terms on wizard
    # page for
    # multiple terms "Wizard  -  " *********** #

    @data(('Kathleen Aukland', 'Compensation Change Letter'))
    @unpack
    @unittest.skip("Skipping")
    def test_21_compensation_change_letter_can_end_multiple_ats_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Support Enhance user can end terms on wizard page for multiple terms " \
                              "in Compensation Change Letter."
        lg.log_results("\t TEST CASE: 21\t " + cwuc.test_case_name)
        total_num_of_terms = 11
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.RELOC, tv.ADMST, tv.CHFST, tv.TRAST,  tv.LDPST, tv.OTHERST, tv.QUABN, tv.WLOAD, tv.HHCBN,
                          tv.ICU, tv.RETBN]
            w.add_agreement_term(test_terms)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'RELOCATION'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'ADMIN'},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': 'CHIEF'},
                                     3: {'action_taken': 'Approve', 'term_type_to_use': 'TRAVEL'},
                                     4: {'action_taken': 'Approve', 'term_type_to_use': 'LEADERSHIP'},
                                     5: {'action_taken': 'Approve', 'term_type_to_use': 'OTHER'},
                                     6: {'action_taken': 'Approve', 'term_type_to_use': 'QUALITY'},
                                     7: {'action_taken': 'Approve', 'term_type_to_use': 'WORKLOAD'},
                                     8: {'action_taken': 'Approve', 'term_type_to_use': 'HOME HEALTH'},
                                     9: {'action_taken': 'Approve', 'term_type_to_use': 'ICU'},
                                     10: {'action_taken': 'Approve', 'term_type_to_use': 'RETENTION'}
                                     }
            hm.approve_reject_terms(test_terms_dictionary)
            pa_pg.cancel_term(dt_login_as_uname, 10, 1)  # user, comp_at_num, addnl_at_num to cancel


    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_22_unapproved_term_added_later(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Unapproved Term Is Added After Initial Agreement Term is Approved"
        lg.log_results("\t TEST CASE: 22\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.WLOAD]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'WORKLOAD'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.SIGBN]
            w.add_agreement_term(test_terms)

    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_23_declined_terms(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Decline Agreement Terms"
        lg.log_results("\t TEST CASE: 23\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.WLOAD, tv.SIGBN]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Reject', 'term_type_to_use': 'WORKLOAD'},
                                     1: {'action_taken': 'Reject', 'term_type_to_use': 'SIGNING'}}
            hm.approve_reject_terms(test_terms_dictionary)
            pa_pg.validate_term_status(dt_login_as_uname, 2, 'Reject')
            w.validate_visibility_of_terms(test_terms, 'Reject')


    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_24_validate_visibility_of_terms_with_end_date(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Validate The Visibility Of Terms With End Date In Past"
        lg.log_results("\t TEST CASE: 24\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD, tv.SDAY_ENDDATE_45_DAYS_OLD, tv.HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD]
            w.add_agreement_term(test_terms)
            w.validate_visibility_of_terms(test_terms, 'end_date')


if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)


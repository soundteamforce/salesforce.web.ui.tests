from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
# from Suite_Entity_Creation import create_entity_data
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search
from Helpers import provider_agreement_helper as pah
from datetime import datetime

# --------------------------------------------------------------------------------------
#   This is BVT to create different compensations
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_bvt(unittest.TestCase):
    # test_to_run = [1,2,3,4,5,6,7]  # Include the test case number to run here...
    test_to_run = [1, 2]  # Include the test case number to run here...
    # -------------------------------------------------------------------------------------------------------------
    # Once cancelled, can't delete. Hence commented in everywhere....
    # pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel
    # pa_pg.delete_terms(3, 0) # comp_at_num, addnl_at_num to delete    -- syntax to delete terms
    # --------------------------------------------------------------------------------------------------------------
    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard BVT  **************")

        hm.login_admin()
        lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))

    def tearDown(self):
        cl.log_off(1)
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard BVT  **************")
        cwuc.wd.quit()

    #
    # -------------## REQUIREMENTS ## ------------------ ## DONE
    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(1 in test_to_run, "test_01_perm_recruiter_pa_pea_term_guideline")
    def test_01_perm_recruiter_pa_pea_term_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "01 Perm Recruiter Adds PEA - Guideline Term"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        cwuc.delete_existing_pa = 0
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.add_agreement_guideline()
        pa_pg.delete_terms(1, 0)


# ------------## REQUIREMENTS - ('Sarah Baugus', 'CDL') ## ------------------ ##
    @data((tv.prod_usr_staffing_admin, tv.pa_type_cdl))
    @unpack
    @unittest.skipUnless(2 in test_to_run, "test_02_staffing_admin_pa_cdl_term_night_shift")
    def test_02_staffing_admin_pa_cdl_term_night_shift(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name =  "02 Staffing Admin Adds CDL - Night Shift Term"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    @data((tv.prod_usr_echo_recruiter, tv.pa_type_initial_cdl)) # right users
    @unpack
    @unittest.skipUnless(3 in test_to_run, "test_03_echo_recruit_i_cdl_shift_pay_comm_bonus_work_unit_term")
    def test_03_echo_recruit_i_cdl_shift_pay_comm_bonus_work_unit_term(self, dt_login_as_uname,
    dt_pa_record_type):
        cwuc.test_case_name = "03 Echo Recruiter Adds Initial CDL - Swing Shift, Commencement Bonus and Workload Terms"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.ADMST, tv.ICU]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'SWING'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'ADMIN'},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': 'ICU'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    # -------------## REQUIREMENTS ('Jill Albach', 'PEA')## ------------------ ##Stays
    #
    @data((tv.prod_usr_perm_recruiter, tv.pa_type_pea))
    @unpack
    @unittest.skipUnless(4 in test_to_run, "test_04_perm_recruiter_pa_non_overlapping_multiple_terms_same_type")
    def test_04_perm_recruiter_pa_non_overlapping_multiple_terms_same_type(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "04 Perm Recruiter Adds PEA - Non Overlapping Multiple Terms Of Same Type"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            lg.log_results("\t \t TP#62142 - Support - Wizard Automation - Add Multiple AT")
            test_terms = [tv.CLSAL, tv.CLSAL2]
            w.add_agreement_term(test_terms)


    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_perm_recruiter, tv.pa_rec_tp_amendment))
    # People support enhance - USE THIS AMMENDMENT , MAy Luu or KAthleen
    @unpack
    @unittest.skipUnless(5 in test_to_run, "test_05_perm_dir_update_approved_guideline")
    def test_05_perm_dir_update_approved_guideline(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "05 Perm Director Adds Amendment - Updated Agreement Guideline"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        cwuc.created_by_admin_id = sh.get_user_id_from_alias(tv.test_admin_alias)
        cwuc.usr_rdo_id, cwuc.user_rdo_name = sh.get_user_id_per_profile('RDO', cwuc.created_by_admin_id)
        lauw_search.login_as_with_search(cwuc.user_rdo_name, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off(cwuc.logoff_sf)
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup('Jill Albach', 'Provider Employment Agreement'):
            cl.log_off(cwuc.logoff_sf)
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
                w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
                pa_pg.navigate_to_provider_agreement_page()
                pa_pg.pa_recordtype_validation(tv.record_type_amnd_ddp)


    # -------------## REQUIREMENTS - PA and AT MUST be Buy Out  ####
    # @data(('Ashley Rogers', 'Buy Out Letter'), ('Ashley Rogers', 'Buy Out Letter'))
    @data((tv.prod_usr_staffing_admin, tv.pa_type_bol))
    @unpack
    @unittest.skipUnless(6 in test_to_run, "test_06_staff_services_pa_buyout_at_buyout_term")
    def test_06_staff_services_pa_buyout_at_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "06 Staffing Services Adds Buyout - Buyout term"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)

    # ************ REQUIREMENTS - Bug 66580 Support - Approval Process not firing when additional terms added
    # *********** KEEP IT for some time..
    @data((tv.prod_usr_people_support_enh, tv.pa_type_ccl))
    @unpack
    @unittest.skipUnless(7 in test_to_run, "07_people_support_pa_compensation_change_letter_rate_three_ats")
    def test_07_people_support_pa_compensation_change_letter_rate_three_ats(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "07 People Support Compensation Change Letter - three Terms"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        tv.exe_changes_btn_on = True
        cwuc.created_by_admin_id = sh.get_user_id_from_alias(tv.test_admin_alias)
        cwuc.usr_rdo_id, cwuc.user_rdo_name = sh.get_user_id_per_profile('RDO', cwuc.created_by_admin_id)
        lauw_search.login_as_with_search(cwuc.user_rdo_name, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off(cwuc.logoff_sf)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SDAY, tv.HHCBN] #
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.SADMIN]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'Home Health Care Bonus'},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': 'Admin Shift'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')



if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'], exit=False)
    unittest.TestLoader.sortTestMethodsUsing = None
    # runner = unittest.TextTestRunner()
    unittest.TestLoader().loadTestsFromTestCase(wizard_bvt)
    # unittest.main(testRunner=runner)

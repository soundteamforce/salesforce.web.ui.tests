from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as v
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Helpers import login_as_user_with_search as lauw_search
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import Home as hm
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
# from Suites import sf_create_entity_data
from sc_util import target_process_helper as tph

# --------------------------------------------------------------------------------------
#   This is Wizard Regression
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class rate_increase_letter(unittest.TestCase):

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)


        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("")

    def tearDown(self):
        cl.log_off()
        cwuc.delete_existing_pa = 1
        cwuc.delete_existing_ats = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results('\t End of TEST CASE.')
        if cwuc.run_target_process:
            u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results(txh.teardown_msg)
        cwuc.wd.quit()


    # ***********
    @data(('May Luu', 'Compensation Change Letter'))
    @unpack
    # @unittest.skip("Skipping")
    def test_19_pa_compensation_change_letter_rate_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Compensation Change Letter By People Enhance Support"
        lg.log_results("\t TEST CASE: \t " + "Compensation Change Letter By People Enhance Support")
        total_num_of_terms = 3
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.SSWING, v.WLOAD]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [v.SDAY]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    # ************ REQUIREMENTS -  Rate Increase Letter Provider Agreement by People Enhance Support ***********
    @data(('Kathleen Aukland', 'Compensation Change Letter'))
    @unpack
    # @unittest.skip("Skipping")
    def test_18_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_18_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_18_name)
        total_num_of_terms = 1
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.HNIGHT]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)


    #************ REQUIREMENTS - Rate Increase letter - Test People Support Enhance user can end terms on wizard
    # page for
    # multiple terms "Wizard  -  " *********** #

    # @data(('Kathleen Aukland', 'Rate Increase Letter'))
    # @unpack
    # # @unittest.skip("Skipping")
    # def test_21_rate_increase_letter_can_end_multiple_ats_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
    #     cwuc.test_case_name = txh.tc_21_name
    #     lg.log_results("\t TEST CASE: \t " + txh.tc_21_name)
    #     total_num_of_terms = 11
    #     lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
    #     ch.create_new_case('Employment Event', cv.case_req_category_guideline)
    #     cl.log_off()
    #     cwuc.delete_existing_pa = 0
    #     if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #         test_terms = [v.RELOC, v.ADMST, v.CHFST, v.TRAST,  v.LDPST, v.OTHERST, v.QUABN, v.WORKL, v.HHCBN, v.ICU, v.RETBN]
    #         w.add_agreement_term(test_terms)
    #         lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
    #         cwuc.delete_existing_pa = 0
    #         hm.approve_terms(total_num_of_terms)
    #         pa_pg.cancel_term(dt_login_as_uname, 10, 1)  # user, comp_at_num, addnl_at_num to cancel
    #         sh.delete_pa(1)


    # # ************ REQUIREMENTS -  System Administrator can add Provider Agreement of Rate Increase Letter type without case number
    # @data(('Adreanna Bourassa', 'Rate Increase Letter'))
    # @unpack
    # # @unittest.skip("Skipping")
    # def test_20_rate_increase_letter_by_administrator(self, dt_login_as_uname, dt_pa_record_type):
    #     cwuc.test_case_name = "RIL by Admin"
    #     lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
    #     if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #         test_terms = [v.SSWING]
    #         w.add_agreement_term(test_terms)
    #         sh.delete_pa(1)


if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'], exit=False)
    unittest.TestLoader.sortTestMethodsUsing = None
    unittest.TestLoader().loadTestsFromTestCase(rate_increase_letter)
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

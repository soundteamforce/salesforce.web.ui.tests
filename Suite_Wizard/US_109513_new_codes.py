from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search

# --------------------------------------------------------------------------------------
#   This is Wizard Regression
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_regres(unittest.TestCase):
    # test_to_run = [2,3,4]  # Tests with guidelines
    test_to_run = [3]      # Tests with People Support
    #test_to_run = [40,41,42,43,44,45]  # These are all the testcases in the regression.

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))

    def tearDown(self):
        cl.log_off(1)
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard Regression  **************")
        cwuc.wd.quit()


    #------------- ## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_people_support_enh, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(1 in test_to_run, "test_01_agreement_guideline")
    def test_01_agreement_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter adds Guideline In Provider Employment Agreement"
        lg.log_results("\t TEST CASE: 01\t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        if w.add_agreement_guideline():
            pa_pg.delete_terms(1, 0)      # comp_at_num, addnl_at_num to delete

    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_people_support_enh, 'Provider Employment Agreement'))
    @unpack
    @unittest.skipUnless(2 in test_to_run, "test_02")
    def test_02_update_approved_guideline(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Perm Recruiter updates values of Approved Agreement Guideline"
        lg.log_results("\t TEST CASE: 02\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['amount'])
        test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'}}
        hm.approve_reject_terms(test_terms_dictionary)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')     # Avoid logoff errors


    @data((tv.prod_usr_people_support_enh, tv.pa_type_ccl))
    @unpack
    @unittest.skipUnless(3 in test_to_run, "test_03")
    def test_03_test_shift_pay_and_commencement_bonus_and_work_unit_term(self, dt_login_as_uname,
                                                                         dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - People Support Adds new Shift Pay and Hourly Pay Agreement Terms onto " \
                              "Compensation Change Letter"
        lg.log_results("\t TEST CASE: 03\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.INCLL, tv.SONCL, tv.PAGEX, tv.INCLW, tv.XCOVR, tv.DONCL, tv.PAGXX, tv.SONCX]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.INCLL['at_description']},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': tv.SONCL['at_description']},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': tv.PAGEX['at_description']},
                                     3: {'action_taken': 'Approve', 'term_type_to_use': tv.INCLW['at_description']},
                                     4: {'action_taken': 'Approve', 'term_type_to_use': tv.XCOVR['at_description']},
                                     5: {'action_taken': 'Approve', 'term_type_to_use': tv.DONCL['at_description']},
                                     6: {'action_taken': 'Approve', 'term_type_to_use': tv.PAGXX['at_description']},
                                     7: {'action_taken': 'Approve', 'term_type_to_use': tv.SONCX['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
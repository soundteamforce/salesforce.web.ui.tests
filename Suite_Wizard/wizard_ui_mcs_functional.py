from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search
from Helpers import provider_agreement_helper as pah

# --------------------------------------------------------------------------------------
#   This is Wizard Regression
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_regres(unittest.TestCase):

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.today_datetime_sec)

    def tearDown(self):
        cl.log_off()
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.today_datetime_sec)
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard Regression  **************")
        cwuc.wd.quit()


    #
    # ------------## REQUIREMENTS - ('Sarah Baugus', 'CDL') ## ------------------ ##
    @data(('Sarah Baugus', 'CDL'))
    @unpack
    # @unittest.skip("Skipping")
    def test_03_cdl_night_shift_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Staffing Services Adds Night Shift Term In CDL"
        lg.log_results("\t TEST CASE: 03\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel

    #
    # --------## REQUIREMENTS 'Saira Ayub', 'Ashley Rogers' for Both 'Initial CDL')## ------- ##
    @data(('Saira Ayub', 'Initial CDL')) # right users
    @unpack
    # @unittest.skip("Skipping")
    def test_04_test_shift_pay_and_commencement_bonus_and_work_unit_term(self, dt_login_as_uname,
                                                                         dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Staffing Consultant Adds Swing Shift, Commencement " \
                              "Bonus and Work Unit Terms In Initial CDL"
        lg.log_results("\t TEST CASE: 04\t " + cwuc.test_case_name)
        lg.log_results("Change the test name n title. ")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.ADMST, tv.ICU]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'SWING'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'ADMIN'},
                                     2: {'action_taken': 'Approve', 'term_type_to_use': 'ICU'}}
            hm.approve_reject_terms(test_terms_dictionary)
            pa_pg.cancel_term(dt_login_as_uname, 3, 0)  #user, comp_at_num, addnl_at_num to cancel
            pa_pg.delete_terms(3, 0) # comp_at_num, addnl_at_num to delete


    #
    # ------------ REQUIREMENTS - ('Ashley Rogers', 'Initial CDL')- PA MUST be NON BO, AT - BO -----------#
    @data(('Ashley Rogers', 'Initial CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_9_check_non_buyout_pa_and_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        #     This is the test to check the BO option should not exist in NON-BO PA drop-down.
        cwuc.test_case_name = "Wizard  - Buy Out Term doesn't exist under Non Buy Out Provider Agreement"
        lg.log_results("\t TEST CASE: 09\t " + cwuc.test_case_name)
        lg.log_results("\t \t TP#62778 - 2 - SPE - Build Buy Out Agreement Terms")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)


    # *********** REQUIREMENTS -  Admin can change stage TO  and FROM Final Agrmt Sent **************
    # Testing changing to FAS after executing whole process
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_11_admin_change_pa_stage_to_and_from_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Admin can change stage value TO and FROM Final Agreement Sent"
        lg.log_results("\t TEST CASE: 11\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        test_terms = [tv.HNIGHT]
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement Sent
            # Change the value of stage FROM FAS to approval requested.
            stg_h.change_pa_stage_value(tv.pa_stage_approvd_value)    # Approved
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            #To avoid the error due to log off in  teardown.



    # ************ REQUIREMENTS -  Admin can change stage TO  and FROM Final Agrmt Execution  ***********
    # Testing change to FAE after PA is created.
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_12_admin_change_pa_stage_to_and_from_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Admin can change stage value TO and FROM Final Agreement Executed"
        lg.log_results("\t TEST CASE: 12\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            pa_pg.unlock_record()
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # New
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')  #To avoid the error due to log off in
            # teardown.



    # ************ REQUIREMENTS -  non Admin can NOT change stage FROM Final Agrmt Execution to new  ***********
    # Testing change to FAE after PA is created.-
    @data(('Saira Ayub', 'CDL'))
    @unpack
    # @unittest.skip("Skipping")
    def test_13_non_admin_change_pa_stage_from_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin should not change stage value FROM Final Agreement Executed"
        lg.log_results("\t TEST CASE: 13\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed as a setup by admin
            # admin logoff after changing to FAE, non-admin user will try to update the value FROM FAE
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # change FROM Final Agreement Executed to new
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can change stage value from  Final Agrmt SENT to New  ***********
    # Testing change to FAE after PA is created.
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_14_non_admin_change_pa_stage_from_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin can change stage value FROM Final Agreement Sent"
        lg.log_results("\t TEST CASE: 14\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement sent as a setup by admin
            # admin logoff after changing to FAE, non-admin user will try to update the value FROM FAE
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # can change FROM FAS to New
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can change stage value TO Final Agrmt SENT  ***********
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_15_non_admin_can_not_change_pa_stage_to_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin should not change stage value TO Final Agreement Sent"
        lg.log_results("\t TEST CASE: 15\t " + cwuc.test_case_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'SWING'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement sent
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can NOT change stage value TO Final Agrmt EXECUTED  ***********
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_16_non_admin_can_not_change_pa_stage_to_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Non Admin should not change stage value TO Final Agreement Executed"
        lg.log_results("\t TEST CASE: 16\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'SWING'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement executed



    @data(('Ashley Rogers', 'Initial CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_25_check_review_status_for_term_added_later(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Check Review Status Updated Correctly For Term Added Later"
        lg.log_results("\t TEST CASE: 25\t " + cwuc.test_case_name)
        lg.log_results("\t TEST CASE: 25\t #69497 Bug- Wizard - AT Not Submitting for Approval")
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SDAY]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            pah.validate_review_n_status('Pending', 'Pending Addition')
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'},
                                     1: {'action_taken': 'Approve', 'term_type_to_use': 'NIGHT'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


    @data(('Aubrey Singleton', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_26_cancel_or_decline_term_after_pa_final_executed_changed_to_declined_cancelled(self, dt_login_as_uname,
                                                                                             dt_pa_record_type):
        cwuc.test_case_name = "Wizard  - Term Can Be Approved, Rejected After Final Executed Provider Agreement"
        lg.log_results("\t TEST CASE: 26\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SDAY, tv.HNIGHT]
            w.add_agreement_term(test_terms)
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement executed
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'},
                                     1: {'action_taken': 'Reject', 'term_type_to_use': 'NIGHT'}}
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pa_pg.edit_provider_agreement_terms()
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
            pa_pg.navigate_to_provider_agreement_page()
            pah.validate_review_n_status_dict(test_terms_dictionary)

if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)


import sys
from Helpers import ignore_login_data as ild
from simple_salesforce import Salesforce
import ui_config as uc

if len(sys.argv) < 2:
    test_env = uc.default_ui_test_env
else:
    test_env = sys.argv[1]


uname, pswd, org_id, base_url, test_domain = ild.set_env(str(test_env))

if 'PROD' in test_env:
    sf_connect = Salesforce(username=uname, password=pswd, security_token='eEtxs11yN5rimFIm8hjSD7Gi')   #, sandbox=False
else:
    sf_connect = Salesforce(password=pswd, username=uname, organizationId=org_id, domain=test_domain)



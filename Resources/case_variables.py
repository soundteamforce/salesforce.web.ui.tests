case_req_category_guideline = "Position Guideline Updates"
case_req_category_pa_update = "Provider Agreement Updates"
case_status_new = "New"
case_priority_medium = "Medium"
case_subject = 'Auto - Case Subject '
case_description = 'Automation - Description - Generate and distribute clinical ladder tier provider agreement to [contact name] with Tier 4 clinical ladder 4 assignment in the amount of $20,000 and payback schedule of 2 years.. '
case_RDO_user = 'AATestCT AAUserCT'
case_number = ""

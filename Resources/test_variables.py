from datetime import datetime, timedelta as td, date

threshold_terms = []

now = datetime.now()

today_date_mmdd = now.strftime('%m%d')
today_date_mdyy = now.strftime('%m/%d/%Y')
today_date_mdyy_1 = now.strftime('%#m/%#d/%Y')
today_date_yymd = now.strftime('%Y-%m-%d')
today_date_mdyy_to_sec = now.strftime('%Y-%m-%dT00:00:00.0000000Z')
today_datetime_sec = now.strftime('%m-%d %H:%M:%S')

today_plus_365d_mdyy = (now + td(365)).strftime('%m/%d/%Y')
today_plus_2d_mdyy = (now + td(2)).strftime('%m/%d/%Y')
today_plus_1d_mdyy = (now + td(1)).strftime('%m/%d/%Y')
today_plus_30d_mdyy = (now + td(30)).strftime('%m/%d/%Y')
today_plus_31d_mdyy = (now + td(31)).strftime('%m/%d/%Y')
today_plus_60d_mdyy = (now + td(60)).strftime('%m/%d/%Y')
today_plus_90d_mdyy = (now + td(90)).strftime('%m/%d/%Y')
today_only = (now - td(1)).strftime('%m/%d/%Y')
# 2020-03-29
today_minus_15_days_mdyy_with_dash = (now - td(15)).strftime('%Y-%#m-%#d')
today_minus_30_days_mdyy_with_dash = (now - td(15)).strftime('%Y-%#m-%#d')
today_minus_30_days_mdyy = (now - td(30)).strftime('%#m/%#d/%Y')
today_minus_46_days_mdyy = (now - td(46)).strftime('%#m/%#d/%Y')
today_minus_45_days_mdyy = (now - td(45)).strftime('%#m/%#d/%Y')
today_minus_60_days_mdyy = (now - td(60)).strftime('%#m/%#d/%Y')
today_minus_10_days_mdyy = (now - td(10)).strftime('%#m/%#d/%Y')
today_minus_12_days_mdyy = (now - td(12)).strftime('%#m/%#d/%Y')
today_minus_15_days_mdyy = (now - td(15)).strftime('%#m/%#d/%Y')
today_minus_17_days_mdyy = (now - td(17)).strftime('%#m/%#d/%Y')
today_minus_20_days_mdyy = (now - td(20)).strftime('%#m/%#d/%Y')

today_plus_30_days_mdyy = (now + td(30)).strftime('%#m/%#d/%Y')


exe_changes_btn_on = True
delete_at_line_item = False
docusign_user = 'DocuSign'

source_file_name = ""
# result_file_name = ""

test_admin_alias = 'aauto'
test_admin_user = 'autofn autoln'
usr_perm_recruiter_jill = 'Jill Albach'
usr_staffing_admin_sarah = 'Sarah Baugus'
usr_perm_recruit_dir_adreanna = 'Adreanna Bourassa'
usr_people_support_enh_kathleen = 'Kathleen Aukland'
usr_people_support_spec_may = 'May Luu'
usr_echo_recruit_ashley = 'Ashley Rogers'
usr_staff_services_saira = 'Saira Ayub'
usr_rdo_profile = 'RDO'

#Prod users
prod_usr_perm_recruiter = 'Test_Perm_Recruiter'
prod_usr_staffing_admin = 'Test_Staffing_Admin'
prod_usr_people_support_enh = 'Test_People_Support_Enh'
prod_usr_echo_recruiter = 'Test_Echo_Recruiter'
prod_usr_rdo_profile = 'Test RDO'

pa_type_pea = 'Provider Employment Agreement'
pa_type_cdl = 'CDL'
pa_type_initial_cdl = 'Initial CDL'
pa_type_bol = 'Buy Out Letter'
pa_type_amendment = 'Amendment'
pa_type_ccl = 'Compensation Change Letter'
pa_type_ica = 'Independent Contractor Agreement'
pa_type_esa = 'Education Stipend Agreement'
pa_type_ilpl = 'Interim Leadership Position Letter'
pa_type_prl = 'Position Removal Letter'
pa_type_ssl = 'Secondary Site Letter'
pa_type_APPCL = 'Clinical Ladder Tier'
pa_type_unsp = 'Unspecified'

pa_stage_new_value = 'New'
pa_rec_tp_amendment = 'Amendment'
pa_rec_tp_education = 'Education Stipend Agreement'
pa_rec_tp_compass = 'Compass Program Agreement'
pa_rec_tp_medical_corps = 'Medical Corps Agreement'
pa_rec_tp_intl_cdl = "Initial CDL"
pa_rec_tp_intl_cdl_ddp = 'Initial CDL DDP'
record_type_amnd_ddp = 'Amendment - DDP'
record_type_amnd_ddp_in_two = ['Amendment', 'DDP']
pa_stage_app_reqtd_value = 'Agreement Terms Approval Requested'
pa_stage_approvd_value = 'Agreement Terms Approved'
pa_stage_value_fae = 'Final Agreement Executed'
pa_stage_value_fas = 'Final Agreement Sent'
pa_stage_value_dar = 'Document Approval Requested'
pa_stage_value_da = 'Document Approved'
pa_stage_value_dr = 'Document Rejected'
pa_stage_value_declined = 'Declined'
pa_stage_value_cancelled = 'Cancelled'

thrhld_interval_quarterly = 'Quarterly'
thrhld_interval_monthly  = 'Monthly'
thrhld_interval_annually = 'Annually'

ACPBN = {}
ACPBN['at_category'] = 'Compensation'
ACPBN['at_type'] = 'ACP Bonus Eligibility'
ACPBN['type__c'] = 'ACP Bonus'
ACPBN['at_description'] = 'ACP Bonus'
#ACPBN['at_amount'] = '14000'
ACPBN['at_short_code'] = 'ACPBN'
ACPBN['at_type_append_str'] = ' Eligibility'
ACPBN['start_date'] = today_date_mdyy_1
# ACPBN['end_date'] = ''
# EDLNR	Education Loan Repayment	Education Loan Repayment

EDLNR = {}
EDLNR['at_category'] = 'Compensation'
EDLNR['at_type'] = 'Education Loan Repayment'
EDLNR['type__c'] = 'Education Loan Repayment'
EDLNR['at_description'] = 'Education Loan Repayment'
EDLNR['at_amount'] = '13000'
# EDLNR['at_shift_hours'] = ''
EDLNR['at_short_code'] = 'EDLNR'
EDLNR['start_date'] = today_date_mdyy_1
# EDLNR['end_date'] = ''
EDLNR['at_payback_schedule'] = '1 Year Full Repayment'

TEACH={}
TEACH['at_category'] = 'Compensation'
TEACH['at_type'] = 'Shift Pay'
TEACH['type__c'] = 'Shift Pay'
TEACH['at_description'] = 'Teaching Shift'
TEACH['at_amount'] = '130'
TEACH['at_shift_hours'] = '12'
TEACH['at_short_code'] = 'TEACH'
TEACH['start_date'] = today_date_mdyy_1
# TEACH['end_date'] = ''



EDLOAN={}
EDLOAN['at_category'] = 'Compensation'
EDLOAN['at_type'] = 'Education Loan Repayment'
EDLOAN['at_description'] = 'Education Loan Repayment'
EDLOAN['at_short_code'] = 'EDLNR'
EDLOAN['type__c'] = 'Education Loan Repayment'
EDLOAN['at_amount'] = '12000.00'
#EDLOAN['at_shift_hours'] = None
EDLOAN['start_date'] = today_date_mdyy_1
# EDLOAN['end_date'] = ''
EDLOAN['at_payback_schedule'] = '1 Year Full Repayment'



BUYOUT = {}
BUYOUT['at_category'] = 'Compensation'
BUYOUT['at_type'] = 'Buy Out'
BUYOUT['type__c'] = 'Buy Out'
BUYOUT['at_description'] = 'Buy Out'
BUYOUT['at_amount'] = '500.00'
BUYOUT['start_date'] = today_date_mdyy_1
# BUYOUT['end_date'] = ''


CLSAL = {}
CLSAL['at_category'] = 'Compensation'
CLSAL['at_type'] = 'Clinical Salary'
CLSAL['at_description'] = 'Clinical Salary'
CLSAL['type__c'] =  'Clinical Salary'
CLSAL['at_amount']= '133333.00'
# CLSAL['at_shift_hours']= None
CLSAL['at_short_code'] = 'CLSAL'
CLSAL['start_date'] = today_date_mdyy
CLSAL['end_date'] = today_plus_30_days_mdyy

CLSAL2 = {}
CLSAL2['at_category'] = 'Compensation'
CLSAL2['at_type'] = 'Clinical Salary'
CLSAL2['at_description'] = 'Clinical Salary'
CLSAL2['type__c'] =  'Clinical Salary'
CLSAL2['at_amount']= '155555.00'
# CLSAL2['at_shift_hours']= None
CLSAL2['at_short_code'] = 'CLSAL'
CLSAL2['start_date'] = today_plus_31d_mdyy
CLSAL2['end_date'] = today_plus_60d_mdyy


HNIGHT = {}
HNIGHT['at_category'] = 'Compensation'
HNIGHT['at_type'] = 'Hourly Pay'
HNIGHT['type__c'] =  'Hourly Pay'
HNIGHT['at_description'] = 'Night Shift'
HNIGHT['at_amount']= '144.00'
# HNIGHT['at_shift_hours']= '14'
HNIGHT['at_short_code'] = 'NIGHT'
HNIGHT['start_date'] = today_date_mdyy_1
# HNIGHT['end_date'] = ''

SSWING = {}
SSWING['at_category'] = 'Compensation'
SSWING['at_type'] = 'Shift Pay'
SSWING['type__c'] = 'Shift Pay'
SSWING['at_description'] = 'Swing Shift'
SSWING['at_amount']= '165.00'
SSWING['at_shift_hours']= '11'
SSWING['at_short_code'] = 'SWING'
SSWING['start_date'] = today_date_mdyy_1
# SSWING['end_date'] = ''

DAY = {}
DAY['at_category'] = 'Compensation'
DAY['at_type'] = 'Shift Pay'
DAY['type__c'] =  'Shift Pay'
DAY['at_description'] = 'Day Shift'
DAY['start_date'] = today_date_mdyy_1
DAY['end_date'] = today_date_mdyy_1
DAY['at_amount'] = '155.00'
DAY['at_shift_hours'] = '12'
DAY['at_short_code'] = 'DAY'

#SIGBN	Bonus	Commencement Bonus
SIGBN = {}
SIGBN['at_category'] = 'Compensation'
SIGBN['at_type'] =  'Commencement Bonus'
SIGBN['type__c'] =  'Bonus'
SIGBN['at_short_code'] =  'SIGBN'
SIGBN['at_description'] = 'Commencement Bonus'
SIGBN['at_amount'] = '3000.00'
# SIGBN['at_shift_hours'] = ''
SIGBN['start_date'] = today_date_mdyy_1
# SIGBN['end_date'] = ''
SIGBN['at_payback_schedule'] = '18 Month Full Repayment'         # New value added in 98056

ORNTN = {}              #Hourly Pay	ORNTN
ORNTN['at_category'] = 'Compensation'
ORNTN['at_type'] =  'Shift Pay'
ORNTN['type__c'] =  'Shift Pay'
ORNTN['at_description'] = 'Orientation Shift'
ORNTN['at_shift_hours'] = '12'
ORNTN['at_amount'] = '1000.00'
ORNTN['start_date'] = today_date_mdyy_1
# ORNTN['end_date'] = ''


VISA = {}
VISA['at_category'] = 'Additional'
VISA['at_type'] =  'Visa Type'
VISA['type__c'] =  'Visa Type'
# VISA['at_description'] = 'Green Card Only'
VISA['Visa_Type__c'] = 'Green Card Only'
VISA['start_date'] = today_date_mdyy_1
# VISA['end_date'] = ''


WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD = {}
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['at_category'] = 'Additional'
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['at_type'] =  'Workload & Schedule'
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['type__c'] =  'Workload & Schedule'
# WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['at_description'] = ''
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['start_date'] = today_minus_60_days_mdyy
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['end_date'] = today_minus_30_days_mdyy
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['delta_days'] = 30
WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['at_quantity'] = 12
#WLOAD_ENDDATE_LESSTHAN_45_DAYS_OLD['at_amount'] = '130'


HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD = {}
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['at_category'] = 'Compensation'
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['at_type'] = 'Hourly Pay'
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['type__c'] = 'Hourly Pay'
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['at_description'] =  'Night Shift'
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['start_date'] = today_minus_60_days_mdyy
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['end_date'] = today_minus_46_days_mdyy
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['delta_days'] = 46
HNIGHT_ENDDATE_MORETHAN_45_DAYS_OLD['at_amount'] = '130'


SDAY_ENDDATE_45_DAYS_OLD = {}
SDAY_ENDDATE_45_DAYS_OLD['at_category'] = 'Compensation'
SDAY_ENDDATE_45_DAYS_OLD['at_type'] = 'Shift Pay'
SDAY_ENDDATE_45_DAYS_OLD['type__c'] = 'Shift Pay'
SDAY_ENDDATE_45_DAYS_OLD['at_description'] = 'Day Shift'
SDAY_ENDDATE_45_DAYS_OLD['at_amount'] = '100'
SDAY_ENDDATE_45_DAYS_OLD['at_shift_hours'] = '9'
SDAY_ENDDATE_45_DAYS_OLD['start_date'] = today_minus_60_days_mdyy
SDAY_ENDDATE_45_DAYS_OLD['end_date'] = today_minus_45_days_mdyy
SDAY_ENDDATE_45_DAYS_OLD['delta_days'] = 45

PTO = {}
PTO['at_category'] = 'Compensation'
PTO['at_type'] =  'PTO or Sick'
PTO['type__c'] =  'PTO/Sick'
PTO['at_description'] = 'PTO'
PTO['at_amount'] = '100.00'
# PTO['at_shift_hours'] = None
PTO['at_short_code'] = 'PTO'
PTO['start_date'] = today_date_mdyy_1
PTO['end_date'] = ''

#Hourly Pay	PALLI	Palliative Shift
PALLI = {}
PALLI['at_category'] = 'Compensation'
PALLI['at_type'] =  'Hourly Pay'
PALLI['type__c'] =  'Hourly Pay'
PALLI['at_description'] = 'Palliative Shift'
PALLI['at_amount'] =  '100'
PALLI['start_date'] = today_date_mdyy_1
# PALLI['end_date'] = ''

ICU = {}
ICU['at_category'] = 'Compensation'
ICU['at_type'] = 'Shift Pay'
ICU['type__c'] = 'Shift Pay'
ICU['at_description'] = 'ICU Shift'
ICU['at_amount'] = '1000'
ICU['at_shift_hours'] = '10'
ICU['start_date'] = today_date_mdyy_1
# ICU['end_date'] = ''

RELOC = {}
RELOC['at_category'] = 'Compensation'
RELOC['at_type'] =  'Relocation Allowance'
RELOC['type__c']  =  'Relocation Allowance'
RELOC['at_description'] = 'Relocation Allowance'
RELOC['at_amount'] = '5100'
RELOC['at_short_code'] = 'RELOC'
RELOC['start_date'] = today_date_mdyy_1
RELOC['at_payback_schedule'] = '1 Year Full Repayment'
RELOC['end_date'] = today_plus_365d_mdyy

ADMST = {}
ADMST['at_category'] = 'Compensation'
ADMST['at_type'] =  'Stipend'
ADMST['type__c'] = 'Stipend'
ADMST['at_description'] = 'Admin Stipend'
ADMST['at_amount'] = '400'
ADMST['start_date'] = today_date_mdyy_1
ADMST['end_date'] = today_plus_365d_mdyy
#['at_forgiveness_period'] = '12'
ADMST['at_interval'] = 'Monthly'


EJEOP = {}
EJEOP['at_category'] = 'Compensation'
EJEOP['at_type'] = 'Stipend'
EJEOP['type__c'] = 'Stipend'
EJEOP['at_description'] = 'Jeopardy Stipend'
EJEOP['at_interval'] = 'Monthly'
EJEOP['at_amount'] = '2000'
EJEOP['start_date'] = today_date_mdyy_1
EJEOP['end_date'] = today_plus_365d_mdyy

CHFST = {}
CHFST['at_category'] = 'Compensation'
CHFST['at_type'] = 'Stipend'
CHFST['type__c'] = 'Stipend'
CHFST['at_description'] = 'Chief Stipend'
CHFST['at_amount'] = '1010.10'
CHFST['at_interval'] = 'Monthly'
CHFST['at_short_code'] = 'CHFST'
CHFST['start_date'] = today_date_mdyy_1
CHFST['end_date'] = today_plus_365d_mdyy
#CHFST['at_forgiveness_period'] = '12'


TRAST = {}
TRAST['at_category'] = 'Compensation'
TRAST['at_type'] =  'Stipend'
TRAST['type__c'] =  'Stipend'
TRAST['at_description'] = 'Travel Stipend'
TRAST['at_amount'] = '1200'
TRAST['start_date'] = today_date_mdyy_1
TRAST['end_date'] = today_plus_365d_mdyy

LDPST = {}
LDPST['at_category'] = 'Compensation'
LDPST['at_type'] =  'Stipend'
LDPST['type__c'] =  'Stipend'
LDPST['at_description'] = 'Leadership Stipend'
LDPST['at_amount'] = '1400'
LDPST['start_date'] = today_date_mdyy_1
LDPST['end_date'] = today_plus_365d_mdyy
LDPST['at_forgiveness_period'] = '12'
LDPST['at_interval'] = 'Monthly'

STPND = {}
STPND['at_category'] = 'Compensation'
STPND['at_type'] =  'Stipend'
STPND['type__c'] =  'Stipend'
STPND['at_description'] = 'Other Stipend'
STPND['at_amount'] = '5000'
STPND['start_date'] = today_date_mdyy_1
STPND['end_date'] = today_plus_365d_mdyy
STPND['at_forgiveness_period'] = '12'
STPND['at_interval'] = 'Monthly'

# with end dates for Agreement terms
COMSST = {}
COMSST['at_category'] = 'Compensation'
COMSST['at_type'] =  'Stipend'
COMSST['type__c'] =  'Stipend'
COMSST['at_description'] = 'Compass Stipend'
COMSST['at_interval'] = 'Monthly'
COMSST['at_amount'] = '5000'
COMSST['at_forgiveness_period'] = '12'
COMSST['start_date'] = today_date_mdyy_1
COMSST['end_date'] = today_plus_365d_mdyy

# without end dates for Agreement term Guidelines
COMSST2 = {}
COMSST2['at_category'] = 'Compensation'
COMSST2['at_type'] =  'Stipend'
COMSST2['type__c'] =  'Stipend'
COMSST2['at_description'] = 'Compass Stipend'
COMSST2['at_interval'] = 'Monthly'
COMSST2['at_amount'] = '5000'
COMSST2['at_forgiveness_period'] = '12'
COMSST2['start_date'] = today_date_mdyy_1

RESIST = {}
RESIST['at_category'] = 'Compensation'
RESIST['at_type'] =  'Stipend'
RESIST['type__c'] =  'Stipend'
RESIST['at_description'] = 'Residency Stipend'
RESIST['at_interval'] = 'Monthly'
RESIST['at_amount'] = '5000'
RESIST['at_forgiveness_period'] = '24'
RESIST['start_date'] = today_date_mdyy_1
RESIST['end_date'] = today_plus_365d_mdyy

MEDCORPST = {}
MEDCORPST['at_category'] = 'Compensation'
MEDCORPST['at_type'] =  'Stipend'
MEDCORPST['type__c'] =  'Stipend'
MEDCORPST['at_description'] = 'Medical Corps Stipend'
MEDCORPST['at_interval'] = 'Monthly'
MEDCORPST['at_amount'] = '5000'
MEDCORPST['start_date'] = today_date_mdyy_1
MEDCORPST['end_date'] = today_plus_365d_mdyy

# Without end dates
MEDEDUST = {}
MEDEDUST['at_category'] = 'Compensation'
MEDEDUST['at_type'] = 'Stipend'
MEDEDUST['type__c'] = 'Stipend'
MEDEDUST['at_description'] = 'Medical Education Stipend'
MEDEDUST['at_interval'] = 'Monthly'
MEDEDUST['at_amount'] = '12000'
MEDEDUST['start_date'] = today_date_mdyy_1
# MEDEDUST['end_date'] = today_plus_365d_mdyy

# With end dates
MEDEDUST2 = {}
MEDEDUST2['at_category'] = 'Compensation'
MEDEDUST2['at_type'] = 'Stipend'
MEDEDUST2['type__c'] = 'Stipend'
MEDEDUST2['at_description'] = 'Medical Education Stipend'
MEDEDUST2['at_interval'] = 'Monthly'
MEDEDUST2['at_amount'] = '12000'
MEDEDUST2['start_date'] = today_date_mdyy_1
MEDEDUST2['end_date'] = today_plus_365d_mdyy

CLINC = {}
CLINC['at_category'] = 'Compensation'
CLINC['at_type'] =  'Shift Pay'
CLINC['type__c'] =  'Shift Pay'
CLINC['at_description'] = 'Clinic Shift'
CLINC['at_amount'] = '5000'
CLINC['start_date'] = today_date_mdyy_1
CLINC['at_shift_hours'] = '14'

SADMIN = {}
SADMIN['at_category'] = 'Compensation'
SADMIN['at_type'] =  'Shift Pay'
SADMIN['type__c'] =  'Shift Pay'
SADMIN['at_description'] = 'Admin Shift'
SADMIN['at_amount'] = '5000'
SADMIN['start_date'] = today_date_mdyy_1
SADMIN['at_shift_hours'] = '12'
# SADMIN['end_date'] = ''
# SADMIN['end_at'] = None

RETBN = {}
RETBN['at_category'] = 'Compensation'
RETBN['at_type'] = 'Retention Bonus'
RETBN['at_description'] = 'Retention Bonus'
RETBN['type__c'] = 'Bonus'
RETBN['at_short_code'] = 'RETBN'
RETBN['at_amount'] = '3051.00'
RETBN['start_date'] = today_date_mdyy_1
RETBN['end_date'] = today_plus_2d_mdyy
RETBN['at_payback_schedule'] = '1 Year Prorated Repayment'

PRDBN = {}
PRDBN['at_category'] = 'Compensation'
PRDBN['at_type'] = 'Productivity Bonus Eligibility'
PRDBN['type__c'] = 'Bonus'
PRDBN['at_short_code'] = 'PRDBN'
# PRDBN['at_amount'] = '1651.00'
# PRDBN['at_shift_hours'] = None
PRDBN['at_description'] = 'Productivity Bonus'
PRDBN['start_date'] = today_date_mdyy_1
PRDBN['end_date'] = today_plus_365d_mdyy
# PRDBN['at_schedule_c'] = 'Shifts'

PRMNT = {}
PRMNT['at_category'] = 'Compensation'
PRMNT['at_type'] = 'Promissory Note Amount'
PRMNT['at_description'] = 'Note Amount'
PRMNT['type__c'] = 'Promissory Note'
PRMNT['at_short_code'] = 'PRMNT'
PRMNT['at_amount'] = '1012.00'
# PRMNT['at_shift_hours'] = None
PRMNT['start_date'] = today_date_mdyy_1
PRMNT['end_date'] = today_plus_365d_mdyy
PRMNT['at_forgiveness_period'] = '12'
# PRMNT['at_schedule_c'] = 'Shifts'

QUABN = {}
QUABN['at_category'] = 'Compensation'
QUABN['at_type'] = 'Quality Bonus Eligibility'
QUABN['type__c'] = 'Bonus'
QUABN['at_description'] = 'Quality Bonus'
QUABN['at_short_code'] = 'QUABN'
QUABN['start_date'] = today_date_mdyy_1
QUABN['end_date'] = today_plus_365d_mdyy

WLOAD = {}
WLOAD['at_category'] = 'Additional'
WLOAD['at_type'] = 'Workload & Schedule'
WLOAD['type__c'] = 'Workload & Schedule'
WLOAD['at_quantity'] = '44'
WLOAD['at_schedule'] = 'Weeks'
WLOAD['at_period'] = 'Annual'
WLOAD['at_workload'] = 'Minimum'
WLOAD['start_date'] = today_date_mdyy_1
WLOAD['end_date'] = today_plus_2d_mdyy

WLOAD2 = {}
WLOAD2['at_category'] = 'Additional'
WLOAD2['at_type'] = 'Workload & Schedule'
WLOAD2['type__c'] = 'Workload & Schedule'
WLOAD2['at_quantity'] = '40'
WLOAD2['at_schedule'] = 'Hours'
WLOAD2['at_period'] = 'Weeks'
WLOAD2['at_workload'] = 'Average'
WLOAD2['start_date'] = today_date_mdyy_1
WLOAD2['end_date'] = today_plus_2d_mdyy

HHCBN = {}
HHCBN['at_category'] = 'Compensation'
HHCBN['at_type']  = 'Home Health Care Bonus Eligibility'
HHCBN['at_description'] = 'Home Health Care Bonus'
HHCBN['type__c']  = 'Home Health Care Bonus'
HHCBN['at_short_code'] = 'HHCBN'
HHCBN['start_date'] = today_date_mdyy_1
# HHCBN['at_shift_hours'] = None
HHCBN['end_date'] = today_plus_2d_mdyy

#HOUSEAL
HSALL = {}
HSALL['at_category'] = 'Compensation'
HSALL['at_type']  = 'Housing Allowance'
HSALL['type__c']  = 'Housing Allowance'
HSALL['at_description'] = 'Housing Allowance'
HSALL['at_amount'] = '15000'
# HSALL['at_shift_hours'] = None
HSALL['at_short_code'] = 'HSALL'
HSALL['start_date'] = today_date_mdyy_1
HSALL['end_date'] = ''
HSALL['at_interval'] = 'Monthly'

#MLDAY
MLDAY = {}
MLDAY['at_category'] = 'Compensation'
MLDAY['at_type']  = 'Moonlighting'
MLDAY['type__c']  = 'Moonlighting'
MLDAY['at_description'] = 'Day'
MLDAY['at_amount'] = '145'
MLDAY['at_short_code'] = 'MLDAY'
MLDAY['start_date'] = today_date_mdyy_1
MLDAY['at_schedule'] = 'Shifts'
MLDAY['at_threshold_min'] = '5.00'
MLDAY['at_threshold_max'] = '45.00'
MLDAY['at_threshold_type'] = 'Shifts'

#MoonLight Night
MNIGHT = {}
MNIGHT['at_category'] = 'Compensation'
MNIGHT['at_type'] = 'Moonlighting'
MNIGHT['type__c'] = 'Moonlighting'
MNIGHT['at_description'] = 'Night'
MNIGHT['at_amount'] = '200'
MNIGHT['at_short_code'] = 'MLNGT'
MNIGHT['start_date'] = today_date_mdyy_1
MNIGHT['at_schedule'] = 'Hours'
MNIGHT['at_threshold_min'] = '90'
MNIGHT['at_threshold_max'] = '200'
MNIGHT['at_threshold_type'] = 'Hours'

#Moonlight ICU
MLICU = {}
MLICU['at_category'] = 'Compensation'
MLICU['at_type'] = 'Moonlighting'
MLICU['type__c'] = 'Moonlighting'
MLICU['at_description'] = 'Moonlighting ICU'
MLICU['at_amount'] = '255'
MLICU['at_short_code'] = 'MOON'
MLICU['start_date'] = today_date_mdyy_1
MLICU['at_schedule'] = 'Shifts'
MLICU['at_threshold_min'] = '105.00'
MLICU['at_threshold_max'] = '145.00'
MLICU['at_threshold_type'] = 'Hours'


#WRVU
WRVU = {}
WRVU['at_category'] = 'Compensation'
WRVU['at_type']  = 'WRVU Pay Only'
WRVU['type__c']  = 'WRVU'             #'WRVU'
WRVU['at_description'] = 'WRVU Pay Only'
WRVU['at_amount'] = '135'
# WRVU['at_shift_hours'] = None
WRVU['at_short_code'] = 'WRVU'
WRVU['start_date'] = today_date_mdyy_1
# WRVU['end_date'] = ''


#THREAD
TREAD = {}
TREAD['at_category'] = 'Compensation'
TREAD['at_type']  = 'Work Unit Pay'
TREAD['type__c']  = 'Work Unit Pay'
TREAD['at_description'] = 'Treadmill'
TREAD['at_amount'] = '299.90'
# TREAD['at_shift_hours'] = None
# TREAD['at_short_code'] = None          #'THREAD'
TREAD['start_date'] = today_date_mdyy_1
# TREAD['end_date'] = ''


# PDRST
PDRST = {}
PDRST['at_category'] = 'Compensation'
PDRST['at_type']  = 'Stipend'
PDRST['type__c']  = 'Stipend'
PDRST['at_description'] = 'Program Director Stipend'
PDRST['at_amount'] = '1299.90'
PDRST['at_forgiveness_period'] = '12'
# PDRST['at_short_code'] = None          #'THREAD'
PDRST['start_date'] = today_date_mdyy_1
PDRST['end_date'] = today_plus_365d_mdyy
PDRST['at_short_code'] = 'PDRST'
PDRST['at_interval'] = 'Monthly'

TRAST = {}
TRAST['at_category'] = 'Compensation'
TRAST['at_type']  = 'Stipend'
TRAST['type__c']  = 'Stipend'
TRAST['at_description'] = 'Travel Stipend'
TRAST['at_amount'] = '190.90'
#TRAST['at_forgiveness_period'] = '12'
TRAST['at_short_code'] = 'TRAST'          #'THREAD'
TRAST['start_date'] = today_date_mdyy_1
TRAST['end_date'] = today_plus_365d_mdyy
TRAST['at_interval'] = 'Monthly'


APPSU = {}
APPSU['at_category'] = 'Compensation'
APPSU['at_type']  = 'Moonlighting'
APPSU['type__c']  = 'Moonlighting'
APPSU['at_description'] = 'Day'
APPSU['at_amount'] = '145'
# APPSU['at_shift_hours'] = None
APPSU['at_short_code'] = 'APPSU'
APPSU['start_date'] = today_date_mdyy_1
# APPSU['end_date'] = ''
#APPSU['at_interval'] = 'Monthly'
APPSU['at_schedule'] = 'Shifts'
APPSU['at_threshold_min'] = '5.00'
APPSU['at_threshold_max'] = '45.00'
APPSU['at_threshold_type'] = 'Shifts'

WKNDN = {}
WKNDN['at_category'] = 'Compensation'
WKNDN['at_type']  = 'Hourly Pay'
WKNDN['type__c']  = 'Hourly Pay'
WKNDN['at_description'] = 'Weekend Night Shift'
WKNDN['at_amount'] = '145'
# WKNDN['at_shift_hours'] = None
WKNDN['at_short_code'] = 'WKNDN'
WKNDN['start_date'] = today_date_mdyy_1
# WKNDN['end_date'] = ''

SWOCL = {}
SWOCL['at_category'] = 'Compensation'
SWOCL['at_type']  = 'Shift Pay'
SWOCL['type__c']  = 'Shift Pay'
SWOCL['at_description'] = 'Weekend On Call Shift'
SWOCL['at_amount'] = '195.00'
SWOCL['at_short_code'] = 'WOCL'
SWOCL['start_date'] = today_date_mdyy_1
SWOCL['at_shift_hours'] = '11'

HWOCL = {}
HWOCL['at_category'] = 'Compensation'
HWOCL['at_type']  = 'Hourly Pay'
HWOCL['type__c']  = 'Hourly Pay'
HWOCL['at_description'] = 'Weekend On Call Shift'
HWOCL['at_amount'] = '197.00'
HWOCL['at_short_code'] = 'WOCL'
HWOCL['start_date'] = today_date_mdyy_1

TELE = {}
TELE['at_category'] = 'Compensation'
TELE['at_type']  = 'Level Shift Rate'
TELE['type__c']  = 'Level Shift Pay'
TELE['at_description'] = 'Telemedicine/Remote Shift'
TELE['at_amount'] = '195.00'
TELE['at_short_code'] = 'TELE'
TELE['start_date'] = today_date_mdyy_1
TELE['at_shift_hours'] = '14'
TELE['at_shift_level'] = 'Level 10'
TELE['at_experience_modifier'] = 'Resident'

APPL = {}
APPL['at_category'] = 'Compensation'
APPL['at_type']  = 'Clinical Case'
APPL['type__c']  = 'Case Rate'
APPL['at_description'] = 'Appeal Letter'
APPL['at_amount'] = '199.90'
APPL['at_short_code'] = 'APPL'
APPL['start_date'] = today_date_mdyy_1

BHAPL = {}
BHAPL['at_category'] = 'Compensation'
BHAPL['at_type']  = 'Clinical Case'
BHAPL['type__c']  = 'Case Rate'
BHAPL['at_description'] = 'BH Appeals'
BHAPL['at_amount'] = '189.98'
BHAPL['at_short_code'] = 'BHAPL'
BHAPL['start_date'] = today_date_mdyy_1

RETRO = {}
RETRO['at_category'] = 'Compensation'
RETRO['at_type'] = 'Clinical Case'
RETRO['type__c'] = 'Case Rate'
RETRO['at_description'] = 'Retro Status Review'
RETRO['at_amount'] = '200.70'
RETRO['at_short_code'] = 'RETRO'
RETRO['start_date'] = today_date_mdyy_1

CASE = {}
CASE['at_category'] = 'Compensation'
CASE['at_type'] = 'Clinical Case'
CASE['type__c'] = 'Case Rate'
CASE['at_description'] = 'Pre-Admit'
CASE['at_amount'] = '500.35'
CASE['at_short_code'] = 'CASE'
CASE['start_date'] = today_date_mdyy_1

APPCL = {}
APPCL['at_category'] = 'Compensation'
APPCL['at_type'] = 'APP Clinical Ladder Tier'
APPCL['type__c'] = 'Tier 3'
APPCL['at_description'] = 'Clinical Ladder Tier'
APPCL['at_amount'] = '5000'
APPCL['at_short_code'] = 'APPCL'
APPCL['start_date'] = today_date_mdyy_1
APPCL['end_date'] = today_plus_365d_mdyy
APPCL['at_payback_schedule'] = '1 Year Prorated Repayment'


APPCL2 = {}
APPCL2['at_category'] = 'Compensation'
APPCL2['at_type'] = 'APP Clinical Ladder Tier'
APPCL2['type__c'] = 'Tier 4'
APPCL2['at_description'] = 'Clinical Ladder Tier'
APPCL2['at_amount'] = '5000'
APPCL2['at_short_code'] = 'APPCL'
APPCL2['start_date'] = today_date_mdyy_1
APPCL2['end_date'] = today_plus_365d_mdyy
APPCL2['at_payback_schedule'] = '2 Year Prorated Repayment'

INCLL = {}
INCLL['at_category'] = 'Compensation'
INCLL['at_type'] = 'Shift Pay'
INCLL['type__c'] = 'Shift Pay'
INCLL['at_description'] = 'In House Call'
INCLL['at_amount'] = '130'
INCLL['at_shift_hours'] = '13'
INCLL['at_short_code'] = 'INCLL'
INCLL['start_date'] = today_date_mdyy_1

SDAY = {}
SDAY['at_category'] = 'Compensation'
SDAY['at_type'] = 'Shift Pay'
SDAY['type__c'] = 'Shift Pay'
SDAY['at_description'] = 'Specialty Day'
SDAY['at_amount'] = '120'
SDAY['at_shift_hours'] = '12'
SDAY['at_short_code'] = 'SDAY'
SDAY['start_date'] = today_date_mdyy_1

SONCL = {}
SONCL['at_category'] = 'Compensation'
SONCL['at_type'] = 'Shift Pay'
SONCL['type__c'] = 'Shift Pay'
SONCL['at_description'] = 'Specialty On Call'
SONCL['at_amount'] = '135'
SONCL['at_shift_hours'] = '10'
SONCL['at_short_code'] = 'SONCL'
SONCL['start_date'] = today_date_mdyy_1

PAGEX = {}
PAGEX['at_category'] = 'Compensation'
PAGEX['at_type'] = 'Shift Pay'
PAGEX['type__c'] = 'Shift Pay'
PAGEX['at_description'] = '12 Hr Pager'
PAGEX['at_amount'] = '100'
PAGEX['at_shift_hours'] = '10'
PAGEX['at_short_code'] = 'PAGEX'
PAGEX['start_date'] = today_date_mdyy_1

INCLW = {}
INCLW['at_category'] = 'Compensation'
INCLW['at_type'] = 'Hourly Pay'
INCLW['type__c'] = 'Hourly Pay'
INCLW['at_description'] = 'In House Call Weekend'
INCLW['at_amount'] = '100'
INCLW['start_date'] = today_date_mdyy_1

XCOVR = {}
XCOVR['at_category'] = 'Compensation'
XCOVR['at_type'] = 'Hourly Pay'
XCOVR['type__c'] = 'Hourly Pay'
XCOVR['at_description'] = 'X Cover'
XCOVR['at_amount'] = '100'
XCOVR['start_date'] = today_date_mdyy_1

DONCL = {}
DONCL['at_category'] = 'Compensation'
DONCL['at_type'] = 'Hourly Pay'
DONCL['type__c'] = 'Hourly Pay'
DONCL['at_description'] = 'Day On Call'
DONCL['at_amount'] = '150'
DONCL['start_date'] = today_date_mdyy_1

PAGXX = {}
PAGXX['at_category'] = 'Compensation'
PAGXX['at_type'] = 'Hourly Pay'
PAGXX['type__c'] = 'Hourly Pay'
PAGXX['at_description'] = '24 Hr Pager'
PAGXX['at_amount'] = '100'
PAGXX['start_date'] = today_date_mdyy_1

SONCX = {}
SONCX['at_category'] = 'Compensation'
SONCX['at_type'] = 'Hourly Pay'
SONCX['type__c'] = 'Hourly Pay'
SONCX['at_description'] = 'Specialty On Call 2'
SONCX['at_amount'] = '100'
SONCX['start_date'] = today_date_mdyy_1

MIPBN = {}
MIPBN['at_category'] = 'Compensation'
MIPBN['at_type'] = 'Management Incentive Bonus Eligibility'
MIPBN['type__c'] = 'Bonus'
MIPBN['at_description'] = 'Management Incentive Bonus'
MIPBN['at_short_code'] = 'MIPBN'
MIPBN['start_date'] = today_minus_20_days_mdyy

#TODO add Base Case Rate
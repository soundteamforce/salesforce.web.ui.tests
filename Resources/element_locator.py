# login page

firstpg_login_uname = "//input[@id='username']"
firstpg_login_pswd = "//input[@id='password']"
firstpg_login_btn = "//input[@id='Login']"
home_sandbox_lbl = "//span[contains(text(), 'Sandbox:')]"
home_env_lbl = "//span[contains(text(),env)]"
home_usr_full_name = "//a[@id='globalHeaderNameMink']//span[contains(text(),user_full_name)]"

home_view_profile = "//span[@class='button-container-a11y']//li[9]"
home_switch_to_classic = "/html/body[1]/div[4]/div[2]//div[5]//a"
home_logout_popup_1 = "//a[@id='globalHeaderNameMink']//b[@class='zen-selectArrow']"
home_logout_popup = "//a[@id='globalHeaderNameMink']"
home_logout_link = "//a[contains(text(),'Logout')]"
home_logout_home = "//li[@id='home_Tab']//a[contains(text(),'Home')]"

# navigate to cases page /html/body/div[4]/div[2]/div[7]/div[1]/div[1]/div/div[5]
all_tab_xpath = "//li[@id='AllTab_Tab']"
cases_tab_xpath = "//li[@id='Case_Tab']//a[contains(text(),'Cases')]"
new_case_btn = "//*[@id='hotlist']/table/tbody/tr/td[2]//input[@name='new']"
case_emp_event = "//option[@value='0120y000000cc0d']"
continue_btn = "//td[@id='bottomButtonRow']//input[@name='save']"
new_case_record_type = "//select[@id='p3']"
case_change_type = "//select[@id='00N0y000006u3CR']"
case_new_agrmt_trm_track = "//select[@id='00N0y000006u3CS']"
case_pro_agreemt_needed = "//select[@id='00N0y000006u3CV']"
case_sound_service_line = "//input[@id='00N0y000006u3CW']"
# New Case Page
new_case_subject_in = "//input[@id='cas14']"
new_case_req_catergory_drpdwn = "//select[@id='00N34000005kLKT']"
new_case_ini_description = "//textarea[@id='cas15']"
new_case_status_drpdwn = "//select[@id='cas7']"
new_case_priority_drpdwn = "//select[@id='cas8']"
new_case_ini_eff_date = "//input[@id='00N0y000004pReL']"
new_case_due_date = "//input[@id='00N34000005ZjEF']"
new_case_select_date = "//*[@id='ep']/div[2]/div[3]/table/tbody/tr[2]/td[2]/div/span/span"


#TODO: Hard coded value -- needs to change so that it will pickup the value from DB
new_case_contact_name = "//input[@id='cas3']"
new_case_ini_position = "//input[@id='CF00N0y000004pReV']"

new_case_status_on_hold = "//input[@id='00N0y000004pRei']"
new_case_status_to_cancelled =  "//input[@id='00N0y000004pReh']"

new_case_save_btn = "//td[@id='bottomButtonRow']//input[@name='save']"
new_case_save_n_new_btn = "//td[@id='bottomButtonRow']//input[contains(@name,'save_new')]"
new_case_cancel_btn = "//td[@id='bottomButtonRow']//input[contains(@name,'cancel')]"

#validate_case page
#
v_page_type_e = "//h1[@class='pageType']"

v_page_description_e = "//h2[@class='pageDescription']"


v_request_category_e = "//div[@id='00N34000005kLKT_ileinner']"
v_record_type_e = "//div[@id='RecordType_ileinner']"
v_case_owner_e = "//div[@id='cas1_ileinner']"
v_case_number_e = "//div[@id='cas2_ileinner']"

# Compensation Wizard
home_autotester = home_usr_full_name
home_setup = "//a[contains(text(),'Setup')]"
home_manage_users_arrow = "//a[@id='Users_icon']//img[@class='setupImage']"
home_manage_users = "//a[@id='ManageUsers_font']"
home_all_users = "//*[@id='fcf']/option[8]"
home_sort_asc = "//img[@class='sortAsc']"
home_sort_desc = "//img[@class='sortDesc']"
home_full_name = "//div[@class='pbBody']//table[@class='list']//th[2]/a[1]"


hardcoded_login_path_for_jill = "//*[@id='ResetForm']/div[2]/table/tbody/tr[16]/td[1]/a[2]"
home_jill_albach = "//a[contains(text(),'Albach, Jill')]"
home_login_jill_albach = "//*[@id='ResetForm']/div[2]/table[1]/tbody[1]//a[contains(text(),'Albach, Jill')]"
home_login_as_user_login_btn = "//td[@id='topButtonRow']//input[@name='login']"
home_search_jill = ("//*[contains(text(), 'Albach, Jill')]")

home_search = "//input[@id='phSearchInput']"
home_search_button = "//input[@id='phSearchButton']"
home_search_candidate = "//input[@id='phSearchInput']"
    # "//a[contains(text(),'C-')]"
home_second_search_btn = "//*[@id='secondSearchButton']"
home_second_search_input = "//input[@id='secondSearchText']"
home_candidate_search_link = "//div[@id='Candidate__c']//tr[contains(@class,'dataRow')]//th//a[1]"
home_candidate_search_str = "//div[@id='Candidate__c']//table[contains(@class,'list')]//tbody//a[contains(text(),'"

home_search_options_link = "//a[@id='searchMoreOptionsLink']"
home_search_limit_to_iown_chbox = "//input[@id='moreOptionsLimitToItemsIOwn']"
home_search_exact_phrase = "//input[@id='moreOptionsExactPhrase']"
home_save_n_search_btn = "//input[@id='moreOptionsSearchBtn']"

home_search_page_contact_found = "//div[@class='chatterBreadcrumbs']"
home_search_page_contact_arrow = "//a[@id='moderatorMutton']//b[@class='zen-selectArrow']"

home_search_user_found = "//*[@id='User_body']/table/tbody/tr[2]/th/div/div"
# home_search_user_found = "//table[contains(@class,'list')]/tbody[1]/tr[2]/th[1]/div[1]//div[@class='displayName']"
home_search_down_arrow = "//a[@id='moderatorMutton']"
home_search_page_arrow_user_Detail = "//a[@id='USER_DETAIL']"
home_user_page_title_desc = "//h2[@class='pageDescription']"

# candidate page
cand_generate_pa_btn = "//td[@id='topButtonRow']//input[@name='generate_provider_agreement']"
cand_collab_engine_link = "//td[@id='bodyCell']//div[14]//tr[2]//th[1]//a[1]"
cand_position_link = "//div[@id='CF00N80000003oR82_ileinner']//a[1]"

#position page
position_title = "//h1[@class='pageType']"
position_edit_btn = "//td[@id='topButtonRow']//input[@name='edit']"
position_no_req_checkbox = "//table[@class='detailList']//tr[7]/td[2]/input[1]"
position_save_btn = "//td[@id='topButtonRow']//input[contains(@name,'save')]"
position_no_req_chkbox_img = "//table[@class='detailList']//tr[7]/td[2]//div[1]//img[1]"
#Collaboration Engine Page
ce_create_new_ce_btn = "//table[@id='bodyTable']//td[@id='bodyCell']//div[14]//tbody/tr/td[2]/input"
ce_create_new_ce_btn_new = "//input[contains(@name,'new00N0y000004tvDD')]"
ce_acquisition_chbox_div = "//div[@id='ep']//table[1]//tr[5]//td[2]//div[1]"
ce_acquisition_chbox = "//div[@id='ep']//table[1]//tr[5]//td[2]//input[1]"
ce_inline_save_btn = "//td[@id='topButtonRow']//input[@name='inlineEditSave']"
ce_tble_xpath_str = "//div[contains(@class ,'pbBody')]//div[3]//table[1]"
ce_table_ce_recordtype_td = "//div[contains(@class ,'pbBody')]//div[5]//table[1]//tr[2]//td[4]"
ce_new_recordtype_select = "//select[@id='p3']"
ce_new_recordtype_continue_btn = "//td[@id='bottomButtonRow']//input[contains(@name,'save')]"
ce_new_effective_date = "//input[@id='00N0y000004tvDH']"
ce_new_save_btn = "//div[contains(@class,'pbBottomButtons')]//input[1]"

ce_new_privileging_btn = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[18]//input[1]"
ce_new_privileging_btn_new = "//input[@name='new00N0y000004tlwV']"
ce_new_privileging_record_type = "//select[@id='p3']"
ce_new_privileging_continue_btn = "//td[@id='bottomButtonRow']//input[@name='save']"
ce_new_privileging_status =  "//div[contains(@class,'pbBody')]//div[3]//table[@class='detailList']//tr[1]/td[" \
                             "4]//select[1]"
ce_new_privileging_file_flag_text = "//div[contains(@class,'pbBody')]//div[3]//table[@class='detailList']//tr[3]/td[" \
                           "4]//select[1]"
ce_new_privileging_file_save_btn = "//td[@id='bottomButtonRow']//input[@name='save']"
ce_er_position_name = "//div[@id='ep']//div[contains(@class,'pbBody')]//table[@class='detailList']/tbody[1]/tr[1]/td[4]/div[1]/a[1]"
ce_er_new_er_button = "//td[@class='pbButton']/input[@value='New Employment Requirement']"  #"//input[@name='new_employment_requirement']"
ce_er_record_type = "//div[@class='apexp']//div[@class='pbBody']//table[@class='detailList']//tr[1]/td[1]/select[1]"
ce_er_continue_button = "//div[@class='pbBottomButtons']//table//tbody//input[1]"
ce_cpa_new_effective_date_link = "//div[contains(@class,'pbBody')]//div[3]//table[1]//tr[2]/td[2]//a[1]"
ce_cpa_new_er_pos_xpath_str = "//div[@id='ep']//div[contains(@class,'pbBody')]//table[@class='detailList']//tr[2]/td[" \
                              "2]/div[1]/a[1]"
ce_qa_new_effective_date_link = "//div[@id='ep']//div[@class='pbBody']//div[3]//table[@class='detailList']//tr[2]//td[2]//a[1]"
ce_qa_new_er_pos_xpath_str = "//div[contains(@class,'pbBody')]//div[3]//tr[2]//td[" \
                                                           "2]//a[1]"
ce_qa_new_interval_input = "//div[@id='ep']//div[@class='pbBody']//div[5]//tr[1]/td[2]//input[1]"
ce_ratio_new_effective_date_link = "//div[contains(@class,'pbBody')]//table[1]/tbody[1]/tr[2]/td[2]//a[1]"
ce_ratio_new_desc_input = "//td[@id='bodyCell']//div[3]//tr[1]//td[4]//select[1]"
ce_ratio_new_pri_ratio_num = "//td[@id='bodyCell']//div[5]//table[1]//tr[1]//td[2]//input[1]"
ce_ratio_new_er_pos_xpath_str = "//div[contains(@class,'pbBody')]//div[3]//table[1]//tr[2]//td[2]//a[1]"
ce_new_emp_req_save_btn = "//div[contains(@class,'pbBottomButtons')]//input[1]"

# Supporting Document
sd_name = "//td[@id='bodyCell']//div[8]//tr[2]//th[1]/a[1]"
sd_edit_button = "//td[@id='topButtonRow']//input[@name='edit']"
sd_save_button = "//td[@id='topButtonRow']//input[@name='save']"
sd_issue_date_div = "//div[@class='pbBody']//div[2]//table[1]//tr[2]/td[4]/div[1]"
sd_issue_date_div_1 = "//div[@class='pbBody']//div[3]//table[1]//tr[2]//td[4]//input[1]"
sd_issue_date_div_link = "//div[@class='pbBody']//div[2]//table[1]//tr[2]/td[" \
                                                          "4]//div[2]//input[1]"
sd_expiration_date_div = "//div[@class='pbBody']//div[2]//table[1]//tr[3]/td[4]/div[1]"

sd_expiration_date_div_link = "//div[@class='pbBody']//div[2]//table[1]//tr[3]/td[" \
                                                          "4]//div[2]//input[1]"
sd_expiration_date_div_1 = "//div[@class='pbBody']//div[3]//table[1]//tr[3]//td[4]//input[1]"
sd_inline_save_button = "//td[@id='topButtonRow']//input[contains(@name,'inlineEditSave')]"
page_issue_date = "//div[@class='pbBody']//div[2]//table[1]//tr[2]/td[4]/div[1]"
page_expiration_date = "//div[@class='pbBody']//div[2]//table[1]//tr[3]/td[4]/div[1]"
page_next_sd_create_date = "//div[@class='pbBody']//div[6]//table[1]//tr[3]//td[2]"

# provider_agreement page
pa_create_new_pa = "//input[@id='j_id0:j_id3:j_id5:j_id51:j_id53']"
pa_agreement_type_pea = "//table[@class='detailList']/tbody/tr[2]/td[4]/div/span/span/select"
#pa_agreement_type_pea = "//*[@class='detailList']//tr[2]//td[4]//div[1]//select[1]"
pa_new_record_type_str = "//option[contains(text(), "
pa_pea_option = "//option[contains(text(),'Provider Employment Agreement')]"
pa_initial_cdl_option = "//option[contains(text(),'Initial CDL')]"
pa_continue_btn = "//td[@id='bottomButtonRow']//input[@name='save']"
pa_agreement_stage = "//table[@class='detailList']/tbody/tr[2]/td[4]/div/span/select"
pa_agreement_type = "(//table[@class='detailList']/tbody/tr/td[4]/div/span/select)[1]"
pa_ag_type_service_line = "//*[@class='detailList']//tr[1]//td[4]//div[1]//select[1]"
pa_anticipated_start_date = "//div[contains(@class,'pbBody')]//div[3]//table[1]/tbody[1]/tr[3]/td[4]/div[1]/span[1]/span[@class='dateFormat']//a"
pa_anticipated_start_date_1 = "//tbody/tr[4]/td[4]/div[1]/span[1]/span[1]/a[1]"
pa_anticipated_amendment_start_date = "//div[contains(@class,'pbBody')]//tr[3]/td[2]/div//a"
# pa_anticipated_amendment_start_date = "//div[contains(@class,'pbBody')]//tr[3]/td[2]/div//a"
pa_cdl_return_date = "//*[@id='ep']/div[2]/div[3]/table/tbody/tr[4]/td[4]/div/span/span/a"
pa_comments_text = "//div[@class='pbBody']//div[3]//table[@class='detailList']//textarea[1]"
#/div[2]/div[3]/table[1]/tbody[1]/tr[4]/td[4]/div[1]/span[1]/span[1]/a[1]
pa_save_btn = "//div[contains(@class,'pbBottomButtons')]//input[1]"
pa_save_inline_btn =  "//td[@id='topButtonRow']//input[@name='inlineEditSave']"
# pa_existing_pa_label = "//h3[contains(text(),'Select Existing Provider Agreement')]"
# pa_existing_pa_table = "//*[@id='j_id0:j_id3:j_id5:j_id31:j_id32:0:j_id33']/input"
pa_existing_pa_table = "//*[@id='j_id0:j_id3:j_id5:j_id31:j_id32:tb']"
pa_existing_pa_tbody = "//*[@id='j_id0:j_id3:j_id5:j_id31:j_id32:tb']"
pa_record_type_pea = "//*[@id='j_id0:j_id3:j_id5:j_id31:j_id32:0:j_id50']"
# //*[@id="j_id0:j_id3:j_id5:j_id31:j_id32:tb"]
pa_select_existing_pa_chbx = "//input[@name='j_id0:j_id3:j_id5:j_id31:j_id32:0:j_id34']"
pa_select_existing_pa_next_btn = "//input[@id='j_id0:j_id3:j_id5:j_id51:j_id52']"
pa_c_terms_tbl_body = "//*[@id='a340R000000HeS6_00N34000005l2K5']"
pa_buy_primary_cand_input = "//div[@class='pbBody']//div[3]//table[1]//tr[2]//td[2]//input[1]"
pa_buy_previous_cand_input = "//div[@class='pbBody']//div[3]//table[1]//tr[3]//td[2]/div[1]/span[1]/input[1]"
# pa_buy_previous_cand_input = "//input[@id='CF00N2g000000dYZF']"
pa_buy_previous_cand_lookup = "//div[@class='pbBody']//div[3]//table[1]//tr[3]//td[2]//a[1]/img[1]"
pa_buy_Agreement_type_select = "//div[@class='pbBody']//div[3]//table[1]//tr[1]//td[4]//select[1]"
pa_buy_stage_select = "//div[@class='pbBody']//div[3]//table[1]//tr[2]//td[4]//select[1]"
pa_app_email_sent_date = "//table[@class='detailList']/tbody/tr[7]/td[2]"
pa_act_htry_link = "//div[@class='listHoverLinks']/a[6]"
pa_app_wc_email_sub = "(//div[@class='listRelatedObject taskBlock'])[2]/div/div[2]/table/tbody/tr[2]/th/a"




ct_delete_btn = "//td[@id='topButtonRow']//input[@name='delete1']"
ct_confirm_delete_btn = "//input[@name='j_id0:j_id2:j_id29:j_id30:j_id31']"
home_searched_pa_link = "//table[@class='list']/tbody[1]/tr[2]/th[1]/a[1]"
home_searched_user_link_str = "//div[@id='User']//div[@id='User_body']//a[contains(text(),'"
home_user_div_table_list = "//div[@id='User']"
home_searched_pa_link_str = "//div[@id='Provider_Agreement__c_body']//a[contains(text(),'"
# home_Searched_user_link = "//*[@id='User_body']/table/tbody/tr[2]/th/div/div/a"
pa_agreement_term_name="//div[@id='Provider_Agreement__c_body']//tr[contains(@class,'dataRow')]//th[1]/a"
home_search_pa_no_matches = "//div[@id='searchResultsWarningMessageBox']"
pa_approver_name = "//tbody/tr[3]/td[3]/a[1]"    #"//tr[@class='dataRow even first']/td[3]/a"
# pa_term_at_rows = "//div[@id='a340R000000Hem7_00N34000005l2K5']//tr[contains(@class,'dataRow')]"
compensation_term_link_tbody = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[5]/div[1]/div[1]/div[2]/table[1]"
comp_term_xpath = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[5]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr["

addtnal_term_link_tbody = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[6]/div[1]/div[1]/div[2]/table[1]"
addtnal_term_xpath_row_link = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[6]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr["

# /tbody[1]/tr[2]/th[1]

pa_existing_ct_a = "//*[@id='a340R000000Hdz8_00N34000005l2K5_body']/table/tbody/tr[2]/th/a"
pa_delete_at_btn = "//td[@id='topButtonRow']//input[@name='delete1']"
pa_edit_pa_terms_btn = "//td[@id='topButtonRow']//input[@name='edit_provider_agreement']"

# pa_agreement_term_name = "//td[@id='topButtonRow']//input[@name='edit_provider_agreement']"

# Provider Agreement Compilation
# pac_pgdln_add_link = "//a[@id='compilationPageId:compilationFormId:j_id36:0:j_id38']"
pac_pgdln_guide = "//*[@id='leftAgreementSection']/div[2]/div[1]//table[1]/tbody"
pac_pgdln_add_link = pac_pgdln_guide + "/tr[1]/td[1]//a[1]"
pa_unlock_button = "//td[@id='topButtonRow']//input[@name='unlock']"
pa_agdln_end_date = "//input[@id='compilationPageId:compilationFormId:GuidelineEndDate']"
pa_agdln_start_date = "//input[@id='compilationPageId:compilationFormId:GuidelineStartDate']"


# Agreement term page
at_delete_confirm_btn = "//input[@name='j_id0:j_id2:j_id29:j_id30:j_id31']"

# Provider agreement wizard page
w_pa_body_tbl = "//table[@id='bodyTable']"
w_pa_new_name = "//*[@id='leftAgreementSection']/div[1]/h4"
# w_add_unlisted_agreement_type = "//input[@id='compilationPageId:compilationFormId:j_id42']"
w_add_unlisted_agreement_type = "//input[@value = 'Add Unlisted Agreement Type']"
w_add_unlisted_agreement_type_new = "//form[@id='compilationPageId:compilationFormId']/div[1]/div[1]/div[2]/div[" \
                                   "1]/div[1]/div[1]/input[2]"
    # "//input[@id='compilationPageId:compilationFormId:j_id42']"
w_add_defaults_btn = "//input[@id='compilationPageId:compilationFormId:asIsButton']"
# w_execute_changes_btn = "//*[@id='compilationPageId:compilationFormId:j_id179']"
w_execute_changes_btn = "//form[1]/div[2]//div[1]/input[1]"
w_execute_changes_btn_old = "//form[1]/div[2]//div[1]/input[2]"
w_reset_btn = "//input[@value ='Reset']"

w_no_existing_at_left_sectn = "//*[@id='leftAgreementSection']/div[2]/div[2]/div/div/table/tbody"

w_existing_at_left_agreement_sectn_table = "//*[@id='leftAgreementSection']/div[2]/div[1]/div/div/table"
w_add_updated_btn = "//input[@id='compilationPageId:compilationFormId:editAdd']"
# Right Section of the wizard page

w_wizard_right_table = "//*[@id='compilationPageId:compilationFormId:j_id67']/table"
w_wizard_right_tbl_bdy = "//*[@id='compilationPageId:compilationFormId:j_id67']/table/tbody"
w_wizard_main_at_type_input = "//select[@id='compilationPageId:compilationFormId:unlistedATTypeSelected']"
# w_wizard_main_at_type_input = "//select[@name='compilationPageId:compilationFormId:j_id67']"
# w_wizard_main_at_type_input = "//select[@name='compilationPageId:compilationFormId:j_id64']"
w_line_item_rows = "//table[@class='slds-table slds-table_bordered slds-table_striped innerPadding']/tbody/tr"
w_line_item_desc = "//table[@class='slds-table slds-table_bordered slds-table_striped innerPadding']/tbody/tr[1]/td[6]"

# Agreement Term Guideline table
w_atg_table = "//table[@class='slds-table slds-table_bordered slds-table_striped']"
w_atg_table_body = "//table[@class='slds-table slds-table_bordered slds-table_striped']/tbody"
w_atg_table_row = "//table[@class='slds-table slds-table_bordered slds-table_striped']/tbody//tr[@class='slds-hint-parent']"





w_wizard_type_input = "//select[@id='compilationPageId:compilationFormId:Type']"
w_wizard_description_input_75 = "//select[@id='compilationPageId:compilationFormId:j_id75']"
w_wizard_description_input_orig = "//select[@id='compilationPageId:compilationFormId:descriptionOrig']"
#w_wizard_description_input_pop = "//select[contains(@id, 'description')]"
w_wizard_description_input_pop ="//select[@id='compilationPageId:compilationFormId:descriptionPop']"
w_wizard_description_input_relpath = "//div[@id='rigthSectionInnerDiv']/span/span/span/table/tbody/tr[4]/td[2]/div/span/select"
w_wizard_description_visa_input = "//select[@id='compilationPageId:compilationFormId:VisaType']"
w_wizard_amount_input = "//input[@id = 'compilationPageId:compilationFormId:Amount2']"
w_wizard_shift_hours_input = "//input[@id='compilationPageId:compilationFormId:ShiftHours']"
w_wizard_payback_schedule_input = "//select[@id='compilationPageId:compilationFormId:PaybackSchedule']"
w_wizard_start_date_input = "//input[@id='compilationPageId:compilationFormId:TermStartDate']"
# w_wizard_start_date_input = "//tbody/tr[19]/td[2]/span[1]/div[1]/span/input"
w_wizard_quantity_input = "//input[@id='compilationPageId:compilationFormId:Quantity']"
w_wizard_schedule_input = "//select[@id='compilationPageId:compilationFormId:Schedule']"
w_wizard_period_dropbox_input = "//select[@id='compilationPageId:compilationFormId:Period']"
w_wizard_end_date_input = "//input[@id='compilationPageId:compilationFormId:TermEndDate']"
w_wizard_visa_type_input = "//select[@id='compilationPageId:compilationFormId:Type']"
w_wizard_workload_input = "//select[@id='compilationPageId:compilationFormId:Workload']"
w_wizard_stipend_interval_input = "//select[@id='compilationPageId:compilationFormId:IntervalIn']"
w_wizard_forgiveness_period_input = "//select[@id='compilationPageId:compilationFormId:ForgivenessPeriod']"
w_wizard_shift_level_input = "//select[@id='compilationPageId:compilationFormId:ShiftLevel']"
w_wizard_threshold_min_input = "//input[@id='compilationPageId:compilationFormId:ThresholdMinimum']"
w_wizard_threshold_max_input = "//input[@id='compilationPageId:compilationFormId:ThresholdMaximum']"
w_wizard_threshold_type_dd_input = "//select[@id='compilationPageId:compilationFormId:ThresholdType']"
# w_wizard_threshold_interval_dd_input = "//select[@id='compilationPageId:compilationFormId:j_id83']"

w_candidate_to_use_input = "//input[@id='compilationPageId:compilationFormId:Candidate']"
w_wizard_guideline_shift_hours_input = "//input[@id='compilationPageId:compilationFormId:ShiftHours']"
w_wizard_guideline_shift_amount_input = "//input[@id='compilationPageId:compilationFormId:amount']"

w_left_at_table = "//table[@id='bodyTable']//div[@id='leftAgreementSection']/div[2]/div[2]/div/div/table"
w_left_at_tr_str = "//table[@id='bodyTable']//div[@id='leftAgreementSection']/div[2]/div[2]/div/div/table//tr["
w_left_at_cancellation_chbx_115 = "//input[@id='compilationPageId:compilationFormId:j_id115']"
w_left_at_cancellation_chbx = "//input[@id='compilationPageId:compilationFormId:Cancellation']"
w_left_at_cancellation_chbx_119 = "//input[@id='compilationPageId:compilationFormId:j_id119']"
# w_left_at_cancellation_chbx_116 = "//input[@id='compilationPageId:compilationFormId:j_id116']"

w_left_at_end_btn = "//input[@id='compilationPageId:compilationFormId:endAT']"

pa_list_terms_page  = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[5]/div[1]/div[1]/div[2]/div[1]/a[2]"
pa_list_terms_page_tbl = "/html[1]/body[1]//table[1]/tbody[1]/tr[1]/td[2]//table[@class='list']"
pa_ct_terms_show_more = "//a[contains(text(),'Show')]"
# pa_ct_terms_go_to_list = "//a[contains(text(),'Go to list ']"
pa_ct_terms_go_to_list = "/html[1]/body[1]/div[1]/div[3]/table[1]/tbody[1]/tr[1]/td[2]/div[5]//a[contains(text(),'Go to list (')]"
pa_stage = "(//table[@class='outer']//table[@class='detailList']/tbody[1]/tr[2]/td[4]/div[1])[1]"
pa_stage_1 = "//table[@class='outer']//table[@class='detailList']/tbody[1]/tr[3]/td[4]"
pa_stage_td = "//table[@class='outer']//table[@class='detailList']/tbody[1]/tr[2]/td[4]"
pa_stage_to_dblcl = "//div[@id='00N34000005l2Kh_ileinner']"
pa_select_stage_dropdown = "//*[@id='00N34000005l2Kh']"
pa_save_inline_btn = "//td[@id='topButtonRow']//input[@name='inlineEditSave']"
pa_edit_pa_record_btn = "//td[@id='topButtonRow']//input[@name='edit']"
pa_record_lock_td = "//td[@id='bodyCell']/table[1]//tr[1]/td[1]"
pa_record_lock_to_back = "//a[contains(text(),'here')]"
pa_error_body = "//td[@id='bodyCell']"
pa_stage_field = "//select[@name='00N34000005l2Kh']"
pa_stage_select = "//select[@name='00N34000005l2Kh']"
pa_save_button = "//td[@class='pbButton']//input[@name='save']"
pa_cancel_button = "//td[@class='pbButtonb']//input[@name='cancel']"
pa_div_error_msg = "//div[@class='errorMsg']"

# Approver home page
appvr_items_to_appvr_tr = "//div[@id='PendingProcessWorkitemsList_body']//table[@class='list']//tr"
# appvr_items_to_appvr_tbl = "//div[@id='PendingProcessWorkitemsList']//div[@id='PendingProcessWorkitemsList_body']//table[@class='list']"
# appvr_items_to_appvr_tbl = "//table[@id='bodyTable']//div[@id='PendingProcessWorkitemsList']//div[
# @class='listRelatedObject processInstanceWorkitemBlock']//table[@class='list']"  "//*[@id="PendingProcessWorkitemsList_body"]/table
appvr_items_to_appvr_tbl =  "//*[@id='PendingProcessWorkitemsList_body']/table"
appvr_items_to_appvr_rej_link = "//table[@id='bodyTable']//table[@class='list']//a[contains(text(),'Approve / Reject')]"

appvr_items_to_appvr_tbody = "//div[@id='PendingProcessWorkitemsList_body']//table[@class='list']//tbody[1]"
appvr_items_to_appvr_column_appv_clmn = "//td[1]"
appvr_items_to_appvr_column_appv_link = "//td[1]/a[2]"
appvr_items_to_appvr_comment = "//textarea[@id='Comments']"
appvr_items_to_appvr_btn = "//input[@name='goNext']"
appvr_items_to_reject_btn = "//input[@name='Reject']"
appvr_items_to_appvr_home = "//ul[@id='tabBar']//a[contains(text(),'Home')]"
appvr_items_to_appvr_overall_status = "//span[@class='extraStatus']"
appvr_items_to_appvr_description = "//h2[@class='pageDescription']"

### PA Approval recall ########
pa_recall_btn = "//input[@name='piRemove']"
pa_recall_comnts = "//textarea[@id='Comments']"
pa_recall_apvl_req = "//input[@value='Recall Approval Request']"









import os
from os import sys, path
import unittest
from ddt import ddt, unpack
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
sys.path.append(os.path.realpath('.'))
from Helpers import collab_engine_helper as ceh
from Helpers import ce_employment_requirements as emp_req
from Pages import Home as hm
from Pages import comp_login as cl
from sc_util import target_process_helper as tph
from Helpers import log_helper as lg
import ui_config
from Pages import comp_login as clogin
from Pages import privileging_file as pg_pf
from Pages import collaboration_engine as pg_ce
from Pages import supporting_document as pg_sd

@ddt
class CollabEngUiBvt(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        ui_config.test_cases, ui_config.child_test_plan_run_id = tph.initialize_target_process(ui_config.tp_tag, ui_config.test_plan_run_id,
                                                                                               ui_config.suite_name,
                                                                                               ui_config.run_target_process)


        # lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        ceh.collab_engine_setup()
        # lg.log_results("")

    def tearDown(self):
        cl.log_off()
        # if cwuc.run_target_process:
        #     u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        ui_config.wd.quit()

    @unpack
    def test_01_create_collab_engine_check_acquisition_option(self):
        ui_config.test_case_name = "01 Verify an acquisition option can be checked for Collaboration Engine"
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        emp_req_type = ['cpa', 'ratio', 'qa']
        emp_req.create_emp_req(emp_req_type)
        pri_file_name = pg_pf.create_privileging_file()
        # pg_ce.check_ce_acquisition()
        # pg_sd.update_supporting_document_dates()

        # ceh.delete_pf(pri_file_name)
        # ceh.delete_emp_req()
        # ceh.delete_collab_engine()



if __name__ == "__main__":
    unittest.main(module='__main__', argv=['test_env'], exit=False)
    unittest.TestLoader.sortTestMethodsUsing = None
    unittest.TestLoader().loadTestsFromTestCase(CollabEngUiBvt)
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)


import os
from os import sys, path
import unittest
from ddt import ddt, unpack
from datetime import datetime, timedelta as td, date
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
sys.path.append(os.path.realpath('.'))
from Helpers import collab_engine_helper as ceh
from Helpers import ce_employment_requirements as emp_req
from Pages import Home as hm
from Pages import comp_login as cl
from Pages import collaboration_engine as ce_pg
from sc_util import target_process_helper as tph
from Helpers import log_helper as lg
import ui_config
from Pages import comp_login as clogin
from Pages import privileging_file as pg_pf
from Resources import test_variables as tv
from Helpers import utilities as u
from Pages import supporting_document as pg_sd

@ddt
class CollabEng69277(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        lg.log_results("\n\t ************** \t START of Collaboration Engine #69277  **************")
        is_login = clogin.login_sf()
        ui_config.test_cases, ui_config.child_test_plan_run_id = tph.initialize_target_process(ui_config.tp_tag, ui_config.test_plan_run_id,
                                                                                               ui_config.suite_name,
                                                                                               ui_config.run_target_process)

    def setUp(self):
        lg.log_results("\n \t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
        hm.login_admin()
        ceh.collab_engine_setup()
        # lg.log_results("")

    def tearDown(self):
        cl.log_off()
        lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
        if ui_config.run_target_process:
            u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        ui_config.wd.quit()
        lg.log_results("\n\t ************** \t END of Collaboration Engine #69277  **************")

#
    @unpack
    @unittest.skip("Skipping")
    def test_01(self):
        ui_config.test_case_name = "1. Collaboration Engine did not get created after creating privileging file " \
                                   "for NP position with No Requirements is checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('EM', 'Nurse Practitioner', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            pri_file_name = pg_pf.create_privileging_file()
            if pri_file_name:
                ret_ce_check = ceh.check_ce_exists()
                lg.assert_and_log(not ret_ce_check, "\t \t FAIL : Collaboration Engine should not be getting "
                "created.", "\t \t PASS : Collaboration Engine does not exist as expected.")
                ceh.delete_pf(pri_file_name)

    @unpack
    @unittest.skip("Skipping")
    def test_09(self):
        ui_config.test_case_name = "9. Verify a requirement that is not CPA, QA or Ratio can be added " \
                                   "to a Position that has No Requirements checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('EM', 'Nurse Practitioner', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            ceh.add_note()
            ceh.delete_note()

    @unittest.skip("Skipping")
    def test_06(self):
        ui_config.test_case_name = "6. Verify a CPA requirement can't be added to a Position" \
                                   " that has No Requirements checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('EM', 'Nurse Practitioner', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            emp_req_type = ['cpa']
            return_value = emp_req.create_emp_req(emp_req_type)
            lg.assert_and_log('error' in return_value,
                              "\t \t FAIL : CPA requirement should not have got added.",
                              "\t \t PASS : CPA requirement did not get added for position that has No Requirements checkbox "
                              "marked as expected.")
            ceh.delete_collab_engine()
            ceh.delete_emp_req()

    @unittest.skip("Skipping")
    def test_07(self):
        ui_config.test_case_name = "7. Verify a QA requirement can't be added to a Position" \
                                   " that has No Requirements checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('EM', 'Nurse Practitioner', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            emp_req_type = ['qa']
            return_value = emp_req.create_emp_req(emp_req_type)
            lg.assert_and_log('error' in return_value,
                              "\t \t FAIL : QA requirement should not have got added.",
                              "\t \t PASS : QA requirement did not get added for position that has No Requirements "
                              "checkbox marked as expected.")
            ceh.delete_collab_engine()
            ceh.delete_emp_req()

    @unittest.skip("Skipping")
    def test_08(self):
        ui_config.test_case_name = "8. Verify a Ratio requirement can't be added to a Position" \
                                   " that has No Requirements checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('EM', 'Nurse Practitioner', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            emp_req_type = ['ratio']
            return_value = emp_req.create_emp_req(emp_req_type)
            lg.assert_and_log('error' in return_value,
                              "\t \t FAIL : Ratio requirement should not have got added.",
                              "\t \t PASS : Ratio requirement did not get added for position that has No Requirements "
                              "checkbox marked as expected.")
            ceh.delete_collab_engine()
            ceh.delete_emp_req()


    @unpack
    @unittest.skip("Skipping")
    def test_03(self):
        ui_config.test_case_name = "3. Collaboration Engine did not get created after creating " \
                                   "Privileging File for a PA Position with No Requirements is checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('Hospitalist', 'Physician Assistant', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            pri_file_name = pg_pf.create_privileging_file()
            if pri_file_name:
                ret_ce_check = ceh.check_ce_exists()
                lg.assert_and_log(not ret_ce_check, "\t \t FAIL : Collaboration Engine should not be getting "
                                                    "created.", "\t \t PASS : Collaboration Engine does not exist as "
                                                                "expected.")
                ceh.delete_pf(pri_file_name)

    @unpack
    @unittest.skip("Skipping")
    def test_04(self):
        ui_config.test_case_name = "4. Compliance Admin could not create Collaboration Engine " \
                                   "for Position with No Requirements checkbox checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('Hospitalist', 'Physician Assistant', 'true')
        if ret_success:
            ceh.set_candidate_for_pos()
            ceh.create_ce(ui_config.compliance_admin_user)
            ceh.delete_collab_engine()


    # @unittest.skip("Skipping")
    def test_10(self):
        ui_config.test_case_name = "10.	Verify No Requirements can be Checked for a Position " \
                                   "that already has a CPA, Ratio or QA and the connected requirements are not Active."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('Hospitalist', 'Physician Assistant', 'false')
        if ret_success:
            ceh.set_candidate_for_pos()
            emp_req_type = ['cpa', 'ratio', 'qa']
            emp_req.create_emp_req(emp_req_type)
            pri_file_name = pg_pf.create_privileging_file()
            ret_ce_name = ceh.check_ce_exists()
            lg.assert_and_log(ret_ce_name, "\t \t FAIL : Collaboration Engine did NOT get "
                                           "created as expected.", "\t \t PASS : Collaboration Engine got "
                                                                   "created as expected " + str(ret_ce_name))
            eff_date = tv.today_minus_30_days_mdyy_with_dash
            end_date = tv.today_minus_15_days_mdyy_with_dash
            emp_req.update_employment_dates(eff_date, end_date)
            ce_pg.mark_collab_engine_no_req()
            ceh.delete_pf(pri_file_name)
            ceh.delete_emp_req()
            ceh.delete_collab_engine()


    @unittest.skip("Skipping")
    def test_05(self):
        ui_config.test_case_name = "5. Collaboration Engine created after creating privileging file" \
                                   " for PA position with No Requirements NOT checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('Hospitalist', 'Physician Assistant', 'false')
        if ret_success:
            ceh.set_candidate_for_pos()
            emp_req_type = ['cpa', 'ratio', 'qa']
            emp_req.create_emp_req(emp_req_type)
            pri_file_name = pg_pf.create_privileging_file()
            ret_ce_check = ceh.check_ce_exists()
            lg.assert_and_log(ret_ce_check, "\t \t FAIL : Collaboration Engine did NOT get "
                                            "created as expected.", "\t \t PASS : Collaboration Engine got "
                                                                    "created as expected " + str(ret_ce_check))
            ceh.delete_pf(pri_file_name)
            ceh.delete_emp_req()
            ceh.delete_collab_engine()

    @unittest.skip("Skipping")
    def test_02(self):
        ui_config.test_case_name = "2. Collaboration Engine created after creating privileging file for NP position" \
                                   " with No Requirements is NOT checked."
        lg.log_results("\t TEST CASE: \t " + ui_config.test_case_name)
        ret_success = ceh.collab_engine_setup_per_position('EM', 'Nurse Practitioner', 'false')
        if ret_success:
            ceh.set_candidate_for_pos()
            emp_req_type = ['cpa', 'ratio', 'qa']
            emp_req.create_emp_req(emp_req_type)
            pri_file_name = pg_pf.create_privileging_file()
            ret_ce_check = ceh.check_ce_exists()
            lg.assert_and_log(ret_ce_check, "\t \t FAIL : Collaboration Engine did NOT get "
                                            "created as expected.", "\t \t PASS : Collaboration Engine got "
                                                                    "created as expected " + str(ret_ce_check))
            ceh.delete_pf(pri_file_name)
            ceh.delete_collab_engine()
            ceh.delete_emp_req()
    #


if __name__ == "__main__":
    unittest.main(module='__main__', argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    unittest.TestLoader().loadTestsFromTestCase(CollabEng69277)
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)


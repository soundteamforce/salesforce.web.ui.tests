# python config for sf_create_entity_data.py

import sys
import sf_connection_config as sf_cc
from datetime import datetime
import pytz
from simple_salesforce import Salesforce
from Helpers import ignore_login_data as ild

now = datetime.now()
# now = datetime.datetime.now(pytz.UTC)

today_date = now.strftime('%m/%d/%Y')
today_datetime = now.strftime('%m-%d %H:%M')


log_file_name = "Suite_Entity_Creation"



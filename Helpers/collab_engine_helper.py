from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import login_as_user_with_search as lauws
import sf_connection_config as sf_cc
from Helpers import browser_utilities as bu
from Helpers import ui_helper
import ui_config as uic
from Resources import element_locator as e
from Resources import test_variables as tv
from Helpers import candidate_helper as ch
from Helpers import utilities as u

def collab_engine_setup():
        uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
        bu.sleep_for(1)
        contact_id, contact_name = sh.get_recent_contact(uic.created_by_admin_id)
        bu.sleep_for(1)

        uic.candidate_id_1, uic.candidate_name_1, uic.candidate_id_2, uic.candidate_name_2 = \
            sh.get_recent_candidate(contact_id)
        lg.assert_and_log(uic.candidate_id_1, "\t \t FAIL : Collaboration Engine Setup ")
        bu.sleep_for(5)
        uic.position_id, uic.position_name = sh.get_position_from_candidate()
        lg.assert_and_log(uic.position_id, "\t \t FAIL : Collaboration Engine Setup ")



def collab_engine_setup_per_position(ssl_type, position_title, no_req):
    str_pos_no_req = 'False'
    uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
    bu.sleep_for(5)
    uic.position_id, pos_no_req, uic.position_name = sh.get_pos_id_with_type(ssl_type, position_title)
    if pos_no_req:
        str_pos_no_req = 'True'
    if str_pos_no_req.casefold() != no_req.casefold():
        ret_success = sh.set_pos_no_req(no_req)
    else:
        ret_success = True

    lg.log_results("\t \t Position name will be " + uic.position_name)
    lg.assert_and_log(ret_success, "\t \t FAIL : The position for checking CE is not set correctly.",\
        "\t \t PASS : The position for checking the Collaboration Engine is set correctly. ")

    return ret_success



def set_candidate_for_pos():
    uic.candidate_id_1, uic.candidate_name_1 = sh.set_cand_for_pos()
    if not uic.candidate_id_1:
#         Create a candidate for the position, get recordtype of w2
        lg.log_results("\t \t Creating a new candidate for the position " + uic.position_name)
        w2_candidate_rec_type_name = 'W2'
        w2_candidate_rec_type_id = sh.get_record_type_id('W2', 'Candidate__c')
        contact_id_of_candidate = sh.get_recent_contact(uic.created_by_admin_id)
        uic.candidate_id_1, uic.candidate_name_1 = ch.create_new_candidate_rec_on_position(w2_candidate_rec_type_name,
                        w2_candidate_rec_type_id, contact_id_of_candidate[0], uic.position_id, 'Primary')
        lg.assert_and_log(uic.candidate_id_1, "\t \t FAIL : Error while creating candidate for position ", "\t \t New candidate for the position is " + uic.candidate_name_1)


def check_ce_exists():
    retvalue = sh.check_db_for_ce()
    lg.assert_and_log(retvalue,"\t \t FAIL : Checking Collaboration Engine exists. " )
    return retvalue


def delete_pf(privileging_file):
    ret_value = sh.delete_pf(privileging_file)
    if ret_value == 204:
        lg.log_results("\t \t Cleanup - Successfully deleted the privileging file " + str(privileging_file))
    else:
        lg.log_results("\t \t Cleanup - Couldn't delete the privileging file " + str(privileging_file))

def delete_collab_engine():
    ret_value = sh.delete_ce()

def delete_emp_req():
    ret_value = sh.delete_er()
    # if not ret_value:
    #     lg.log_results("\t \t Cleanup - Employment Requirement doesn't exist for position " + str(
    #         uic.position_name))
    # elif ret_value == 204:
    #     lg.log_results("\t \t Cleanup - Successfully deleted Employment Requirement for position " + str(
    #         uic.position_name))
    # else:
    #     lg.log_results("\t \t Cleanup - Couldn't delete Employment Requirement for position " + str(uic.position_name) +
    #                    " return value is " + str(ret_value))


def delete_note():
    ret_value = sh.delete_note()
    if ret_value == 204:
        lg.log_results("\t \t Cleanup - Successfully deleted Note for position " + str(
        uic.position_name))
    elif ret_value is None:
        lg.log_results("\t \t Cleanup - Note doesn't exist for position " + str(
            uic.position_name))
    else:
        lg.log_results("\t \t Cleanup - Couldn't delete Note for position " + str(uic.position_name) +
                       " return value is " + str(ret_value))


def create_ce(login_as_user):
    lg.log_results("\t \t Starting to create Collaboration Engine. ")
    bu.sleep_for(2)
    lauws.login_as_with_search(login_as_user, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')
    element = bu.is_visible_xpath(e.ce_create_new_ce_btn_new)
    if element:
        bu.click_elmt(e.ce_create_new_ce_btn_new)
        header_element = bu.find_element_by_xpath("//h2[contains(@class,'pageDescription')]")
        header_text = header_element.text
        record_type_text =  "Select Collaboration Engine Record Type"
        if record_type_text in header_text:
            bu.send_keys_elmt_xpath(e.ce_new_recordtype_select, "Collaboration Engine - Acquisition")
            bu.click_elmt(e.ce_new_recordtype_continue_btn)
        bu.send_keys_elmt_xpath(e.ce_new_effective_date, tv.today_date_mdyy)
        bu.click_elmt(e.ce_new_save_btn)
        expected_err_msg = "You cannot create Collaboration Engine if Position has 'No Requirements' set to True"
        check_error(expected_err_msg, 'ce')
    else:
        lg.assert_and_log(1 == 1, "\t \t FAIL : Collaboration Engine. ",
                          "\t \t PASS : " + login_as_user + " can't create Collaboration Engine as expected. ")
    return

def add_note():
    lg.log_results("\t \t Adding Note ")
    lauws.login_as_with_search(uic.compliance_admin_user, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')
    bu.click_elmt("//div[@id='CF00N80000003oR82_ileinner']/a[1]")
    bu.click_elmt("//input[contains(@name,'newNote')]")
    bu.send_keys_elmt_xpath("//input[@id='Title']", "Title of New Note #" + str(u.get_random_number(4)))
    bu.send_keys_elmt_xpath("//textarea[@id='Body']", " This is the note added by automation script. #" +
                            str(u.get_random_number(4)))
    bu.click_elmt("//td[@id='bottomButtonRow']//input[contains(@name,'save')]")
    check_error('Error', 'note')

def check_error(err_msg, object):
    element = bu.find_element_by_xpath("//div[@id='ep']")
    if 'ce' in object:
        lg.assert_and_log(err_msg in element.text,
            "\t \t FAIL : Should NOT be able to create Collaboration Engine.",\
            "\t \t PASS : Could not create Collaboration Engine, and got expected error message - " + \
                      err_msg)
    elif 'note' in object:
        lg.assert_and_log(err_msg.casefold() not in element.text,
                "\t \t FAIL : Could NOT add note." + err_msg, "\t \t PASS : Added a new note successfully.")


from Helpers import salesforce_helper as sfh
from Helpers import log_helper as lg
from Helpers import utilities as u
import entity_create_config as c
import sf_connection_config as sf_cc
import datetime as d
import random as r
import api_config as aconfig
import time as t

def begin_contact_record():

    lg.log_results("\nINFO: \t Begin creating CONTACT records ")
    if not aconfig.all_account_ids:
        lg.log_results("INFO: \t Could not create Contact Record: Accounts Id(s) not found")
        return

    # get accountId with Hospital record type created by loggedin user
    acct_hospital_recd_type_id = sfh.get_record_type_id(aconfig.account_rectype_for_contact, 'Account')
    user_id = sfh.get_user_id(sf_cc.uname)
    account_id = sfh.get_hospital_type_account_id(acct_hospital_recd_type_id, user_id)

    for i, each_contact_rec_type in aconfig.contact_rec_type.items():
        lg.log_results("\nINFO: \t Begin creating " + str(each_contact_rec_type['num_contact_rec'])
                       + " CONTACT records of type " + str(each_contact_rec_type['con_rec_type']))
        contact_record_type_id = sfh.get_record_type_id(each_contact_rec_type['con_rec_type'], 'Contact')
        record_create_count = each_contact_rec_type['num_contact_rec']
        contact_rec_suffix = each_contact_rec_type['con_rec_type']
        contact_rec_suffix = contact_rec_suffix[0:2]
        kount_suffix = 0
        while record_create_count > 0:
            if contact_record_type_id:
                name_suffix = contact_rec_suffix + "_" + str(kount_suffix)
                contact_id, contact_name = create_new_contact_rec(name_suffix, contact_record_type_id,
                                                                                  account_id)
                aconfig.all_contact_ids.append(contact_id)
                lg.assert_and_log(contact_id, "FAIL: \t Could not create contact record",
                                  "PASS : \t Successfully created Contact record: Id " + contact_id + " Name:"+
                                  contact_name)
                kount_suffix += 1
            record_create_count -= 1

        # lg.log_results()
        i += 1

    lg.log_results("\n\t aconfig.all_contact_ids = " + str(aconfig.all_contact_ids))
    return

def create_new_contact_rec(con_name_suffix, contact_record_type_id, account_id):

    name_str = 'AA'
    ran_str = u.get_random_chars(2)
    suffix_str = 'MD, PhD'
    first_contact_date = str(c.now)
    contact_fname = name_str + con_name_suffix + ran_str
    contact_lname = 'AutoUser' + ran_str
    email = contact_fname + '@' + contact_lname + 'test.com'
    phone = u.get_random_number(10)
    referral_value_str = r.choice(['3rd Party', 'Acquired/On Staff at Hospital', 'Soundphysicians.com', 'Marketing Team Campaign'])
    state_of_interest_value = 'WA'
    contact_name = contact_fname + ' ' + contact_lname
    f_contact_date = d.datetime.today().strftime('%Y-%m-%d')
    speciality = r.choice(['Gastroenterology', 'Cardiology', 'Anesthesiology', 'Pediatrics', 'General Surgery', 'Emergency Medicine'])
    mailing_street = '123 Main Auto Street'
    mailing_city = 'AutoCity'
    mailing_p_code = '98000'
    mailing_state = 'WA'
    mailing_country = 'USA'
    today_is = d.datetime.today()
    birthdate = today_is.replace(year=1990)
    birthdate_str = birthdate.strftime('%Y-%m-%d')
    employment_type_string = 'FTE (W2)'
    npi = u.get_random_number(10)

    contact_record = sf_cc.sf_connect.contact.create({'FirstName': contact_fname,
                                    'LastName': contact_lname,
                                    'birthdate': birthdate_str,
                                    'RecordTypeId': contact_record_type_id,
                                    'Suffix__c' : suffix_str,
                                    'AccountId' : account_id,
                                    'Referral__c' : referral_value_str,
                                    'State_of_Interest__c' : state_of_interest_value,
                                    'First_Contact__c' : f_contact_date,
                                    'email' : email,
                                    'Specialty__c': speciality,
                                    'MailingStreet': mailing_street,
                                    'MailingCity': mailing_city,
                                    'MailingPostalCode' : mailing_p_code,
                                    'MailingState' : mailing_state,
                                    'MailingCountry' : mailing_country,
                                    'Employment_Type__c' : employment_type_string,
                                    'NPI_Not_Available__c': 'Yes',
                                    'NPI__c': npi,
                                    'X1Med_Licn_State__c': 'CA',
                                    'Med_Lic1__c': 'CA1234'
                                    })

    contact_id =  sfh.check_for_new_record(contact_record, 'contact')

    return contact_id, contact_name


# Added to get the Clinical Ladder Tier value via API, Sprint 113, US 110455
def verify_clt_value_frm_cont(cont_id,exp_value):
    t.sleep(10)
    clt_val = sfh.verify_clt_value_cont(cont_id)
    lg.assert_and_log(clt_val == exp_value, "\t \t FAIL: Expected Clinical ladder tier to be " + str(exp_value) +
                      " but is " + str(clt_val), "\t \t PASS: Clinical Ladder Tier value on Contact is found to be as expected " + str(clt_val))

    return
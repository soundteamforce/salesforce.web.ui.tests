import sys
import time as t
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import browser_utilities as bu
import ui_config as cwuc
import sf_connection_config as sf_cc
from Resources import element_locator as e
from Helpers import login_as_user_with_search as lauw_search
from Pages import provider_agreement as pa_pg
# from Helpers import login_as_user_helper as lauh
from Pages import pa_wizard as w
# from Pages import provider_agreement as pa


def search_for_recent_candidate(cand_name):

    lg.log_results("\t \t Searching for the Test candidate " + cand_name)
    cand_search = bu.is_visible_xpath(e.home_search, cwuc.web_longer_wait)
    cand_search.send_keys(cand_name)
    t.sleep(5)
    bu.click_elmt(e.home_search_button)

# +C-89816')]"
    if bu.find_element_by_xpath(e.home_candidate_search_link):
        page_cand_name = bu.get_text(e.home_candidate_search_link)
        if cand_name in page_cand_name:
            bu.click_elmt(e.home_candidate_search_link)
    elif bu.is_invisible_elmt(e.home_candidate_search_str + cand_name + "')]" ):
        t.sleep(5)
        # bu.send_keys_elmt_xpath(e.home_search, cand_name)
        # bu.click_elmt(e.home_search_button)
        # bu.send_keys_elmt_xpath(e.home_search, cand_name)
        home_candidate_search_link2 = e.home_candidate_search_str + cand_name + "')]"
        bu.click_elmt("//a[@id='searchAllSummaryView']")
        if bu.is_invisible_elmt(home_candidate_search_link2):
            lg.log_results("\t \t Could not find test candidate: " + cand_name)
            sys.exit(1)
        else:
            bu.click_elmt(home_candidate_search_link2)



def search_on_top(search_text, search_category):
    element_1_str = ""
    t.sleep(5)
    top_search = bu.find_element_by_xpath(e.home_search)
    top_search.send_keys(search_text)
    # lg.log_results("\t \t Searching for " + search_category + " : " + search_text, "LIGHTBLACK_EX")
    bu.click_elmt(e.home_search_button)
    # If the table has some rows, then data exists
    if 'user' in search_category:
        element_1_str = e.home_searched_user_link_str + search_text + "')]"
        element_1 = bu.is_visible_xpath(element_1_str, cwuc.web_longer_wait)
        # bu.execute_tab(element_1)
    elif 'Provider Agreement' in search_category:
        element_1_str = e.home_searched_pa_link_str + search_text + "')]"
        element_1 = bu.is_visible_xpath(element_1_str, cwuc.web_longer_wait)
    else:
        element_1 = bu.is_visible_xpath(e.home_searched_pa_link, cwuc.web_longer_wait)

    lg.assert_and_log(element_1 is not None, "\t \t FAIL : The " + search_category + " : "
                      + search_text + " does not exist. ")

    return(element_1_str)


def wizard_setup(login_as_uname, pa_record_type, sl_check_val = None, c_clinician_type = None):
    candidate_set = False
    lauw_search.login_as_with_search(login_as_uname, 'user')
    cwuc.created_by_admin_id = sh.get_user_id(sf_cc.uname)
    bu.sleep_for(1)
    cwuc.contact_id, cwuc.contact_name = sh.get_recent_contact(cwuc.created_by_admin_id)
    lg.log_results("\t\t The contact Id is " + cwuc.contact_id + " Contact Name is " + cwuc.contact_name)
    bu.sleep_for(1)
    cwuc.candidate_id_1, cwuc.candidate_name_1, cwuc.candidate_id_2, cwuc.candidate_name_2 = sh.get_recent_candidate(
        cwuc.contact_id)
    bu.sleep_for(5)
    candidate_set = select_req_candidate(sl_check_val, c_clinician_type)

    if candidate_set :
        search_for_recent_candidate(cwuc.candidate_name_1) #moved after candidates are set correctly if needed in above
        cwuc.new_pa_name, cwuc.new_pa_type, cwuc.new_pa_id = pa_pg.create_new_pa(pa_record_type, cwuc.delete_existing_pa)
        bu.sleep_for(5)
        return cwuc.new_pa_name, cwuc.new_pa_type, cwuc.new_pa_id

def select_req_candidate(exp_service_ln, exp_clinician_type):
    #Check clinician type first, sl may depend on candidate.
    sl_n_clinician_ck = False
    if not exp_service_ln and not exp_clinician_type:
        return True
    elif not exp_service_ln and exp_clinician_type:
        is_cand =  check_candidate_clinician_type(exp_clinician_type)
        return is_cand
    elif exp_service_ln and not exp_clinician_type:
        is_service_line  = check_ssl_service_line_val(exp_service_ln, sl_n_clinician_ck)
        return is_service_line
    else:
        sl_n_clinician_ck = True
        is_cand =  check_candidate_clinician_type(exp_clinician_type)
        if is_cand:
            is_service_line = check_ssl_service_line_val(exp_service_ln, sl_n_clinician_ck)
            return is_service_line
        else:
            return is_cand




def check_ssl_service_line_val(exp_service_line, cli_type_ck):
    candidate_changed = False
    lg.log_results("\t \t Checking Service Line." )
    cand_name = cwuc.candidate_name_1
    ret_pos_id, ret_pos_name = sh.get_position_from_candidate(cand_name)
    ret_service_line = sh.get_service_line_on_position(ret_pos_id)
    if ret_service_line in exp_service_line:
        lg.log_results("\t \t The service line of candidate " + cand_name + " is " + ret_service_line )
        return True
    else:
        if cli_type_ck:
            lg.log_results("\t \t The candidate is not in expected " +
                           str(exp_service_line) + " service line(s).", 'LIGHTRED_EX')
            return False
        else:
            cand_name = cwuc.candidate_name_2
            cand_id = cwuc.candidate_id_2
            ret_pos_id, ret_pos_name = sh.get_position_from_candidate(cand_name)
            ret_service_line = sh.get_service_line_on_position(ret_pos_id)
            if ret_service_line in exp_service_line:
                lg.log_results("\t \t The service line of candidate " + cand_name + " is " + ret_service_line )
                #switch candidates 1 and 2 so as to create PA on correct cand.
                candidate_changed = True
                cwuc.candidate_name_2 = cwuc.candidate_name_1
                cwuc.candidate_name_1 = cand_name
                cwuc.candidate_id_2 = cwuc.candidate_id_1
                cwuc.candidate_id_1 = cand_id
                return True


def check_candidate_clinician_type(exp_clinician_type):
    lg.log_results("\t \t Getting candidate of clinician type " + exp_clinician_type)
    cand_name = cwuc.candidate_name_1
    ret_clinician_type = sh.get_cand_clinican_type(cand_name)
    if exp_clinician_type == ret_clinician_type:
        lg.log_results("\t \t The candidate " + cand_name + " is of " + ret_clinician_type + " clinican type.")
        return True
    elif cwuc.candidate_name_2:
        cand_name = cwuc.candidate_name_2
        cand_id = cwuc.candidate_id_2
        ret_clinician_type = sh.get_cand_clinican_type(cand_name)
        if exp_clinician_type == ret_clinician_type:
            lg.log_results("\t \t The candidate " + cand_name + " is of " + ret_clinician_type + " clinican type.")
            # change the primary candidates while creating the agreement term.
            cwuc.candidate_name_2 = cwuc.candidate_name_1
            cwuc.candidate_name_1 = cand_name
            cwuc.candidate_id_2 = cwuc.candidate_id_1
            cwuc.candidate_id_1 = cand_id
            return True
        else:
            lg.log_results("\t \t The candidate of " +
                           exp_clinician_type + " clinican type could not be found. ", 'LIGHTRED_EX')
            return False
    else:
        lg.log_results("\t \t The candidate of " +
                       exp_clinician_type + " clinican type could not be found. ", 'LIGHTRED_EX')
        return False


from Helpers import salesforce_helper as sfh
from Helpers import log_helper as lg
import sf_connection_config as sf_cc
from Helpers import ignore_login_data as ild
from _datetime import datetime as d, timedelta as td
import random as r
import ui_config as uic
from Helpers import api_helper as ah
import api_config as aconfig

def begin_hub_mnged_candidate_record():

    hc_name = ''
    lg.log_results("\nINFO: \t Begin creating HUB Managed Candidate records.")
    if not (aconfig.all_contact_ids and aconfig.all_hmp_ids):
        lg.log_results("INFO: \t Could not create HUB Managed Candidate Record: Contacts or Hub Managed Positions not "
                       "found")
        return

    # for all contacts and HMP
    for each_contact in aconfig.all_contact_ids:
        for each_hmp_ids in aconfig.all_hmp_ids:
            create_hub_candidate(each_contact, each_hmp_ids)

    return

def create_hub_candidate(contact_id, hmp_id):
    lg.log_results('INFO: \t Creating Hub Managed Candidate record with Hub Managed Position - ' + hmp_id + ' ,'
                                                                                                           'contact - '
                   + contact_id + " Hud id " + aconfig.hub_id)

    now = d.now()
    today_date_yyyymmdd = now.strftime('%Y-%m-%d')
    today_date_plus_30_yyyymmdd = (now + td(30)).strftime('%Y-%m-%d')
    today_date_plus_year_yyyymmdd = (now + td(365)).strftime('%Y-%m-%d')
    today_date_mdyy = now.strftime('%m/%d/%Y')
    today_date_yyyymmdd_to_sec = now.strftime('%Y-%m-%d 00:00:00.0000000')
    today_date_plus_30_yyyymmdd = (now + td(30)).strftime('%Y-%m-%d 00:00:00.0000000')

    hc_name = ''
    rc_user_id = sfh.get_user_id_from_alias('AAUserCT')

    hm_hc_record = sf_cc.sf_connect.hub_candidate__c.create({
        'Contact__c': contact_id,           #need to change this logic
        'Hub_connection__c': aconfig.hub_id,
        'Hub_Managed_Position__c' : hmp_id,
        'Active__c': True,
        'Additional_Certifications__c':	'Additional Certifications ' + aconfig.hub_name,
        'availability__C':	'Availibility in text ' + aconfig.hub_name,
        'Background_Notes__c': 'Background Notes are added ' + aconfig.hub_name,
        'Background__c' : 'Not Yet Discussed ' + aconfig.hub_name,
        'Candidate_status__c': 'Interview',
        'Colleague_Status__c': 'Active',
        'Compass_Participant__c': False,
        'Clinician_Role__c': 'Standard',
        # 'Clinician_Organization__c': 'Sound',
        # 'Labor_Commitment_Type__c': 'Full Time',
        # 'Credit__c': 'Yes',
        # 'Lookup_Recruitment_Credit__c': rc_user_id,
        'Credit__c': 'No',
        'Lookup_Recruitment_Credit__c': '',
        'Date_Screened__c': today_date_yyyymmdd,
        'Employment_Type__c': 'W2',
        'FCVS__c': False,
        'HHS_Office_of_Inspector_General_OIG__c': False,
        'Immigration_Type__c' : 'H1B',
        'J1_Extenuating_Circumstances__c': "",
        'Labor_Commitment_Type__c': "",
        'Medical_Corps_Program_Year__c': "",
        'Medical_Corps__c': False,
        'Previous_position_type__C': 'Practicing',
        # 'Missed_Reason_Detail__c':	'Missed Reason Detail Text',
        # 'Missed_Reason__c':	    'Provider',
        'Needs_Immigration__c':	'Yes',
        'Outcome_Details__c': 'Outcome Details not added ' + aconfig.hub_name,
        'Presentation_Comments__c':	'Presentation Comments text',
        'Presentation_Results__c':	'Presentation Results text',
        'Previous_Position_Type__c':	'Practicing',
        # 'Priv_Con_Confirmed__c':	False,
        'Provider_Highlights__c': 'Provider Highlights Provider Highlights ',
        'Provider_Monthly_Capacity_shifts__c': '1',
        'Resident_Start_Prior_to_Boards__c': '',
        'Result__c': 'Accepted',
        # 'Sound_Employing_Legal_Entity__c': '',
        'Site_Commitment__c': 'Primary',
        'System_for_Award_Management_SAM__c': False,
        'Candidate_Termination_Date__c':    today_date_plus_year_yyyymmdd,
        'Compass_Program_Year__c':	'2021',
        'Contract_Sent_Date__c':	today_date_yyyymmdd,
        #'Contract_Start_Date__c':	today_date_yyyymmdd,
        'OIG_Verification_Date__c':	today_date_yyyymmdd,
        'Original_CIF_SAS_Issue_Date__c':	today_date_yyyymmdd,
        'Presented_Date__c':	today_date_yyyymmdd,
        'Primary_Interview__c':	today_date_yyyymmdd,
        'Revised_CIF_SAS_Issue_Date__c': today_date_yyyymmdd,
        'SAM_Verification_Date__c': today_date_yyyymmdd,
        'Secondary_Interview__c': today_date_yyyymmdd,
        'UltiPro_ID__c': ''
    })
    # hc_id = str(hm_hc_record['id'])
    #
    # if hc_id:
    #     hc_dict = sf_cc.sf_connect.hub_candidate__c.get(hc_id)
    #     hc_name = str(hc_dict['Name'])
    #     aconfig.all_hc_ids.append(hc_id)
    #
    # lg.assert_and_log(hc_id, 'FAIL: \t Could not create Hub Managed Candidate record',
    #                   'PASS : \t Successfully created Hub Candidate record with Id: ' + hc_id + " , Name: " + hc_name)
    if isinstance(hm_hc_record,dict):
        if 'id' in hm_hc_record.keys():
            hc_id = str(hm_hc_record['id'])
            hc_dict = sf_cc.sf_connect.hub_candidate__c.get(hc_id)
            hc_name = str(hc_dict['Name'])
            aconfig.all_hc_ids.append(hc_id)
            lg.log_results("\t \t PASS : Successfully created Hub Candidate record with Id: " + hc_id + " , Name: " + hc_name)
    elif isinstance(hm_hc_record, list):
        hc_rec_dict = hm_hc_record[0]
        if 'errorCode' in  hc_rec_dict:
            lg.log_results("\t \t FAIL - Could not create Hub Managed Candidate record " + \
                           str(hc_rec_dict), 'LIGHTRED_EX')

    return

import sf_connection_config as sf_cc
import ui_config as uic
from Helpers import log_helper as lg
from Resources import test_variables as tv
import comp_wizard_ui_config as cwuc


def check_for_new_record(entity_record, tbl_name):
    entity_id = ''
    entity_name = ''

    if isinstance(entity_record,dict):
        if 'id' in entity_record.keys():
            entity_id = str(entity_record['id'])
            # entity_dict = sf_cc.sf_connect.tbl_name.get(entity_id)
            # entity_name = str(entity_dict['Name'])
    elif isinstance(entity_record, list):
        entity_dict = entity_record[0]
        if 'errorCode' in  entity_dict:
            lg.log_results("\t \t FAIL - Could not create record in " + tbl_name + " "+ \
                           str(entity_dict), 'LIGHTRED_EX')

    return entity_id


def get_record_type_id(name_val, sobject_val):
    record_type_id = ""
    record_type_query = "SELECT Id FROM RecordType as rt where rt.name = '" + name_val + "' and rt.SobjectType = '" + sobject_val + "' "

    result_name = sf_cc.sf_connect.query(record_type_query)
    records = result_name['records']
    if records:
        record_type_dict = records[0]
        record_type_id = str(record_type_dict['Id'])

    return record_type_id


def get_ce_recordtype_name(id_value, sobject_value):
    record_type_name = ""
    record_type_query = "SELECT Name FROM RecordType as rt where rt.id = '" + id_value + "' and rt.SobjectType = '" + \
                        sobject_value + "' "
    result_name = sf_cc.sf_connect.query(record_type_query)
    records = result_name['records']
    if records:
        record_type_dict = records[0]
        record_type_name = str(record_type_dict['Name'])

    return record_type_name


def get_hospital_type_account_id(acct_hospital_recd_type_id, created_by_id):
    # get accountId created by userid
    account_id = ''
    account_query = "SELECT Id FROM ACCOUNT WHERE recordTypeId = '" + acct_hospital_recd_type_id + "' and CreatedById = '" + created_by_id + "' ORDER BY CreatedDate DESC LIMIT 1"
    result_name = sf_cc.sf_connect.query(account_query)
    account_rec = result_name['records']
    if account_rec:
        account_id_dict = account_rec[0]
        account_id = str(account_id_dict['Id'])

    return account_id


def get_candidate_from_id(id_value):
    fullname_type = ''
    cand_query = "SELECT Full_Name_and_Type__c  from Candidate__c where Id  = '" + id_value + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_record = cand_result['records']
    if cand_record:
        cand_dict = cand_record[0]
        fullname_type = str(cand_dict['Full_Name_and_Type__c'])

    return fullname_type

def get_candidate_id_from_name(name_value):
    cand_id = ''
    cand_query = "SELECT Id from Candidate__c where Name  = '" + name_value + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_record = cand_result['records']
    if cand_record:
        cand_dict = cand_record[0]
        cand_id = str(cand_dict['Id'])

    return cand_id

def update_candidate_status(cand_ids, col_name, col_value):
    i = 0
    for cand_id in cand_ids:
        ret_code = sf_cc.sf_connect.Candidate__c.update(cand_id, {col_name: col_value})
        if ret_code >= 400:
            lg.log_results("FAIL: Could not update Candidate Status to " + str(col_value))
        else:
            lg.log_results("PASS: Candidate Status updated successfully to " + str(col_value))
    i += 1
    return


def update_cand_training_path(cand_id, col_value, training_length):
    if col_value == 'No Training Required':
        ret_code = sf_cc.sf_connect.Candidate__c.update(cand_id, {'APP_Training_Path__c': col_value})
        if ret_code >= 400:
            lg.log_results("\t \t FAIL: Could not update Candidate Training Path to " + str(col_value))
        else:
            lg.log_results("\t \t PASS: Candidate Training Path updated successfully to " + str(col_value))
    else:
        ret_code = sf_cc.sf_connect.Candidate__c.update(cand_id, {'APP_Training_Path__c': col_value, 'Program_Length_Weeks__c': training_length})
        if ret_code >= 400:
            lg.log_results("\t \t FAIL: Could not update Candidate Training Path to " + str(col_value))
        else:
            lg.log_results("\t \t PASS: Candidate Training Path updated successfully to " + str(col_value))
    return


def get_cand_clinican_type(c_name):
    cand_cl_type = None
    cand_query = "SELECT Clinician_Type__c from Candidate__c where Name  = '" + c_name + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_record = cand_result['records']
    if cand_record:
        cand_dict = cand_record[0]
        cand_cl_type  = str(cand_dict['Clinician_Type__c'])

    return cand_cl_type

def get_cand_details(c_name):
    cand_query = "SELECT Candidate_Status__c, Candidate_Outcome__c, Contract_Start_Date__c, " \
                 "Colleague_Status__c, Active__c from Candidate__c where Name = '" + c_name + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_record = cand_result['records']
    if cand_record:
        cand_dict = cand_record[0]
        cand_status = str(cand_dict['Candidate_Status__c'])
        cand_outcome = cand_dict['Candidate_Outcome__c']
        cand_ctrc_str_date = str(cand_dict['Contract_Start_Date__c'])
        cand_col_status = str(cand_dict['Colleague_Status__c'])
        cand_active = str(cand_dict['Active__c'])

    return cand_status, cand_outcome, cand_ctrc_str_date, cand_col_status, cand_active


def get_ini_priv_date_lat_exp_date_candidate(c_id):
    ini_priv_date = ""
    lat_exp_date = ""
    cand_id = c_id[0]
    cand_query = "SELECT Id,Initial_Privilege_Date__c,Latest_Expiration_Date_PrivFile__c,Name FROM Candidate__c where " \
                 "id = '" + cand_id + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_record = cand_result['records']
    if cand_record:
        cand_dict = cand_record[0]
        ini_priv_date = str(cand_dict['Initial_Privilege_Date__c'])
        lat_exp_date = str(cand_dict['Latest_Expiration_Date_PrivFile__c'])

    return ini_priv_date, lat_exp_date


def get_priv_file_status(pf_id):
    pf_status = ""
    pf_query = "SELECT Status__c FROM Privileging_Files__c WHERE Id = '" + pf_id + "'"
    pf_result = sf_cc.sf_connect.query(pf_query)
    pf_record = pf_result['records']
    if pf_record:
        pf_dict = pf_record[0]
        pf_status = str(pf_dict['Status__c'])

    return pf_status


def get_active_col_status(c_name):
    cand_query = "SELECT Colleague_Status__c, Active__c from Candidate__c where Name = '" + c_name + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_record = cand_result['records']
    if cand_record:
        cand_dict = cand_record[0]
        cand_col_status = str(cand_dict['Colleague_Status__c'])
        cand_active = str(cand_dict['Active__c'])
    lg.log_results('Candidate Colleague Status is ' + cand_col_status)
    lg.log_results('Candidate Active checkbox is set to ' + cand_active)
    #return cand_col_status, cand_active

def get_user_id(username):
    user_id = ""
    user_query = "SELECT Id FROM USER WHERE Username = '" + username + "'"
    user_result_name = sf_cc.sf_connect.query(user_query)
    user_rec = user_result_name['records']
    if user_rec:
        user_rec_dict = user_rec[0]
        user_id = str(user_rec_dict['Id'])

    return user_id

def get_user_id_name(name):
    user_id = ""
    user_query = "SELECT Id FROM USER WHERE Name = '" + name + "'"
    user_result_name = sf_cc.sf_connect.query(user_query)
    user_rec = user_result_name['records']
    if user_rec:
        user_rec_dict = user_rec[0]
        user_id = str(user_rec_dict['Id'])

    return user_id

def get_user_id_from_alias(user_alias):
    user_query = "SELECT Id FROM USER WHERE alias = '" + user_alias + "'"
    user_result_name = sf_cc.sf_connect.query(user_query)
    user_rec = user_result_name['records']
    if user_rec:
        user_rec_dict = user_rec[0]
        user_id = str(user_rec_dict['Id'])

    return user_id


def get_user_id_per_profile(profile_name, created_by_id=''):
    user_id = ''
    user_name = ''
    if created_by_id == '' :
        user_pquery = "SELECT Id, Name FROM USER WHERE ProfileName__c = '" + profile_name + "' ORDER BY CreatedDate desc LIMIT 1"
    else:
        user_pquery = "SELECT Id, Name FROM USER WHERE ProfileName__c = '" + profile_name + "' and CreatedById = '" + \
                  created_by_id + "'  ORDER BY CreatedDate desc LIMIT 1"

    user_presult = sf_cc.sf_connect.query(user_pquery)
    user_prec = user_presult['records']
    if user_prec:
        user_pdict = user_prec[0]
        user_id = str(user_pdict['Id'])
        user_name = str(user_pdict['Name'])

    return user_id, user_name


def get_position_id(user_id, ssl_ids, account_ids, limit_pos_records):
    position_ids = []
    account_ids_str = str(account_ids).replace('[', '(')
    account_ids_str = account_ids_str.replace(']', ')')
    position_query = ''
    ssl_ids_str = str(ssl_ids).replace('[', '(')
    ssl_ids_str = ssl_ids_str.replace(']', ')')
    # lg.log_results("account Id is " + str(account_ids))
    if limit_pos_records:
        position_query = "SELECT Id FROM position__C WHERE Service_Line_Record__c IN " + ssl_ids_str + " and " \
                                                                                                      "Partner_Hospital__c IN " + account_ids_str + " and CreatedById ='" + user_id + "' ORDER BY CreatedDate desc LIMIT 2"
    else:
        position_query = "SELECT Id FROM position__C WHERE Service_Line_Record__c IN " + ssl_ids_str + " and " \
                                                                                                      "Partner_Hospital__c IN " + account_ids_str + " and CreatedById ='" + user_id + "' ORDER BY CreatedDate desc"


    pos_result_name = sf_cc.sf_connect.query(position_query)
    pos_records = pos_result_name['records']
    if pos_records:
        # position_ids = []
        y = 0
        number_of_pos_records = len(pos_records)
        while y < number_of_pos_records:
            position_ids.append(pos_records[y]['Id'])
            y += 1
        # lg.log_results(str(position_ids))

    return position_ids


def get_position_name(user_id):
    result_name = sf_cc.sf_connect.query("SELECT Name FROM position__c WHERE CreatedById ='" + user_id +"' ORDER BY CreatedDate DESC LIMIT 1" )
    pos_record = result_name['records']
    if pos_record:
        position_dict = pos_record[0]
        ret_pos_name = position_dict['Name']

    return ret_pos_name


def get_contact_id(contact_nm):
    ret_contact_id = ''
    result_name = sf_cc.sf_connect.query("SELECT Id FROM Contact WHERE Name = '" + contact_nm + "'" +
                                         " and createdById = '" + uic.created_by_admin_id + "'")
    contact_id_rec = result_name['records']
    if contact_id_rec:
        cont_id_dict = contact_id_rec[0]
        ret_contact_id = cont_id_dict['Id']

    return ret_contact_id


def get_pos_id(pos_name):
    ret_pos_id = ''
    result_name = sf_cc.sf_connect.query("SELECT Id FROM Position__c WHERE Name = '" + pos_name + "'" +
                " and createdById = '" + uic.created_by_admin_id + "'")
    pos_id_rec = result_name['records']
    if pos_id_rec:
        pos_id_dict = pos_id_rec[0]
        ret_pos_id = pos_id_dict['Id']

    return ret_pos_id


def get_provider_agreement_id(pa_name):
    ret_pa_id = ''
    result_name = sf_cc.sf_connect.query("SELECT Id FROM Provider_Agreement__c WHERE Name = '" + pa_name + "'" +
                                         " and createdById = '" + uic.created_by_admin_id + "'")
    pa_id_rec = result_name['records']
    if pa_id_rec:
        pa_id_dict = pa_id_rec[0]
        ret_pa_id = pa_id_dict['Id']

    return ret_pa_id

def get_collab_engine_details():

    ce_dict_v = {}
    ce_query = "SELECT Id, APP_Candidate__c, QA_Employment_Requirement__c, CPA_Employment_Requirement__c, " \
             "Employment_Requirement__c ," \
            "RecordTypeId, Status__c, acquisition__c FROM Collaboration_Engine__c where Name  = '" + uic.ce_name \
               + "' and APP_Candidate__c = '" + uic.candidate_id_1 + "'"
    # + "' and Position__c = '" + uic.position_name + "' and acquisition__c = TRUE"

    ce_resultname = sf_cc.sf_connect.query(ce_query)
    ce_records = ce_resultname['records']
    if ce_records:
        ce_dict = ce_records[0]
        ce_dict_v['ce_id'] = str(ce_dict['Id'])
        ce_dict_v['ce_app_candidate_id'] = str(ce_dict['APP_Candidate__c'])
        ce_dict_v['ce_qa_er_id'] = str(ce_dict['QA_Employment_Requirement__c'])
        ce_dict_v['ce_cpa_er_id'] = str(ce_dict['CPA_Employment_Requirement__c'])
        ce_dict_v['ce_ratio_er_id'] = str(ce_dict['Employment_Requirement__c'])
        ce_dict_v['ce_recordtype_id'] = str(ce_dict['RecordTypeId'])
        ce_dict_v['ce_status'] = str(ce_dict['Status__c'])
        # ce_dict_v['acquisition__c'] = ce_dict['acquisition__c']

    return ce_dict_v


def get_er_record(er_id_value, record_description):
    er_query = "SELECT NAME FROM Employment_Requirement__c where Id  = '" + er_id_value \
               + "' and description__c LIKE '%" + record_description + "%'"
    er_resultname = sf_cc.sf_connect.query(er_query)
    er_records = er_resultname['records']
    if er_records:
        er_dict = er_records[0]
        er_name = str(er_dict['Name'])

    return er_name

def get_all_er_records():
    i = 0
    er_query = "SELECT Name FROM Employment_Requirement__c where Position__c  = '" + \
               uic.position_id + "'"
    er_resultname = sf_cc.sf_connect.query(er_query)
    er_records = er_resultname['records']
    len_er = len(er_records)
    er_name = []
    while i < len_er:
        er_dict = er_records[i]
        er_name.append(str(er_dict['Name']))
        i += 1

    return er_name

def get_docusign_status():
    sdoc_query = "SELECT Name, DocuSign_Status__c FROM Supporting_Document__c WHERE collaboration_engine__c = '" \
               + uic.ce_id + "'"
    sdoc_result = sf_cc.sf_connect.query(sdoc_query)
    sdoc_count = sdoc_result['totalSize']
    sdoc_records = sdoc_result['records']
    sdoc_dict = {}
    i = 0
    if sdoc_records:
        for item in sdoc_records:
            name = item['Name']
            docusign_status = item['DocuSign_Status__c']
            sdoc_dict[i] = {'name': name, 'ds_status': docusign_status}
            i +=1

    return sdoc_dict

def get_supporting_doc(spp_doc_name):

    sd_query = "SELECT Issue_Date__c, Expiration_Date__c, Next_Supporting_Document_Creates_On__c FROM " \
               "Supporting_Document__c WHERE Name  = '" + spp_doc_name + "' and collaboration_engine__C = '" + \
               uic.ce_id + "' LIMIT 1"

    sd_result = sf_cc.sf_connect.query(sd_query)
    sd_records = sd_result['records']
    sd_issue_date = ''
    sd_expiration_date = ''
    sd_next_sd_create_date = ''
    if sd_records:
        sd_dict = sd_records[0]
        sd_issue_date = str(sd_dict['Issue_Date__c'])
        sd_expiration_date = str(sd_dict['Expiration_Date__c'])
        sd_next_sd_create_date = str(sd_dict['Next_Supporting_Document_Creates_On__c'])

    return sd_issue_date, sd_expiration_date, sd_next_sd_create_date

def is_user_active_or_frozen(name):
    usr_active = 0
    usr_frozen = 0
    usr_query = "SELECT Id,Frozen_User__c,IsActive,Name FROM User WHERE Name = '" + name + "'"
    usr_result_name = sf_cc.sf_connect.query(usr_query)
    usr_records = usr_result_name['records']
    if usr_records:
        usr_dict = usr_records[0]
        usr_active = usr_dict['IsActive']
        usr_frozen = usr_dict['Frozen_User__c']
        # lg.log_results("\t\t User active = " + usr_active)
        # lg.log_results("\t\t User frozen = " + usr_frozen)
    return usr_active, usr_frozen


def is_agreement_term_deleted(at_name):
    # at_query = "SELECT Id FROM Agreement_Term__c WHERE Name LIKE '" + at_name + "%'"
    at_query = "SELECT Id FROM Agreement_Term__c WHERE Name LIKE '" + at_name + "%' and Candidate__c  = '" + \
               uic.candidate_id_1 + "' "
    at_result_name = sf_cc.sf_connect.query(at_query)
    at_records = at_result_name['records']
    if not at_records:
        return True
    else:
        return False


def get_existing_pa(candidate_id):
    exsting_pa_id = None
    exsting_pa_name = None

    existiting_pa_query = "SELECT Id, Name FROM provider_Agreement__C WHERE Candidate__c  = '" + str(candidate_id) + "'"
    exsting_pa_resultname = sf_cc.sf_connect.query(existiting_pa_query)
    exsting_pa_records = exsting_pa_resultname['records']

    # Check if more than one records returned.

    if exsting_pa_records:
        exsting_pa_dict = exsting_pa_records[0]
        if exsting_pa_dict:
            exsting_pa_id = str(exsting_pa_dict['Id'])
            exsting_pa_name = str(exsting_pa_dict['Name'])

    return exsting_pa_id, exsting_pa_name

def get_existing_pa_from_contact(contact_id, recordtypeid, pos_name):
    exsting_pa_id = None
    exsting_pa_name = None
    existiting_pa_query = "SELECT Id, Name FROM provider_Agreement__C WHERE Contact__c  = '" + str(contact_id) + \
                          "' and RecordTypeId = '" + recordtypeid + "' and Position_Name__c = '" + str(pos_name) + "'"

    exsting_pa_resultname = sf_cc.sf_connect.query(existiting_pa_query)
    exsting_pa_records = exsting_pa_resultname['records']
    if exsting_pa_records:
        exsting_pa_dict = exsting_pa_records[0]
        if exsting_pa_dict:
            exsting_pa_id = str(exsting_pa_dict['Id'])
            exsting_pa_name = str(exsting_pa_dict['Name'])

    return exsting_pa_id, exsting_pa_name


def get_recent_contact(created_by_id):
    contact_id = ''
    contact_name = ''

    contact_query = "SELECT Id, Name FROM CONTACT WHERE  CreatedById = '" + str(created_by_id) + "' ORDER BY CreatedDate desc LIMIT 1"
    contact_result_name = sf_cc.sf_connect.query(contact_query)
    contact_records = contact_result_name['records']
    if contact_records:
        contact_dict = contact_records[0]
        contact_id = str(contact_dict['Id'])
        contact_name = str(contact_dict['Name'])

    return contact_id, contact_name

def get_contact_id_from_cand(can_id):
    contact_id = ''
    contact_name = ''

    contact_query = "SELECT Contact__c FROM candidate__c WHERE  Id = '" + str(can_id) + "'"
    contact_result_name = sf_cc.sf_connect.query(contact_query)
    contact_records = contact_result_name['records']
    if contact_records:
        contact_dict = contact_records[0]
        contact_id = str(contact_dict['Contact__c'])

    return contact_id


def get_recent_candidate(contact_id):
    candidate_id = ''
    candidate_name = ''
    candidate_id_2 = ''
    candidate_name_2 = ''
    # candidate_query = "SELECT Id, Name FROM Candidate__c WHERE Contact__c = '" + contact_id + "' ORDER BY CreatedDate DESC LIMIT 1"
    # Adding code for second candidate 8.12
    candidate_query = "SELECT Id, Name FROM Candidate__c WHERE Contact__c = '" + contact_id + "' and createdById = '" + uic.created_by_admin_id + "' ORDER BY CreatedDate DESC"
    cand_result_name = sf_cc.sf_connect.query(candidate_query)
    cand_records = cand_result_name['records']
    if cand_records:
        candidate_dict = cand_records[0]
        candidate_id = str(candidate_dict['Id'])
        candidate_name = str(candidate_dict['Name']).strip()
    # Adding code for second candidate 8.12
        if len(cand_records) == 2:
            candidate_dict_2 = cand_records[1]
            candidate_id_2 = str(candidate_dict_2['Id'])
            candidate_name_2 = str(candidate_dict_2['Name']).strip()
            # lg.log_results("\t \t The primary candidate name is '" + candidate_name + "' and previous candidate is '" +
            #                candidate_name_2 + "'")


    return candidate_id, candidate_name, candidate_id_2, candidate_name_2


def get_position_from_candidate(can_name):
    pos_id = ''
    pos_name = ''
    pos_query = "SELECT Position__c, Position_Name_Text__c FROM Candidate__C where Name ='" + can_name + "'"
    pos_result = sf_cc.sf_connect.query(pos_query)
    pos_records = pos_result['records']
    if pos_records:
        pos_dict = pos_records[0]
        pos_id = pos_dict['Position__c']
        pos_name = pos_dict['Position_Name_Text__c']
    return pos_id, pos_name

def get_service_line_on_position(pos_id):
    service_line_value = ''
    pos_query = "SELECT Service_Line__c FROM Position__C where Id ='" + pos_id + "'"
    pos_result = sf_cc.sf_connect.query(pos_query)
    pos_records = pos_result['records']
    if pos_records:
        pos_dict = pos_records[0]
        service_line_value = pos_dict['Service_Line__c']
    return service_line_value


def update_agreement_term(pa_id, at_col_name, at_col_value):
    ret_response = False
    at_query = "SELECT Id, Name FROM Agreement_Term__c where Starting_Agreement_CT__c ='" + pa_id + "'"
    at_result_name = sf_cc.sf_connect.query(at_query)
    at_records = at_result_name['records']
    if at_records:
        i = 0
        len_at = len(at_records)
        while i < len_at:
            at_id_dict = at_records[i]
            at_id = str(at_id_dict['Id'])
            at_name = at_id_dict['Name']
            ret_code = sf_cc.sf_connect.Agreement_Term__c.update(at_id, {at_col_name : at_col_value})
            if ret_code >= 400:
                lg.log_results("\t \t Could not delete the Agreement Term, error code - " + str(ret_code))
            else:
                lg.log_results(("\t \t Updated the Agreement Term " + at_name + " successfully"))
            i += 1


def get_terms_count(term_name, term_type):

    ret_count = 0
    term_query = "select count() from  Agreement_Term__c   at where Provider_Agreement__c = '" + term_name + "' and RecordTypeId in (SELECT Id  FROM  RECORDTYPE WHERE SobjectType = 'Agreement_Term__c' and Description = '" + term_type + "' )"
    # lg.log_results("query is " + term_query)
    term_count_result = sf_cc.sf_connect.query(term_query)
    term_count = term_count_result['totalSize']
    if term_count:
        ret_count = term_count

    return ret_count

def get_at_ids_names(pa_name, is_cancel, addnl_filter = ''):
    i = 0
    at_name_dict = []
    at_id_dict = []
    at_short_code_dict = []
    if 'cancel' in is_cancel:
        at_query = "SELECT Id, Name FROM Agreement_Term__c  WHERE Provider_Agreement__c = '" + pa_name + "' and " \
                   " ( candidate__C IN ('" + uic.candidate_id_1 + "' , " \
                    "'" + uic.candidate_id_2 + "') OR Candidate_2__C IN ('" + uic.candidate_id_1 + "' , " \
                    "'" + uic.candidate_id_2 + "'))"
    elif 'Reject' in is_cancel:
        at_query = "SELECT Id, Name FROM Agreement_Term__c  WHERE Provider_Agreement__c = '" + pa_name + "' and " \
                    "Review_Status__c = 'Declined' and cancellation__C = FALSE and ( candidate__C IN ('" + \
                   uic.candidate_id_1 + "' , '" + uic.candidate_id_2 + "') OR Candidate_2__C IN ('" + \
                   uic.candidate_id_1 + "' , '" + uic.candidate_id_2 + "'))"
    else:
        at_query = "SELECT Id, Name FROM Agreement_Term__c  WHERE Provider_Agreement__c = '" + pa_name + "' and " \
                    "Review_Status__c = 'Pending' and cancellation__C = FALSE and ( candidate__C IN ('" + \
                   uic.candidate_id_1 + "' , " \
                        "'" + uic.candidate_id_2 + "') OR Candidate_2__C IN ('" + uic.candidate_id_1 + "' , " \
                        "'" + uic.candidate_id_2 + "'))"

    if addnl_filter != '':
        at_query = at_query + " " + addnl_filter

    at_name_result = sf_cc.sf_connect.query(at_query)
    at_name_records = at_name_result['records']
    total_ats = at_name_result['totalSize']
    for item in at_name_records:
        name = item['Name']
        id = item['Id']
        at_name_dict.append(name)
        at_id_dict.append(id)

    return at_name_dict, at_id_dict


def get_term_details(pa_name):
    i = 0
    term_details_dict = {}
    # get_at_ids_names functn
    at_query = "SELECT Id, Name, Description__c, Cancellation__c, Status__c, Ending_Agreement_CT__c, Term_End_Date__c" \
               ", Short_Code__c FROM Agreement_Term__c  WHERE Provider_Agreement__c = '" + pa_name \
          + "' and cancellation__C = FALSE and ( candidate__C IN ('" + uic.candidate_id_1 + "' , " \
          "'" + uic.candidate_id_2 + "') OR Candidate_2__C IN ('" + uic.candidate_id_1 + "' , " \
          "'" + uic.candidate_id_2 + "'))"

    at_name_result = sf_cc.sf_connect.query(at_query)
    at_name_records = at_name_result['records']
    total_ats = at_name_result['totalSize']
    for item in at_name_records:
        term_details_dict[i] = {}
        term_details_dict[i]['name'] = item['Name']
        term_details_dict[i]['id'] = item['Id']
        term_details_dict[i]['description'] = item['Description__c']
        term_details_dict[i]['cancellation_status'] = item['Cancellation__c']
        term_details_dict[i]['status'] = item['Status__c']
        term_details_dict[i]['ending_pa'] = item['Ending_Agreement_CT__c']
        term_details_dict[i]['term_end_date'] = item['Term_End_Date__c']
        term_details_dict[i]['short_code'] = item['Short_Code__c']
        i += 1

    return term_details_dict


def get_rw_stat_and_stat(pa_name, each_term):
    review_status = ""
    status__c = ""
    at_query_stat = "SELECT Review_Status__c, Status__c FROM Agreement_Term__c  WHERE Provider_Agreement__c = '" + \
                 pa_name + "' and Name = '" + each_term + "' and ( candidate__C IN ('" + uic.candidate_id_1 + "' , " \
                            "'" + uic.candidate_id_2 + "') OR Candidate_2__C IN ('" + uic.candidate_id_1 + "' , " \
                            "'" + uic.candidate_id_2 + "'))"
    at_name_stat_result = sf_cc.sf_connect.query(at_query_stat)
    at_name_stat_records = at_name_stat_result['records']
    if at_name_stat_records:
        at_name_dict = at_name_stat_records[0]
        review_status = at_name_dict['Review_Status__c']
        status__c = at_name_dict['Status__c']

    return review_status, status__c

def get_term_threshold_interval(pa_name, term_name):
    at_interval = None
    at_query_stat = "SELECT Interval__c FROM Agreement_Term__c  WHERE Provider_Agreement__c = '" + \
                    pa_name + "' and Name = '" + term_name + "' and ( candidate__C IN ('" + uic.candidate_id_1 + "' , " \
                    "'" + uic.candidate_id_2 + "') OR Candidate_2__C IN ('" + uic.candidate_id_1 + "' , " \
                    "'" + uic.candidate_id_2 + "'))"
    at_name_stat_result = sf_cc.sf_connect.query(at_query_stat)
    at_name_stat_records = at_name_stat_result['records']
    if at_name_stat_records:
        at_name_dict = at_name_stat_records[0]
        at_interval = at_name_dict['Interval__c']

    return at_interval

def delete_al_ats():
    # if (uic.candidate_id_1 is '' and uic.candidate_id_2 is ''):
    #     return
    at_query = "SELECT Id from Agreement_term__C WHERE " \
            " (candidate__C IN ('" + uic.candidate_id_1 + "' , '" + uic.candidate_id_2 + "') OR Candidate_2__C IN " \
            "('" + uic.candidate_id_1 + "' , '" + uic.candidate_id_2 + "')) AND (Contact__c IN ('" + uic.contact_name\
               + "') OR Contact_2__c IN ('" + uic.contact_name + "'))"
    #lg.log_results("at_query is " + at_query)

    at_id_result = sf_cc.sf_connect.query(at_query)
    at_records = at_id_result['records']
    if at_records:
        i = 0
        len_at = len(at_records)
        while i < len_at:
            at_id_dict = at_records[i]
            at_id = str(at_id_dict['Id'])
            ret_code = sf_cc.sf_connect.Agreement_term__C.delete(at_id)
            if ret_code >= 400:
                lg.log_results("\t \t Could not delete the Agreement Term, error code - " + str(ret_code))
            i += 1


def delete_pa(delete_pa, delete_ats):
    # def delete_pa(with_at):
    if delete_ats == 1:
        lg.log_results('\t \t Checking if deletion of Provider Agreement & terms is needed. ')
        delete_al_ats()

    if uic.candidate_id_1 is None:
        return

    if delete_pa == 1:
        pa_id = ""
        pa_query = "SELECT Id FROM provider_Agreement__C WHERE candidate__C = '" + uic.candidate_id_1 + "'"

        pa_id_result = sf_cc.sf_connect.query(pa_query)
        pa_id_records = pa_id_result['records']
        if pa_id_records:
            i = 0
            len_pa_records = len(pa_id_records)
            while i < len_pa_records:
                pa_id_dict = pa_id_records[i]
                pa_id = str(pa_id_dict['Id'])
                ret_code = sf_cc.sf_connect.Provider_Agreement__c.delete(pa_id)
                if ret_code >= 400:
                    lg.log_results("\t \t Could not delete the Provider Agreement, error code - " + str(ret_code))
                i += 1

    return

def get_primary_approver(pa_name):
    app_id = ""
    app_user_name = ""
    app_id_q = "SELECT primary_compensation_approver__c FROM provider_Agreement__C WHERE NAME = '" + pa_name \
                   + "' LIMIT 1"
    app_id_result = sf_cc.sf_connect.query(app_id_q)
    app_id_records = app_id_result['records']
    if app_id_records:
        app_id_dict = app_id_records[0]
        app_id = app_id_dict['Primary_Compensation_Approver__c']
        if app_id:
            app_user_dict = sf_cc.sf_connect.user.get(app_id)
            app_user_name = app_user_dict['Name']

    return app_user_name

def get_approvers_ssl(ssl_name):
    apvr_dict = {}
    apvr_name_list = []
    usr_name = ""
    usr_is_active = 0
    usr_frozen = 0
    i = 0
    apvr_id_query = "SELECT Id,Name,Primary_Clinical_Approver__c,Primary_Compensation_Approver__c," \
                    "Secondary_Clinical_Approver__c,Secondary_Compensation_Approver__c,Primary_CRNA_Approver__c," \
                    "Secondary_CRNA_Approver__c FROM Sound_Service_Lines__c " \
                    "where Name = '" + ssl_name + "'"
    apvr_id_result = sf_cc.sf_connect.query(apvr_id_query)
    apvr_id_records = apvr_id_result['records']
    if apvr_id_records:
        apvr_id_dict = apvr_id_records[0]
        apvr_dict['Primary_Clinical_Approver__c'] = apvr_id_dict['Primary_Clinical_Approver__c']
        apvr_dict['Secondary_Clinical_Approver__c'] = apvr_id_dict['Secondary_Clinical_Approver__c']
        apvr_dict['Primary_Compensation_Approver__c'] = apvr_id_dict['Primary_Compensation_Approver__c']
        apvr_dict['Secondary_Compensation_Approver__c'] = apvr_id_dict['Secondary_Compensation_Approver__c']
        apvr_dict['Primary_CRNA_Approver__c'] = apvr_id_dict['Primary_CRNA_Approver__c']
        apvr_dict['Secondary_CRNA_Approver__c'] = apvr_id_dict['Secondary_CRNA_Approver__c']

        for i in apvr_dict.values():
            if i:
                usr_name, usr_is_active, usr_frozen = get_user_frm_id(i)
                if usr_is_active and not usr_frozen:
                    apvr_name_list.append(usr_name)

    return apvr_name_list, apvr_dict


def get_user_frm_id(usr_id):
    usr_query = "SELECT Frozen_User__c,Id,IsActive,Name FROM User where Id = '" + usr_id + "'"
    usr_result = sf_cc.sf_connect.query(usr_query)
    usr_records = usr_result['records']
    if usr_records:
        usr_dict = usr_records[0]
        usr_name = usr_dict['Name']
        usr_is_active = usr_dict['IsActive']
        usr_frozen = usr_dict['Frozen_User__c']

    return usr_name, usr_is_active, usr_frozen


def get_pa_id(pa_name):
    pa_id_query = "SELECT Id FROM provider_Agreement__C  where Name = '" + pa_name + "'"
    pa_id_result = sf_cc.sf_connect.query(pa_id_query)
    pa_id_records = pa_id_result['records']
    if pa_id_records:
        pa_id_dict = pa_id_records[0]
        pa_id_value = pa_id_dict['Id']
        return pa_id_value


def get_stage_for_pa(pa_name):
    pa_stg_q = "SELECT Contract_Stage__c FROM provider_Agreement__C  where Name = '" + pa_name + \
                "'"
    # pa_stg_q = "SELECT Contract_Stage__c FROM provider_Agreement__C  where Name = '" + pa_name + \
    #            "' and candidate__C = '" + uic.candidate_id_1 + "' LIMIT 1"
    pa_stg_result = sf_cc.sf_connect.query(pa_stg_q)
    pa_stg_records = pa_stg_result['records']
    if pa_stg_records:
        pa_stg_dict = pa_stg_records[0]
        pa_stg_value = pa_stg_dict['Contract_Stage__c']
        if not pa_stg_value:
            pa_stg_value = ''
        return pa_stg_value

def get_ant_str_date_pa(pa_name):
    pa_query = "SELECT Anticipated_Start_Date__c FROM Provider_Agreement__C where Name = '" + pa_name + "'"
    pa_result = sf_cc.sf_connect.query(pa_query)
    pa_record = pa_result['records']
    if pa_record:
        pa_ant_str_date = pa_record[0]['Anticipated_Start_Date__c']

    return pa_ant_str_date

def usr_profile_value(u_name):
    usr_record = sf_cc.sf_connect.query("SELECT profileId from USER where Name = '" + u_name + "'")
    usr_records = usr_record['records']
    usr_id_dict = usr_records[0]
    p_id = usr_id_dict['ProfileId']
    if p_id:
        p_dict = sf_cc.sf_connect.profile.get(p_id)
        profile_name = p_dict['Name']
        return profile_name


def get_user_created_by_auto(c_by_admin_id):
    usr_record = sf_cc.sf_connect.query("SELECT Name from USER where CreatedById = '" + c_by_admin_id + "'")
    usr_records = usr_record['records']
    usr_name_dict = usr_records[0]
    p_name = usr_name_dict['Name']

    return p_name

def get_ssl_name(cand_name):

    ssl_query = "SELECT Id,Name,Service_line_Code__c FROM Candidate__c where Name = '" + cand_name + \
                "'and CreatedById = '" + uic.created_by_admin_id + "'"
    ssl_result_name = sf_cc.sf_connect.query(ssl_query)
    ssl_records = ssl_result_name['records']
    if ssl_records:
        ssl_name = ssl_records[0]['Service_line_Code__c']

    return ssl_name

# def get_ssl_id():
#     ssl_query = "SELECT Id FROM Sound_Service_Lines__c WHERE Service_Line__c = '" + ssl_value + "' and " \
#                 " CreatedById ='" + uic.created_by_admin_id + "' ORDER BY CreatedDate desc LIMIT 1"
#
#     ssl_result_name = sf_cc.sf_connect.query(ssl_query)
#     ssl_records = ssl_result_name['records']
#     if ssl_records:
#         ssl_id = ssl_records['Id']
#         lg.log_results(str(ssl_id))
#
#     return ssl_id


def get_pos_id_with_type(ssl_value, pos_title):
    ret_pos_id = None
    ret_no_req = None
    pos_name = None
    pos_query = "SELECT Id, No_Requirements__c, Name FROM Position__c WHERE Service_Line__c = '" + ssl_value +  \
                "' and CreatedById = '" + uic.created_by_admin_id + "' and position_title__c = '" + pos_title + \
                "' ORDER BY CreatedDate DESC LIMIT 1"
    result_name = sf_cc.sf_connect.query(pos_query)
    pos_id_rec = result_name['records']
    if pos_id_rec:
        pos_id_dict = pos_id_rec[0]
        ret_pos_id = pos_id_dict['Id']
        ret_no_req = pos_id_dict['No_Requirements__c']
        pos_name = pos_id_dict['Name']

    return ret_pos_id, ret_no_req, pos_name


def set_pos_no_req(no_req_val):
    return_value = False
    ret_response = sf_cc.sf_connect.Position__c.update(uic.position_id, {'No_Requirements__C' :
                                                                             no_req_val})

    if ret_response == 204:
        return_value = True

    return return_value

def get_pri_file():
    pf_name = None
    pri_file_query = "SELECT Name FROM Privileging_Files__c WHERE Candidate__c = '" + uic.candidate_id_1 +\
        "' and Position_Number__c = '" + uic.position_name + "'"

    result_name = sf_cc.sf_connect.query(pri_file_query)
    pf_rec = result_name['records']
    if pf_rec:
        pf_dict = pf_rec[0]
        pf_name = pf_dict['Name']

    return pf_name


def set_cand_for_pos():
    ret_cand_id = None
    ret_cand_name = None
    cad_query = "SELECT Id, Name FROM candidate__c WHERE Position__c = '" + uic.position_id + "' and " +\
            " CreatedById ='" + uic.created_by_admin_id + \
            "' ORDER BY CreatedDate desc LIMIT 1"
    result_name = sf_cc.sf_connect.query(cad_query)
    cand_rec = result_name['records']
    if cand_rec:
        cand_dict = cand_rec[0]
        ret_cand_id = cand_dict['Id']
        ret_cand_name = cand_dict['Name']

    return ret_cand_id, ret_cand_name

def check_db_for_ce():
    c_name = None
    query = "SELECT Name FROM Collaboration_Engine__c where APP_Candidate__c = '" + uic.candidate_id_1  + "'"
    result_name = sf_cc.sf_connect.query(query)
    c_rec = result_name['records']
    if c_rec:
        c_dict = c_rec[0]
        c_name = c_dict['Name']

    return c_name

def delete_pf(privileging_file):
    ret_val = None
    query = "SELECT Id FROM Privileging_Files__c WHERE Candidate__c = '" + uic.candidate_id_1 + "' and " \
            "Position_Number__c LIKE '" + uic.position_name + "%'"
    result_name = sf_cc.sf_connect.query(query)
    pf_rec = result_name['records']
    if pf_rec:
        pf_dict = pf_rec[0]
        pf_id = pf_dict['Id']
        ret_val = sf_cc.sf_connect.Privileging_Files__c.delete(pf_id)

    return ret_val

def delete_ce():
    ret_val = None
    query = "SELECT Id FROM Collaboration_Engine__c WHERE Position__c = '" + uic.position_name + "' LIMIT 1"
    result_name = sf_cc.sf_connect.query(query)
    ce_rec = result_name['records']
    if ce_rec:
        ce_dict = ce_rec[0]
        ce_id = ce_dict['Id']
        ret_value = sf_cc.sf_connect.Collaboration_Engine__c.delete(ce_id)
        if ret_value == 204:
            lg.log_results("\t \t Cleanup - Successfully deleted collaboration engine for position " + str(
                uic.position_name))
        if ret_value is None:
            lg.log_results("\t \t Cleanup - Collaboration Engine doesn't exist for position " + str(
                uic.position_name))
        else:
            lg.log_results("\t \t Cleanup - Couldn't delete collaboration engine for position " + str(uic.position_name) +
                           " return value is " + str(ret_value))


def delete_er():
    ret_val = None
    i = 0
    query = "SELECT Id, Name FROM employment_requirement__c WHERE Position__c = '" + uic.position_id + "'"
    result_name = sf_cc.sf_connect.query(query)
    er_rec = result_name['records']
    len_er = len(er_rec)
    while i < len_er:
        er_dict = er_rec[i]
        er_id = str(er_dict['Id'])
        er_name = str(er_dict['Name'])
        ret_val = sf_cc.sf_connect.employment_requirement__c.delete(er_id)
        if ret_val == 204:
            lg.log_results("\t \t Cleanup - Deleted employment requirement " + str(er_name))
        elif ret_val:
            lg.log_results("\t \t Cleanup - Deleting employment requirement " + str(er_name) + str(ret_val))
        else:
            lg.log_results("\t \t Cleanup - Trying to Delete employment requirement " + str(er_name))

        i += 1

    return ret_val

def update_er_date(eff_date, end_date):
    ret_response = False
    query = "SELECT Id, Name, Effective_Date__c FROM employment_requirement__c WHERE Position__c = '" + uic.position_id + "'"
    result_name = sf_cc.sf_connect.query(query)
    er_rec = result_name['records']
    len_er = len(er_rec)
    i = 0
    while i < len_er:
        er_dict = er_rec[i]
        er_id = str(er_dict['Id'])
        er_name = str(er_dict['Name'])
        er_effective_date = str(er_dict['Effective_Date__c'])
        ret_val = sf_cc.sf_connect.employment_requirement__c.update(er_id, {'Effective_Date__c': eff_date,
                                                                            'End_Date__c': end_date})
        lg.assert_and_log(ret_val == 204, "\t \t FAIL : Couldn't update employment requirement end date " + str(
            er_name) + " return value is " + str(ret_val),  "\t \t PASS : Updated employment requirement " + str(
            er_name) + " with date " + str(er_effective_date))
        i += 1


def delete_note():
    ret_value = None
    query = "SELECT Id FROM Note WHERE ParentId = '" + uic.position_id + "' LIMIT 1"
    result_name = sf_cc.sf_connect.query(query)
    note_rec = result_name['records']
    if note_rec:
        note_dict = note_rec[0]
        note_id = note_dict['Id']
        ret_value = sf_cc.sf_connect.Note.delete(note_id)

    return ret_value



def delete_current_record(candidate_ids, position_ids, ssl_ids):

    # Delete candidate first
    for each_cand in candidate_ids:
        result_name = sf_cc.sf_connect.query("SELECT Id FROM Candidate__C WHERE Id = '" + each_cand + "'" +
                                     " and createdById = '" + uic.created_by_admin_id + "'")

        c_rec = result_name['records']
        if c_rec:
            c_dict = c_rec[0]
            c_id = c_dict['Id']
            ret_value = sf_cc.sf_connect.Candidate__c.delete(c_id)
            if ret_value == 204:
                lg.log_results("\t \t Cleanup - Candidate Successfully deleted - " + str(c_id))
            elif ret_value is None:
                lg.log_results("\t \t Cleanup - Candidate doesn't exist " + str(c_id))
            else:
                lg.log_results("\t \t Cleanup - Couldn't delete Candidate, return value is " + str(ret_value))

    # Delete Position
    for each_pos in position_ids:
        result_name = sf_cc.sf_connect.query("SELECT Id FROM Position__C WHERE Id = '" + each_pos + "'" +
                                             " and createdById = '" + uic.created_by_admin_id + "'")
        p_rec = result_name['records']
        if p_rec:
            p_dict = p_rec[0]
            p_id = p_dict['Id']
            ret_value = sf_cc.sf_connect.Position__c.delete(p_id)
            if ret_value == 204:
                lg.log_results("\t \t Cleanup - Position Successfully deleted - " + str(p_id))
            elif ret_value is None:
                lg.log_results("\t \t Cleanup - Position doesn't exist " + str(p_id))
            else:
                lg.log_results("\t \t Cleanup - Couldn't delete Position, return value is " + str(ret_value))

    # Delete SSL
    for each_ssl in ssl_ids:
        result_name = sf_cc.sf_connect.query("SELECT Id FROM Sound_Service_Lines__c WHERE Id = '" + each_ssl + "'" +
                                             " and createdById = '" + uic.created_by_admin_id + "'")
        ssl_rec = result_name['records']
        if ssl_rec:
            ssl_dict = ssl_rec[0]
            ssl_id = ssl_dict['Id']
            ret_value = sf_cc.sf_connect.Sound_Service_Lines__c.delete(ssl_id)
            if ret_value == 204:
                lg.log_results("\t \t Cleanup - Sound_Service_Lines__c Successfully deleted - " + str(ssl_id))
            elif ret_value is None:
                lg.log_results("\t \t Cleanup - Sound_Service_Lines__c doesn't exist " + str(ssl_id))
            else:
                lg.log_results("\t \t Cleanup - Couldn't delete Sound_Service_Lines__c, return value is " + str(ret_value))


def get_phy_adr_frm_ssl_acnt(ssl_ids, acnt_ids, choice):
    # account_ids_str = str(acnt_ids).replace('[', '(')
    # account_ids_str = account_ids_str.replace(']', ')')
    ssl_ids_str = str(ssl_ids).replace("['", '')
    ssl_ids_str = ssl_ids_str.replace("']", '')
    account_ids_str = str(acnt_ids).replace("['", '')
    account_ids_str = account_ids_str.replace("']", '')
    ssl_phy_adr = ''
    ssl_cty_zip = ''
    acnt_phy_str = ''
    acnt_phy_city = ''
    acnt_phy_state = ''
    acnt_phy_zc = ''
    acnt_phy_ctry = ''
    if choice == 'ssl':
        ssl_query = "SELECT Id,Name,Physical_Address__c,Physical_City_State_Zip__c FROM Sound_Service_Lines__c " \
                    "WHERE Id = '" + ssl_ids_str + "' and Hospital_Partner__c = '" + account_ids_str + "'"
        ssl_result = sf_cc.sf_connect.query(ssl_query)
        ssl_rec = ssl_result['records']
        if ssl_rec:
            ssl_dict = ssl_rec[0]
            ssl_phy_adr = ssl_dict['Physical_Address__c']
            ssl_cty_zip = ssl_dict['Physical_City_State_Zip__c']
            return ssl_phy_adr, ssl_cty_zip
    else:
        acnt_query = "SELECT Id, Name, ShippingCity, ShippingPostalCode, ShippingState, ShippingStreet " \
                     "FROM Account where Id = '" + account_ids_str + "'"
        acnt_result = sf_cc.sf_connect.query(acnt_query)
        acnt_rec = acnt_result['records']
        if acnt_rec:
            acnt_dict = acnt_rec[0]
            acnt_phy_str = str(acnt_dict['ShippingStreet'])
            acnt_phy_city = str(acnt_dict['ShippingCity'])
            acnt_phy_state = str(acnt_dict['ShippingState'])
            acnt_phy_zc = str(acnt_dict['ShippingPostalCode'])

            return acnt_phy_str, acnt_phy_city, acnt_phy_state, acnt_phy_zc


def get_phy_adr_pos(pos_ids):
    pos_phy_adr = ''
    pos_cty_zip = ''
    pos_id_str = str(pos_ids).replace("['", '')
    pos_id_str = pos_id_str.replace("']", '')
    pos_query = "SELECT Address__c,City_State_Zip__c FROM Position__c WHERE Id = '" + pos_id_str + "'"
    pos_result = sf_cc.sf_connect.query(pos_query)
    pos_rec = pos_result['records']
    if pos_rec:
        pos_dict = pos_rec[0]
        pos_phy_adr = pos_dict['Address__c']
        pos_cty_zip = pos_dict['City_State_Zip__c']

        return pos_phy_adr, pos_cty_zip


def get_cand_id_by_admin(admin_id):
    cand_id = ""
    cand_query = "SELECT Id from Candidate__c Where CreatedById = '" + admin_id + \
                 "' ORDER BY CreatedDate DESC LIMIT 1"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_records = cand_result['records']
    if cand_records:
        cand_rec_dict = cand_records[0]
        cand_id = cand_rec_dict['Id']
    return cand_id


def get_ssl_id(acnt_id):
    ssl_id = ""
    ssl_name = ""
    ssl_query = "SELECT Id,Name FROM Sound_Service_Lines__c WHERE Hospital_Partner__c = '" + acnt_id + "'"
    ssl_result = sf_cc.sf_connect.query(ssl_query)
    ssl_records = ssl_result['records']
    if ssl_records:
        ssl_dict = ssl_records[0]
        ssl_id = ssl_dict['Id']
        ssl_name = ssl_dict['Name']
    return ssl_id

def get_pos_id_frm_acnt(acnt_id):
    pos_id = ""
    pos_query = "SELECT Id,Name FROM Position__c WHERE Partner_Hospital__c = '" + acnt_id + "'"
    pos_result = sf_cc.sf_connect.query(pos_query)
    pos_records = pos_result['records']
    if pos_records:
        pos_dict = pos_records[0]
        pos_id = pos_dict['Id']
    return pos_id


def get_app_cand():
    cand_name = ""
    cand_query = "SELECT Clinician_Type__c,Id,Labor_Commitment_Type__c,Name,Service_Line__c FROM Candidate__c WHERE " \
              "Clinician_Type__c = 'APP' AND Labor_Commitment_Type__c = 'Full Time' AND " \
              "Service_Line__c = 'Hospital Medicine' AND CreatedById = '" + uic.created_by_admin_id + "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_records = cand_result['records']
    if cand_records:
        cand_name = cand_records[0]['Name']
    return cand_name


def get_clt_val_cand(cand):
    clt_val = ""
    cand_query = "SELECT Name,Clinical_Ladder_Tier__c FROM Candidate__c WHERE Name = '" + cand + \
                 "'"
    cand_result = sf_cc.sf_connect.query(cand_query)
    cand_records = cand_result['records']
    if cand_records:
        clt_val = cand_records[0]['Clinical_Ladder_Tier__c']
    return clt_val

def verify_clt_value_cont(cont):
    clt_val = ""
    cont_query = "SELECT Clinical_Ladder_Tier__c FROM Contact WHERE Id = '" + cont + "' "
    cont_result = sf_cc.sf_connect.query(cont_query)
    cont_records = cont_result['records']
    if cont_records:
        clt_val = cont_records[0]['Clinical_Ladder_Tier__c']
    return clt_val


def update_cand_start_date(cand_id, start_date):
    ret_code = sf_cc.sf_connect.Candidate__c.update(cand_id, {'Contract_Start_Date__c': start_date})
    if ret_code >= 400:
        lg.log_results("\t \t FAIL: Could not update Contract Start Date to " + str(start_date))
    else:
        lg.log_results("\t \t PASS: Contract Start Date updated successfully to " + str(start_date))


def delete_clt_value_from_cont(cont_id):
    ret_code = sf_cc.sf_connect.Contact.update(cont_id, {'Clinical_Ladder_Tier__c': ''})
    if ret_code == 204:
        lg.log_results("\t \t PASS: Clinical Ladder Tier value on Contact is updated to Null.")
    else:
        lg.log_results("\t \t FAIL: Could not update the Clinical Ladder Tier value on Contact")


def get_pa_rec_type_id(pa_name):
    pa_rectype_id = ''
    pa_query = "SELECT Name,RecordTypeId FROM Provider_Agreement__c WHERE Name = '" + pa_name + "'"
    pa_result = sf_cc.sf_connect.query(pa_query)
    pa_records = pa_result['records']
    if pa_records:
        pa_rectype_id = pa_records[0]['RecordTypeId']

    return pa_rectype_id


def update_user_active(name, is_active):
    user_id = get_user_id_name(name)
    ret_code = sf_cc.sf_connect.User.update(user_id, {'IsActive': is_active})
    if ret_code == 204:
        lg.log_results("\t \t PASS: User is updated correctly.")
    else:
        lg.log_results("\t \t FAIL: User couldn't be updated correctly.")


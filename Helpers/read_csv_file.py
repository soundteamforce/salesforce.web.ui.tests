import csv
from Helpers import log_helper as lg_hlpr


# matrix_file = r"C:\SOUND PHYSICIANS\UserStories - Team Force\77783 TME - Enhance Uniqueness Rule for " \
#               r"Candidate\test.csv"
matrix_file = r"C:\SOUND PHYSICIANS\UserStories - Team Force\77783 TME - Enhance Uniqueness Rule for " \
              r"Candidate\ITP_Candidate Record_StatusMatrix_200717_2.csv"
# matrix_file = r"C:\SOUND PHYSICIANS\UserStories - Team Force\77783 TME - Enhance Uniqueness Rule for " \
#               r"Candidate\ITP_Candidate Record_Issues.csv"
# ITP_Candidate Record_StatusMatrix_200717_1.csv
#  test.csv
def read_file():
    lg_hlpr.log_results("INFO: \t Reading uniqueness rule file." + matrix_file)
    num_rows = 0
    with open(matrix_file) as csv_file:
        matrix_reader = csv.DictReader(csv_file, skipinitialspace=True, delimiter=',')
        matrix_header = matrix_reader.fieldnames
        matrix_dict = {name: [] for name in matrix_reader.fieldnames}
        for row in matrix_reader:
            num_rows += 1
            for name in matrix_reader.fieldnames:
                matrix_dict[name].append(row[name])

    lg_hlpr.log_results("INFO: \t Number of Rows To Read Are - " + str(num_rows))
    return matrix_dict, num_rows


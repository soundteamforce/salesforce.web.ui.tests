from Helpers import log_helper as lg
from Helpers import salesforce_helper as sfh
import random as r
from Helpers import utilities as u
import sf_connection_config as sf_cc
import api_config as aconfig

def begin_account_record():

    lg.log_results("\nINFO: \t Begin creating ACCOUNT records.")

    sobject_account_val = 'Account'

    for acct_rec_type in aconfig.account_rec_type:
        record_type_id = sfh.get_record_type_id(acct_rec_type, sobject_account_val)
        if record_type_id:
            ret_account_id, ret_account_name = create_new_account_rec(acct_rec_type, record_type_id)
            if ret_account_id:
                lg.log_results("PASS : \t Successfully created account record: " + ret_account_id + " " +
                ret_account_name)
                aconfig.all_account_ids.append(ret_account_id)
                aconfig.all_account_names.append(ret_account_name)
            else:
                return

    lg.log_results("PASS: \t Account Ids are : " + str(aconfig.all_account_ids))

    return


def create_new_account_rec(acct_rec_type, record_type_id):

    account_name_str = 'Auto'
    account_region = 'A Team'
    account_name = account_name_str + "Hosp-" + u.get_random_chars(2)
    billing_street = '345 Main Auto Street'
    billing_city = 'AutoCity'
    billing_p_code = '94580'
    billing_state = 'CA'
    billing_country = 'USA'
    phy_street = '38765 Auto Blvd'
    phy_city = 'AutoCity'
    phy_p_code = '94565'
    phy_state = 'FL'
    acct_record = sf_cc.sf_connect.account.create({'Name': account_name,
                                    'Primary_Type__c': 'Hospital',
                                    'RecordTypeId': record_type_id,
                                    'region__c': account_region,
                                    'Division__c': r.choice(['Blue Ridge', 'Mid-Atlantic', 'Northeast']),
                                    'Phone': u.get_random_number(10),
                                    'BillingStreet': billing_street,
                                    'BillingCity': billing_city,
                                    'BillingPostalCode' : billing_p_code,
                                    'BillingState' : billing_state,
                                    'BillingCountry' : billing_country,
                                    'ShippingStreet' : phy_street,
                                    'ShippingCity' : phy_city,
                                    'ShippingState' : phy_state,
                                    'ShippingPostalCode' : phy_p_code,
                                    'ShippingCountry' : billing_country
                                   })
    # account_id = str(acct_record['id'])
    #TODO - Check if check_for_new_record is added to every create...
    account_id = sfh.check_for_new_record(acct_record, 'account')

    return account_id, account_name




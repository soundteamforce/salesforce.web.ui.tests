from Helpers import salesforce_helper as sfh
from Helpers import utilities as u
import sf_connection_config as sf_cc
from Helpers import log_helper as lg
import api_config as aconfig
from Helpers import api_helper as apih

def begin_at_guideline_record():
    at_guidline_ids = []
    entity_str = ''
    if aconfig.is_hub:
        hmp_or_pos_ids = aconfig.all_hmp_ids
        hmg_or_pos_to_create = aconfig.hmg_to_create
        entity_str = 'Hub Managed'
        lg.log_results("INFO: \t Creating Agreement Guideline Record: for hub "  + aconfig.hub_id)
    else:
        hmp_or_pos_ids = aconfig.all_position_ids
        hmg_or_pos_to_create = aconfig.ag_to_create


    if not hmp_or_pos_ids:
        lg.log_results("INFO: \t Could not create Agreement Guideline Record: " + entity_str + " Position Ids not "
                                                                                               "found")
        return

    # Create a agt_guideln with shift_pay type.
    at_sobject_type = 'Agreement_Term_Guideline__c'
    for each_hmg_or_pos_id in hmp_or_pos_ids:
        for each_hmg_or_pos_create in hmg_or_pos_to_create:
            init_for_new_guideline()
            each_gdln_to_create_name = each_hmg_or_pos_create['at_category'] + " - " + each_hmg_or_pos_create[
                'at_type']
            at_record_type_id = sfh.get_record_type_id(each_gdln_to_create_name, at_sobject_type)
            if at_record_type_id != "":
                ret_gdln_id, ret_gdln_name = create_new_gdln(each_hmg_or_pos_id, at_record_type_id,
                                                           each_hmg_or_pos_create)
                if ret_gdln_id:
                    lg.log_results("PASS : \t Successfully created " + entity_str + "Hub Managed Guideline record: " +
                                   each_gdln_to_create_name + " - " + ret_gdln_name + " on " + str(each_hmg_or_pos_id))
                    aconfig.at_guideline_ids.append(ret_gdln_id)
            else:
                lg.log_results("FAIL : \t Record Type Id is NOT valid - " + each_gdln_to_create_name, "LIGHTRED_EX")

    return

def create_new_gdln(ag_id, at_rec_type_id, ag_details):
    hmp_ct_id = None
    hmp_at_id = None
    pos_ct_id = None
    pos_at_id = None
    atg_id = ''
    atg_name = ''
    no_days_ago = 5
    at_guideline_start_date = u.get_days_ago(no_days_ago) # as Guideline Start Date must be prior to today.
    at_guideline_end_date = u.get_days_in_future(300)
    set_all_values(ag_details)
    if aconfig.is_hub and 'Compensation' in ag_details['at_category']: hmp_ct_id = ag_id
    if aconfig.is_hub and 'Compensation' not in ag_details['at_category']: hmp_at_id = ag_id
    if not aconfig.is_hub and 'Compensation' in ag_details['at_category']: pos_ct_id = ag_id
    if not aconfig.is_hub and 'Compensation' not in ag_details['at_category']: pos_at_id = ag_id


    atg_record = sf_cc.sf_connect.Agreement_Term_Guideline__c.create({
                                                        'Hub_Managed_Position_CT__c': hmp_ct_id
                                                        ,'Hub_Managed_Position_AT__c': hmp_at_id
                                                        ,'Position_AT__c': pos_at_id
                                                        ,'Position_CT__c': pos_ct_id
                                                        ,'Description__c':aconfig.at_description
                                                        ,'RecordTypeId': at_rec_type_id
                                                        ,'amount__c': aconfig.at_amount
                                                        ,'shift_hours__c': aconfig.at_shift_hours
                                                        ,'Short_code__c': aconfig.at_short_code
                                                        ,'guideline_start_date__c': at_guideline_start_date
                                                        ,'guideline_end_date__c': at_guideline_end_date
                                                        ,'Review_status__c' : aconfig.ag_review_status
                                                        ,'Type__c': ag_details['type__c']
                                                        ,'Case__c': aconfig.at_case
                                                        ,'Foregiveness_Period__c': aconfig.at_forgiveness_period
                                                        ,'Interval__c': aconfig.at_interval
                                                        ,'Payback_Schedule__c': aconfig.at_payback_schedule
                                                        ,'Period__c': aconfig.at_period
                                                        ,'Quantity__c': aconfig.at_quantity
                                                        ,'Schedule__c': aconfig.at_schedule
                                                        ,'Threshold_Maximum__c': aconfig.at_threshold_max
                                                        ,'Threshold_Minimum__c': aconfig.at_threshold_min
                                                        ,'Threshold_Type__c': aconfig.at_threshold_type
                                                        ,'Time_Off_Type__c': aconfig.at_time_off_type
                                                        ,'Workload__c': aconfig.at_workload
                                                        ,'Shift_Level__c' : aconfig.at_shift_level
                                                        ,'Experience_Modifier__c' : aconfig.at_experience_modifier
                                                  })

    atg_id =  sfh.check_for_new_record(atg_record, 'Agreement_Term_Guideline__c')
    if atg_id:
        atg_dict = sf_cc.sf_connect.Agreement_Term_Guideline__c.get(atg_id)
        atg_name = str(atg_dict['Name'])

    return atg_id, atg_name

def set_all_values(gdln_details):
    if 'at_forgiveness_period' in gdln_details.keys(): aconfig.at_forgiveness_period = gdln_details[
        'at_forgiveness_period']
    if 'at_interval' in gdln_details.keys(): aconfig.at_interval = gdln_details['at_interval']
    if 'at_payback_schedule' in gdln_details.keys(): aconfig.at_payback_schedule = gdln_details['at_payback_schedule']
    if 'at_period' in gdln_details.keys(): aconfig.at_period = gdln_details['at_period']
    if 'at_quantity' in gdln_details.keys(): aconfig.at_quantity = gdln_details['at_quantity']
    if 'at_review_status' in gdln_details.keys(): aconfig.at_review_status = gdln_details['at_review_status']
    if 'at_schedule' in gdln_details.keys(): aconfig.at_schedule = gdln_details['at_schedule']
    if 'at_shift_hours' in gdln_details.keys(): aconfig.at_shift_hours = gdln_details['at_shift_hours']
    if 'at_threshold_max' in gdln_details.keys(): aconfig.at_threshold_max = gdln_details['at_threshold_max']
    if 'at_threshold_min' in gdln_details.keys(): aconfig.at_threshold_min = gdln_details['at_threshold_min']
    if 'at_threshold_type' in gdln_details.keys(): aconfig.at_threshold_type = gdln_details['at_threshold_type']
    if 'at_time_off_type' in gdln_details.keys(): aconfig.at_time_off_type = gdln_details['at_time_off_type']
    if 'at_workload' in gdln_details.keys(): aconfig.at_workload = gdln_details['at_workload']
    if 'at_amount' in gdln_details.keys(): aconfig.at_amount = gdln_details['at_amount']
    if 'at_description' in gdln_details.keys(): aconfig.at_description = gdln_details['at_description']
    if 'at_short_code' in gdln_details.keys(): aconfig.at_short_code = gdln_details['at_short_code']
    if 'at_shift_level' in gdln_details.keys(): aconfig.at_shift_level = gdln_details['at_shift_level']
    if 'at_experience_modifier' in gdln_details.keys(): aconfig.at_experience_modifier = gdln_details['at_experience_modifier']
    return


def init_for_new_guideline():
    aconfig.at_case = None
    aconfig.at_description = None
    aconfig.at_amount = None
    aconfig.at_short_code = None
    aconfig.at_forgiveness_period = None
    aconfig.at_interval  = None
    aconfig.at_payback_schedule = None
    aconfig.at_period = None
    aconfig.at_quantity = None
    aconfig.at_schedule = None
    aconfig.at_shift_hours = None
    aconfig.at_threshold_max = None
    aconfig.at_threshold_min = None
    aconfig.at_threshold_type = None
    aconfig.at_time_off_type = None
    aconfig.at_workload = None
    aconfig.at_shift_Level = None
    aconfig.at_experience_modifier = None
    return

def begin_at_guideline_record_per_name(pos_ids, at_guidline_name):
    at_guidline_ids = []

    # Create a agt_guideln with shift_pay type.
    # at_guidline_name = 'Compensation - Shift Pay'
    at_sobject_type = 'Agreement_Term_Guideline__c'
    at_record_type_id = sfh.get_record_type_id(at_guidline_name, at_sobject_type)
    ret_atg_name = ''

    if at_record_type_id != "":

        # for position_id in pos_ids:
        position_id = pos_ids
        ret_atg_id, ret_atg_name = create_new_agreement_term_guideline_simpli_email(position_id,
                                                                                    at_record_type_id, at_guidline_name)
        if ret_atg_id:
            lg.log_results("PASS : \t Successfully created Agreement Term Guideline record: " + ret_atg_name + " with id " + str(ret_atg_id))
            at_guidline_ids.append(ret_atg_id)
        else:
            lg.log_results("FAIL: \t Could not create Agreement Term Guideline record", "LIGHTRED_EX")
    else:
        lg.log_results("FAIL : \t Record Type Id is NOT valid", "LIGHTRED_EX")

    return at_guidline_ids, ret_atg_name



def create_new_agreement_term_guideline_simpli_email(pos_id, at_rec_type_id, gdline_name):
    no_days_ago = 5
    at_guideline_start_date = u.get_days_ago(no_days_ago)
    type_value = ''
    description_value =  ''
    short_code_value =  ''
    amount_value = ''

    if 'ACP Bonus' in gdline_name:
        type_value = 'ACP Bonus'
        description_value =  'ACP Bonus'
        short_code_value =  'ACPBN'
        amount_value = '1011.11'
    elif 'Clinical Salary' in gdline_name:
        type_value = 'Clinical Salary'
        description_value =  'Clinical Salary'
        short_code_value =  'CLSAL'
        amount_value = '1012.12'
    elif 'Commencement Bonus' in gdline_name:
        type_value = 'Bonus'
        description_value =  'Commencement Bonus'
        short_code_value =  'SIGBN'
        amount_value = '1013.13'
    elif 'Education Loan Repayment' in gdline_name:
        type_value = 'Education Loan Repayment'
        description_value =  'Education Loan Repayment'
        short_code_value =  'EDLNR'
        amount_value = '1014.14'
    elif 'Home Health Care' in gdline_name:
        type_value = 'Home Health Care Bonus'
        description_value =  'Home Health Care Bonus'
        short_code_value =  'HHCBN'
        amount_value = '1015.15'
    elif 'Hourly Pay' in gdline_name:
        type_value = 'Hourly Pay'
        description_value =  'Admin Shift'
        short_code_value =  'ADMIN'
        amount_value = '116.16'
    elif 'Housing Allowance' in gdline_name:
        type_value = 'Housing Allowance'
        description_value =  'Housing Allowance'
        short_code_value =  'HSALL'
        amount_value = '1017.17'
    elif 'Moonlighting' in gdline_name:
        type_value = 'Moonlighting'
        description_value =  'Day'
        short_code_value =  'MLDAY'
        amount_value = '1018.18'
    elif 'PTO or Sick' in gdline_name:
        type_value = 'PTO/Sick'
        description_value =  'PTO'
        short_code_value =  'PTO'
        amount_value = '119.19'
    elif 'Productivity Bonus' in gdline_name:
        type_value = 'Bonus'
        description_value =  'Productivity Bonus'
        short_code_value =  'PRDBN'
        amount_value = '1020.20'
    elif 'Promissory Note' in gdline_name:
        type_value = 'Promissory Note'
        description_value =  'Note Amount'
        short_code_value =  'PRMNT'
        amount_value = '1021.21'
    elif 'Quality Bonus' in gdline_name:
        type_value = 'Bonus'
        description_value =  'Quality Bonus'
        short_code_value =  'QUABN'
        amount_value = '1022.22'
    elif 'Relocation Allowance' in gdline_name:
        type_value = 'Relocation Allowance'
        description_value =  'Relocation Allowance'
        short_code_value =  'RELOC'
        amount_value = '1023.23'
    elif 'Retention Bonus' in gdline_name:
        type_value = 'Bonus'
        description_value =  'Retention Bonus'
        short_code_value =  'RETBN'
        amount_value = '1024.24'
    elif 'Shift Pay' in gdline_name:
        type_value = 'Shift Pay'
        description_value =  'Night Shift'
        short_code_value =  'NIGHT'
        amount_value = '1025.25'
    elif 'Stipend' in gdline_name:
        type_value = 'Stipend'
        description_value =  'Jeopardy Stipend'
        short_code_value =  'EJEOP'
        amount_value = '1026.26'
    elif 'WRVU Pay' in gdline_name:
        type_value = 'WRVU'
        description_value =  'WRVU Pay Only'
        short_code_value =  'WRVU'
        amount_value = '1027.27'
    elif 'Work Unit Pay' in gdline_name:
        type_value = 'Work Unit Pay'
        description_value =  'Treadmill'
        short_code_value =  'TREAD'
        amount_value = '1028.28'
    elif 'Workload & Schedule' in gdline_name:
        type_value = 'Workload & Schedule'
        description_value =  ''
        short_code_value =  ''
        amount_value = '1029.29'

    #TODO aconfig.aguideline_review_status instead of 'pending'
    atg_record = sf_cc.sf_connect.Agreement_Term_Guideline__c.create({
        'Position_CT__c': pos_id,
        'RecordTypeId': at_rec_type_id ,
        'guideline_start_date__c': at_guideline_start_date,
        'Review_status__c' : 'Pending',
        'Type__c': type_value,
        'Description__c': description_value,
        'Short_code__c' : short_code_value,
        'Amount__c': amount_value
    })
    atg_id = str(atg_record['id'])
    # atg_name = sfh.get_atg_name(atg_id)

    if atg_id:
        atg_dict = sf_cc.sf_connect.Agreement_Term_Guideline__c.get(atg_id)
        atg_name = str(atg_dict['Name'])


    return atg_id, atg_name
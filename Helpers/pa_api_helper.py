import sys
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
import ui_config as uic
from Resources import element_locator as e
from Pages import provider_agreement as pa_pg
from Helpers import salesforce_helper as sfh
from Helpers import utilities as u
import sf_connection_config as sf_cc
import api_config as aconfig

def begin_pa_creation(each_pa_type):
    new_pa = True
    pa_sobject_type = 'Provider_Agreement__c'
    pa_record_type_id = sfh.get_record_type_id(each_pa_type, pa_sobject_type)
    ret_pa_name = ''
    no_days_ago = 1
    pa_start_date = u.get_days_ago(no_days_ago)
    ret_pa_name = ''
    ret_pa_id = ''
    if pa_record_type_id != "":
        pa_dictnry = {}
        pa_dictnry['Contract_Stage__c'] = 'New'
        pa_dictnry['RecordTypeId'] = pa_record_type_id
        pa_dictnry['Agreement_Type__c'] = each_pa_type
        pa_dictnry['Actual_Term_Start_Date__c'] = pa_start_date
        pa_dictnry['CDL_Return_Date__c'] = ''
        pa_dictnry['Comments__c'] = 'Automation script creation'
        # pa_dictnry['Previous_Candidate__c'] = None
        pa_dictnry = set_values_for_new_pa(pa_dictnry)
        if aconfig.candidate_name == '':
            aconfig.candidate_name = sfh.get_candidate_from_id(aconfig.contact_id)

        ret_pa_id, ret_pa_name, new_pa = create_new_provider_agreement(pa_dictnry)
        if new_pa:
            lg.assert_and_log(ret_pa_id, "FAIL: \t Could not create Provider Agreement record",
                              "PASS : \t Successfully created Provider Agreement record: " + ret_pa_name + " on candidate " +
                              aconfig.candidate_name)

    else:
        lg.log_results("FAIL : \t Record Type Id is NOT valid", "LIGHTRED_EX")

    return ret_pa_id, ret_pa_name, new_pa

def set_values_for_new_pa(pa_dictionary):
    if 'Provider Employment Agreement' in pa_dictionary['Agreement_Type__c']:
        pa_dictionary['Agreement_Type__c'] = 'Employment Agreement, Physician, Critical Care' # will overwite earlier
        pa_dictionary['Agreement_Type_Service_Line__c'] = aconfig.service_lines
        pa_dictionary['Agreement_Type_PEA__c'] = 'Employment Agreement, Nurse Practitioner, Telemedicine (Post Acute)'
# value
    elif 'Initial CDL' in pa_dictionary['Agreement_Type__c']:
        pa_dictionary['CDL_Return_Date__c'] = pa_dictionary['Actual_Term_Start_Date__c']

    elif 'Buy Out Letter' in pa_dictionary['Agreement_Type__c']:
        pa_dictionary['Agreement_Type__c'] = 'W2 Buy Out (3rd Party)'
        pa_dictionary['Previous_Candidate__c'] = uic.candidate_id_2

    elif 'Independent Contractor Agreement' in pa_dictionary['Agreement_Type__c']:
        pa_dictionary['Agreement_Type_Service_Line__c'] = aconfig.service_lines
        pa_dictionary['Agreement_Type__c'] = 'IC Agreement, Physician, Telemedicine'
        pa_dictionary['Agreement_Type_PEA__c'] = 'IC Agreement, Physician, Telemedicine'

    return pa_dictionary



def create_new_provider_agreement(pa_dictnry):
    #TODO check here if it is hub, then create PA on primary contracting candidate.
    #TODO remove redudant logic for getting contact_id and name etc.

    pa_name = ''
    pa_id = ''
    if aconfig.is_hub and aconfig.pri_contractng_can_name != '':
        aconfig.pri_contractng_can_id = sfh.get_candidate_id_from_name(aconfig.pri_contractng_can_name)
        aconfig.candidate_id = aconfig.pri_contractng_can_id
        aconfig.contact_id = sfh.get_contact_id_from_cand(aconfig.candidate_id)
        aconfig.position_id, aconfig.position_name = sfh.get_position_from_candidate(aconfig.pri_contractng_can_name)
    else:
        aconfig.contact_id = sfh.get_contact_id_from_cand(aconfig.candidate_id)

    pa_record = sf_cc.sf_connect.Provider_Agreement__c.create({
        'RecordTypeId': pa_dictnry['RecordTypeId']
        ,'Candidate__c': aconfig.candidate_id
        # ,'Position_Name__c': aconfig.position_name
        ,'Contact__c': aconfig.contact_id
        ,'Contract_Stage__c': pa_dictnry['Contract_Stage__c']
        ,'Agreement_Type__c' : pa_dictnry['Agreement_Type__c']
        # ,'Actual_Term_Start_Date__c' : pa_dictnry['Actual_Term_Start_Date__c']
        ,'Effective_Date__c' : pa_dictnry['Actual_Term_Start_Date__c']
        , 'Comments__c' : pa_dictnry['Comments__c']
        # , 'Previous_Candidate__c' : pa_dictnry['Previous_Candidate__c']
    })
    pa_id =  sfh.check_for_new_record(pa_record, 'Provider_Agreement__c')
    if pa_id:
        pa_dict = sf_cc.sf_connect.Provider_Agreement__c.get(pa_id)
        pa_name = str(pa_dict['Name'])

    return pa_id, pa_name, True

def validate_pa_stage_value(pa_name, expected_pa_stage):
    actual_pa_stage = sfh.get_stage_for_pa(pa_name)
    lg.assert_and_log(actual_pa_stage == expected_pa_stage, '\t \t FAIL: Expected ' + expected_pa_stage +
                      ' but found ' + actual_pa_stage, '\t \t PASS: Stage is successfully updated to ' + actual_pa_stage)
    return

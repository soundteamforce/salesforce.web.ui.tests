import sys
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
import ui_config as cwuc
from Resources import element_locator as e
from Pages import provider_agreement as pa_pg


# def verify_cancellation(st_term_type, terms_num):
#         try:
#             term_table_values = pa_pg.loop_thru_term_tables(st_term_type, terms_num)
#             num_of_terms_in_table = len(term_table_values)
#             # assertEqual(terms_num, num_of_terms_in_table, msg="The wrong number of cancelled terms.")
#
#             lg.assert_and_log(terms_num == num_of_terms_in_table,"\t \t The number of " + st_term_type + " terms cancelled are " + str(
#                     num_of_terms_in_table) + " , NOT " + str(terms_num) + " as expected",
#                 "\t \t The number of " + st_term_type + " terms cancelled are " + str(terms_num))
#             for table_id, t_values in term_table_values.items():
#                 rw_stat = term_table_values[table_id]['review_status']
#                 stat = term_table_values[table_id]['status_value']
#                 term_name = term_table_values[table_id]['term_name']
#                 prepend_msg = "\t \t The term " + term_name
#                 append_msg = " Review Status - " + rw_stat + " and Status - " + stat
#                 lg.assert_and_log('Approved' in rw_stat or 'Pending Removal' in stat,
#                                   prepend_msg + " may not have correct values for " + append_msg,
#                                   prepend_msg + " cancelled correctly " + append_msg)
#
#             return
#
#         except Exception as err:
#             lg.log_results("FAIL :\t\t Error in Provider Agreement Helper. " + str(err), 'LIGHTRED_EX')
#

def process_term_table(term_type, at_number):
    bu.sleep_for(2)

    if 'Compensation' in term_type:
        table_element = bu.is_visible_xpath(e.compensation_term_link_tbody, cwuc.web_longer_wait)
    else:
        table_element = bu.is_visible_xpath(e.addtnal_term_link_tbody, cwuc.web_longer_wait)

    if 'Compensation' in term_type:
        row_link = e.comp_term_xpath
        if at_number > 5:
            go_to_list_element = bu.is_visible_xpath(e.pa_ct_terms_go_to_list, cwuc.web_longer_wait)
            row_count_value = go_to_list_element.get_attribute('text')
            row_count = row_count_value.split('(')[1].split(')')[0]
            index = int(row_count) + 1
        else:
            row_count = len(table_element.find_elements_by_xpath(".//tr"))
            index = int(row_count)
    else:
        row_link = e.addtnal_term_xpath_row_link
        row_count = len(table_element.find_elements_by_xpath(".//tr"))
        index = int(row_count)

    # if at_number != (index - 1):
        # lg.log_results("\t \t The Number of " + str(term_type) + " Agreement Terms " + str(index - 1) + " are "
        #                                                                                                 "NOT equal to the Actual Number " + str(
        #     at_number), "LIGHTRED_EX")
    lg.assert_and_log(int(at_number) == int(index - 1), "\t \t FAIL : The Number of " + str(term_type) + " Agreement "
                                                                                                      "Terms "
                        + str(index - 1) + " are NOT equal to the Actual Number " + str(at_number),
                          "\t \t PASS : The Number of " + str(term_type) + " Agreement Terms is equal to Actual "
                                                                           "Number." )

    return row_link, index


def validate_review_n_status_dict(exp_terms_dictionary):
    lg.log_results("\t \t Starting to validate the review status and status of the terms. ")
    total_terms = len(exp_terms_dictionary)
    # TODO: RIGHT NOW IT IS LOOPING THRU' COMPENSATION side only
    # TODO: DO LOOPING THRU' ADDITIONAL also.. when needed... loop_thru_term_tables can handle both.
    i = 0
    x = 0
    expected_review_status = ''
    expected_status = ''
    page_comp_terms_dictnry = pa_pg.loop_thru_term_tables('Compensation', total_terms)
    # lg.log_results(str(page_comp_terms_dictnry))

    # test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'DAY'},
    #                          1: {'action_taken': 'Reject', 'term_type_to_use': 'NIGHT'}}

    for x, each_exp_term_dictionary in exp_terms_dictionary.items():
        if 'Approve' in each_exp_term_dictionary['action_taken']:
            expected_review_status = 'Approved'
            expected_status = 'Confirmed Addition'
        elif 'Reject' in each_exp_term_dictionary['action_taken']:
            expected_review_status = 'Declined'
            expected_status = 'Confirmed Addition'
        elif 'Decline' in each_exp_term_dictionary['action_taken']:
            expected_review_status = 'Declined'
            expected_status = 'Declined'

        for i, each_page_dictnry in page_comp_terms_dictnry.items():
            # comparing on basic of the term type
            if each_exp_term_dictionary['term_type_to_use'].casefold() in each_page_dictnry['description'].casefold():
                lg.assert_and_log((expected_review_status in each_page_dictnry['review_status'] and
                                   expected_status in each_page_dictnry['status_value']),
                                  '\t \t FAIL: The Review Status and Status value should be ' + expected_review_status
                                  + " " +
                                  expected_status + " and got " + each_page_dictnry['review_status'] + " " +
                                  each_page_dictnry['status_value'],
                                  '\t \t PASS: The Review Status and Status values are as expected ' +
                                  each_page_dictnry[
                                      'review_status'] + " and " + each_page_dictnry['status_value'])
            i += 1
        x += 1


def validate_review_n_status(expected_review_status, expected_status_value):
    comp_terms_dictnry = pa_pg.loop_thru_term_tables('Compensation', 2)
    lg.log_results(str(comp_terms_dictnry))

    for i, each_dictnry in comp_terms_dictnry.items():
        # print(str(dict1))
        lg.assert_and_log((each_dictnry['review_status'] in expected_review_status and each_dictnry['status_value'] in expected_status_value),
                          '\t \t FAIL: The Review Status and Status value should be ' + expected_review_status + " " +
                          expected_status_value + " and got " + each_dictnry['review_status'] + " " + each_dictnry['status_value'],
                          '\t \t PASS: The Review Status and Status values are as expected ' + each_dictnry[
                              'review_status'] + " and " + each_dictnry['status_value'])
        i += 1

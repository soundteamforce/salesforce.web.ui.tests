
from Helpers import utilities as u
from Helpers import ignore_login_data as ild
from Helpers import log_helper as lg
import sf_connection_config as sf_cc


def begin_user_record(users):

    user_role_id = ''
    user_profile_id = ''
    ran_chars = u.get_random_chars(2)
    user_ids_values = []
    user_names_values = []

    for user_profile in users:
        if 'Privileging Intake Coordinator'.casefold() in user_profile.casefold():
            # user_nickname = 'PrivInCordntr' + '-' + ran_chars
            user_role_id = '00E0y000001XdFHEA0'
            user_profile_id = '00e0y000001aqnUAAQ'
            firstName = 'AAPIC_' + ran_chars
            lastName = 'AAPIC_' + ran_chars
        elif 'Privileging Consultant'.casefold() in user_profile.casefold():
            user_role_id = '00E34000001wkyHEAQ'
            user_profile_id = '00e0y000001WffEAAS'
            firstName = 'AAPCN_' + ran_chars
            lastName = 'AALPCN_' + ran_chars
        elif 'Privileging Coordinator'.casefold() in user_profile.casefold():
            user_role_id = '00E0y000001XdFGEA0'
            user_profile_id = '00e0y000001WfezAAC'
            firstName = 'AAPCR_' + ran_chars
            lastName = 'AAPCR_' + ran_chars
        elif 'RDO'.casefold() in user_profile.casefold():
            user_role_id = '00E34000002D3gUEAS'
            user_profile_id = '00e34000001GCe7AAG'
            firstName = 'AARDO_' + ran_chars
            lastName = 'AALRDO_' + ran_chars
        elif 'Staffing Services Team'.casefold() in user_profile.casefold():
            user_role_id = '00E34000001wkzFEAQ'
            user_profile_id = '00e340000015S1BAAU'
            firstName = 'AASST_' + ran_chars
            lastName = 'AALSST_' + ran_chars
        elif 'ECHO Recruiting'.casefold() in user_profile.casefold():
            user_role_id = '00E34000001wkylEAA'
            user_profile_id = '00e340000015RtbAAE'
            firstName = 'AAECO_' + ran_chars
            lastName = 'AALECO_' + ran_chars


        user_name_name = firstName + ' ' + lastName
        user_email = 'autotester@soundphysicians.com.invalid'
        username_part = firstName[1:] + lastName
        # it is developed to create user in only test environment.
        user_alias = username_part[:8]
        username_email = user_alias + '@soundphysicians.com.' + sf_cc.test_env     #username_part,     # ild.test_env
        user_locale = 'en_US'
        LanguageLocalKey = 'en_US'
        user_receive_email = 'false'
        user_timezone_key = 'America/Los_Angeles'
        user_active = 'true'
        email_encoding_key = 'ISO-8859-1'


        user_record = sf_cc.sf_connect.User.create({
                        'FirstName': firstName,
                        'LastName': lastName,
                        'Alias': firstName,
                        'Email': user_email,
                        'EmailEncodingKey': email_encoding_key,
                        'Username': username_email,
                        'CommunityNickname': firstName,                        #user_nickname,
                        'LocaleSidKey': user_locale,
                        'LanguageLocaleKey': LanguageLocalKey,
                        'ReceivesAdminInfoEmails': user_receive_email,
                        'UserRoleId': user_role_id,
                        'ProfileId': user_profile_id,
                        'TimeZoneSidKey': user_timezone_key
            })
        lg.log_results("Id " + str(user_record['id']))
        user_id = user_ids_values.append(str(user_record['id']))
        user_name = user_names_values.append(str(user_name_name))

    return user_id, user_name



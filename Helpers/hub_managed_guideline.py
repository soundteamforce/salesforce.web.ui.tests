# from Helpers import salesforce_helper as sfh
# from Helpers import utilities as u
# import sf_connection_config as sf_cc
# from Helpers import log_helper as lg
# import api_config as aconfig
# from Resources import test_variables as tv
# from Helpers import api_helper as apih
#
# def begin_hmg_record():
#     if not (aconfig.all_hmp_ids):
#         lg.log_results("INFO: \t Could not create Hub Managed Guideline Record: Hub Managed Position Ids not found")
#         return
#
#     hmg_ids = []
#     # aconfig.hub_hmguideline_ids #hmg_to_create
#     # Create a new hub managed guidline on hmp
#     at_sobject_type = 'Agreement_Term_Guideline__c' #TODO update it to Hub Managed Guideline.
#     for each_hmp_id in aconfig.all_hmp_ids:
#         for each_hmg_to_create in aconfig.hmg_to_create:
#             init_for_new_hmg()
#             each_hmg_to_create_name = each_hmg_to_create['at_category'] + " - " + each_hmg_to_create['at_type']
#             at_record_type_id = sfh.get_record_type_id(each_hmg_to_create_name, at_sobject_type)
#             if at_record_type_id != "":
#                 ret_hmg_id, ret_hmg_name = create_new_hmg(each_hmp_id, at_record_type_id, each_hmg_to_create)
#                 if ret_hmg_id:
#                     lg.log_results("PASS : \t Successfully created Hub Managed Guideline record: " +
#                                    each_hmg_to_create_name + " - " + ret_hmg_name + " on " + str(each_hmp_id))
#                     aconfig.hub_hmguideline_ids.append(ret_hmg_id)
#             else:
#                 lg.log_results("FAIL : \t Record Type Id is NOT valid - " + each_hmg_to_create_name, "LIGHTRED_EX")
#
#     return
#
#
# def create_new_hmg(hmp_id, at_rec_type_id, hmg_details):
#     atg_id = ''
#     atg_name = ''
#     no_days_ago = 5
#     at_guideline_start_date = u.get_days_ago(no_days_ago) # as Guideline Start Date must be prior to today.
#     at_guideline_end_date = u.get_days_in_future(300)
#     set_all_values(hmg_details)
#     if 'Compensation' in hmg_details['at_category']:
#         hmp_ct_id = hmp_id
#         hmp_at_id = None
#     else:
#         hmp_ct_id = None
#         hmp_at_id = hmp_id
#
#     atg_record = sf_cc.sf_connect.Agreement_Term_Guideline__c.create({'Hub_Managed_Position_CT__c': hmp_ct_id
#                                                                         , 'Hub_Managed_Position_AT__c': hmp_at_id
#                                                                          ,'Description__c':hmg_details['at_description']
#                                                                          ,'RecordTypeId': at_rec_type_id
#                                                                          ,'amount__c': hmg_details['at_amount']
#                                                                          ,'shift_hours__c': hmg_details['at_shift_hours']
#                                                                          ,'Short_code__c': hmg_details['at_short_code']
#                                                                          ,'guideline_start_date__c': at_guideline_start_date
#                                                                          ,'guideline_end_date__c': at_guideline_end_date
#                                                                          ,'Review_status__c' : aconfig.at_review_status
#                                                                          , 'Type__c': hmg_details['type__c']
#                                                                          ,'Case__c': aconfig.at_case
#                                                                          ,'Foregiveness_Period__c':
#                                                                           aconfig.at_forgiveness_period
#                                                                          ,'Interval__c': aconfig.at_interval
#                                                                          ,'Payback_Schedule__c': aconfig.at_payback_schedule
#                                                                          ,'Period__c': aconfig.at_period
#                                                                         ,'Quantity__c': aconfig.at_quantity
#                                                                         ,'Schedule__c': aconfig.at_schedule
#                                                                         ,'Threshold_Maximum__c': aconfig.at_threshold_max
#                                                                         ,'Threshold_Minimum__c': aconfig.at_threshold_min
#                                                                         ,'Threshold_Type__c': aconfig.at_threshold_type
#                                                                         ,'Time_Off_Type__c': aconfig.at_time_off_type
#                                                                         ,'Workload__c': aconfig.at_workload
#     })
#
#     atg_id =  apih.check_for_new_record(atg_record, 'Agreement_Term_Guideline__c')
#     if atg_id:
#         atg_dict = sf_cc.sf_connect.Agreement_Term_Guideline__c.get(atg_id)
#         atg_name = str(atg_dict['Name'])
#
#     return atg_id, atg_name
#
# def set_all_values(hmg_details):
#     if 'at_forgiveness_period' in hmg_details.keys(): aconfig.at_forgiveness_period = hmg_details[
#         'at_forgiveness_period']
#     if 'at_interval' in hmg_details.keys(): aconfig.at_interval = hmg_details['at_interval']
#     if 'at_payback_schedule' in hmg_details.keys(): aconfig.at_payback_schedule = hmg_details['at_payback_schedule']
#     if 'at_period' in hmg_details.keys(): aconfig.at_period = hmg_details['at_period']
#     if 'at_quantity' in hmg_details.keys(): aconfig.at_quantity = hmg_details['at_quantity']
#     if 'at_review_status' in hmg_details.keys(): aconfig.at_review_status = hmg_details['at_review_status']
#     if 'at_schedule' in hmg_details.keys(): aconfig.at_schedule = hmg_details['at_schedule']
#     if 'at_shift_hours' in hmg_details.keys(): aconfig.at_shift_hours = hmg_details['at_shift_hours']
#     if 'at_threshold_max' in hmg_details.keys(): aconfig.at_threshold_max = hmg_details['at_threshold_max']
#     if 'at_threshold_min' in hmg_details.keys(): aconfig.at_threshold_min = hmg_details['at_threshold_min']
#     if 'at_threshold_type' in hmg_details.keys(): aconfig.at_threshold_type = hmg_details['at_threshold_type']
#     if 'at_time_off_type' in hmg_details.keys(): aconfig.at_time_off_type = hmg_details['at_time_off_type']
#     if 'at_workload' in hmg_details.keys(): aconfig.at_workload_c = hmg_details['at_workload']
#     return
#
#
# def init_for_new_hmg():
#     aconfig.at_case = None
#     aconfig.at_forgiveness_period = None
#     aconfig.at_interval  = None
#     aconfig.at_payback_schedule = None
#     aconfig.at_period = None
#     aconfig.at_quantity = None
#     aconfig.at_schedule = None
#     aconfig.at_shift_hours = None
#     aconfig.at_threshold_max = None
#     aconfig.at_threshold_min = None
#     aconfig.at_threshold_type = None
#     aconfig.at_time_off_type = None
#     aconfig.at_workload = None
#     return
#
# def create_new_agreement_term_guideline_simpli_email(pos_id, at_rec_type_id, gdline_name):
#     no_days_ago = 5
#     at_guideline_start_date = u.get_days_ago(no_days_ago)
#     type_value = ''
#     description_value =  ''
#     short_code_value =  ''
#     amount_value = ''
#
#     if 'ACP Bonus' in gdline_name:
#         type_value = 'ACP Bonus'
#         description_value =  'ACP Bonus'
#         short_code_value =  'ACPBN'
#         amount_value = '1011.11'
#     elif 'Clinical Salary' in gdline_name:
#         type_value = 'Clinical Salary'
#         description_value =  'Clinical Salary'
#         short_code_value =  'CLSAL'
#         amount_value = '1012.12'
#     elif 'Commencement Bonus' in gdline_name:
#         type_value = 'Bonus'
#         description_value =  'Commencement Bonus'
#         short_code_value =  'SIGBN'
#         amount_value = '1013.13'
#     elif 'Education Loan Repayment' in gdline_name:
#         type_value = 'Education Loan Repayment'
#         description_value =  'Education Loan Repayment'
#         short_code_value =  'EDLNR'
#         amount_value = '1014.14'
#     elif 'Home Health Care' in gdline_name:
#         type_value = 'Home Health Care Bonus'
#         description_value =  'Home Health Care Bonus'
#         short_code_value =  'HHCBN'
#         amount_value = '1015.15'
#     elif 'Hourly Pay' in gdline_name:
#         type_value = 'Hourly Pay'
#         description_value =  'Admin Shift'
#         short_code_value =  'ADMIN'
#         amount_value = '116.16'
#     elif 'Housing Allowance' in gdline_name:
#         type_value = 'Housing Allowance'
#         description_value =  'Housing Allowance'
#         short_code_value =  'HSALL'
#         amount_value = '1017.17'
#     elif 'Moonlighting' in gdline_name:
#         type_value = 'Moonlighting'
#         description_value =  'Day'
#         short_code_value =  'MLDAY'
#         amount_value = '1018.18'
#     elif 'PTO or Sick' in gdline_name:
#         type_value = 'PTO/Sick'
#         description_value =  'PTO'
#         short_code_value =  'PTO'
#         amount_value = '119.19'
#     elif 'Productivity Bonus' in gdline_name:
#         type_value = 'Bonus'
#         description_value =  'Productivity Bonus'
#         short_code_value =  'PRDBN'
#         amount_value = '1020.20'
#     elif 'Promissory Note' in gdline_name:
#         type_value = 'Promissory Note'
#         description_value =  'Note Amount'
#         short_code_value =  'PRMNT'
#         amount_value = '1021.21'
#     elif 'Quality Bonus' in gdline_name:
#         type_value = 'Bonus'
#         description_value =  'Quality Bonus'
#         short_code_value =  'QUABN'
#         amount_value = '1022.22'
#     elif 'Relocation Allowance' in gdline_name:
#         type_value = 'Relocation Allowance'
#         description_value =  'Relocation Allowance'
#         short_code_value =  'RELOC'
#         amount_value = '1023.23'
#     elif 'Retention Bonus' in gdline_name:
#         type_value = 'Bonus'
#         description_value =  'Retention Bonus'
#         short_code_value =  'RETBN'
#         amount_value = '1024.24'
#     elif 'Shift Pay' in gdline_name:
#         type_value = 'Shift Pay'
#         description_value =  'Night Shift'
#         short_code_value =  'NIGHT'
#         amount_value = '1025.25'
#     elif 'Stipend' in gdline_name:
#         type_value = 'Stipend'
#         description_value =  'Jeopardy Stipend'
#         short_code_value =  'EJEOP'
#         amount_value = '1026.26'
#     elif 'WRVU Pay' in gdline_name:
#         type_value = 'WRVU'
#         description_value =  'WRVU Pay Only'
#         short_code_value =  'WRVU'
#         amount_value = '1027.27'
#     elif 'Work Unit Pay' in gdline_name:
#         type_value = 'Work Unit Pay'
#         description_value =  'Treadmill'
#         short_code_value =  'TREAD'
#         amount_value = '1028.28'
#     elif 'Workload & Schedule' in gdline_name:
#         type_value = 'Workload & Schedule'
#         description_value =  ''
#         short_code_value =  ''
#         amount_value = '1029.29'
#
#
#     atg_record = sf_cc.sf_connect.Agreement_Term_Guideline__c.create({
#         'Position_CT__c': pos_id,
#         'RecordTypeId': at_rec_type_id ,
#         'guideline_start_date__c': at_guideline_start_date,
#         'Review_status__c' : 'Pending',
#         'Type__c': type_value,
#         'Description__c': description_value,
#         'Short_code__c' : short_code_value,
#         'Amount__c': amount_value
#     })
#     atg_id = str(atg_record['id'])
#     # atg_name = sfh.get_atg_name(atg_id)
#
#     if atg_id:
#         atg_dict = sf_cc.sf_connect.Agreement_Term_Guideline__c.get(atg_id)
#         atg_name = str(atg_dict['Name'])
#
#
#     return atg_id, atg_name
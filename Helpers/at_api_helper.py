from Helpers import salesforce_helper as sfh
from Helpers import utilities as u
from Helpers import browser_utilities as bu
import sf_connection_config as sf_cc
from Helpers import log_helper as lg
import ui_config as uic
import api_config as aconfig
import ui_config as cwuc
from Resources import test_variables as tv

def update_existing_term(pa_id, at_col_name, at_col_value):
    lg.log_results("\t \t Updating the agreement term field " + at_col_name + " to " + str(at_col_value))
    sfh.update_agreement_term(pa_id, at_col_name, at_col_value)

def validate_at_details(pa_name):
    lg.log_results("\t \t Getting Agreement Term details.")
    at_end_date = ""
    at_end_pa = ""
    at_status = ""
    i = 0
    bu.sleep_for(5)
    at_dict = sfh.get_term_details(pa_name)

    for i, each_at_dict in at_dict.items():
        at_end_date = str(each_at_dict['term_end_date'])
        at_status = str(each_at_dict['status'])
        at_end_pa = each_at_dict['ending_pa']
        i += 1

    lg.assert_and_log(at_end_date == cwuc.at_end_date, "\t \t FAIL: Expected " + cwuc.at_end_date + " but found " + str(each_at_dict['term_end_date']),
                      "\t \t PASS: The Agreement term end dates are as expected " + str(each_at_dict['term_end_date']))
    lg.assert_and_log(at_status == 'Confirmed Addition', "\t \t FAIL: Expected Confirmed Addition but found " + str(each_at_dict['status']),
                      "\t \t PASS: The Agreement status is updated to " + at_status + " as expected.")
    lg.assert_and_log(at_end_pa is None, "\t \t FAIL: Expected Ending PA to be None, but found " + str(at_end_pa),
                      "\t \t PASS: The Ending Provider Agreement has been removed as expected")


def create_agreement_term(at_types):
# adding updated version usinf ATs in hub. Once it is stable, replace current logic with it.
    all_local_at_names = []
    all_local_at_ids = []

    for each_at_type in at_types:
        at_id, at_name = creating_new_term(each_at_type)
        if at_name:
            all_local_at_names.append(at_name)
            all_local_at_ids.append(at_id)

    return all_local_at_ids, all_local_at_names


def creating_new_term(at_to_cre):
    at_id  = ''
    at_name = ''

    at_type_name = at_to_cre['at_category'] + ' - ' + at_to_cre['at_type']
    at_record_type_id = sfh.get_record_type_id(at_type_name, 'Agreement_Term__c')
    no_days_ago = 1
    at_start_date = u.get_days_ago(no_days_ago)
    at_end_date = u.get_days_in_future(300)

    if 'at_amount' not in at_to_cre.keys(): at_to_cre['at_amount'] = ''
    if 'at_interval' not in at_to_cre.keys(): at_to_cre['at_interval'] = None
    if 'at_schedule' not in at_to_cre.keys(): at_to_cre['at_schedule'] = None
    if 'at_payback_schedule' not in at_to_cre.keys(): at_to_cre['at_payback_schedule'] = None
    if 'at_period' not in at_to_cre.keys(): at_to_cre['at_period'] = None
    if 'at_quantity' not in at_to_cre.keys(): at_to_cre['at_quantity'] = None
    if 'at_shift_hours' not in at_to_cre.keys(): at_to_cre['at_shift_hours'] = ''
    if 'at_threshold_type' not in at_to_cre.keys(): at_to_cre['at_threshold_type'] = None
    if 'at_threshold_max' not in at_to_cre.keys(): at_to_cre['at_threshold_max'] = None
    if 'at_threshold_min' not in at_to_cre.keys(): at_to_cre['at_threshold_min'] = None
    if 'at_time_off_type' not in at_to_cre.keys(): at_to_cre['at_time_off_type'] = None
    if 'at_workload' not in at_to_cre.keys(): at_to_cre['at_workload'] = None
    if 'at_forgiveness_period' not in at_to_cre.keys(): at_to_cre['at_forgiveness_period'] = None
    if 'at_description' not in at_to_cre.keys(): at_to_cre['at_description'] = None
    if 'at_short_code' in at_to_cre.keys(): aconfig.at_short_code = at_to_cre['at_short_code'] = None
    if 'at_shift_level' in at_to_cre.keys(): aconfig.at_shift_level = at_to_cre['at_shift_level'] = None
    if 'at_experience_modifier' in at_to_cre.keys(): aconfig.at_experience_modifier = at_to_cre['at_experience_modifier'] = None


    at_record = sf_cc.sf_connect.Agreement_Term__c.create({
        'Candidate__c' : aconfig.candidate_id
        ,'Amount__c' : at_to_cre['at_amount']
        ,'Description__c' : at_to_cre['at_description']
        ,'Meets_Guideline__c': 'No'
        ,'Auto_Submit_for_Approval__c' : True
        ,'Status__c' : 'Pending Addition'
        ,'Starting_Agreement_CT__c' : aconfig.pa_id
        , 'Interval__c' : at_to_cre['at_interval']
        , 'Term_Start_Date__c' : at_start_date
        , 'Quantity__c' : at_to_cre['at_quantity']
        , 'Schedule__c' : at_to_cre['at_schedule']
        , 'Period__c' : at_to_cre['at_period']
        , 'Workload__c' : at_to_cre['at_workload']
        , 'Threshold_Maximum__c' : at_to_cre['at_threshold_max']
        , 'Threshold_Minimum__c' : at_to_cre['at_threshold_min']
        , 'Threshold_Type__c': at_to_cre['at_threshold_type']
        , 'Term_End_Date__c' : at_end_date
        , 'RecordTypeId' : at_record_type_id
        ,'Shift_Level__c' : aconfig.at_shift_level

        # ,'Experience_Modifier__c' : aconfig.at_experience_modifier
    })
    at_id =  sfh.check_for_new_record(at_record, 'Agreement_Term__c')
    if at_id:
        at_dict = sf_cc.sf_connect.Agreement_Term__c.get(at_id)
        at_name = str(at_dict['Name'])

    return at_id, at_name


def begin_at_record(pa_id, pa_name, at_types):
    all_at_names = []
    for each_at_type in at_types:
        at_name = set_new_at_values(pa_id, pa_name, each_at_type)
        if at_name:
            all_at_names.append(at_name)

    return all_at_names


def set_new_at_values(pa_id, pa_name, at_name):
    no_days_ago = 0
    at_start_date = u.get_days_ago(no_days_ago)
    at_end_date = u.get_days_in_future(300)

    new_at_d = {}
    new_at_d['Provider_Agreement__c'] = pa_name


    at_sobject_type = 'Agreement_Term__c'
    if 'Time Off' in at_name or 'Visa Type' in at_name or 'Workload & Schedule' in at_name :
        at_name_type = 'Additional - ' + at_name
    else:
        at_name_type =  'Compensation - ' + at_name
    at_record_type_id = sfh.get_record_type_id(at_name_type, at_sobject_type)

    new_at_d['amount'] = '111.11'
    new_at_d['Candidate_Start_Date__c'] = None
    new_at_d['Compensation_RecType__c'] = None
    new_at_d['desc'] = at_name
    new_at_d['desc_2'] : None
    new_at_d['Interval__c'] = None
    # new_at_d['Interval_2__c'] = None
    new_at_d['Meets_Guideline__c'] = 'No'
    new_at_d['Period__c'] = None
    new_at_d['Provider_Agreement__c'] = pa_name
    new_at_d['pa_Contract_Stage__c'] = 'Agreement Terms Approval Requested'
    new_at_d['payback_schedule'] = None
    new_at_d['Quantity__c'] = None
    new_at_d['RecordTypeId'] = at_record_type_id
    new_at_d['Starting_Agreement_CT__c'] = pa_id
    new_at_d['Starting_Agreement_AT__c'] = None
    new_at_d['status'] = 'Pending Addition'
    new_at_d['Schedule__c'] = None
    new_at_d['Term_Start_Date__c'] = at_start_date
    new_at_d['Term_End_Date__c'] = None
    new_at_d['Threshold_Minimum__c']	= None
    new_at_d['Threshold_Maximum__c'] 	= None
    new_at_d['Threshold_Type_2__c']	= None
    new_at_d['Threshold_Type__c'] = None
    new_at_d['Workload__c'] = None
    new_at_d['Workload_and_Schedule__c'] = None


    if 'Buy Out' in at_name:
        # new_at_d['desc_2'] = 'Shift'
        print(str(new_at_d['RecordTypeId']))
        # new_at_d['Candidate_Start_Date__c'] = at_start_date
        new_at_d['Buy_Out__C'] = 'Buy Out'
        # # new_at_d['Compensation_RecType__c'] = 'Compensation_Buy_Out'
        new_at_d['Meets_Guideline__c'] = 'Yes'
        # new_at_d['Workload_and_Schedule__c'] = False
        # new_at_d['Threshold_Type_2__c']	= 'Shift'
        # new_at_d['review_Status'] = 'Approved'
        # new_at_d['status'] = 'Pending Addition'
    elif 'Clinical Salary' in at_name:
        new_at_d['desc'] =  'Clinical Salary'
    elif 'ACP Bonus' in at_name:
        new_at_d['desc'] =  'ACP Bonus'
    elif 'Home Health Care Bonus' in at_name:
        new_at_d['desc'] = 'Home Health Care Bonus'
        new_at_d['amount'] = ''
    elif 'Hourly Pay' in at_name:
        new_at_d['desc'] = 'Swing Shift'
    elif 'Housing Allowance' in at_name:
        new_at_d['Interval__c'] = 'Monthly'
    elif 'PTO or Sick' in at_name:
        new_at_d['desc'] = 'PTO'
        new_at_d['desc_2'] = 'Shift'
        new_at_d['amount'] = '100.11'
        new_at_d['Threshold_Minimum__c']	= '0'
        new_at_d['Threshold_Maximum__c'] 	= '1'
    elif 'Productivity Bonus' in at_name:
        new_at_d['desc'] = 'Productivity Bonus'
    elif 'Promissory Note Amount' in at_name:
        new_at_d['desc'] = 'Note Amount'
    elif 'Quality Bonus' in at_name:
        new_at_d['desc'] = 'Quality Bonus'
    elif 'Relocation Allowance' in at_name:
        new_at_d['desc'] = 'Relocation Allowance'
        new_at_d['payback_schedule'] = '1 Year Full Repayment'
    elif 'Retention Bonus' in at_name:
        new_at_d['desc'] = 'Retention Bonus'
        new_at_d['payback_schedule'] = '1 Year Full Repayment'
        new_at_d['Term_End_Date__c'] = at_end_date
    elif 'Shift Pay' in at_name:
        new_at_d['desc'] = 'Night Shift'
    elif 'Stipend' in at_name:
        new_at_d['desc'] = 'Admin Stipend'
        new_at_d['Interval__c'] = 'Monthly'
    elif 'WRVU' in at_name:
        new_at_d['desc'] = at_name
    elif 'Work Unit Pay' in at_name:
        new_at_d['desc'] = 'Treadmill'
    elif 'Workload & Schedule' in at_name:
        new_at_d['desc'] = ''
        new_at_d['Quantity__c'] = '22'
        new_at_d['Schedule__c'] = 'Day Shift'
        new_at_d['Period__c'] = 'Month'
        new_at_d['Workload__c'] = 'Average'
        new_at_d['Starting_Agreement_CT__c'] = ''
        new_at_d['Starting_Agreement_AT__c'] = pa_id
    elif 'Moonlighting' in at_name:
        new_at_d['desc'] = 'Wknd Day'
        new_at_d['Threshold_Maximum__c'] 	= '50'
        new_at_d['Threshold_Minimum__c']	= '10'
        new_at_d['Threshold_Type__c']       = 'Hours'
        new_at_d['Interval__c'] = 'Monthly'

    if at_record_type_id != "":
        # for position_id in pos_ids:
        at_names = []
        at_id, at_name = create_new_agreement_term(new_at_d)
        if at_id:
            lg.log_results("PASS : \t Successfully created Agreement Term record: " + at_name + " - " + at_name_type)
            at_names.append(at_name)
        else:
            lg.log_results("FAIL: \t Could not create Agreement Term record", "LIGHTRED_EX")
    else:
        lg.log_results("FAIL : \t Record Type Id is NOT valid", "LIGHTRED_EX")

    return at_names


def create_new_agreement_term(at_d):

    print("term start date " + str(at_d['Term_Start_Date__c']))
    print("term Candidate_Start_Date__c date " + str(at_d['Candidate_Start_Date__c']))

    at_record = sf_cc.sf_connect.Agreement_Term__c.create({
        # 'RecordTypeId' : '0120y000000UoJqAAK'
        'Candidate__c' : uic.candidate_id_1
        ,'Amount__c' : at_d['amount']
        ,'Description__c' : at_d['desc']
        # , 'Type__c' : 'Buy Out'
        # , 'Short_Code__c' : 'BUY'
        ,'Meets_Guideline__c': at_d['Meets_Guideline__c']
        ,'Auto_Submit_for_Approval__c' : True
        ,'Status__c' : at_d['status']
        ,'Starting_Agreement_CT__c' : at_d['Starting_Agreement_CT__c']
        , 'Interval__c' : at_d['Interval__c']
        , 'Term_Start_Date__c' : str(at_d['Term_Start_Date__c'])
        , 'Quantity__c' : at_d['Quantity__c']
        , 'Schedule__c' : at_d['Schedule__c']
        , 'Period__c' : at_d['Period__c']
        , 'Workload__c' : at_d['Workload__c']
        , 'Threshold_Maximum__c' : at_d['Threshold_Maximum__c']
        , 'Threshold_Minimum__c' : at_d['Threshold_Minimum__c']
        , 'Threshold_Type__c': at_d['Threshold_Type__c']
        , 'Term_End_Date__c': at_d['Term_End_Date__c']
        , 'RecordTypeId': str(at_d['RecordTypeId'])

    })
    at_id = str(at_record['id'])

    if at_id:
        at_dict = sf_cc.sf_connect.Agreement_Term__c.get(at_id)
        at_name = str(at_dict['Name'])


    return at_id, at_name


def validate_at_short_code(pa_name, sc_des_dict):
    exp_dict = sc_des_dict
    at_dict = sfh.get_term_details(pa_name)

    for i, each_dict in at_dict.items():
        at_short_code = at_dict[i]['short_code']
        at_name = at_dict[i]['name']
        at_description = at_dict[i]['description']
        if str(at_description) in exp_dict.keys():
            lg.assert_and_log(str(at_short_code) in exp_dict[str(at_description)], "\t \t FAIL: Short Code of the term "
                              + str(at_name) + " is expected to be " + str(exp_dict[str(at_description)]) + " but is " + str(at_short_code),
                              "\t \t PASS: The short code of " + str(at_name) + " is correctly set as " + str(at_short_code))
        else:
            lg.log_results("\t \t FAIL: Wrong term")



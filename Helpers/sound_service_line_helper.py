from Helpers import log_helper as lg
from Helpers import salesforce_helper as sfh
from Helpers import utilities as u
import random as r
import sf_connection_config as sf_cc
from Helpers import ignore_login_data as ild
import api_config as aconfig


def begin_sound_service_line_record():
    i = 0
    lg.log_results("\nINFO: \t Begin creating " + str(aconfig.num_ssl_records) + " SOUND SERVICE LINE "
                                                                                 "records.")
    if not aconfig.all_account_ids:
        lg.log_results("INFO: \t Could not create Sound Service Line Records: Accounts Ids not found")
        return

    sobject_ssl_val = 'Sound_Service_Lines__c'

    ssl_ids = []
    for account_id in aconfig.all_account_ids:
        for s_service_line in aconfig.service_lines:
            ssl_record_type_id = sfh.get_record_type_id(aconfig.ssl_rec_type, sobject_ssl_val)
            if ssl_record_type_id != "":
                ret_ssl_id, ret_ssl_name = create_new_snd_srvce_line(s_service_line, ssl_record_type_id, account_id,
                                                                     str(i))
                if ret_ssl_id:
                    lg.log_results("PASS : \t Successfully created Sound Service Line record: " + ret_ssl_name)
                    aconfig.all_ssl_ids.append(ret_ssl_id)
                    i += 1
                else:
                    lg.log_results("FAIL: \t Could not create Sound Service record", "LIGHTRED_EX")
            else:
                lg.log_results("FAIL: \t Could not get valid Record Type for Sound Service Line.", "LIGHTRED_EX")

    lg.log_results("PASS: \t Sound Service Line Ids are : " + str(aconfig.all_ssl_ids))

    return

def begin_hub_ssl_records():
    # Hub will be only Telemedicine Service Line.
    sobject_ssl_val = 'Sound_Service_Lines__c'
    lg.log_results("\nINFO: \t Begin creating " + str(aconfig.num_ssl_records) + " SOUND SERVICE LINE "
                                                                         "records.")

    if not aconfig.all_account_ids:
        lg.log_results("INFO: \t Could not create Sound Service Line Records: Accounts Ids not found")
        return

    num_of_accounts = len(aconfig.all_account_ids)
    num_ssl_on_each_account = aconfig.num_ssl_records / num_of_accounts

    ssl_record_type_id = sfh.get_record_type_id(aconfig.ssl_rec_type, sobject_ssl_val)
    if ssl_record_type_id == "":
        lg.log_results("FAIL: \t Could not get valid Record Type for Sound Service Line.")
        return
    else:
        for account_id in aconfig.all_account_ids:
            loop_count = 0
            while(loop_count < int(num_ssl_on_each_account)):
                ret_ssl_id, ret_ssl_name = create_new_snd_srvce_line(aconfig.service_lines, ssl_record_type_id,
                                                                     account_id, str(loop_count), aconfig.hub_id)
                loop_count += 1 #aconfig.num_ssl_records,
                if ret_ssl_id:
                    lg.log_results("PASS : \t Successfully created Sound Service Line record: " + ret_ssl_name)
                    aconfig.all_ssl_ids.append(str(ret_ssl_id))
                else:
                    lg.log_results("FAIL: \t Could not create Sound Service record", "LIGHTRED_EX")

        lg.log_results("PASS: \t Sound Service Line Ids are : " + str(aconfig.all_ssl_ids))
        return



def create_new_snd_srvce_line(service_line, ssl_rec_type_id, account_id, record_number='',hub_id=''):
    # def create_new_snd_srvce_line(service_line, ssl_rec_type_id, account_id, ssl_rec_type, record_number='',
    #                               hub_id=''):
    approver_id = ""
    ssl_name_pre = 'SL-XX.'
    if "Emergency Medicine" in service_line:
        ssl_name_pre = 'SL-EM.'
    elif "Hospital Medicine" in service_line:
        ssl_name_pre = 'SL-HM.'
    elif "Telemedicine" in service_line:
        ssl_name_pre = 'SL-TM.'
    elif "TeleICU" in service_line:
        ssl_name_pre = 'SL-TEI.'
    elif "Critical Care" in service_line:
        ssl_name_pre = 'SL-CC.'

    ssl_name = ""
    ssl_line = ""
    region = "A Team"
    # Get the user and assign these variables to these fields.
    created_by_id = sfh.get_user_id(sf_cc.uname)
    privileging_consultant_id = ''
    privileging_coordinator_id = ''
    staffing_consultant__c = ''
    echo_recruiter__c = ''
    Reappointment_Specialist__c = ''
    # director_perm_recruiter_id = created_by_id
    # approver_id = sfh.get_approver_user(created_by_id)
    # if not approver_id:
    #     approver_id = created_by_id
    account_rec = sf_cc.sf_connect.account.get(account_id)
    account_name = str(account_rec['Name']) + '-' + record_number
    aconfig.accounting_id_code = str(u.get_random_number(4))
    one_number = str(u.get_random_number(1))
    site_code = aconfig.accounting_id_code + '.' + one_number
    ssl_name = ssl_name_pre + site_code + " " + account_name

    # is_hub
    if 'MCS' in service_line:
        echo_recruiter__c = sfh.get_user_id_per_profile('ECHO Recruiting', created_by_id)
        staffing_consultant__c = sfh.get_user_id_per_profile('Staffing Services Team', created_by_id)
        director_perm_recruiter_id = sfh.get_user_id_per_profile('RVP', created_by_id)

    else:
        # removed created by autouser to run in qabeta
        # privileging_consultant_id, privileging_con_name = sfh.get_user_id_per_profile('Privileging Consultant')
        # privileging_coordinator_id, pv_co_name = sfh.get_user_id_per_profile('Privileging Coordinator')
        privileging_consultant_id, privileging_con_name = sfh.get_user_id_per_profile('Privileging Consultant',
                                                                                      created_by_id)
        privileging_coordinator_id, pv_co_name = sfh.get_user_id_per_profile('Privileging Coordinator', created_by_id)
        # # hotfix to remove the inakecoordinator from hub
        # intake_coordinator_id, intake_name = sfh.get_user_id_per_profile('Privileging Intake Coordinator',
        director_perm_recruiter_id, dir_name = sfh.get_user_id_per_profile('RVP', created_by_id)
        approver_id = director_perm_recruiter_id

    #todo add logic if the one of the ids are blank use autotestCT or autouser.

    ssl_record = sf_cc.sf_connect.Sound_Service_Lines__c.create({'Name': ssl_name,
                                     'HOSPITAL_PARTNER__C': account_id,
                                     'RecordTypeId': ssl_rec_type_id,
                                     'Service_Line__c': service_line,
                                     'Program_Status__c': "Active",
                                     'Primary_Compensation_Approver__c': approver_id,
                                     'Primary_Guideline_Approver__c': approver_id,
                                     'Secondary_Compensation_Approver__c': approver_id,
                                     'Secondary_Guideline_Approver__c': approver_id,
                                     'Primary_Clinical_Approver__c': '',
                                     'Secondary_Clinical_Approver__c': '',
                                     'Region__c': region,
                                     'echo_recruiter__c': echo_recruiter__c,
                                     'staffing_consultant__c': staffing_consultant__c,
                                     'Director_Perm_Recruiting__c': director_perm_recruiter_id,
                                     'Site_Code__c': site_code,
                                     'Accounting_ID__c': aconfig.accounting_id_code,
                                     'Hub_Connection__c': hub_id,
                                     'Privileging_Consultant__c': approver_id,
                                     'Privileging_Coordinator__c': approver_id,
                                     'Reappointment_Specialist__c':approver_id
                                     })

    ssl_id =  sfh.check_for_new_record(ssl_record, 'Sound_Service_Lines__c')

    return ssl_id, ssl_name


def begin_new_combined_ssl_record():

    lg.log_results("\nINFO: \t Begin creating " + str(aconfig.num_ssl_records) + " SOUND SERVICE LINE "
                                                                                 "records.")

    if not aconfig.all_account_ids:
        lg.log_results("INFO: \t Could not create Sound Service Line Records: Accounts Ids not found")
        return

    num_of_accounts = len(aconfig.all_account_ids)
    num_ssl_on_each_account = aconfig.num_ssl_records / num_of_accounts
    ssl_record_type_id = sfh.get_record_type_id(aconfig.ssl_rec_type, 'Sound_Service_Lines__c')
    if ssl_record_type_id == "":
        lg.log_results("FAIL: \t Could not get valid Record Type for Sound Service Line.")
        return
    else:
        for account_id in aconfig.all_account_ids:
            for s_service_line in aconfig.service_lines:
                loop_count = 0
                while(loop_count < int(num_ssl_on_each_account)):
                    ret_ssl_id, ret_ssl_name = create_new_snd_srvce_line(s_service_line, ssl_record_type_id,
                                                                         account_id, str(loop_count), aconfig.hub_id)
                    loop_count += 1 #aconfig.num_ssl_records,
                    if ret_ssl_id:
                        lg.log_results("PASS : \t Successfully created Sound Service Line record: " + ret_ssl_name)
                        aconfig.all_ssl_ids.append(str(ret_ssl_id))
                    else:
                        lg.log_results("FAIL: \t Could not create Sound Service record", "LIGHTRED_EX")

        lg.log_results("PASS: \t Sound Service Line Ids are : " + str(aconfig.all_ssl_ids))


    return
from Helpers import salesforce_helper as sfh
from Helpers import log_helper as lg
import sf_connection_config as sf_cc
from Helpers import ignore_login_data as ild
from datetime import datetime, timedelta as td
import random as r
import ui_config as uic


def begin_creating_privileging_file(all_candidate_ids, recordtype):
    i = 0
    all_pfile_names = []
    all_pfile_ids = []
    pfile_recordtype_id = sfh.get_record_type_id(recordtype, 'Privileging_Files__c')

    while i < len(all_candidate_ids):
        candidate_id = all_candidate_ids[i]
        # Only one site type, employment type (w2), and record type (w2 candidate)
        ret_pfile_id, ret_pfile_name = create_new_privileging_file(candidate_id, pfile_recordtype_id)
        if ret_pfile_id:
            all_pfile_names.append(ret_pfile_name)
            all_pfile_ids.append(ret_pfile_id)
            lg.log_results("PASS : \t Successfully created privileging file record: " + ret_pfile_name )
        else:
            lg.log_results("FAIL : \t Could not create privileging file record. ")
        return ret_pfile_id
        i += 1


def create_new_privileging_file(candidate_id, pfile_recordtype_id):
    pfile_id = ''
    pfile_name = ''
    now = datetime.now()
    ini_rec_type_id = '0120y000000YP65AAG'

    if pfile_recordtype_id == ini_rec_type_id:
        today_date_mdyy = now.strftime('%Y-%m-%d')
        today_plus_365d_mdyy = (now + td(365)).strftime('%Y-%m-%d')
    else:
        today_date_mdyy = (now + td(60)).strftime('%Y-%m-%d')
        today_plus_365d_mdyy = (now + td(425)).strftime('%Y-%m-%d')

    pfile_record = sf_cc.sf_connect.Privileging_Files__c.create({
        'RecordTypeId': pfile_recordtype_id,
        'Candidate__C': candidate_id,
        'Status__c': 'Privileges Issued',
        'Privileges_Effective_Date__c': today_date_mdyy,
        'Privileges_Expiration_Date__c': today_plus_365d_mdyy,
        'Kick_Off_Next_Date__c': today_date_mdyy,
        'File_Flag_Text__c' : 'Green'
    })
    pfile_id = str(pfile_record['id'])
    if pfile_id:
        pfile_dict = sf_cc.sf_connect.Privileging_Files__c.get(pfile_id)
        pfile_name = str(pfile_dict['Name'])

    return pfile_id, pfile_name


def update_priv_file_status(pfile_id):
    sf_cc.sf_connect.Privileging_Files__c.update(pfile_id, {'Status__c': 'Closed'})

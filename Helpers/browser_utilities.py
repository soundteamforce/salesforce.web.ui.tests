from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from Helpers import log_helper as lg
import datetime as d
from datetime import timedelta
import random as r
import string
import ui_config as cwuc
from Helpers import utilities as u
import time as t
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains as ac
from selenium.common.exceptions import TimeoutException


# User story #59810 - To enable or disable the screenshot feature.
def get_web_driver():
    drvr = webdriver.Chrome()
    return drvr


def click_elmt(xpath):
    try:
        element = WebDriverWait(cwuc.wd, cwuc.web_longer_wait).until(EC.element_to_be_clickable((By.XPATH, xpath)))
        element.click()
    # except TimeoutException as ex:
    #     cwuc.errors = cwuc.errors + str(ex)
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In click_elmt " + str(err.msg), 'LIGHTRED_EX')
        raise


def click_elmt_with_sleep(xpath, web_wait = cwuc.web_wait):
    try:
        sleep_for(1)
        element = WebDriverWait(cwuc.wd, web_wait).until(EC.element_to_be_clickable((By.XPATH, xpath)))
        element.click()
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: " + str(err.msg), 'LIGHTRED_EX')
        raise


def click_elmt_n_ss_b4_after(xpath):
    try:
        take_screenshot()
        element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.element_to_be_clickable((By.XPATH, xpath)))
        element.click()
        take_screenshot()
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In click_elmt_n_ss_b4_after " + str(xpath) + " error - "+ str(err.msg))
        raise

def send_keys_elmt_xpath(xpath, select_value):

    try:
        element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.presence_of_element_located((By.XPATH, xpath)))
        element.send_keys(select_value)
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        # lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        lg.log_results("\t \t FAIL: In send_keys " + str(xpath) + " error - "+ str(err.msg))
        raise


def find_elmt_send_keys_xpath(xpath, select_value):
    try:
        element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.visibility_of_element_located((By.XPATH, xpath)))
        element.send_keys(select_value)
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        # lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        lg.log_results("\t \t FAIL: In find_element_send_keys " + str(xpath) + " error - "+ str(err.msg))
        raise


def clear_elmt_text(xpath):
    try:
        element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.presence_of_element_located((By.XPATH, xpath)))
        element.clear()
    except Exception as err:
        # lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        # lg.log_results("\t \t FAIL: In click_elmt " + str(err.msg), 'LIGHTRED_EX')
        lg.log_results("\t \t FAIL: In click_elmt_text " + str(xpath) + " error - "+ str(err.msg))
        raise


def is_visible_xpath(element_xpath, web_wait = cwuc.web_wait):
    try:
        element = WebDriverWait(cwuc.wd, web_wait).until(EC.visibility_of_element_located((By.XPATH, element_xpath)))
        return element
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        # lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        lg.log_results("\t \t FAIL: In is_visible " + str(element_xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise

# def is_visible_xpath_wait_longer(element_xpath):
#
#     element = WebDriverWait(cwuc.wd, 30).until(EC.visibility_of_element_located((By.XPATH, element_xpath)))
#     return element


def is_not_visible_xpath(elemt_value):
    try:
        ret_boolean = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.invisibility_of_element_located((By.XPATH, elemt_value)))
        return ret_boolean
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In is_not_visible " + str(elemt_value) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise



def select_val_visible_elmt(xpath, select_text):
    try:
        element = Select(WebDriverWait(cwuc.wd, cwuc.web_longer_wait).until(EC.visibility_of_element_located((By.XPATH,xpath))))
        element.select_by_visible_text(select_text)
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In is_visible " + str(xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise

# def validate_text(xpath, req_text):
#
#     is_matching = False
#     element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.visibility_of_element_located((By.XPATH, xpath)))
#     text_val = str(element.text)
#     if req_text in text_val:
#         is_matching = True
#
#     return is_matching


def select_option_of_dropbox(xpath, index):
    # dropdown_element = Select(find_element_by_xpath(xpath))
    try:
        dropdown_element = Select(find_element_to_be_clickable(xpath))
        dropdown_element.select_by_index(index)
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In select_option " + str(xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise


def find_element_by_xpath(xpath):
    try:
        element = cwuc.wd.find_element_by_xpath(xpath)
        return element
    except Exception as err:
        # lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        lg.log_results("\t \t FAIL: In 'find_element_by_xpath' " + xpath + " error - " + str(err), 'LIGHTRED_EX')
        raise

def get_webelements(xpath):
    try:
        elements = cwuc.wd.find_elements_by_xpath(xpath)
        return elements
    except Exception as err:
        lg.log_results("\t \t FAIL: In find_elements_by_xpath " + str(xpath) + " error - " + str(err), 'LIGHTRED_EX')
        raise

def find_element_to_be_clickable(xpath):
    try:
        ret_value = WebDriverWait(cwuc.wd, cwuc.web_longer_wait).until(EC.element_to_be_clickable((By.XPATH, xpath)))
        return ret_value
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        # lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        # lg.log_results("\t \t FAIL: In click_elmt " + str(err), 'LIGHTRED_EX')
        lg.log_results("\t \t FAIL: In find_element_to_be_clicable " + str(xpath) + " error - "+ str(err.msg),
                       'LIGHTRED_EX')

    raise

    # def find_elmt_s_by_xpath(xpath):
#     elements = cwuc.wd.find_elements_by_xpath(xpath)
#     return elements


def is_invisible_elmt(xpath):
    try:
        element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.invisibility_of_element_located((By.XPATH, xpath)))
        return element
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error  ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In is_invisible " + str(xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise


# def presence_of_elemt(xpath):
#     element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.presence_of_element_located((By.XPATH, xpath)))
#     return element

def text_to_be_present_in_element_value(xpath, text_str):
    element =  WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.text_to_be_present_in_element_value((By.XPATH, text_str)))
    return element

def link_xpath_contains_text(text):
    xpath_str = "//a[contains(text(),'" + text + "')]"
    return xpath_str

# def link_xpath_contains_text_with_click(text):
#     click_elmt("//a[contains(text(),'" + text + "')]")

# def link_xpath_start_text(text):
#     xpath_str = "//a[starts-with(text(), '" + text + "')]"
#     return xpath_str

def get_text(xpath):
    try:
        element = WebDriverWait(cwuc.wd, cwuc.web_wait).until(EC.visibility_of_element_located((By.XPATH, xpath)))
        text_val = str(element.text)
        return text_val
    # except TimeoutException as ex:
    #     lg.log_results("\t \t FAIL: \t Time out error ", 'LIGHTRED_EX')
    #     raise
    except Exception as err:
        lg.log_results("\t \t FAIL: In get_text " + str(xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise


def take_screenshot():
    try:
        if cwuc.enable_screenshot:
            cwuc.wd.save_screenshot(lg.screenshot_path + next_num(cwuc.screen_num) + '.png')
    except Exception as err:
        lg.assert_and_log(err is None, "\t \t FAIL: " + str(err))
        raise


def next_num(number):
    number = int(number) + 1
    cwuc.screen_num = number
    return str(number)


def sleep_for(sleep_level):
        t.sleep(sleep_level)


def scroll_to(element_xpath):
    try:
        element = find_element_by_xpath(element_xpath)
        cwuc.wd.execute_script("return arguments[0].scrollIntoView(true);", element)
        take_screenshot()
    except Exception as err:
        lg.log_results("\t \t FAIL: In scroll_to " + str(element_xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise


def focus_to(element_xpath):
    try:
        element = find_element_by_xpath(element_xpath)
        cwuc.wd.execute_script("window.focus();", element)
        take_screenshot()
    except Exception as err:
        lg.log_results("\t \t FAIL: In focus_to " + str(element_xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise


def goto_page(url):
    cwuc.wd.get(url)


def get_current_url():
    return cwuc.wd.current_url

def execute_tab(element):
    element.send_keys(Keys.TAB)

def execute_escape(element):
    element.send_keys(Keys.ESCAPE)

def execute_shift_tab(element):
    element.send_keys(Keys.SHIFT, Keys.TAB)

def execute_enter(element):
    element.send_keys('keys.ENTER')


def num_of_tblrows(element):
    tr_num = element.find_element_by_tag_name("tr")
    return tr_num


def double_click(element_xpath):
    try:
        action_chains = ac(cwuc.wd)
        element = find_element_by_xpath(element_xpath)
        action_chains.double_click(element).perform()
    except Exception as err:
        lg.log_results("\t \t FAIL: In double_click " + str(element_xpath) + " error - "+ str(err.msg), 'LIGHTRED_EX')
        raise

# def get_row_number_from_text(tble_xpath, text_within):
#         index = 0
#         tbl_element = find_element_by_xpath(tble_xpath)
#         num_rows = num_of_tblrows(tbl_element)
#
#         while index < num_rows:
#             row_xpath = tble_xpath + "tr[" + str(index) + "]"
#             row_element = find_element_by_xpath(row_xpath)
#             if text_within in row_element.text:
#                 return index
#
#             index += 1


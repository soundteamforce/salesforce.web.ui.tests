
from Helpers import log_helper as lg
import datetime as d
from datetime import timedelta
import random as r
import string
import ui_config as cwuc
from sc_util import target_process_helper as tp


def get_random_number(digits):
    num_gen = 0
    if digits == 3:
        num_gen = r.randrange(1, 999)
    elif (digits == 4):
        num_gen = r.randrange(1, 9999)
    elif (digits == 10):
        num_gen = r.randrange(1, 9999999999)
    elif (digits == 1):
        num_gen = r.randrange(1, 9)
    elif (digits == 2):
        num_gen = r.randrange(1, 99)

    return num_gen


def get_random_chars(len):
    chars=string.ascii_uppercase
    ret_randm = ''.join(r.choice(chars) for x in range(len))

    return ret_randm


def get_hour_min():
    hhmm = d.datetime.now().strftime("%H%M")
    return str(hhmm)


def get_days_ago(no_of_days):
    today_is = d.datetime.today()
    date_to_calculate = today_is - timedelta(days=no_of_days)
    date_str = date_to_calculate.strftime('%Y-%m-%d')

    return date_str

def get_days_in_future(no_of_days):
    today_is = d.datetime.today()
    date_to_calculate = today_is + timedelta(days=no_of_days)
    date_str = date_to_calculate.strftime('%Y-%m-%d')

    return date_str


def set_test_status():
    test_result = ''
    if cwuc.errors == "" or cwuc.errors is None:
        test_result = 'PASSED'
    else:
        test_result = 'FAILED'

    file = open("C:\\salesforce_reports\\wizard_results.txt", "a+")
    file.write(cwuc.test_case_name + ' ' + test_result + '\n')
    file.close()

    if cwuc.run_target_process:
        tp.update_target_process(cwuc.test_cases, cwuc.test_case_name, cwuc.child_test_plan_run_id, str(cwuc.errors))

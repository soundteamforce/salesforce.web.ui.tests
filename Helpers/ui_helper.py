import sys
from Helpers import browser_utilities as bu
from Resources import element_locator as e
from Helpers import log_helper as lg
import ui_config

def search_on_top(search_text, search_category):
    bu.sleep_for(5)
    top_search = bu.find_element_by_xpath(e.home_search)
    top_search.send_keys(search_text)
    # lg.log_results("\t \t Searching for " + search_category + " : " + search_text, "LIGHTBLACK_EX")
    bu.click_elmt(e.home_search_button)
    # If the table has some rows, then data exists
    if 'user' in search_category:
        element_1_str = e.home_searched_user_link_str + search_text + "')]"
        bu.sleep_for(1)
        element_1 = bu.is_visible_xpath(element_1_str, ui_config.web_longer_wait)
        # bu.execute_tab(element_1)
    elif 'Provider Agreement' in search_category:
        element_1_str = e.home_searched_pa_link_str + search_text + "')]"
        element_1 = bu.is_visible_xpath(element_1_str, ui_config.web_longer_wait)
    elif 'Candidate' in search_category:
        lg.log_results("\t \t The Test candidate is " + search_text)
        bu.sleep_for(5)
        element_1 = bu.is_visible_xpath(e.home_candidate_search_link, ui_config.web_longer_wait)
        if not element_1:
            lg.log_results("\t \t Could not find test candidate: " + search_text)
        else:
            bu.click_elmt(e.home_candidate_search_link)
    else:
        element_1 = bu.is_visible_xpath(e.home_searched_pa_link, ui_config.web_longer_wait)

        lg.assert_and_log(element_1 is not None, "\t \t FAIL : The " + search_category + " : "
                          + search_text + " does not exist.")




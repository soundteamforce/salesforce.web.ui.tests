import os
from os import sys, path
import logging
import datetime as datetime
import time
import shutil
from threading import Thread
from Resources import test_variables as tv
from colorama import Fore
import ui_config as cwuc


localtime = datetime.datetime.now()
file_date = time.strftime('_%m-%d_%H-%M')
today_file = time.strftime('%b%d')

path = '../Logs/' + today_file + "/"

file_name = os.path.basename(sys.argv[0])

if not os.path.exists(path):
    os.mkdir(path)

str_file_name_nodot = path  + file_name + "_" + str(file_date)
str_file_name = str_file_name_nodot + '.log'
uniq_can_file_name =  str_file_name_nodot + '_uc.csv'

tv.source_file_name = str_file_name

logging.basicConfig(filename=str_file_name,filemode='w',level=logging.INFO, format='%(message)s', datefmt='%m/%d/%Y %I:%M %p')

screenshot_path = str_file_name_nodot + str(file_date) + "_screen"


def log_results(log_msg, colorname='LIGHTBLACK_EX'):
    color = getattr(Fore, colorname)
    logging.info(log_msg)
    print(color + log_msg)

    return


def assert_and_log(assert_statement, error_message, pass_msg=None):
    if not assert_statement:
        cwuc.errors += error_message    # for target process document
        log_results(error_message, 'LIGHTRED_EX')           # adding it to log file
    elif pass_msg:
        log_results(pass_msg, 'GREEN')           # adding success in log file

    assert assert_statement, error_message      # ddt checking



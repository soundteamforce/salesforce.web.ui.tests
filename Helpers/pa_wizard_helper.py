import ui_config as cwuc
from Helpers import browser_utilities as bu
from Resources import element_locator as e
import random as r
from Helpers import log_helper as lg
from selenium.webdriver.support.ui import Select
from Resources import test_variables as tv
from Helpers import salesforce_helper as sh

def add_at_details(each_term):
    # def add_at_details(at_category, at_type, at_description, at_start_date, at_end_date):
    #Added these lines whihc for new code implementation
    at_category = each_term['at_category']
    at_type = each_term['at_type']
    at_type_append_str = ''


    w_agreement_term_type = "-".join(filter(bool, (at_category + " - " + at_type, at_type_append_str)))
    if 'at_description' in each_term.keys(): add_str = " with description " + each_term['at_description']
    else: add_str = " "

    lg.log_results("\t \t Adding Agreement type " + w_agreement_term_type + add_str)
    buy_out_exists = False
    bu.sleep_for(1)

    dropdown_element = Select(bu.find_element_by_xpath(e.w_wizard_main_at_type_input))
    if 'Buy Out'.casefold() in cwuc.new_pa_type.casefold():
        lg.assert_and_log('Buy Out'.casefold() in at_type.casefold(),
                          "\t \t FAIL: The invalid case of '" + at_type + "' under '" + cwuc.new_pa_type + "' type provider "
                    "agreement. No error flagging needed.", "\t \t PASS: 'Buy Out' Term exists under 'Buy Out' Provider "
                            "Agreement. ")
        buy_out_exists = True

    else:
        if 'Buy Out'.casefold() in at_type.casefold():
            for option in dropdown_element.options:
                if 'Buy Out'.casefold() in option.text.casefold():
                    buy_out_exists = True
            # sh.delete_pa(0)
            tv.exe_changes_btn_on = False

            lg.assert_and_log(buy_out_exists is False, "\t \t FAIL: The 'Buy Out' agreement term should NOT exist in '"
                              + cwuc.new_pa_type
                              + "' type provider agreement", "\t \t PASS: The 'Buy Out' agreement term does NOT exist in '" +
                              cwuc.new_pa_type +
                              "' type provider agreement as expected")
            return

    bu.click_elmt(e.w_wizard_main_at_type_input)
    bu.sleep_for(1)
    dropdown_element.select_by_value(w_agreement_term_type)
    bu.sleep_for(2)
    bu.take_screenshot()

    if 'Visa Type' in each_term['type__c'] :
        if 'at_type' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_description_visa_input, each_term[
            'Visa_Type__c'])
    if 'APP Clinical Ladder Tier' in each_term['at_type']:
        bu.send_keys_elmt_xpath(e.w_wizard_type_input, each_term['type__c'])
        bu.send_keys_elmt_xpath(e.w_wizard_description_input_orig, each_term['at_description'])

    else:
        if 'at_description' in each_term.keys():
            if 'Stipend' in each_term['at_description']:
                bu.send_keys_elmt_xpath(e.w_wizard_description_input_relpath, each_term['at_description'])
                # desc_ele_xpath = "//div[@id='rigthSectionDiv']//table/tbody[1]/tr[4]/td[2]"
                # desc_text = bu.get_text(desc_ele_xpath)
                # if 'Stipend' in desc_text:
                #     if bu.is_visible_xpath(desc_ele_xpath + "//select[1]"):
                #         bu.send_keys_elmt_xpath(desc_ele_xpath + "//select[1]", each_term['at_description'])
                # if bu.is_visible_xpath(e.w_wizard_description_input_pop):
                #     bu.send_keys_elmt_xpath(e.w_wizard_description_input_pop, each_term['at_description'])
                # else:
                #     bu.send_keys_elmt_xpath(e.w_wizard_description_input_orig, each_term['at_description'])
            else:
                bu.send_keys_elmt_xpath(e.w_wizard_description_input_orig, each_term['at_description'])


    if 'at_amount' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_amount_input, each_term[
        'at_amount'])
    if 'start_date' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_start_date_input, each_term['start_date'])
    if 'end_date' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_end_date_input, each_term['end_date'])
    if 'at_interval' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_stipend_interval_input, each_term['at_interval'])
    if 'at_schedule' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_schedule_input, each_term['at_schedule'])
    if 'at_payback_schedule' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_payback_schedule_input, each_term['at_payback_schedule'])
    if 'at_period' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_period_dropbox_input, each_term['at_period'])
    if 'at_quantity' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_quantity_input, each_term['at_quantity'])
    if 'at_shift_hours' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_shift_hours_input, each_term['at_shift_hours'])
    if 'at_threshold_type' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_threshold_type_dd_input, each_term['at_threshold_type'])
    if 'at_threshold_min' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_threshold_min_input, each_term['at_threshold_min'])
    if 'at_threshold_max' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_threshold_max_input, each_term['at_threshold_max'])
    # if 'at_time_off_type' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_t, each_term['at_time_off_type'])
    if 'at_workload' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_workload_input, each_term['at_workload'])
    if 'at_forgiveness_period' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_forgiveness_period_input, each_term['at_forgiveness_period'])
    if 'at_shift_level' in each_term.keys(): bu.send_keys_elmt_xpath(e.w_wizard_shift_level_input, each_term['at_shift_level'])

    bu.sleep_for(1)
    bu.click_elmt_n_ss_b4_after(e.w_add_updated_btn)
    bu.sleep_for(3)
    return



def check_at_interval(terms_dictionary):
    current_term_name = ''
    ret_th_interval = ''
    for x, each_term_d in terms_dictionary.items():
            #test_terms_dictionary = {0: {'test_term': tv.MLDAY, 'expected_interval':tv.thrhld_interval_quarterly}}
            #Get the ATs for current PA and then filter out for the type/description we will use to check for interval
            is_cancel = ''
            term_values = each_term_d['test_term']
            additional_filter_criteria = " and short_code__c = '" + term_values['at_short_code'] + "' and type__c = " \
                                         "'" + term_values['type__c'] + "'"

            at_name_dict, at_id_dict = sh.get_at_ids_names(cwuc.new_pa_name, is_cancel, additional_filter_criteria)
            if len(at_name_dict) == 1:
                current_term_name = at_name_dict[0]
                if current_term_name:
                    ret_th_interval = sh.get_term_threshold_interval(cwuc.new_pa_name, current_term_name)
                    lg.assert_and_log(ret_th_interval.casefold() in str(each_term_d['expected_interval']).casefold(),
                'FAIL: \t \t The expected threshold interval is ' + str(each_term_d['expected_interval']) + ' but '
                    'found ' + str(ret_th_interval), '\t \t PASS:\t The threshold interval is set correctly as ' \
                    + str(ret_th_interval))
            else:
                lg.log_results('\t \t FAIL: \t Did not find Only one term for Provider Agreement ' +
                               str(cwuc.new_pa_name)  + ' with terms ' + str(at_name_dict))

    return

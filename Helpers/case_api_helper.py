from Helpers import salesforce_helper as sfh
from Helpers import utilities as u
import sf_connection_config as sf_cc
from Helpers import log_helper as lg
import api_config as aconfig
from Resources import test_variables as tv

#AccountId, ATG_Case_Sub_Status__c, Description,Position_Number__c, RecordTypeId, Request_Category__c, Status, Subject

def create_case_aguideline(account_id, pos_id):

    case_record_type_id = sfh.get_record_type_id('Employment Event', 'Case')
    case_id = ''
    case_name = ''

    case_record = sf_cc.sf_connect.Case.create({'AccountId': account_id
                                            ,'ATG_Case_Sub_Status__c': 'In Progress'
                                            ,'Description': 'Description - Automation Creation of the Case'
                                            ,'Position_Number__c': pos_id
                                            ,'RecordTypeId': case_record_type_id
                                            ,'Request_Category__c': 'Position Guideline Updates'
                                            ,'Status': 'New'
                                            ,'Subject': 'Automation Creation of the Case'
                                            })
    if isinstance(case_record,dict):
        if 'id' in case_record.keys():
            case_id = str(case_record['id'])
            case_dict = sf_cc.sf_connect.Case.get(case_id)
            case_name = str(case_dict['Name'])
    elif isinstance(case_record, list):
        case_record_dict = case_record[0]
        if 'errorCode' in  case_record_dict:
            lg.log_results("\t \t FAIL - Could not create Hub Managed Agreement record " + \
                           str(case_record_dict), 'LIGHTRED_EX')

    return case_id, case_name

from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
from Pages import employment_requirement as pg_emp_req
import ui_config as uic
from Helpers import salesforce_helper as sh
from Pages import comp_login as cl
from Helpers import login_as_user_with_search as lauw_search
from Helpers import ui_helper
from Resources import element_locator as e

def create_emp_req(emp_req_types):
    lg.log_results("\t \t Creating Employement Requirement.")

    # check_privilaging_consult_usr_on_ssl()
    lauw_search.login_as_with_search(uic.compliance_admin_user, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')
    uic.position_name = bu.get_text(e.ce_er_position_name)
    uic.position_id = sh.get_pos_id(uic.position_name)
    bu.sleep_for(2)
    bu.click_elmt(e.ce_er_position_name)    # position element
    bu.sleep_for(1)
    for each_emp_req_type in emp_req_types:
        # if bu.is_visible_elmt(e.ce_er_new_er_button):
        bu.click_elmt(e.ce_er_new_er_button)
        bu.send_keys_elmt_xpath(e.ce_er_record_type, each_emp_req_type)
        bu.click_elmt_n_ss_b4_after(e.ce_er_continue_button)
        return_value, position_xpth_str = pg_emp_req.create_new_emp_req(each_emp_req_type)
        bu.sleep_for(1)
        if 'error' in return_value:
            return return_value

        bu.click_elmt(position_xpth_str)
        # else:
        #     lg.log_results("\t \t Could not create employment requirement " + each_emp_req_type)

    # Add db call for empl req for all three
    emp_records = sh.get_all_er_records()
    lg.log_results("\t \t Employment records are " + str(emp_records))
    cl.log_off()


def update_employment_dates(eff_date, end_date):
    lg.log_results("\t \t Updating Employement Requirement dates, Effective Date will be " + str(eff_date) + " end "
        "date will be " + str(end_date) )
    ret_value = sh.update_er_date(eff_date, end_date)
    if not ret_value:
        lg.log_results("\t \t Updated Employment Requirement.")
    elif ret_value == 204:
        lg.log_results("\t \t Successfully updated Employment Requirement")
    else:
        lg.log_results("\t \t Couldn't update Employment Requirement")



import sys
from Helpers import log_helper as lg
from Resources import element_locator as e
import ui_config as cwuc
from Helpers import browser_utilities as bu
from Helpers import comp_wizard_bvt_helper as comp_bvt_h
from Pages import comp_login as cl

def login_as_with_search(login_as_uname, search_category):

    lg.log_results("\t \t Logging in with search for " + login_as_uname + " " + search_category )
    # Check if the logged in user is same as user asked to be logged in. If so, do nothing.
    if 'user' in search_category:
        bu.sleep_for(2)
        page_user_fullname = bu.find_element_by_xpath("//a[@id='globalHeaderNameMink']//span[contains(text(),"
                                                           "login_as_uname)]").text

        if not login_as_uname.casefold() in page_user_fullname.casefold():
            bu.sleep_for(2)
            element_xpath = comp_bvt_h.search_on_top(login_as_uname, "user")
            bu.sleep_for(1)
            bu.scroll_to(e.home_user_div_table_list)
            bu.click_elmt(element_xpath)
            bu.click_elmt(e.home_search_down_arrow)
            bu.click_elmt(e.home_search_page_arrow_user_Detail)
            bu.sleep_for(1)
            lgn_btn = bu.is_visible_xpath(e.home_login_as_user_login_btn)
            lg.assert_and_log(lgn_btn, "\t \t FAIL: Couldn't Login")
            bu.click_elmt(e.home_login_as_user_login_btn)
            cl.switch_to_classic()





# def login_as_with_search(login_as_uname, search_category):
#
#     lg.log_results("\t \t Logging in with search for " + login_as_uname + " " + search_category )
#     # Check if the logged in user is same as user asked to be logged in. If so, do nothing.
#     bu.sleep_for(2)
#     if 'user' in search_category:
#         element_xpath = comp_bvt_h.search_on_top(login_as_uname, "user")
#         bu.scroll_to(e.home_user_div_table_list)
#         bu.click_elmt(element_xpath)
#         bu.click_elmt(e.home_search_down_arrow)
#         bu.click_elmt(e.home_search_page_arrow_user_Detail)
#         bu.sleep_for(1)
#         # bu.is_visible_xpath(e.home_login_as_user_login_btn)
#         bu.click_elmt(e.home_login_as_user_login_btn)
#         cl.switch_to_classic()

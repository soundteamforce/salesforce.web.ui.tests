import datetime
from Resources import element_locator as e
import case_config as c
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
from Resources import case_variables as cv
from Helpers import  salesforce_helper as sh
from Helpers import ignore_login_data as ild
import sf_connection_config as sf_cc
import ui_config as cwuc
from Resources import test_variables as tv

case_subject = cv.case_subject + c.today_datetime
case_description = 'AutoRegression - Description - This is new case. ' + c.today_datetime


def create_new_case(record_type, request_category):

    try:
        bu.sleep_for(1)
        lg.log_results("\t \t Creating New Case with Request Category '" + str(request_category) + "' ",
                       "LIGHTBLACK_EX")
        bu.click_elmt(e.cases_tab_xpath)
        bu.click_elmt(e.new_case_btn)
        bu.select_val_visible_elmt(e.new_case_record_type, record_type)
        bu.click_elmt(e.continue_btn)

        bu.sleep_for(1)
        bu.send_keys_elmt_xpath(e.new_case_subject_in, case_subject)
        bu.select_val_visible_elmt(e.new_case_req_catergory_drpdwn, request_category)
        bu.send_keys_elmt_xpath(e.new_case_due_date, c.today_date)
        bu.select_val_visible_elmt(e.new_case_status_drpdwn, cv.case_status_new)
        bu.select_val_visible_elmt(e.new_case_priority_drpdwn, cv.case_priority_medium)
        bu.send_keys_elmt_xpath(e.new_case_ini_description, case_description)
        position_name = get_position_name()
        bu.send_keys_elmt_xpath(e.new_case_ini_position,  position_name)
        bu.send_keys_elmt_xpath(e.new_case_ini_eff_date, c.today_date)
        bu.sleep_for(1)
        bu.click_elmt(e.new_case_save_btn)
        page_title = bu.get_text("//h1[@class='pageType']")
        if 'Edit' in page_title:
            text_present = bu.get_text("//div[@id='errorDiv_ep']")
            lg.assert_and_log('Error' not in text_present, '\t \t Could not create new Case. ', '\t \t Created Case.' )
        elif 'Case' in page_title:
            cv.case_number = bu.get_text("//h2[@class='pageDescription']")
            lg.assert_and_log(cv.case_number is not None, "\t \t Case Number is Invalid. " , "\t \t Case Number is " +
            cv.case_number)
            # verify_case_page(request_category)

    except TimeoutError as ex:
        lg.log_results("\nFAIL :\t\t Error on new case creation. " + str(ex), 'RED')

    return



def verify_case_page(req_category):
    try:
        bu.sleep_for(1)
        case_created_by = "//div[@id='CreatedBy_ileinner']//a[contains(text(),'" + cwuc.user_rdo_name + "')]"

        # if bu.is_not_visible_xpath(case_created_by):
        # bu.click_elmt("//img[@title='Show Section - System Information']")
        page_created_by = bu.get_text(case_created_by)
        case_owner_queue = bu.get_text("//div[@id='cas1_ileinner']//a[contains(text(),'Queue')]")
        case_owner_queue_sub = case_owner_queue.replace('Queue - Agreement Term ', '')
        req_category_sub = req_category.replace('Position', '')

        lg.assert_and_log(cwuc.user_rdo_name in page_created_by, "\t \t FAIL : RDO does not match. ", "\t \t PASS: RDO "
                                                                                                      "matches.")
        lg.assert_and_log(case_owner_queue_sub in req_category_sub, "\t \t FAIL : Case is in correct Queue.",
                          "\t \t PASS: Case Owner is correct.")

        return
    except TimeoutError as ex:
        lg.log_results("\t \tFAIL :\t Verify case creation. ", 'RED')



def get_position_name():
    created_by_admin_id = sh.get_user_id(sf_cc.uname)
    pos_name = sh.get_position_name(created_by_admin_id)

    return pos_name



def get_user_id(logged_uname):
    result_name = c.sf_connect.query("SELECT Id FROM user where USERNAME = '" + logged_uname + "' LIMIT 1" )
    user_record = result_name['records']
    if user_record:
        user_dict = user_record[0]
        user_id = user_dict['Id'].encode('ascii', 'ignore')



    return user_id





def get_contact_name(uname_id):
    ret_contact_name = ""
    contact_query = "Select Name from Contact WHERE CreatedById = '" + uname_id.decode() + "'  LIMIT 1"
    result_name = c.sf_connect.query(contact_query)
    records = result_name['records']
    if records:
        contact_dict = records[0]
        ret_contact_name = contact_dict['Name']

    return ret_contact_name

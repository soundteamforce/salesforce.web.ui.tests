from Helpers import log_helper as lg
import api_config as acnfg
import sf_connection_config as sf_cc


def start_script():
    lg.log_results("\n\t START TIME: \t " + acnfg.current_time)
    lg.log_results("INFO: Begin Creating Salesforce records in test environment - " + sf_cc.test_env)
    return

def end_script():
    lg.log_results("\n\t END TIME: \t " + acnfg.current_time)
    return


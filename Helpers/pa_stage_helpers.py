import ui_config as cwuc
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Pages import provider_agreement as pa
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
from Resources import element_locator as e
from Helpers import salesforce_helper as sh
from Resources import test_variables as tv
from Pages import comp_login as cl

def pa_stage_value(pa_name, expected_pa_stage):
    w_bvt_hlpr.search_on_top(pa_name, 'Provider Agreement')
    pa.navigate_to_provider_agreement_page()
    bu.sleep_for(5)
    pa_stage_name = bu.get_text(e.pa_stage)
    lg.assert_and_log(pa_stage_name == expected_pa_stage, '\t \t FAIL: Expected ' + expected_pa_stage + 'but found ' + pa_stage_name,
                      '\t \t PASS: PA Stage is: ' + pa_stage_name)
    return

def change_pa_stage(pa_stage_to_value, is_log_off):
    try:
        w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
        pa.navigate_to_provider_agreement_page()
        bu.double_click(e.pa_stage_to_dblcl)
        bu.select_val_visible_elmt(e.pa_select_stage_dropdown, pa_stage_to_value)
        bu.click_elmt(e.pa_save_inline_btn)
        updated_stage_val_on_page = bu.get_text(e.pa_stage_td)
        lg.assert_and_log(updated_stage_val_on_page == pa_stage_to_value,
                          '\t \t Error in changing the stage value to ' + pa_stage_to_value,
                          '\t \t Stage value changed successfully as: ' + updated_stage_val_on_page)
        bu.sleep_for(5)
        if is_log_off:
            cl.log_off()

    except Exception as err:
        lg.log_results("\t \t FAIL : Change PA stage value " + str(err), 'LIGHTRED_EX')

    return

def change_pa_stage_value(update_to_stage_value):

    w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
    pa.navigate_to_provider_agreement_page()
    stage_value_element = bu.find_element_by_xpath(e.pa_stage_td)
    existing_stage_value = stage_value_element.text

    logged_user = bu.find_element_by_xpath(e.home_usr_full_name).text
    user_profile_ret = sh.usr_profile_value(logged_user)
    if "Administrator" in user_profile_ret:
        # edit_stage = bu.is_visible_xpath(e.pa_stage_to_dblcl)     why is it bombing?
        # if edit_stage:
        if existing_stage_value:
            bu.double_click(e.pa_stage_to_dblcl)
            bu.select_val_visible_elmt(e.pa_select_stage_dropdown, update_to_stage_value)
            bu.click_elmt(e.pa_save_inline_btn)
            bu.sleep_for(3)
            bu.take_screenshot()
            updated_pa_stage_element = bu.find_element_by_xpath(e.pa_stage_td)
            updated_pa_stage_value = updated_pa_stage_element.text
            validate_admin_stage_update(existing_stage_value, update_to_stage_value)
    elif user_profile_ret in ['Staffing Admin', 'Staffing Services Team', 'ECHO Recruiting']:
        validate_non_admin_stage_update(existing_stage_value, update_to_stage_value, user_profile_ret)
    else:
        validate_non_admin_stage_update(existing_stage_value, update_to_stage_value)



def validate_admin_stage_update(from_stage_val, update_to_stage_value):
        # Check if the updated value is as expected. AND  Check if the user profile allows this update.
    lg.assert_and_log(from_stage_val != update_to_stage_value, '\t \t Error in changing the stage value from ' +
                      from_stage_val
                      + ' to ' + update_to_stage_value, '\t \t Stage value changed successfully as: ' +
                      update_to_stage_value)


def validate_non_admin_stage_update(from_stage_value , updt_to_stage_value, user_profile_ret = None):
    # Check if the record is locked..
    stage_value_fae_fas = [tv.pa_stage_value_fae, tv.pa_stage_value_fas]
    rest_stage_values = [tv.pa_stage_approvd_value, tv.pa_stage_new_value, tv.pa_stage_app_reqtd_value]
    non_admin_msg_1 = 'Non Admin User changed stage value from '
    non_admin_msg_2 = 'Non admin user could not change stage value from '
    from_to_to_msg = from_stage_value + ' to ' + updt_to_stage_value


    bu.click_elmt(e.pa_edit_pa_record_btn)
    element = bu.find_element_by_xpath(e.pa_error_body)
    if 'Record Locked' in element.text:
        lg.assert_and_log(0 == 0, "\t \t FAIL : " + non_admin_msg_1 + from_to_to_msg ,
                          "\t \t PASS : " + non_admin_msg_2 + from_to_to_msg)
        bu.click_elmt(e.pa_record_lock_to_back)
    else:
        bu.click_elmt(e.pa_stage_field)
        bu.send_keys_elmt_xpath(e.pa_stage_select, updt_to_stage_value)
        bu.click_elmt(e.pa_save_button)
        bu.sleep_for(3)

        if 'Error: Invalid Data' in bu.find_element_by_xpath(e.pa_error_body).text:
            lg.assert_and_log(bu.is_visible_xpath(e.pa_div_error_msg), "\t \t FAIL : " + non_admin_msg_1 + from_to_to_msg
            , "\t \t PASS : Expected Message: " + bu.find_element_by_xpath(e.pa_div_error_msg).text)
            bu.click_elmt(e.pa_cancel_button)
        else:
            # Check if the value is updated on the page
            stage_val_on_page = bu.find_element_by_xpath(e.pa_stage_td).text
            if updt_to_stage_value in stage_val_on_page:        # Non Admin Could update the value
                if user_profile_ret in ['Staffing Admin', 'Staffing Services Team', 'ECHO Recruiting'] and from_stage_value in rest_stage_values:
                    lg.log_results('\t\t PASS: Non Admin could successfully update the stage value to ' + tv.pa_stage_value_dar)

                elif from_stage_value in tv.pa_stage_value_fas:
                    lg.assert_and_log(updt_to_stage_value in rest_stage_values,  "\t \t FAIL : " + non_admin_msg_2 + from_to_to_msg,
                                      "\t \t PASS : " + non_admin_msg_1 + from_to_to_msg  + " successfully.")
                elif from_stage_value in tv.pa_stage_value_fae:
                    lg.assert_and_log(updt_to_stage_value in rest_stage_values, "\t \t FAIL : " +  non_admin_msg_1 +
                                      from_to_to_msg,  "\t \t PASS : " + non_admin_msg_2 + from_to_to_msg + " successfully.")
                elif from_stage_value in rest_stage_values:
                    lg.assert_and_log(updt_to_stage_value in stage_value_fae_fas, "\t \t FAIL : " + non_admin_msg_1 + from_to_to_msg,
                                      "\t \t PASS : " + non_admin_msg_2 + from_to_to_msg + " successfully.")

            elif updt_to_stage_value in stage_val_on_page:      # Could not update the value but no errors either..
                    lg.assert_and_log(0 == 0, 'Fail - CHECK HERE  ', "PASS :CHECK HERE  ")


def recall_pa_approval():
    bu.sleep_for(3)
    w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
    pa.navigate_to_provider_agreement_page()
    bu.sleep_for(5)
    bu.scroll_to(e.pa_recall_btn)
    bu.click_elmt(e.pa_recall_btn)
    bu.send_keys_elmt_xpath(e.pa_recall_comnts, 'Automation Test Recall')
    bu.click_elmt(e.pa_recall_apvl_req)


def validate_pa_record_type(pa_name, exp_rctype_val):
    pa_rec_type_id = sh.get_pa_rec_type_id(pa_name)
    pa_rec_type_name = sh.get_ce_recordtype_name(pa_rec_type_id, 'Provider_Agreement__c')
    lg.assert_and_log(pa_rec_type_name == exp_rctype_val, '\t \t FAIL: Expected ' + exp_rctype_val + 'but found ' + pa_rec_type_name,
                      '\t \t PASS: PA Record type is: ' + pa_rec_type_name)


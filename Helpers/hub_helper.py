from Helpers import log_helper as lg
from Helpers import salesforce_helper as sfh
import random as r
from Helpers import utilities as u
import sf_connection_config as sf_cc
from Resources import test_variables as tv
import api_config as aconfig
import random
def create_hub_record():

    if not aconfig.create_hub:
        return

    lg.log_results("\nINFO: \t TeleMedicine Hub - Begin creating SSL HUB records.")
    if aconfig.populate_all_hub_fields:
        hub_ids, hub_names = get_ids_for_hub()
        lg.log_results(str(hub_ids))

    created_by_admin_id = sfh.get_user_id(sf_cc.uname)
    hub_name_code = tv.today_date_mmdd + "-" + str(u.get_random_number(2))
    hub_name = 'AutoHub' + str(hub_name_code)
    if aconfig.populate_all_hub_fields is False:
        hub_record = sf_cc.sf_connect.Hub__c.create({'Name': hub_name,
                                                     'Start_Date__c': str(tv.today_date_mdyy_to_sec),
                                                     # 'Privileging_Coordinator__c': '00534000009iV8oAAE',
                                                     # 'Privileging_Consultant__c': '00534000009R8txAAC',
                                                    'Service_Line__c': 'Telemedicine'
                                                    })
    else:
        hub_record = sf_cc.sf_connect.Hub__c.create({'Name': hub_name,
                                                     'Start_Date__c': str(tv.today_date_mdyy_to_sec),
                                                     'Service_Line__c': 'Telemedicine',
                                                     # 'Sound_Employing_Legal_Entity__c':	hub_ids['Sound_Employing_Legal_Entity'],
                                                     'Sound_Employing_DBA__c':	hub_ids['Sound_Employing_DBA'],
                                                     'National_Clinical_Leader__c':	hub_ids['National_Clinical_Leader'],
                                                     'Echo_Recruiter__c':	hub_ids['Echo_Recruiter'],
                                                     'National_Operations_Leader__c':	hub_ids['National_Operations_Leader'],
                                                     'People_Support_Generalist__c':	hub_ids['People_Support_Generalist'],
                                                     'Operations_Vice_President__c':	hub_ids['Operations_Vice_President'],
                                                     'Staffing_Consultant__c':	hub_ids['Staffing_Consultant'],
                                                     'Clinical_Program_Lead__c': hub_ids['Clinical_Program_Lead'],
                                                     # 'Staffing_Coordinator__c':	hub_ids['Staffing_Coordinator'],
                                                     'Program_Manager__c': hub_ids['Program_Manager'],
                                                     'Staffing_Coordinator_Support__c': hub_ids['Staffing_Coordinator_Support'],
                                                     'Analyst__c': hub_ids['Analyst'],
                                                     'Privileging_Coordinator__c': hub_ids['Privileging_Coordinator'],
                                                     'Finance_Manager__c': hub_ids['Finance_Manager'],
                                                     'Privileging_Consultant__c':	hub_ids['Privileging_Consultant'],
                                                     'Program_Close_Manager__c':	hub_ids['Program_Close_Manager'],
                                                     'Sr_Director_Perm_Recruiting__c': hub_ids['Sr_Director_Perm_Recruiting'],
                                                     'Program_Launch_Manager__c':	hub_ids['Program_Launch_Manager'],
                                                     'Director_Perm_Recruiting__c':	hub_ids['Director_Perm_Recruiting'],
                                                     'Perm_Recruiter__c':	hub_ids['Perm_Recruiter'],
                                                     'Talent_Scout__c':			'Nick Barone',
                                                     'Staffing_Score__c':			'50',
                                                     'Group_Size_Chief__c':			'1.0',
                                                     'Credentialing_Tier__c':		'2',
                                                     'Group_Size_MD__c':			'4.0',
                                                     'Perm_Recruitability_Rating__c':	'Tier 1',
                                                     'Group_Size_APP__c':			'4.0',
                                                     # 'Group_Size_Total__c':	'9.00', #INVALID_FIELD_FOR_INSERT_UPDATE
                                                     'Daily_Coverage__c':			'12-15 Patients',
                                                     'Staffing_Scheduling_Details__c':	'7 on, 7 off',
                                                     'Procedures__c':				'n/a',
                                                     'Duties__c':					'review patient concerns, recommend course of action',
                                                     'Interview_Process__c':		'Meet with telemedicine leadership & hospitalist team at site',
                                                     'Search_Barriers__c':			'Must wear pants',
                                                     'Perm_Recruiting_Notes__c':	 'Needs wifi',
                                                     'Approved_Day_Rate_HR__c':		'130',
                                                     'Approved_Call_Rate_HR__c':	'140',
                                                     'Approved_Night_Rate_HR__c':	'150',
                                                     'Approved_Other_Compensation__c':	'will cover cost of wifi',
                                                     'Echo_Recruiting_Notes__c':	'3 years post grad'
                                                     })

    aconfig.hub_id =  sfh.check_for_new_record(hub_record, 'Hub__c')
    if aconfig.hub_id:
        h_dict = sf_cc.sf_connect.Hub__c.get(aconfig.hub_id)
        aconfig.hub_name = str(h_dict['Name'])
    lg.assert_and_log(aconfig.hub_id is not None, 'FAIL: \t Could Not create Telemedicine Hub. ',
                  "PASS: \t TeleMedicine Hub Created Id - '" + aconfig.hub_id + "' And Name - '" + aconfig.hub_name +
                      "'" )
    return

#

def get_ids_for_hub():

    hub_ids = {}
    hub_names = {}
    # hub_ids['Sound_Employing_Legal_Entity'], hub_names['Sound_Employing_Legal_Entity'] = sfh.get_user_id_per_profile('ECHO Recruiting')
    hub_ids['Sound_Employing_DBA'], hub_names['Sound_Employing_DBA'] = sfh.get_user_id_per_profile('Sound_Employing_DBA__c')
    hub_ids['National_Clinical_Leader'], hub_names['National_Clinical_Leader'] = sfh.get_user_id_per_profile('National_Clinical_Leader__c')
    hub_ids['Echo_Recruiter'], hub_names['Echo_Recruiter'] = sfh.get_user_id_per_profile('Echo_Recruiter__c')
    hub_ids['National_Operations_Leader'], hub_names['National_Operations_Leader'] = sfh.get_user_id_per_profile('National_Operations_Leader__c')
    hub_ids['People_Support_Generalist'], hub_names['People_Support_Generalist'] = sfh.get_user_id_per_profile('People_Support_Generalist__c')
    hub_ids['Operations_Vice_President'], hub_names['Operations_Vice_President'] = sfh.get_user_id_per_profile('Operations_Vice_President')
    hub_ids['Staffing_Consultant'], hub_names['Staffing_Consultant'] = sfh.get_user_id_per_profile('Staffing_Consultant__c')
    hub_ids['Clinical_Program_Lead'], hub_names['Clinical_Program_Lead'] = sfh.get_user_id_per_profile('Clinical_Program_Lead__c')
    # hub_ids['Staffing_Coordinator'], hub_names['Staffing_Coordinator'] = sfh.get_user_id_per_profile('Staffing_Coordinator__c')
    hub_ids['Program_Manager'], hub_names['Program_Manager'] = sfh.get_user_id_per_profile('Program_Manager__c')
    hub_ids['Staffing_Coordinator_Support'], hub_names['Staffing_Coordinator_Support'] = sfh.get_user_id_per_profile('Staffing_Coordinator_Support__c')
    hub_ids['Analyst'], hub_names['Analyst'] = sfh.get_user_id_per_profile('Analyst__c')
    hub_ids['Privileging_Coordinator'] , hub_names['Privileging_Coordinator'] = sfh.get_user_id_per_profile(
        'Privileging_Coordinator__c')
    hub_ids['Finance_Manager'], hub_names['Finance_Manager'] = sfh.get_user_id_per_profile('Finance_Manager__c')
    hub_ids['Privileging_Consultant'], hub_names['Privileging_Consultant'] = sfh.get_user_id_per_profile('Privileging_Consultant__c')
    hub_ids['Program_Close_Manager'] , hub_names['Program_Close_Manager']= sfh.get_user_id_per_profile('Program_Close_Manager__c')
    hub_ids['Sr_Director_Perm_Recruiting'], hub_names['Sr_Director_Perm_Recruiting'] = sfh.get_user_id_per_profile('Sr_Director_Perm_Recruiting__c')
    hub_ids['Program_Launch_Manager'], hub_names['Program_Launch_Manager'] = sfh.get_user_id_per_profile('Program_Launch_Manager__c')
    hub_ids['Director_Perm_Recruiting'] , hub_names['Director_Perm_Recruiting']= sfh.get_user_id_per_profile('Director_Perm_Recruiting__c')
    hub_ids['Perm_Recruiter'], hub_names['Perm_Recruiter'] = sfh.get_user_id_per_profile('Perm_Recruiter__c')

    return hub_ids, hub_names

def update_hub_for_future_anchor_site():
    return_value = False

    hub_anchor_q = "SELECT Id FROM Sound_Service_Lines__c WHERE Hub_Connection__c = '"+ aconfig.hub_id + "'"
    result_name = sf_cc.sf_connect.query(hub_anchor_q)
    ssl_id_rec = result_name['records']
    if ssl_id_rec:
        anchor_ssl_id = random.choice(ssl_id_rec)
        anchor_ssl_id = anchor_ssl_id['Id']
        if anchor_ssl_id:
            ret_response = sf_cc.sf_connect.Hub__c.update(aconfig.hub_id, {'Future_Anchor_Site__c' : anchor_ssl_id})
            if ret_response == 204:
                return_value = True
            else:
                lg.assert_and_log(1 == 0, 'FAIL: \t Future Anchor Site could not be added for hub ' + aconfig.hub_id )

    return return_value

# Saved if direct values are needed
# 'Sound_Employing_Legal_Entity__c':	 'Sound Physicians (Test Health System Record Type)',
# 'Sound_Employing_DBA__c':	'Sound Physicians of Oregon',
# 'National_Clinical_Leader__c':	'Greg Johnson',
# 'Echo_Recruiter__c':	'Ashley Rogers',
# 'National_Operations_Leader__c':	'Matt Alva',
# 'People_Support_Generalist__c':	'Tyra Cotton',
# 'Operations_Vice_President__c':	'Dria McClumskey',
# 'Staffing_Consultant__c':			'Aubrey Singleton',
# 'Clinical_Program_Lead__c': 		'Test Clinical Program Lead',
# 'Staffing_Coordinator__c':			'Matt Kennedy',
# 'Program_Manager__c':				'Test Program Manager',
# 'Staffing_Coordinator_Support__c':	'Full Support',
# 'Analyst__c':						'Test Analyst',
# 'Privileging_Coordinator__c':		'Fran Johnson',
# 'Finance_Manager__c':				'Test Finance Manager',
# 'Privileging_Consultant__c':		'Courtney Stiles',
# 'Program_Close_Manager__c':		'Test Program Close Manager',
# 'Sr_Director_Perm_Recruiting__c':	'Adreanna Bourassa',
# 'Program_Launch_Manager__c':		'Test Program Launch Manager',
# 'Director_Perm_Recruiting__c':		'Stephany Nelson',

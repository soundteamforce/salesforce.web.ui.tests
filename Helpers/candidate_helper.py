from Helpers import salesforce_helper as sfh
from Helpers import log_helper as lg
import sf_connection_config as sf_cc
from Helpers import ignore_login_data as ild
import datetime as d
from datetime import timedelta as td
import random as r
import ui_config as uic
import api_config as aconfig
from simple_salesforce import SalesforceError as e
from Helpers import api_helper as apih
from Resources import test_variables as tv

def create_duplicate_candidate(mdicnry, index):
    lg.log_results("INFO: \t In the create duplicate candidate record module.")
    row_value = []
    i = 0
    candidate_id_values = []
    aconfig.cand_status_str = mdicnry['candidate_status'][index].strip()
    aconfig.cand_has_colleague_status = mdicnry['colleague_status'][index].strip()
    cand_active_chbox_str = mdicnry['active_chkbox'][index].strip()
    expected_candidate_result_v = mdicnry['result'][index].strip()

    if 'NULL' in aconfig.cand_has_colleague_status:         # Some columns have Null(Blank) etc
        aconfig.cand_has_colleague_status = ''

    if aconfig.cand_status_str in ['CDL', 'Locum']:
        record_type = 'LT / 1099'
    elif aconfig.cand_status_str in ['Cross Credential']:
        record_type = 'Cross Credential'
    elif 'NULL' in aconfig.cand_status_str:             # This is tested manually due to validation rule.
        row_value= [str(index), aconfig.cand_status_str, aconfig.cand_has_colleague_status,
                    cand_active_chbox_str, expected_candidate_result_v, 'Skipped The Row']
        return row_value, candidate_id_values
    else:
        record_type = 'W2'

    if cand_active_chbox_str in 'FALSE':
        aconfig.cand_active_chbox = False
    else:
        aconfig.cand_active_chbox = True

    record_type_id = sfh.get_record_type_id(record_type, "Candidate__c")
    position_id = aconfig.all_position_ids[0]
    while i < 2:
        c_id, c_name = create_new_candidate_rec(record_type, record_type_id, aconfig.all_contact_ids[0],
                                 position_id, 'Primary')

        if c_id:
            candidate_id_values.append(c_id)

        if (i == 0):
            lg.assert_and_log(c_id, 'FAIL: \t Candidate could not be created. ' ,
                              "PASS: \t Candidate created successfully with Id: '" + c_id + "' , Name: '" +
                              str(c_name))
        if (i == 1):
            if c_id:
                lg.log_results("INFO: \t Candidate created successfully with Id: '" + c_id + "' , Name: '" + str(c_name) + "'")

            row_value= [str(index), aconfig.cand_status_str, aconfig.cand_has_colleague_status,
                               cand_active_chbox_str, expected_candidate_result_v, 'ERROR']

            return row_value, candidate_id_values

        i += 1


def begin_candidate_record():
    # following code is for first canidate.
    # when the second candidate is created, the position_id should be not the one used previously.
    # def begin_candidate_record(contact_id, account_ids, position_title_value = 'Nurse Practitioner', is_hub= False):

    lg.log_results("\nINFO: \t Begin creating CANDIDATE record." )
    if not aconfig.all_contact_ids:
        lg.log_results("INFO: \t Could not create Candidate Record: Contact Ids not found")
        return


    # sobject_candidate_val = "Candidate__c"
    candidate_ids = []
    position_ids = []
    candidate_names = []
    record_type_ids = []
    local_contact_id = ''
    y = 0
    admin_user_id = sfh.get_user_id(sf_cc.uname)
    position_ids = sfh.get_position_id(admin_user_id, aconfig.all_ssl_ids , aconfig.all_account_ids, False)
    # position_ids = sfh.get_position_id(admin_user_id, aconfig.position_title, aconfig.all_account_ids, True)
    ssl_ids = aconfig.all_ssl_ids
    if aconfig.is_hub:
        record_type_ids.append(sfh.get_record_type_id(aconfig.cand_rec_type, "Candidate__c"))
        site_type = ['Primary']
        if record_type_ids:
            i = 0
            while i < len(position_ids):
                position_id = position_ids[i]
                if aconfig.contact_id != '':
                    local_contact_id = aconfig.contact_id
                elif aconfig.all_contact_ids[0] != '':
                    local_contact_id = aconfig.all_contact_ids[0]
                ret_candidate_id, ret_candidate_name = create_new_candidate_rec(aconfig.cand_rec_type,
                                                                                record_type_ids[0],
                                                                                local_contact_id,
                                                                                position_id, ssl_ids[0], site_type[0])
                if ret_candidate_id:
                    aconfig.all_candidate_names.append(ret_candidate_name)
                    aconfig.all_candidate_ids.append(ret_candidate_id)
                    lg.log_results("PASS : \t Successfully created Candidate record: " + ret_candidate_name )
                else:
                    lg.log_results("FAIL : \t Could not creat Candidate record. " )

                i += 1
    else:
        # employment_type_name = ["LT / 1099", "LT / 1099"]           #MCS
        # employment_type_name = ["LT / 1099", "W2"]               # for
                       # CE 6927
        record_type_name = 'Candidate Standard'
        record_type_id = sfh.get_record_type_id(record_type_name, "Candidate__c")
        site_type = ['Primary', 'Secondary']
        one_emp_type_name = r.choice(aconfig.cand_emp_type)

    # Added account_ids so that separate candidates can be created for two different positions (accounts+SSL) with
    # same title.
        if record_type_id:
            i = 0
            current_site_type = site_type[0]
            while i < len(position_ids):
                position_id = position_ids[i]
                ret_candidate_id, ret_candidate_name = create_new_candidate_rec(one_emp_type_name,
                                                                                record_type_id,
                                                                                aconfig.all_contact_ids[0],
                                                                                position_id, ssl_ids[0], current_site_type)

                if ret_candidate_id:
                    aconfig.all_candidate_names.append(ret_candidate_name)
                    aconfig.all_candidate_ids.append(ret_candidate_id)
                    lg.log_results("PASS : \t Successfully created Candidate record: " + ret_candidate_name )
                else:
                    lg.log_results("FAIL : \t Could not create Candidate record. " )

                i += 1
                current_site_type = site_type[1]


    #TODO Remove the return statement with tthe value as it is now assigned to aconfig
    lg.log_results("aconfig.all_candidate_ids = " + str(aconfig.all_candidate_ids ))
    return


def create_new_candidate_rec(one_emp_type_name, record_type_id, contact_id, position_id, ssl_id, site_type):
    # cand_status = ['Acquired', 'Contract Signed', 'Interview', 'Presentation']
    c_name = ''
    candidate_date = d.datetime.today().strftime('%Y-%m-%d')
    #contract_end_date = (d.datetime.now() - td(30)).strftime('%Y-%m-%d')
    previous_position_type = r.choice(['Residency', 'Fellowship', 'Practicing'])
    candidate_record = sf_cc.sf_connect.Candidate__C.create({
                                                  'RecordTypeId': record_type_id,
                                                  'Position__c': position_id,
                                                  'Sound_Service_Line__c': ssl_id,
                                                  'Previous_Position__c': previous_position_type,
                                                  'Candidate_Status__c': aconfig.cand_status_str,
                                                  'Colleague_Status__c' : aconfig.cand_has_colleague_status,
                                                  'Clinician_Organization__c' : r.choice(aconfig.cand_clcn_org),
                                                  'Labor_Commitment_Type__c' : r.choice(aconfig.lab_com_type),
                                                  'Clinician_Role__c' : r.choice(aconfig.cand_cln_role),
                                                  'contact__c': contact_id,
                                                  'Employment_Type__c': one_emp_type_name,
                                                  'Site_Type__c': site_type,
                                                  'Execution_Date__c' : candidate_date,
                                                  'Contract_start_Date__c': candidate_date,
                                                  'Credit__c': 'Yes',
                                                  'Recruitment_Credit_Text__c': 'Test_Perm_Recruiter',
                                                  'Active__c': aconfig.cand_active_chbox,
                                                  #'Original_CIF_SAS_Issue_Date__c': candidate_date,
                                                  #'Contract_End_Date__c': aconfig.contract_end_date,
                                                  #'Term_Non_Start_Reason__c': aconfig.cand_term_reason,
                                                  #'Initial_Privilege_Date__c': aconfig.ini_priv_date,
                                                  })

    ret_candidate_id =  sfh.check_for_new_record(candidate_record, 'Candidate__C')
    if ret_candidate_id:
        c_dict = sf_cc.sf_connect.Candidate__C.get(ret_candidate_id)
        c_name = str(c_dict['Name'])

    return ret_candidate_id, c_name


def create_new_candidate_rec_on_position(employment_type_name, record_type_id, contact_id, position_id,
                                            site_type):

    cand_status = ['Interview', 'Presentation']
    cand_status_str = r.choice(cand_status)
    cand_recr_credit_str = r.choice(['Sound', '3rd Party'])
    candidate_date = d.datetime.today().strftime('%Y-%m-%d')
    previous_position_type = r.choice(['Residency', 'Fellowship', 'Practicing'])
    candidate_id = ''
    candidate_name = ''

    candidate_record = sf_cc.sf_connect.Candidate__C.create({
        'RecordTypeId': record_type_id,
        'Candidate_Status__c': 'Interview',
        # 'clinician_type__c': cand_clinician_type,
        'Colleague_Status__c' : 'Active',
        'contact__c' : contact_id,
        'Employment_Type__c': employment_type_name,
        'Position__c': position_id,
        'Previous_Position__c': 'Residency',
        # 'Previous_Position__c': previous_position_type,
        'Execution_Date__c' : candidate_date,
        'Contract_start_Date__c': candidate_date,
        # 'Recruitment_Credit__c': cand_recr_credit_str, 'Sound'
        'Recruitment_Credit__c': 'Sound',
        'Site_Type__c': site_type,
        'Active__c': True
    })
    candidate_id =  sfh.check_for_new_record(candidate_record, 'Candidate__C')
    if candidate_id:
        c_dict = sf_cc.sf_connect.Candidate__C.get(candidate_id)
        c_name = str(c_dict['Name'])
    # lg.log_results("candidate_ cretion response " + str(candidate_record))

    return candidate_id, candidate_name

def get_pri_contracting_candidate():
    i = 0
    all_pcc_current_names = []
    pcc_q = "SELECT Primary_Contracting_Candidate__c FROM Candidate__c WHERE Hub__c= '" + \
            aconfig.hub_id + "'"
    result_name = sf_cc.sf_connect.query(pcc_q)
    pcc_id_rec = result_name['records']
    if pcc_id_rec:
        while i < len(pcc_id_rec):
            pcc_id_dict = pcc_id_rec[i]
            all_pcc_current_names.append(pcc_id_dict['Primary_Contracting_Candidate__c'])
            i += 1

        [aconfig.all_pcc_names.append(i) for i in all_pcc_current_names if i not in aconfig.all_pcc_names]

    lg.assert_and_log(aconfig.all_pcc_names, 'Fail - Could not find the Primary Contracting Candidate. ',
                      'Pass - The Primary Contracting Candidate is ' + str(aconfig.all_pcc_names))

def update_candidate_status():
    sfh.update_candidate_status(aconfig.all_candidate_ids, 'Candidate_Status__c', 'Contract Signed')

def verify_candidate_details(c_name, pa_type, pa_name):
    pa_record_type = [tv.pa_type_pea, tv.pa_type_ica]
    pa_ant_str_date = sfh.get_ant_str_date_pa(pa_name)
    status, outcome, ctrc_str_date, col_status, is_active = sfh.get_cand_details(c_name)
    if pa_type in pa_record_type:
        lg.assert_and_log(status == 'Contract Signed', '\t \t FAIL: Unexpected value for Candidate Status: ' + status,
                          '\t \t PASS: Expected value of Contract status is ' + status)
        lg.assert_and_log(outcome is None, '\t \t FAIL: Expected Candidate Outcome to be None but found ' + str(outcome),
                          '\t \t PASS: Expected value of Candidate Outcome is None')
        lg.assert_and_log(ctrc_str_date == pa_ant_str_date, '\t \t FAIL: Expected Contract Start Date to be ' +
                          pa_ant_str_date + ' but found ' + ctrc_str_date,
                          '\t \t PASS: Expected value of Contract Start Date found ' + ctrc_str_date)
        lg.assert_and_log(col_status == 'In Process', '\t \t FAIL: Unexpected colleague status ' + col_status,
                          '\t \t PASS: Expected Colleague Status value found ' + col_status)
        lg.assert_and_log(is_active == 'True', '\t \t FAIL: Expected Active status to be True but found False',
                          '\t \t PASS: Expected Active status found: ' + is_active)

        return


# Added to get the Clinical Ladder Tier value via API, Sprint 113, US 110455
def get_clt_val_frm_cand(cand_name,exp_value):
    clt_val = sfh.get_clt_val_cand(cand_name)
    lg.assert_and_log(clt_val == exp_value, "\t \t FAIL: Expected Clinical ladder tier to be " + str(exp_value) +
                      " but is " + str(clt_val), "\t \t PASS: Clinical Ladder Tier value on Candidate is found to be as expected " + str(clt_val))

    return


def update_can_training_path(cand_id, training_path_val, training_length=None):
    sfh.update_cand_training_path(cand_id, training_path_val, training_length)
    return


def update_ctrct_str_date(cand_id, ctrct_str_date):
    if ctrct_str_date == 'today':
        ctrct_str_date = tv.today_date_yymd
    sfh.update_cand_start_date(cand_id, ctrct_str_date)
    return
from Helpers import salesforce_helper as sfh
from Helpers import log_helper as lg
import sf_connection_config as sf_cc
from Helpers import ignore_login_data as ild
import datetime as d
import random as r
import ui_config as uic
from Helpers import api_helper as ah
import api_config as aconfig
from Helpers import api_helper as apih

def begin_hm_position_record():
    leadership_value = False
    hmp_name = ''
    service_line_value = aconfig.service_lines[0] # the hub has only one service Line i.e 'Telemedicine
    lg.log_results("\nINFO: \t Begin creating HUB MANAGED POSITION records.")
    # if not (aconfig.all_ssl_ids and aconfig.hub_id):
    if not (aconfig.all_ssl_ids):
        lg.log_results("INFO: \t Could not create Hub Managed Position Record: Accounts or Hub Ids not found")
        return

    for i, each_hub_mangd_posn in aconfig.hub_mangd_posn.items():
        if (each_hub_mangd_posn['create_hmp']):
            create_new_hub_mangd_posn(service_line_value, each_hub_mangd_posn['position_title'],
                                      each_hub_mangd_posn[
            'suffix_str'], leadership_value)
        i += 1
    if aconfig.all_hmp_ids:
        lg.log_results("INFO: \t Hub Managed Position Ids are : " + str(aconfig.all_hmp_ids))



def create_new_hub_mangd_posn(service_line, pos_title, pos_suffix, leadership_value):
    lg.log_results('INFO: \t Creating Hub Managed Position record with Service Line - ' + service_line + ' ,Title - '
                   + pos_title + ' , Suffix - ' + pos_suffix)
    hmp_name = ''
    hm_pos_record = sf_cc.sf_connect.Hub_Managed_Position__c.create({
                                                                 'Hub_Record__c': aconfig.hub_id,
                                                                 'Service_Line__c': service_line,
                                                                 # 'IsDeleted': False,
                                                                'Position_Number_Suffix__c': pos_suffix,
                                                                'Position_Title__c': pos_title,
                                                                 'Leadership__c': leadership_value
                                                                 })
    hmp_id = sfh.check_for_new_record(hm_pos_record, 'Hub_Managed_Position__c')
    if hmp_id:
        aconfig.all_hmp_ids.append(hmp_id)

    return

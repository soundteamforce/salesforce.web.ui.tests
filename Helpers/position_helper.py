from Helpers import log_helper as lg
from Helpers import salesforce_helper as sfh
import sf_connection_config as sf_cc
import api_config as aconfig
from Helpers import api_helper as apih

def begin_position_records():

    #TODO aconfig.position_title = 'Hospitalist' ---- check settings for this in position helper
#TODO     hm_pos_name = 'TM.' + aconfig.accounting_id_code + aconfig.all_account_names[0] + aconfig.suffix_str
    # lg.log_results("hub managed postiion name is " + hm_pos_name)


    lg.log_results("\nINFO: \t Begin creating Position records.")
    if not (aconfig.all_account_ids and aconfig.all_ssl_ids):
        lg.log_results("INFO: \t Could not create Position Record: Accounts or SSL Ids not found")
        return

    pos_record_type_id = sfh.get_record_type_id(aconfig.pos_rec_type, 'Position__c')

    if pos_record_type_id != "":
        # lg.log_results("INFO : \t Record Type Id is " + pos_record_type_id, "YELLOW")
        for ssl_id in aconfig.all_ssl_ids:
            # lg.log_results("INFO : \t creating new position record." + pos_rec_type_name + " " + ssl_id, "YELLOW")
            for i, each_pos_d in aconfig.position_details.items():
                if (each_pos_d['create_p']):
                    ret_pos_id, ret_pos_name = create_new_position_rec(ssl_id, pos_record_type_id, each_pos_d)
                    if ret_pos_id:
                        lg.log_results("PASS : \t Successfully created position record: " + ret_pos_name)
                        aconfig.all_position_ids.append(ret_pos_id)
                    else:
                        lg.log_results("FAIL: \t Could not create Position record", "LIGHTRED_EX")

        lg.log_results("PASS: \t Position Ids are : " + str(aconfig.all_position_ids))
    else:
        lg.log_results("FAIL : \t Record Type Id is NOT valid" , "LIGHTRED_EX")

    return


def create_new_position_rec(ssline_id, pos_rec_type_id, pos_details):

    position_name = ''
    pos_id = ''
    pos_name = ''
    ssl_query = "SELECT ssl.name, ssl.Service_Line__c, ssl.Hospital_Partner__c FROM Sound_Service_Lines__c as ssl WHERE ssl.Id = '" +  ssline_id + "'"
    # lg.log_results(ssl_query, "YELLOW")
    result_name = sf_cc.sf_connect.query(ssl_query)
    ssl_rec = result_name['records']
    if ssl_rec:
        ssl_rec_dic = ssl_rec[0]
        ssl_name = str(ssl_rec_dic['Name'])
        ssl_line = str(ssl_rec_dic['Service_Line__c'])
        account_id = str(ssl_rec_dic['Hospital_Partner__c'])
    else:
        lg.log_results("FAIL: Can't create position record.")
        return

        # 1: {'position_title': 'Nurse Practitioner', 'suffix_str': ' (NP)', 'create_p':False},
        # Service_Line__c = Critical Service?
    position_name = ssl_name[3:] + pos_details['suffix_str']

    pos_record = sf_cc.sf_connect.position__C.create({'Name': position_name,
                                                    'Partner_Hospital__c': account_id,
                                                    'Position_Title__c': pos_details['position_title'],
                                                    'RecordTypeId': pos_rec_type_id,
                                                    'Service_Line__c': ssl_line,
                                                    'Service_Line_Record__c': ssline_id,
                                                    'No_Requirements__c': True
                                                    })

    pos_id =  sfh.check_for_new_record(pos_record, 'position__C')
    if pos_id:
        pos_dict = sf_cc.sf_connect.position__C.get(pos_id)
        pos_name = str(pos_dict['Name'])


    return pos_id, pos_name


from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin
from Helpers import login_as_user_with_search as lauw_search

# --------------------------------------------------------------------------------------
# US 70695 - Support - Case 139192 - People Support Recalls Document Approval Request
# --------------------------------------------------------------------------------------

@ddt
class PaApprovalRecall(unittest.TestCase):
    test_to_run = [1,2,3,4,5]

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))

    def tearDown(self):
        cl.log_off(1)
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.now.strftime('%m-%d %H:%M:%S'))
        lg.log_results('\t End of TEST CASE.\n')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard Regression  **************")
        cwuc.wd.quit()

    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.prod_usr_people_support_enh, 'Amendment'))
    @unpack
    @unittest.skipUnless(1 in test_to_run, "test_1")
    def test_1_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "People Enhance Support Recalls Amendment Provider Agreement"
        lg.log_results("\t TEST CASE: 1\t " + cwuc.test_case_name)
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.TREAD]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': 'TREAD'}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 0)
            stg_h.recall_pa_approval()
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_approvd_value)
            lg.log_results('\t\t People Support Enhanced user could recall Amendment Provider Agreement.')
            cl.log_off()
            pa_pg.approve_reject_pa(cv.case_RDO_user, 'Approve')
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    @data((tv.prod_usr_people_support_enh, tv.pa_type_ccl))
    @unpack
    @unittest.skipUnless(2 in test_to_run, "People support enhanced user recalls Compensation Change Letter")
    def test_2_compensation_chng_letter(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "People Enhance Support Recalls Compensation Change Letter Provider Agreement"
        lg.log_results("\t TEST CASE: 2\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.RETRO]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.RETRO['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 0)
            stg_h.recall_pa_approval()
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_approvd_value)
            lg.log_results('\t\t People Support Enhanced user could recall Compensation Change Letter Provider Agreement.')

    @data((tv.prod_usr_people_support_enh, tv.pa_type_ilpl))
    @unpack
    @unittest.skipUnless(3 in test_to_run, "People support enhanced user recalls Interim Leadership Position Letter")
    def test_3_compensation_chng_letter(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "People support enhanced user recalls Interim Leadership Position Letter"
        lg.log_results("\t TEST CASE: 3\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.CASE]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.CASE['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 0)
            stg_h.recall_pa_approval()
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_approvd_value)
            lg.log_results(
                '\t\t People Support Enhanced user could recall Interim Leadership Position Letter Provider Agreement.')

    @data((tv.prod_usr_people_support_enh, tv.pa_type_prl))
    @unpack
    @unittest.skipUnless(4 in test_to_run, "People support enhanced user recalls Position Removal Letter")
    def test_4_compensation_chng_letter(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "People support enhanced user recalls Position Removal Letter"
        lg.log_results("\t TEST CASE: 4\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.EDLNR]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.EDLNR['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 0)
            stg_h.recall_pa_approval()
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_approvd_value)
            lg.log_results(
                '\t\t People Support Enhanced user could recall Position Removal Letter Provider Agreement.')

    @data((tv.prod_usr_people_support_enh, tv.pa_type_ssl))
    @unpack
    @unittest.skipUnless(5 in test_to_run, "People support enhanced user recalls Secondary Site Letter")
    def test_5_compensation_chng_letter(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "People support enhanced user recalls Secondary Site Letter"
        lg.log_results("\t TEST CASE: 5\t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BHAPL]
            w.add_agreement_term(test_terms)
            test_terms_dictionary = {0: {'action_taken': 'Approve', 'term_type_to_use': tv.BHAPL['at_description']}}
            hm.approve_reject_terms(test_terms_dictionary)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 0)
            stg_h.recall_pa_approval()
            stg_h.pa_stage_value(cwuc.new_pa_name, tv.pa_stage_approvd_value)
            lg.log_results(
                '\t\t People Support Enhanced user could recall Secondary Site Letter Provider Agreement.')

if __name__ == "__main__":
    unittest.main(module='__main__', argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
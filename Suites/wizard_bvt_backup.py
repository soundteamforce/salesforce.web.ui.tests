from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as v
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Helpers import login_as_user_with_search as lauw_search
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import Home as hm
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
# from Suites import sf_create_entity_data
from sc_util import target_process_helper as tph

# --------------------------------------------------------------------------------------
#   This is BVT to create different compensations
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_bvt(unittest.TestCase):

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)

        lg.log_results("\n\t ************** \t START of Wizard BVT  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("")

    def tearDown(self):
        cl.log_off()
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results('\t End of TEST CASE.')
        if cwuc.run_target_process:
            u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results("\n\t ************** \t END of Wizard BVT  **************")
        cwuc.wd.quit()

    #
    # -------------## REQUIREMENTS ## ------------------ ## DONE
    @data((tv.usr_perm_recruiter_jill, tv.pa_type_pea))
    @unpack
    # @unittest.skip("Skipping")
    def test_01_perm_recruiter_pa_pea_term_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "01 Perm Recruiter Adds PEA - Guideline Term"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.add_agreement_guideline()
        pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel
        pa_pg.delete_terms(1, 0) # comp_at_num, addnl_at_num to delete


    # ------------## REQUIREMENTS - ('Sarah Baugus', 'CDL') ## ------------------ ##
    @data((tv.usr_staffing_admin_sarah, tv.pa_type_cdl))
    @unpack
    # @unittest.skip("Skipping")
    def test_02_staffing_admin_pa_cdl_term_night_shift(self, dt_login_as_uname, dt_pa_record_type):

        cwuc.test_case_name =  "02 Staffing Admin Adds CDL - Night Shift Term"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.HNIGHT]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel
            pa_pg.delete_terms(1, 0) # comp_at_num, addnl_at_num to delete


    @data((tv.usr_echo_recruit_ashley, tv.pa_type_initial_cdl)) # right users
    @unpack
    # @unittest.skip("Skipping")
    def test_03_echo_recruit_i_cdl_shift_pay_comm_bonus_work_unit_term(self, dt_login_as_uname,
    dt_pa_record_type):
        cwuc.test_case_name = "03 Echo Recruiter Adds Initial CDL - Swing Shift, Commencement Bonus and Workload Terms"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        total_num_of_terms = 3
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.SSWING, v.SIGBN, v.WLOAD]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            #TODO: Approval is needed at Provider Agreement Level.....JMoshay 12.9 - initialDDL_ddt

            pa_pg.cancel_term(dt_login_as_uname, 2, 1)  #user, comp_at_num, addnl_at_num to cancel
            pa_pg.delete_terms(2, 1) # comp_at_num, addnl_at_num to delete


    # -------------## REQUIREMENTS ('Jill Albach', 'PEA')## ------------------ ##Stays
    #
    @data((tv.usr_perm_recruiter_jill, tv.pa_type_pea))
    @unpack
    # @unittest.skip("Skipping")
    def test_04_perm_recruiter_pa_non_overlapping_multiple_terms_same_type(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "04 Perm Recruiter Adds PEA - Non Overlapping Multiple Terms Of Same Type"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        total_num_of_terms = 2
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            lg.log_results(txh.tc_07_2)
            test_terms = [v.CLSAL, v.CLSAL]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data((tv.usr_perm_recruit_dir_adreanna, tv.pa_rec_tp_amendment))
    # People support enhance - USE THIS AMMENDMENT , MAy Luu or KAthleen
    @unpack
    @unittest.skip("Skipping")
    def test_05_perm_dir_update_approved_guideline(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = "05 Perm Director Adds Amendment - Updated Agreement Guideline"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        total_num_of_terms = 2
        tv.exe_changes_btn_on = False
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['amount'])
        w.change_candidate()
        test_terms_2 = [v.SIGBN]
        tv.exe_changes_btn_on = True
        w.add_agreement_term(test_terms_2)
        hm.approve_terms(total_num_of_terms)


    # -------------## REQUIREMENTS - PA and AT MUST be Buy Out  ####
    # @data(('Ashley Rogers', 'Buy Out Letter'), ('Ashley Rogers', 'Buy Out Letter'))
    @data((tv.usr_staff_services_saira, tv.pa_type_bol))
    @unpack
    # @unittest.skip("Skipping")
    def test_06_staff_services_pa_buyout_at_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "06 Staffing Services Adds Buyout - Buyout term"
        lg.log_results("\t TEST CASE: \t " + cwuc.test_case_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.BUYOUT]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    # ************ REQUIREMENTS - Bug 66580 Support - Approval Process not firing when additional terms added
    # *********** KEEP IT for some time..
    @data((tv.usr_people_support_spec_may, tv.pa_type_ccl))
    @unpack
    @unittest.skip("Skipping")
    def test_07_people_support_pa_ccl_rate_three_ats(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = "07 People Support Adds Compensation Change Letter - three Terms"
        lg.log_results("\t TEST CASE: \t " + txh.tc_19_name)
        total_num_of_terms = 3
        tv.exe_changes_btn_on = True
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [v.SSWING, v.WLOAD]
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [v.SDAY]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    # ************ REQUIREMENTS - Amendment Provider Agreement by Perm Recruiting Director ***********
    # DON"T NEED CASE as Director
    # REMOVE TODO: on fence..
    # @data(('Adreanna Bourassa', 'Amendment'))
    # @unpack
    # @unittest.skip("Skipping")
    # def test_17_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
    #     cwuc.test_case_name = txh.tc_17_name
    #     lg.log_results("\t TEST CASE: \t " + txh.tc_17_name)
    #
    #     lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
    #     ch.create_new_case('Employment Event', cv.case_req_category_guideline)
    #     cl.log_off()
    #     cwuc.delete_existing_pa = 0
    #     if w_bvt_hlpr.wizard_setup('Jill Albach', 'Provider Employment Agreement'):
    #         cl.log_off()
    #         stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
    #         # Change the value FROM FAE to New
    #         cwuc.delete_existing_pa = 0
    #         if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #             w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
    #             pa_pg.navigate_to_provider_agreement_page()
    #             pa_pg.pa_recordtype_validation(tv.record_type_amnd_ddp)


    # # ************ REQUIREMENTS -  Amendment Provider Agreement by People Enhance Support ***********
    # # MErge TEst 2 with this one
    # # People     support     enhance - USE     THIS     AMMENDMENT, MAy     Luu or KAthleen
    # @data(('Kathleen Aukland', 'Amendment'))
    # @unpack
    # @unittest.skip("Skipping")
    # def test_18_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
    #     cwuc.test_case_name = txh.tc_18_name
    #     lg.log_results("\t TEST CASE: \t " + txh.tc_18_name)
    #     total_num_of_terms = 1
    #     lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
    #     ch.create_new_case('Employment Event', cv.case_req_category_guideline)
    #     cl.log_off()
    #     cwuc.delete_existing_pa = 0
    #     if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #         test_terms = [v.HNIGHT]
    #         w.add_agreement_term(test_terms)
    #         hm.approve_terms(total_num_of_terms)
    #         lauw_search.login_as_with_search(dt_login_as_uname, 'user')
    #         stg_h.change_pa_stage(tv.pa_stage_value_dar, 1)  # logoff
    #         pa_pg.approve_pa(cv.case_RDO_user)
    #         # pa_pg.run_ddp(dt_login_as_uname)





    #TODO: data(('May Luu/Kathleen', 'Rate Increase Letter'))


    #************ REQUIREMENTS - Rate Increase letter - Test People Support Enhance user can end terms on wizard
    # page for
    # multiple terms "Wizard  -  " *********** # STAYS

    # @data(('Kathleen Aukland', 'Rate Increase Letter'))
    # @unpack
    # @unittest.skip("Skipping")
    # def test_21_ccl_can_end_multiple_ats_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
    #     cwuc.test_case_name = txh.tc_21_name
    #     lg.log_results("\t TEST CASE: \t " + txh.tc_21_name)
    #     total_num_of_terms = 11
    #     lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
    #     ch.create_new_case('Employment Event', cv.case_req_category_guideline)
    #     cl.log_off()
    #     cwuc.delete_existing_pa = 0
    #     if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #         test_terms = [v.RELOC, v.ADMST, v.CHFST, v.TRAST,  v.LDPST, v.OTHERST, v.QUABN, v.WORKL, v.HHCBN, v.ICU, v.RETBN]
    #         w.add_agreement_term(test_terms)
    #         lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
    #         cwuc.delete_existing_pa = 0
    #         hm.approve_terms(total_num_of_terms)
    #         pa_pg.cancel_term(dt_login_as_uname, 10, 1)  # user, comp_at_num, addnl_at_num to cancel
    #         sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)



if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'], exit=False)
    unittest.TestLoader.sortTestMethodsUsing = None
    unittest.TestLoader().loadTestsFromTestCase(wizard_bvt)
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import ui_config as cwuc
from ddt import ddt, data, unpack
from Resources import test_variables as tv
from Resources import case_variables as cv
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
from Helpers import pa_stage_helpers as stg_h
from Helpers import utilities as u
from Helpers import case_helper as ch
from Helpers import login_as_user_with_search as lauw_search
from Pages import comp_login as cl
from Pages import provider_agreement as pa_pg
from Pages import pa_wizard as w
from Pages import Home as hm
# from Suites import sf_create_entity_data
from sc_util import target_process_helper as tph
from Pages import comp_login as clogin

# --------------------------------------------------------------------------------------
#   This is Wizard Regression
# Story 1 - # Original Story #58675 for compensation wizard automation
# Story 2 - # User story #59810 - Create more than 13 Agreement Terms in the PA for a candidate.
# --------------------------------------------------------------------------------------

@ddt
class wizard_ui_regression(unittest.TestCase):

    # -------------------------------------------------------------------------------------------------------------
    # Can create case with Record Type - Employment Event and Request Category - Provider Agreement Updates
    # --------------------------------------------------------------------------------------------------------------

    @classmethod
    def setUpClass(cls):
        is_login = clogin.login_sf()
        cwuc.test_cases, cwuc.child_test_plan_run_id = tph.initialize_target_process(cwuc.tp_tag, cwuc.test_plan_run_id,
                                                                                     cwuc.suite_name,
                                                                                     cwuc.run_target_process)
        lg.log_results("\n\t ************** \t START of Wizard Regression  **************")

    def setUp(self):
        hm.login_admin()
        lg.log_results("\t START TIME: \t " + tv.today_datetime_sec)

    def tearDown(self):
        cl.log_off()
        cwuc.delete_existing_pa = 1
        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)
        lg.log_results("\t END TIME: \t " + tv.today_datetime_sec)
        lg.log_results('\t End of TEST CASE.')
        u.set_test_status()

    @classmethod
    def tearDownClass(cls):
        lg.log_results(txh.teardown_msg)
        cwuc.wd.quit()

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    # @data(('May Luu', 'Provider Employment Agreement'))
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    @unittest.skip("Skipping")
    def test_01_agreement_guideline(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_01_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_01_name)
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.add_agreement_guideline()
        pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel
        pa_pg.delete_terms(1, 0) # comp_at_num, addnl_at_num to delete
        # lauw_search.login_as_with_search(dt_login_as_uname, 'user')

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    # People support enhance - May Luu 10.21., PEA
    @unpack
    @unittest.skip("Skipping")
    def test_02_update_approved_guideline(self, dt_login_as_uname,dt_pa_record_type):
        cwuc.test_case_name = txh.tc_02_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_02_name)
        total_num_of_terms = 1
        w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type)
        w.update_agreement_guideline(cwuc.new_pa_name, cwuc.new_pa_type, ['amount'])
        hm.approve_terms(total_num_of_terms)
        lauw_search.login_as_with_search(dt_login_as_uname, 'user')     #Avoid logoff errors


    #
    # ------------## REQUIREMENTS - ('Sarah Baugus', 'CDL') ## ------------------ ##
    @data(('Sarah Baugus', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_03_test_night_shift_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_03_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_03_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            pa_pg.cancel_term(dt_login_as_uname, 1, 0)  #user, comp_at_num, addnl_at_num to cancel
            pa_pg.delete_terms(1, 0) # comp_at_num, addnl_at_num to delete

    #
    # --------## REQUIREMENTS 'Saira Ayub', 'Ashley Rogers' for Both 'Initial CDL')## ------- ##
    # @data(('Saira Ayub', 'Initial CDL'), ('Ashley Rogers', 'Initial CDL')) # right users
    @data(('Saira Ayub', 'Initial CDL')) # right users
    @unpack
    @unittest.skip("Skipping")
    def test_04_test_shift_pay_and_commencement_bonus_and_work_unit_term(self, dt_login_as_uname,
    dt_pa_record_type):
        cwuc.test_case_name = txh.tc_04_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_04_name)
        lg.log_results("Change the test name n title. ")
        total_num_of_terms = 3
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.ADMST, tv.ICU]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            pa_pg.cancel_term(dt_login_as_uname, 3, 0)  #user, comp_at_num, addnl_at_num to cancel
            pa_pg.delete_terms(3, 0) # comp_at_num, addnl_at_num to delete

    #
    # -------------## REQUIREMENTS ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    @unittest.skip("Skipping")
    def test_05_test_more_than_13_ats(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_05_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_05_name)
        total_num_of_terms = 16
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            tv.exe_changes_btn_on = False
            test_terms = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
                          tv.RELOC, tv.VISA,
                 tv.LEAD, tv.SIGBN, tv.QUABN]
            w.add_agreement_term(test_terms)
            tv.exe_changes_btn_on = True
            test_terms = [tv.WLOAD, tv.RETBN]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    #
    # -------------## REQUIREMENTS Multiple Candidate ## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    # @unittest.skip("Skipping")
    def test_06_multiple_candidates(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_06_name
        tv.exe_changes_btn_on = False
        lg.log_results("\t TEST CASE: \t " + txh.tc_06_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.WLOAD]
            w.add_agreement_term(test_terms)
            w.change_candidate()
            test_terms_2 = [tv.SIGBN]
            tv.exe_changes_btn_on = True
            w.add_agreement_term(test_terms_2)


    #
    # -------------## REQUIREMENTS ('Jill Albach', 'PEA')## ------------------ ##
    @data(('Jill Albach', 'Provider Employment Agreement'))
    @unpack
    @unittest.skip("Skipping")
    def test_07_non_overlapping_multiple_at_terms_same_type(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_07_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_07_name)
        total_num_of_terms = 2
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            lg.log_results(txh.tc_07_2)
            test_terms = [tv.CLSAL, tv.CLSAL]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')  #To avoid the error due to log off in
            # teardown.

    #
    # -------------## REQUIREMENTS - PA and AT MUST be Buy Out  ####
    # @data(('Ashley Rogers', 'Buy Out Letter'), ('Ashley Rogers', 'Buy Out Letter'))
    @data(('Saira Ayub', 'Buy Out Letter'))
    @unpack
    @unittest.skip("Skipping")
    def test_08_test_buyout_pa_and_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_08_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_08_name)
        lg.log_results(txh.tc_08_2)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    #
    # ------------ REQUIREMENTS - ('Ashley Rogers', 'Initial CDL')- PA MUST be NON BO, AT - BO -----------#
    @data(('Ashley Rogers', 'Initial CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_9_check_non_buyout_pa_and_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
        #     This is the test to check the BO option should not exist in NON-BO PA drop-down.
        cwuc.test_case_name = txh.tc_09_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_09_name)
        lg.log_results(txh.tc_09_2)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.BUYOUT]
            w.add_agreement_term(test_terms)


    # INVALID TEst case after Dec 2019
    # # ------------## REQUIREMENTS - ('Saira Ayub', 'Buy Out Letter'), ('Ashley Rogers', 'Buy Out Letter') - PA MUST
    # # @data( ('Saira Ayub', 'Buy Out Letter'), ('Ashley Rogers', 'Buy Out Letter'))
    # @data(('Saira Ayub', 'Buy Out Letter'))
    # @unpack
    # # @unittest.skip("Skipping")
    # # BE Buy Out Letter, AT non BO## ------------------ ##
    # def test_10_test_buyout_pa_and_non_buyout_term(self, dt_login_as_uname, dt_pa_record_type):
    #     cwuc.test_case_name = txh.tc_10_name
    #     lg.log_results("\t TEST CASE: \t " + txh.tc_10_name)
    #     lg.log_results(txh.tc_10_2)
    #     total_num_of_terms = 1
    #     if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #         test_terms = [tv.RELOC]
    #         w.add_agreement_term(test_terms)
    #         hm.approve_terms(total_num_of_terms)
    #         # pa_pg.cancel_term(dt_login_as_uname, 1, 0)      # user, comp_at_num, addnl_at_num to cancel
    #         # pa_pg.delete_terms(1, 0)    # comp_at_num, addnl_at_num to delete
    #         sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # *********** REQUIREMENTS -  Admin can change stage TO  and FROM Final Agrmt Sent **************
    # Testing changing to FAS after executing whole process
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_11_admin_change_pa_stage_to_and_from_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_11_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_11_name)
        total_num_of_terms = 1
        test_terms = [tv.HNIGHT]
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement Sent
            # Change the value of stage FROM FAS to approval requested.
            stg_h.change_pa_stage_value(tv.pa_stage_approvd_value)    # Approved
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            #To avoid the error due to log off in  teardown.



    # ************ REQUIREMENTS -  Admin can change stage TO  and FROM Final Agrmt Execution  ***********
    # Testing change to FAE after PA is created.
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_12_admin_change_pa_stage_to_and_from_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_12_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_12_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            pa_pg.unlock_record()
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # New
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')  #To avoid the error due to log off in
            # teardown.



    # ************ REQUIREMENTS -  non Admin can NOT change stage FROM Final Agrmt Execution to new  ***********
    # Testing change to FAE after PA is created.-
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_13_non_admin_change_pa_stage_from_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_13_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_13_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed as a setup by admin
            # admin logoff after changing to FAE, non-admin user will try to update the value FROM FAE
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # change FROM Final Agreement Executed to new
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can change stage value from  Final Agrmt SENT to New  ***********
    # Testing change to FAE after PA is created.
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_14_non_admin_change_pa_stage_from_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_14_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_14_name)
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            cl.log_off()        # logging off as Saira, Admin is logged in
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement sent as a setup by admin
            # admin logoff after changing to FAE, non-admin user will try to update the value FROM FAE
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_new_value)    # can change FROM FAS to New
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can change stage value TO Final Agrmt SENT  ***********
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_15_non_admin_can_not_change_pa_stage_to_final_agrmt_sent(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_15_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_15_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fas)    # Final Agreement sent
            sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    # ************ REQUIREMENTS -  Non Admin can NOT change stage value TO Final Agrmt EXECUTED  ***********
    @data(('Saira Ayub', 'CDL'))
    @unpack
    @unittest.skip("Skipping")
    def test_16_non_admin_can_not_change_pa_stage_to_final_agrmt_executed(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_16_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_16_name)
        total_num_of_terms = 1
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement executed




    # ************ REQUIREMENTS - Amendment Provider Agreement by Perm Recruiting Director ***********
    @data(('Adreanna Bourassa', 'Amendment'))
    # @data(('Adreanna Bourassa', 'Amendment'))
    @unpack
    @unittest.skip("Skipping")
    def test_17_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_17_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_17_name)

        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup('Jill Albach', 'Provider Employment Agreement'):
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)    # Final Agreement Executed
            # Change the value FROM FAE to New
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
                w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
                pa_pg.navigate_to_provider_agreement_page()
                pa_pg.pa_recordtype_validation(tv.record_type_amnd_ddp)


    # ************ REQUIREMENTS -  Amendment Provider Agreement by People Enhance Support ***********
    @data(('Kathleen Aukland', 'Amendment'))
    @unpack
    @unittest.skip("Skipping")
    def test_18_pa_amendment_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_18_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_18_name)
        total_num_of_terms = 1
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')
            stg_h.change_pa_stage(tv.pa_stage_value_dar, 1)  # logoff
            pa_pg.approve_pa(cv.case_RDO_user)
            lauw_search.login_as_with_search(dt_login_as_uname, 'user')


    # ************ REQUIREMENTS - Bug 66580 Support - Approval Process not firing when additional terms added
    # ***********
    @data(('May Luu', 'Compensation Change Letter'))
    @unpack
    @unittest.skip("Skipping")
    def test_19_pa_ccl_rate_at(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_19_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_19_name)
        total_num_of_terms = 3
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.SSWING, tv.WLOAD] #
            w.add_agreement_term(test_terms)
            pa_pg.edit_provider_agreement_terms()
            test_terms = [tv.HNIGHT]
            w.add_agreement_term(test_terms)
            hm.approve_terms(total_num_of_terms)
            # sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)

    # @data(('Jill Albach', 'Provider Employment Agreement'))
    # @unpack
    # @unittest.skip("Skipping")
    # def test_19a_pa_ccl_rate_at(self, dt_login_as_uname, dt_pa_record_type):
    #     lg.log_results("\t \t Working on Bug .......... ")
    #     cwuc.delete_existing_pa = 0
    #     if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
    #         test_terms = [tv.CHFST] # , tv.WLOAD
    #         w.add_agreement_term(test_terms)
    #         pa_pg.edit_provider_agreement_terms()
    #         test_terms = [tv.WLOAD]
    #         w.add_agreement_term(test_terms)
    #         hm.approve_terms(2)

    # ************ REQUIREMENTS -  System Administrator can add Provider Agreement of Rate Increase Letter type without case number
    @data(('Jill Albach', 'Compensation Change Letter'))
    @unpack
    @unittest.skip("Skipping")
    def test_20_ccl_by_administrator(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_20_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_20_name)
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, 'Provider Employment Agreement'):
            w.add_agreement_guideline()
            cl.log_off()
            stg_h.change_pa_stage_value(tv.pa_stage_value_fae)  # Final Agreement executed
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            cwuc.delete_existing_pa = 0
            if w_bvt_hlpr.wizard_setup(tv.test_admin_user, dt_pa_record_type):
                test_terms = [tv.SSWING]
                w.add_agreement_term(test_terms)
                sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)


    #************ REQUIREMENTS - Rate Increase letter - Test People Support Enhance user can end terms on wizard
    # page for
    # multiple terms "Wizard  -  " *********** #

    @data(('Kathleen Aukland', 'Compensation Change Letter'))
    @unpack
    @unittest.skip("Skipping")
    def test_21_ccl_can_end_multiple_ats_in_wizard(self, dt_login_as_uname, dt_pa_record_type):
        cwuc.test_case_name = txh.tc_21_name
        lg.log_results("\t TEST CASE: \t " + txh.tc_21_name)
        total_num_of_terms = 11
        lauw_search.login_as_with_search(cv.case_RDO_user, 'user')
        ch.create_new_case('Employment Event', cv.case_req_category_guideline)
        cl.log_off()
        cwuc.delete_existing_pa = 0
        if w_bvt_hlpr.wizard_setup(dt_login_as_uname, dt_pa_record_type):
            test_terms = [tv.RELOC, tv.ADMST, tv.CHFST, tv.TRAST,  tv.LDPST, tv.OTHERST, tv.QUABN, tv.WLOAD, tv.HHCBN,
                          tv.ICU, tv.RETBN]
            w.add_agreement_term(test_terms)
            lg.log_results("\t \t Wizard -**-----------**- preset up is done -**-----------**-")
            cwuc.delete_existing_pa = 0
            hm.approve_terms(total_num_of_terms)
            pa_pg.cancel_term(dt_login_as_uname, 10, 1)  # user, comp_at_num, addnl_at_num to cancel



if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'], exit=False)
    unittest.TestLoader.sortTestMethodsUsing = None
    unittest.TestLoader().loadTestsFromTestCase(wizard_ui_regression)
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

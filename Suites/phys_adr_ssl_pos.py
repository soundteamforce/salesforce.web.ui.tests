import sys
sys.path.append('../')
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import log_helper as lg
import api_config as aconfig
from Helpers import api_helper as apih
from Resources import test_variables as tv
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import salesforce_helper as sfh
import sf_connection_config as sf_cc


lg.log_results("The test environment is " + sf_cc.test_env)
lg.log_results("Test to verify Sound Service Line & Position reflects Physical Address (US 105110).")

acnt_rec_type_id = sfh.get_record_type_id('Hospital', 'Account')
admin_id = sfh.get_user_id(sf_cc.uname)
acnt_id = sfh.get_hospital_type_account_id(acnt_rec_type_id, admin_id)
ssl_id = sfh.get_ssl_id(acnt_id)
pos_id = sfh.get_pos_id_frm_acnt(acnt_id)

lg.log_results("account_id = " + str(acnt_id))
lg.log_results("all_ssl_id = " + str(ssl_id))
lg.log_results("position_id = " + str(pos_id))

acnt_phy_str, acnt_phy_city, acnt_phy_state, acnt_phy_zc = sfh.get_phy_adr_frm_ssl_acnt(ssl_id, acnt_id, 'account')
ssl_phy_adr, ssl_cty_zip = sfh.get_phy_adr_frm_ssl_acnt(ssl_id, acnt_id, 'ssl')
pos_phy_adr, pos_cty_zip = sfh.get_phy_adr_pos(pos_id)

lg.log_results("Account address: " + acnt_phy_str + " " + acnt_phy_city + ", " + acnt_phy_state + " " + acnt_phy_zc)
lg.log_results("SSL Address: " + ssl_phy_adr+ssl_cty_zip)
lg.log_results("Pos Address: " + pos_phy_adr+pos_cty_zip)

acnt_cty_zip = acnt_phy_city + ", " + acnt_phy_state + "  " + acnt_phy_zc
lg.log_results("Account City,State & Zipcode: " + acnt_cty_zip)

lg.assert_and_log(ssl_phy_adr == acnt_phy_str, "FAIL: Physical Addresses of SSL and Account does not match",
                  "PASS: Physical Addresses of SSL and Account do match")

lg.assert_and_log(pos_phy_adr == acnt_phy_str, "FAIL: Physical Addresses of Position and Account does not match",
                  "PASS: Physical Addresses of Position and Account do match")



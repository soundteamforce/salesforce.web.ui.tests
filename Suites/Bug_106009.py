import sys
sys.path.append('../')
from Helpers import log_helper as lg
from Helpers import privileging_file_helper as pfh
from Helpers import salesforce_helper as sfh
import sf_connection_config as sf_cc

lg.log_results("The test environment is " + sf_cc.test_env)
lg.log_results("Bug 106009 - Initial Privilege Date & Latest Expiration Date Removed in Error.")

# **** Create a Privileging File on a Candidate ******
admin_id = sfh.get_user_id(sf_cc.uname)
cand_id = sfh.get_cand_id_by_admin(admin_id)
lg.log_results("The candidate id is " + str(cand_id))
cand_list = [cand_id]

pfile_id = pfh.begin_creating_privileging_file(cand_list, 'Initial')
lg.log_results('\t\t Initial Privileging file id is ' + str(pfile_id))

# **** Verify the Initial Privileging Date & Latest Expiration Date are reflected on Candidate ***
ini_pri_date1, lat_exp_date1 = sfh.get_ini_priv_date_lat_exp_date_candidate(cand_list)
lg.assert_and_log(ini_pri_date1 is not None,"\t \t FAIL: Expected Initial Privileging Date to be not None, but found "
                  + ini_pri_date1, "\t \t PASS: Initial Privileging Date is not blank.")
lg.assert_and_log(lat_exp_date1 is not None, "\t \t FAIL: Latest Expiration Date to be not None, but found "
                  + lat_exp_date1, "\t \t PASS: Latest Expiration Date is not blank.")
lg.log_results("\t \t Initial Privileging Date: " + ini_pri_date1 + ", Latest Expiration Date:" + lat_exp_date1)

# **** Update the Privileging File Status to Closed/Non-Start *****
pfh.update_priv_file_status(pfile_id)
pf_status = sfh.get_priv_file_status(pfile_id)
# lg.log_results('\t \t Status of Privileging file is updated to ' + pf_status)

# **** Verify the Initial Privileging Date & Latest Expiration Date are not blank on candidate ****
ini_pri_date2, lat_exp_date2 = sfh.get_ini_priv_date_lat_exp_date_candidate(cand_list)
lg.assert_and_log(ini_pri_date2 is not None, "\t \t FAIL: Expected Initial Privileging Date to be not None, but found "
                  + ini_pri_date2, "\t \t PASS: Initial Privileging Date is not blank.")
lg.assert_and_log(lat_exp_date2 is not None, "\t \t FAIL: Latest Expiration Date to be not None, but found "
                  + lat_exp_date2, "\t \t PASS: Latest Expiration Date is not blank.")
lg.assert_and_log(ini_pri_date2 == ini_pri_date1 and lat_exp_date2 == lat_exp_date1, "\t\t FAIL: Initial Privileging "
                  "Date and Latest Expiration Date are not the same after closing the Privileging File.", "\t\t PASS: "
                  "Initial Privileging Date & Latest Expiration Date are same before & after closing Privileging File")
lg.log_results("\t \t Initial Privileging Date: " + ini_pri_date2 + ", Latest Expiration Date:" + lat_exp_date2)


# *** Create Reappointment Privileging File and verify the dates are updated on candidate ****
pfile_id = pfh.begin_creating_privileging_file(cand_list, 'Reappointment')
lg.log_results('\t \t Reappointment Privileging file id is ' + pfile_id)
ini_pri_date3, lat_exp_date3 = sfh.get_ini_priv_date_lat_exp_date_candidate(cand_list)
lg.assert_and_log(ini_pri_date3 is not None, "\t \t FAIL: Expected Initial Privileging Date to be not None, but found "
                  + ini_pri_date3, "\t \t PASS: Initial Privileging Date is not blank.")
lg.assert_and_log(lat_exp_date3 is not None, "\t \t FAIL: Latest Expiration Date to be not None, but found "
                  + lat_exp_date3, "\t \t PASS: Latest Expiration Date is not blank.")
lg.assert_and_log(lat_exp_date3 != ini_pri_date2 and lat_exp_date3 != lat_exp_date2, "\t\t FAIL: Initial Privileging "
                  "Date and Latest Expiration Date are not updated after creating new Privileging File.", "\t\t PASS: "
                  "Initial Privileging Date & Latest Expiration Date are updated after creating new Privileging File")
lg.log_results("\t \t Initial Privileging Date: " + ini_pri_date3 + ", Latest Expiration Date:" + lat_exp_date3)

from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
from datetime import datetime as dt
from Helpers import log_helper as lg
import sf_connection_config as sf_cc
from simple_salesforce import Salesforce


script_start_time =  dt.now().strftime('%m-%d %H:%M:%S')
lg.log_results("\n\t START TIME: \t " + script_start_time)
lg.log_results("** Candidate REcruitment Credit Verification ** " )

#--If Recruitment Credit contains "Echo" or a user, Credit? equals Yes
#--If Recruitment Credit contains "Echo" or a user, populate user full name in Recruitment Credit Text
#--If Recruitment Credit equals "3rd Party Agency", Credit? equals No

can_query = "SELECT Credit__c,Name,Recruitment_Credit_Text__c,Recruitment_Credit__c FROM Candidate__c WHERE " \
            "RecordTypeId IN ('01280000000BlWs')"

result_name = sf_cc.sf_connect.query_all(can_query)
records = result_name['records']
record_count = len(records)
lg.log_results("The number of records are " + str(record_count))
index = 0

if records:
    while index < record_count:
        expected_string = ''
        expected_credit_bool = ''
        in_logic = 1
        third_party = 0
        data_line = ''
        ret_credit_bool = str(records[index]['Credit__c'])
        ret_can_name = str(records[index]['Name'])
        ret_rec_credit_text = str(records[index]['Recruitment_Credit_Text__c'])
        ret_rec_credit = str(records[index]['Recruitment_Credit__c'])

        if ret_credit_bool == 'None':
            ret_credit_bool = ''
        else:
            ret_credit_bool = ret_credit_bool.replace("'", '')
            lg.log_results("ret_credit_bool: " + ret_credit_bool)

        data_line = " Row: Candidate Name " + ret_can_name + ", Credit? "+ str(ret_credit_bool) \
                    + ", Recruitment_Credit_Text__c: " + str(ret_rec_credit_text) + \
                    ", Recruitment_Credit__c: "+  str(ret_rec_credit)
        lg.log_results("\n" + str(index) + data_line, 'BLACK')

        if ret_rec_credit in [None, 'None','']:
            third_party = 1
            expected_credit_bool = 'No'
        elif 'z ECHO' in ret_rec_credit:
            expected_string = ret_rec_credit.replace('z ECHO ', '')
            expected_credit_bool = 'Yes'
        elif 'ECHO' in ret_rec_credit:
            expected_string = ret_rec_credit.replace('ECHO ', '')
            expected_credit_bool = 'Yes'
        elif 'z ' in ret_rec_credit: #C-101366
            expected_string = ret_rec_credit.replace('z ', '')
            expected_credit_bool = 'Yes'
        elif ret_rec_credit in ['People Support', '3rd Party', 'Cogent','Cross Credential', 'MSA', 'Sound',
                                'Transfer', '']:
            third_party = 1
            expected_credit_bool = 'No'
        elif len(ret_rec_credit) > 1:
            expected_string = ret_rec_credit
            expected_credit_bool = 'Yes'
        else:
            in_logic = 0

        if in_logic:
            if third_party:     # Expected Credit? 'No'
                if ret_credit_bool == '':
                     lg.log_results("\t \t FAIL: Candidate Name " + ret_can_name + ", Expected Credit? '" + expected_credit_bool +
                     "' found " + ret_credit_bool, 'LIGHTRED_EX')
                elif expected_credit_bool == ret_credit_bool:
                    lg.log_results("\t \t PASS: " + ret_can_name + " has Expected Credit? '" + ret_credit_bool + "'",
                                   'GREEN')
                else:
                    lg.log_results("\t \t FAIL: Candidate Name " + ret_can_name + ", Expected Credit? '" + expected_credit_bool +
                     "' found " + ret_credit_bool, 'LIGHTRED_EX')

            else:
                if expected_string in str(ret_rec_credit_text):
                    lg.log_results( " \t \t PASS: "  +  ret_can_name + " - Recruitment_Credit_Text__c value updated as "
                        "expected to " + str(ret_rec_credit_text) ,  'GREEN')
                    lg.assert_and_log(expected_credit_bool == str(ret_credit_bool), "\t \t FAIL: " +
                                      ret_can_name + ", Expected Credit? '"+ str(expected_credit_bool) + "' found "+
                                      str(ret_credit_bool), "\t \t PASS: " + ret_can_name + " has " +
                                    "Expected Credit? '" + str(ret_credit_bool) + "'")
                else:
                    lg.log_results(" \t \t FAIL: " + ret_can_name+ " Recruitment_Credit_Text__c expected: " +
                                   expected_string + ", found: "  + str(ret_rec_credit_text) ,'LIGHTRED_EX')
                    if expected_credit_bool == str(ret_credit_bool):
                        lg.log_results("\t \t PASS: " + ret_can_name + " has " +
                                       "Expected Credit? '" + str(ret_credit_bool) + "'")
                    else:
                        lg.log_results("\t \t FAIL: " +
                                       ret_can_name + ", Expected Credit? '" + str(expected_credit_bool) + "' found " +
                                       str(ret_credit_bool))
                        # lg.assert_and_log(expected_credit_bool == str(ret_credit_bool), "\t \t FAIL: " +
                        #               ret_can_name + ", Expected Credit? '"+ str(expected_credit_bool) + "' found "+
                        #               str(ret_credit_bool), "\t \t PASS: " + ret_can_name + " has " +
                        #               "Expected Credit? '" + str(ret_credit_bool) + "'")
        else:
            lg.log_results( " \t \t INFO: " + " -- No Logic TO Process " ,'LIGHTBLACK_EX')

        index += 1



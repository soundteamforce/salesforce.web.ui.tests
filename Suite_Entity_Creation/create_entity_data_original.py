# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_entity_data.py env
# python create_entity_data.py evrebels
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import log_helper as lg
import api_config as aconfig
from Helpers import api_helper as apih
from Resources import test_variables as tv

# aconfig.account_rec_type = ["Hospital", "Hospital Partner"]
# aconfig.service_lines = ['Emergency Medicine', 'Hospital Medicine']

aconfig.account_rec_type = ["Hospital"]
aconfig.ssl_rec_type = 'Acute Partner'
aconfig.service_lines = ['Emergency Medicine', 'Hospital Medicine'] # wizard regression script depends on this,
# so to create 2 candidates.
aconfig.pos_rec_type = 'CPA Standard'
aconfig.cand_rec_type = ["LT / 1099", "W2"]


aconfig.position_details  = {0: {'position_title': 'Chief Hospitalist', 'suffix_str': ' (Chief)', 'create_p':True},
                      1: {'position_title': 'Nurse Practitioner', 'suffix_str': ' (NP)', 'create_p':True},
                      2: {'position_title': 'Physician Assistant', 'suffix_str': ' (PA)', 'create_p':False},
                      3: {'position_title': 'Locum Tenen', 'suffix_str': ' (Locum)', 'create_p':False}}

aconfig.at_review_status = 'Approved'      #'Pending'
aconfig.ag_to_create = [tv.TELE]            # wizard regression script depends on this.

aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':1}}
aconfig.is_hub = False

apih.start_script()
ah.begin_account_record()
ssl.begin_sound_service_line_record()
pos_h.begin_position_records()
atgl_h.begin_at_guideline_record()

lg.log_results("\n\t #--------------#  INFO: Input For Next Steps Is #--------------# ")
lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
lg.log_results("aconfig.all_ssl_ids = " + str(aconfig.all_ssl_ids))
lg.log_results("aconfig.all_position_ids = " + str(aconfig.all_position_ids))
lg.log_results("\n\t #--------------#  INFO: END #--------------# ")

cs.begin_contact_record()
lg.log_results("aconfig.all_contact_ids = " + str(aconfig.all_contact_ids))
can.begin_candidate_record()





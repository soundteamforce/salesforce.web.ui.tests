# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import log_helper as lg
from Helpers import hub_helper as hub_h
from Helpers import privileging_file_helper as pfile_h
from Helpers import hub_mngd_position_helper as hmpos_h
import datetime


#Part 1 - Create Hub, Account, SSL and Positions. # Input is create_hub, num_ssl_records, position_title
#Part 2 - Create contact and Candidate records. # Input for candidate will be account, position title..

num_ssl_records = 2
is_hub = True
all_hub_fields = False
position_title = 'Nurse Practitioner'
ssl_rec_type = 'Partner Standard'
# position_title = 'Chief Hospitalist'
# position_title = 'Physician Assistant'
# position_title = 'Locum Tenen'
ret_hub_id = ''
ret_hub_name = ''
suffix_str = ''
link_pos_hmposition = True
#Part 1

current_time = datetime.datetime.now().strftime('%m-%d %H:%M:%S')

lg.log_results("\t START TIME: \t " + current_time)
lg.log_results("\nINFO: \t TeleMedicine Hub - Begin Creating Salesforce records in test environment - " + sf_cc.test_env)
if is_hub:
    lg.log_results("\nINFO: \t TeleMedicine Hub - Begin creating SSL HUB records.")
    ret_hub_id, ret_hub_name = hub_h.create_hub_record(all_hub_fields)
    lg.assert_and_log(ret_hub_id is not None, 'FAIL: \t Could Not create Telemedicine Hub. ',
                      "PASS: \t TeleMedicine Hub Created Id - " + ret_hub_id + " And Name - '" + ret_hub_name + "'" )

lg.log_results("\nINFO: \t Begin creating ACCOUNT records.")
ret_account_ids, ret_account_names = ah.begin_account_record(['Hospital'])
if ret_account_ids:
    lg.log_results("PASS: \t Account Ids are : " + str(ret_account_ids))
    lg.log_results("\nINFO: \t Begin creating " + str(num_ssl_records) + " SOUND SERVICE LINE "
                                                                                            "records.")
    ret_sservice_line_ids = ssl.begin_hub_ssl_records(ret_account_ids, 'Partner Standard', num_ssl_records, ret_hub_id)
    if ret_sservice_line_ids:
        lg.log_results("\nSSLs are \n[" + str(ret_sservice_line_ids) + "\n]")
        lg.log_results("\nINFO: \t Begin creating HUB MANAGED POSITION records.")
        ret_hub_mngd_position_ids = hmpos_h.begin_hm_position_record(ret_hub_id,ret_sservice_line_ids,
                                                                     position_title, suffix_str)
        lg.log_results("PASS: \t Hug Managed Position Record : " + str(ret_hub_mngd_position_ids))
        lg.log_results("\nINFO: \t Begin creating POSITION records.")
        ret_position_ids = pos_h.begin_position_records(ret_sservice_line_ids, position_title)
    else:
        lg.log_results("FAIL: \t Could not create Sound Service Line data records.  ", "LIGHTRED_EX")
    # Making sure that accounts are created before contactIds even if there is no dependencies as such.
else:
    lg.log_results("FAIL: \t Could not create Account data records.", "LIGHTRED_EX")

# Part 2
if ret_account_ids:
    lg.log_results("\nINFO: \t Begin creating CONTACT records ")
    ret_contact_id, contact_name = cs.begin_contact_record()
    if ret_contact_id:
        lg.log_results("\nINFO: \t Begin creating CANDIDATE record." )
        all_candidate_ids, all_candidate_names = can.begin_hub_candidate_record()
        # all_candidate_ids, all_candidate_names = can.begin_hub_candidate_record(ret_contact_id, ret_account_ids,
        #                                                                         position_title)
        if all_candidate_ids:
            lg.log_results("\nINFO: \t Begin Creating Privileging File records. ")
            pfile_h.begin_creating_privileging_file(all_candidate_ids)
        else:
            lg.log_results("FAIL: \t CONDIDATE record did not get created." , "LIGHTRED_EX")
    else:
        lg.log_results("FAIL: \t CONTACT record did not get created." , "LIGHTRED_EX")

end_time = datetime.datetime.now().strftime('%m-%d %H:%M:%S')
lg.log_results("\t END TIME: \t " + end_time)

# This is the part 3 -  when Part 1 is successful but Part 2 is unsucessful for some reason can be run again using
# ret_account_ids of successful part 1.
#
#
# lg.log_results("\nINFO: \t Begin creating CONTACT records ")
# ret_contact_id, contact_name = cs.begin_contact_record()
# if ret_contact_id:
#     lg.log_results("\nINFO: \t Begin creating CANDIDATE record." )
#     ret_candidate_id, candidate_name = can.begin_candidate_record(ret_contact_id, ret_account_ids, position_title, is_hub)
#     if not ret_candidate_id:
#         lg.log_results("\nFAIL: \t Could not create CANDIDATE record.", "LIGHTRED_EX")
# else:
#     lg.log_results("\nFAIL: \t CONTACT record did not get created." , "LIGHTRED_EX")


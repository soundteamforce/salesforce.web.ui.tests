
import sys
sys.path.append('../')
from Helpers import log_helper as lg
import api_config as aconfig
from datetime import date, datetime, timedelta as td
from Resources import test_variables as tv
from Helpers import candidate_helper as can_h
lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
from Helpers import pa_api_helper as pah
from Helpers import at_api_helper as ath
from Helpers import salesforce_helper as sh
import random
#--------------# Step 2 #--------------#

lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
aconfig.hub_id = 'xxx'
aconfig.hub_name = 'AutoHubxxx'


lg.log_results("After Manually executing 'Run Hub Update on Hub'" + aconfig.hub_name + "'")
lg.log_results("\t \t Creating Provider Agreements on the Primary Contracting Candidates.")
aconfig.is_hub = True
new_pa = True
all_at_types = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
                tv.RELOC,  tv.CLINC, tv.SIGBN, tv.QUABN] #tv.VISA, taken out tempoirarily.

#create PA and ATs on PCC. This can be done only with one SSL as an anchor site and one PCC #Select PCC for that hub.
can_h.get_pri_contracting_candidate()
lg.log_results("\t \t Total Primary Contracting Candidates are " + str(len(aconfig.all_pcc_names)) )
for each_pcc in aconfig.all_pcc_names:
    aconfig.pri_contractng_can_name = each_pcc
    aconfig.pri_contractng_can_id = sh.get_candidate_id_from_name(aconfig.pri_contractng_can_name)
    aconfig.pri_contact_id = sh.get_contact_id_from_cand(aconfig.pri_contractng_can_id)
    aconfig.pri_position_id, aconfig.pri_position_name = sh.get_position_from_candidate(aconfig.pri_contractng_can_name)
    aconfig.pa_id, aconfig.pa_name, new_pa = pah.begin_pa_creation('Provider Employment Agreement')
    if new_pa:
        at_types = random.sample(all_at_types, 3)   #selecting three random terms.
        #create agreement terms
        all_at_ids, all_at_names = ath.create_agreement_term(at_types)
        lg.log_results("\t \t PASS :Terms " + str(all_at_names) + " Added on " + aconfig.pa_name )
#
lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
# # #
#
#








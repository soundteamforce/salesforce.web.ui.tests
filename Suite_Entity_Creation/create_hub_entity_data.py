# ------------------------56388 Support - QA Automation for creating Salesforce records
# Accounts, SSLs, Positions and Contact, Candidates
# To Execute this program.
# python create_hub_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import contact_helper as cs
from Helpers import log_helper as lg
from Helpers import hub_helper as hub_h
from Helpers import hub_mngd_position_helper as hmpos_h
from Helpers import agreement_term_guideline_helper  as atg_h
import api_config as aconfig
from Helpers import api_helper as apih
from Helpers import hub_mngd_candidate_helper as hmc_h
from datetime import date, datetime, timedelta as td
from Helpers import hub_managed_guideline as hmg
from Resources import test_variables as tv
from Helpers import candidate_helper as can_h
from Helpers import pa_api_helper as pah
from Helpers import at_api_helper as ath
from Helpers import salesforce_helper as sh
from Helpers import hub_helper as hub_h

#--------------##--------------##--------------##--------------##--------------#
#Part 1 - Create Hub, Account, SSL and Positions.
#Part 2 - Create contact and Candidate records. Account value(s) need to be exists for Contact to create.
#--------------##--------------##--------------##--------------##--------------#
# The number of SSL is set at aconfig.num_ssl_records
# The number of hub_managed_position is decided by title + suffix + True/False
# The number of contacts is set at aconfig.contact_rec_type in dictionary
# The number of Hub candidates will be equal to combination of HubManagedPosition + Contact combination.
# The run Hub Candidate on the UI or the api call to the script '' will generate the candidates and positions.
#--------------##--------------##--------------##--------------##--------------#
#--------------# Reinitialized the values #--------------#

aconfig.account_rec_type = ['Hospital']
aconfig.ssl_rec_type = 'Partner Standard'
aconfig.service_lines = ['TeleMedicine']
aconfig.pos_rec_type = 'Standard Position'
aconfig.cand_rec_type = 'Candidate Standard'
aconfig.num_ssl_records = 1
aconfig.is_hub = True          # Is this hub related entity creation?
aconfig.create_hub = True      # Does hub needs to be created as a part of entity creation?
import random

lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':1},
                            1: {'con_rec_type': aconfig.contact_06_rec_type, 'num_contact_rec':0}}

aconfig.hub_mangd_posn = {0: {'position_title': 'Telehospitalist', 'suffix_str': 'TLH-Dec9', 'create_hmp':True},
                          1: {'position_title': 'Nurse Practitioner', 'suffix_str': 'np-19-2', 'create_hmp':True},
                          2: {'position_title': 'Medical Director, Telemedicine', 'suffix_str': 'MD-20-1',
                              'create_hmp':True},
                          3: {'position_title': 'Physician Assistant', 'suffix_str': 'pa-19-2', 'create_hmp':True}}

# # aconfig.hmg_to_create = []
# aconfig.hmg_review_status = 'Approved'      #'Pending'
# # aconfig.hmg_to_create = [tv.ACPBN, tv.CHFST,  tv.CLSAL,  tv.EDLOAN,  tv.HHCBN,  tv.HNIGHT,  tv.HSALL,  tv.MLDAY,
# #                         tv.PRDBN,  tv.PRMNT,  tv.PTO,  tv.QUABN,  tv.RELOC,  tv.RETBN,  tv.SDAY,  tv.SIGBN,
# #                         tv.WLOAD,  tv.TREAD,  tv.WRVU]
aconfig.hmg_to_create = [tv.HNIGHT, tv.SDAY]
all_at_types = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
                tv.RELOC, tv.VISA, tv.CLINC, tv.SIGBN, tv.QUABN]
#
# #--------------# Start Of the Script #--------------#

# #--------------# Step 1 #--------------#
hub_h.create_hub_record()
ah.begin_account_record()
ssl.begin_new_combined_ssl_record()
hmpos_h.begin_hm_position_record()

lg.log_results("\n\t #--------------#  INFO: Input For Next Steps Is #--------------# ")
lg.log_results("aconfig.hub_id = '" + str(aconfig.hub_id) + "'")
lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
lg.log_results("aconfig.all_hmp_ids = " + str(aconfig.all_hmp_ids))
lg.log_results("\n\t #--------------#  INFO: END #--------------# ")
# # #TODO add a case creation...
atg_h.begin_at_guideline_record()
# # #--------------# Step 2 #--------------#
cs.begin_contact_record()
lg.log_results("aconfig.all_contact_ids = '" + str(aconfig.all_contact_ids) + "'")
hmc_h.begin_hub_mnged_candidate_record()
lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
#
hub_h.update_hub_for_future_anchor_site()

# #--------------# Step 3 #--------------#
lg.log_results("After executing 'Run Hub Update' ")
#aconfig.hub_id = 'a4gS0000000Z80CIAS'
#
#create PA and ATs on PCC. This can be done only with one SSL as an anchor site and one PCC #Select PCC for that hub.
at_types = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
            tv.RELOC, tv.VISA, tv.CLINC, tv.SIGBN, tv.QUABN]
can_h.get_pri_contracting_candidate()
for each_pcc in aconfig.all_pcc_names:
    aconfig.pri_contractng_can_name = each_pcc
    aconfig.pri_contractng_can_id = sh.get_candidate_id_from_name(aconfig.pri_contractng_can_name)
    aconfig.pa_id, aconfig.pa_name, new_pa = pah.begin_pa_creation('Provider Employment Agreement')
    #create agreement terms
    if new_pa:
        at_types = random.sample(all_at_types, 2)   #selecting three random terms.
        all_at_ids, all_at_names = ath.create_agreement_term(at_types)
        lg.log_results("\t \t PASS :Terms " + str(all_at_names) + " Added on " + aconfig.pa_name )


lg.log_results("\n \t For Reference " )
lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
lg.log_results("aconfig.begin_hub_ssl_records = " + str(aconfig.all_ssl_ids))
lg.log_results("aconfig.all_position_ids = " + str(aconfig.all_position_ids))
lg.log_results("aconfig.all_contact_ids = " + str(aconfig.all_contact_ids) )
lg.log_results("aconfig.all_candidate_names = " + str(aconfig.all_candidate_names) )
lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))







# Testcase - Scenario 1
# Anchor Site: Blank               # Future Anchor Site has ATs etc
# After Run Hub Updates:
# No action on ATs on Future Anchor Site
# - Automation Notes
# - Hub not created in this script.
# - SSL 6, Contacts - 8, Positions will get created - 24,  Candidates will get created - 192
# - Manual Steps  - Create hub, bring one of the SSLs in the hub
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python name_of_this_file.py env
# python create_entity_data_xxxx.py evrebels
# ------------------------------------END----------------------------------------------------------------#
import sys
sys.path.append('../')
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import log_helper as lg
import api_config as aconfig
from Helpers import api_helper as apih
from Resources import test_variables as tv
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import pa_api_helper as pah
from Helpers import at_api_helper as ath
import random

aconfig.is_hub = True          # Is this hub related entity creation?
aconfig.create_hub = False      # Does hub needs to be created as a part of entity creation?
aconfig.account_rec_type = ["Hospital"]
aconfig.ssl_rec_type = 'Partner Standard'
aconfig.service_lines = ['TeleMedicine'] # wizard regression script depends on this,# so to create 2 candidates.
aconfig.num_ssl_records = 6
aconfig.pos_rec_type = 'Standard Position'
aconfig.cand_rec_type = 'Candidate Standard'

aconfig.position_details  = {0: {'position_title': 'Chief Hospitalist', 'suffix_str': ' (Chief)', 'create_p':True},
                      1: {'position_title': 'Nurse Practitioner', 'suffix_str': ' (NP)', 'create_p':True},
                      2: {'position_title': 'Physician Assistant', 'suffix_str': ' (PA)', 'create_p':True},
                      3: {'position_title': 'Locum Tenen', 'suffix_str': ' (Locum)', 'create_p':True}}

aconfig.at_review_status = 'Approved'      #'Pending'
aconfig.ag_to_create = [tv.TELE, tv.HWOCL, tv.SWOCL]            # wizard regression script depends on this.
aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':4},
                            1: {'con_rec_type': aconfig.contact_06_rec_type, 'num_contact_rec':4}}
all_at_types = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
                tv.RELOC, tv.VISA, tv.CLINC, tv.SIGBN, tv.QUABN]


# aconfig.all_account_ids = ['001P000001kGKr2IAG']
# aconfig.all_ssl_ids = ['a1WP00000021U78MAE', 'a1WP00000021U7DMAU']
# aconfig.all_position_ids = ['a08P0000009hcknIAA', 'a08P0000009hcksIAA']

apih.start_script()
ah.begin_account_record()
ssl.begin_hub_ssl_records()
pos_h.begin_position_records()
cs.begin_contact_record()
for each_contact_id in aconfig.all_contact_ids:
    aconfig.contact_id = each_contact_id
    can.begin_candidate_record()

# #--------------# Step 2  - Create the PAs and ATs on each candidate.#--------------#

for each_can_id in aconfig.all_candidate_ids:
    aconfig.candidate_id = each_can_id
    aconfig.pa_id, aconfig.pa_name, new_pa = pah.begin_pa_creation('Provider Employment Agreement')
    if new_pa:
        at_types = random.sample(all_at_types, 2)   #selecting three random terms.
        all_at_ids, all_at_names = ath.create_agreement_term(at_types)
        lg.log_results("\t \t PASS :Terms " + str(all_at_names) + " Added on " + aconfig.pa_name )


lg.log_results("\n \t Following Test Data Is Created. " )
lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
lg.log_results("aconfig.begin_hub_ssl_records = " + str(aconfig.all_ssl_ids))
lg.log_results("aconfig.all_position_ids = " + str(aconfig.all_position_ids))
lg.log_results("aconfig.all_contact_ids = " + str(aconfig.all_contact_ids) )
lg.log_results("aconfig.all_candidate_names = " + str(aconfig.all_candidate_names) )
apih.end_script()



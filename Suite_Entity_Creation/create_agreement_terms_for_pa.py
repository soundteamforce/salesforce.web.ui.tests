import sys
sys.path.append('../')
from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import at_api_helper as ath
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
import ui_config as uic


class create_at(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        lg.log_results("\nINFO: \t Creating Agreement Terms in test environment - " + sf_cc.test_env)

    def setUp(self):
        # THREE Values are passed .. in the script to make the script run..
        print("autoqa")
        self.pa_name =  'PA-009798' #PA-009798
        uic.position_name =  'EM.8979 AutoHospital UP (Staff)'  #'EM.1867 AutoHospital NO (Staff)'
        uic.contact_name =  'AAfFGJXP AALFGJXP'                  # 'AAfKWPKD AALKWPKD'
        #evrebels
        # print("evrebels")
        # self.pa_name =  'PA-009982'
        # uic.position_name =  'EM.4469 AutoHospital JP (Staff)'  #'EM.1867 AutoHospital NO (Staff)'
        # uic.contact_name =  'AAfTCFAR AALTCFAR'                  # 'AAfKWPKD AALKWPKD'

        lg.log_results("\n \t \t The Contact: \t " + uic.contact_name + "\n \t \t position: \t " + uic.position_name
                       + "\n \t \t Provider Agreement: \t " + self.pa_name)
        uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
        uic.position_id =  sh.get_pos_id(uic.position_name)
        uic.contact_id = sh.get_contact_id(uic.contact_name)
        self.pa_id = sh.get_provider_agreement_id(self.pa_name)
        uic.candidate_id_1, uic.candidate_name_1, uic.candidate_id_2, uic.candidate_name_2 =  sh.get_recent_candidate(
            uic.contact_id)
        lg.log_results("\n \t \t The test candidate: \t " + uic.candidate_name_1 + "\t \t  previous candidate: \t " + uic.candidate_name_2)


    def tearDown(self):
        lg.log_results('')

    @classmethod
    def tearDownClass(cls):
        lg.log_results('\t\n')

    @unittest.skip("Skipping")
    def test_01_create_terms_for_pea(self):
        pa_type = 'Provider Employment Agreement'
        terms = [
            'Clinical Salary'
            ,'ACP Bonus Eligibility'
            ,'Commencement Bonus'
            ,'Education Loan Repayment'
            ,'Home Health Care Bonus Eligibility'
            ,'Housing Allowance'
            , 'PTO or Sick'
            , 'Productivity Bonus Eligibility'
            , 'Promissory Note Amount'
            , 'Quality Bonus Eligibility'
            , 'Relocation Allowance'
            , 'Retention Bonus'
            , 'Moonlighting'
            , 'WRVU Pay Only'
            , 'Work Unit Pay'
            , 'Stipend'
            , 'Shift Pay'
            , 'Hourly Pay'
            , 'Workload & Schedule'
        ]
        lg.log_results("\t \t Adding Agreement Term(s)"  + str(terms))
        at_names = ath.begin_at_record(self.pa_id, self.pa_name , terms)

    @unittest.skip("Skipping")
    def test_02_create_terms_for_initial_cdl(self):
        pa_type = 'Initial CDL'
        terms = [
            'Hourly Pay'
            , 'Shift Pay'
            , 'Stipend'
            , 'WRVU Pay Only'
            , 'Work Unit Pay'
        ]
        lg.log_results("\nINFO: \t Position is " + uic.position_name + " Contact is " + uic.contact_name)
        lg.log_results("\t \n The record type of provider agreement getting created is '" + pa_type + "' ")
        at_names = ath.begin_at_record(self.pa_id, self.pa_name, terms)

    # @unittest.skip("Skipping")
    def test_03_create_terms_for_bol(self):
        pa_type = 'Buy Out Letter'
        terms = ['Buy Out']
        lg.log_results("\nINFO: \t Adding Agreement Term(s)"  + str(terms))
        at_names = ath.begin_at_record(self.pa_id, self.pa_name, terms)


if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

# def test_03_create_bol():
#     uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
#     ret_position_id =  sh.get_pos_id(uic.position_name)
#     uic.contact_id = sh.get_contact_id(uic.contact_name)
#     uic.candidate_id_1, uic.candidate_name_1, uic.candidate_id_2, uic.candidate_name_2 =  sh.get_recent_candidate(
#         uic.contact_id)
#
#     lg.log_results("\nINFO: \t Position is " + uic.position_name + " Contact is " + uic.contact_name)
#     # agreement term guidelines
#     # all_guideline_names = [
#     all_provider_agreements = [
#         'Initial CDL'
#         # 'Provider Employment Agreement'
#         # ,'Amendment'
#         # ,'Interim Leadership Position Letter'
#         # ,'Compensation Change Letter'
#         # ,'Secondary Site Letter'
#         # ,'Buy Out Letter'
#     ]
#     n = 0
#     for each_pa_type in all_provider_agreements:
#         lg.log_results("\t \n The record type of provider agreement getting created is '" + each_pa_type + "' ")
#         n_string = str(n)
#         ret_pa_id = 'ret_pa_id' + n_string
#         ret_pa, new_guideline_name = pah.begin_pa_creation(ret_position_id, each_pa_type)
#         n += 1
#
#
# def test_01_create_pea():
#     uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
#     ret_position_id =  sh.get_pos_id(uic.position_name)
#     uic.contact_id = sh.get_contact_id(uic.contact_name)
#     uic.candidate_id_1, uic.candidate_name_1, uic.candidate_id_2, uic.candidate_name_2 =  sh.get_recent_candidate(
#         uic.contact_id)
#
#     lg.log_results("\nINFO: \t Position is " + uic.position_name + " Contact is " + uic.contact_name)
#     # agreement term guidelines
#     # all_guideline_names = [
#     all_provider_agreements = [
#         'Initial CDL'
#         # 'Provider Employment Agreement'
#         # ,'Amendment'
#         # ,'Interim Leadership Position Letter'
#         # ,'Compensation Change Letter'
#         # ,'Secondary Site Letter'
#         # ,'Buy Out Letter'
#     ]
#     n = 0
#     for each_pa_type in all_provider_agreements:
#         lg.log_results("\t \n The record type of provider agreement getting created is '" + each_pa_type + "' ")
#         n_string = str(n)
#         ret_pa_id = 'ret_pa_id' + n_string
#         ret_pa, new_guideline_name = pah.begin_pa_creation(ret_position_id, each_pa_type)
#         n += 1
#
#
#





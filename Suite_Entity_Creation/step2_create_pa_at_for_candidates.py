
import sys
sys.path.append('../')
from Helpers import log_helper as lg
import api_config as aconfig
from datetime import date, datetime, timedelta as td
from Resources import test_variables as tv
from Helpers import candidate_helper as can_h
lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
from Helpers import pa_api_helper as pah
from Helpers import at_api_helper as ath
from Helpers import salesforce_helper as sh
import random
#--------------# Step 2 #--------------#
aconfig.service_lines = ['TeleMedicine']
aconfig.all_candidate_names = ['C-102446','C-102452','C-102458','C-102464','C-102471','C-102476','C-102482','C-102488','C-102495','C-102500','C-102506','C-102512','C-102519','C-102524','C-102530','C-102536','C-102543','C-102548','C-102554','C-102560','C-102567','C-102572','C-102578','C-102584','C-102591','C-102596','C-102602','C-102608','C-102615','C-102620','C-102626','C-102632','C-102639','C-102645','C-102650','C-102656','C-102662','C-102669','C-102674','C-102680']
all_at_types = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
                tv.RELOC, tv.VISA, tv.CLINC, tv.SIGBN, tv.QUABN]
all_pa_types = ['Provider Employment Agreement']

aconfig.is_hub = False
new_pa = True
#create PA and ATs on candidates.
lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
lg.log_results("\t \t Total Candidates are " + str(len(aconfig.all_candidate_names)) )
for each_can in aconfig.all_candidate_names:
    new_pa_type = random.sample(all_pa_types, 1)
    aconfig.candidate_name = each_can
    aconfig.candidate_id = sh.get_candidate_id_from_name(aconfig.candidate_name)
    aconfig.contact_id = sh.get_contact_id_from_cand(aconfig.candidate_id)
    aconfig.position_id, aconfig.position_name = sh.get_position_from_candidate(aconfig.candidate_name)
    aconfig.pa_id, aconfig.pa_name, new_pa = pah.begin_pa_creation('Provider Employment Agreement')
    if new_pa:
        at_types = random.sample(all_at_types, 3)   #selecting three random terms.
        #create agreement terms
        all_at_ids, all_at_names = ath.create_agreement_term(at_types)
        lg.log_results("\t \t PASS :Terms " + str(all_at_names) + " Added on " + aconfig.pa_name )

lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))
# # #
#
#








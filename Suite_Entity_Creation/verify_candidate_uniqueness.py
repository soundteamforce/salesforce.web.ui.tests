# ------------------------https://soundphysicians.tpondemand.com/entity/77783-tme-enhance-uniqueness-rule-for-candidate#
# Automation script to create duplicate candidates.
# Script will read the CSV file in the format of
# candidate_status,colleague_status,active_chkbox,result
# CDL,Terminated,FALSE,Not Active
# For various combinations of first three columns, the script will consider candidate active or not active as in
# result column.
# For all Active results, the matching rule will trigger and won't be able to create duplicate candidate.
# Expected message is 'DUPLICATES_DETECTED'.
# For all Non Active result values, the script should be able to create duplicate candidate as
# matching rule will not trigger. return value is [] or None
# For each contact, the 1 ssl, 1 position is created. Candidate is created twice for that position.
# Matching rule will trigger or not during creation of second/duplicate candidate depending on Result column.
# Values are logged in seperate log file and message column has the response code from saleforce.
########### MODIFIED SIMPLE-SALESFORCE UTILITY as a part of this project. ###########################
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
import api_config as aconfig
from Helpers import api_helper as apih
from Helpers import read_csv_file as rfile
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
import ui_config as uic
import sf_connection_config as sf_cc
import csv

aconfig.account_rec_type = ["Hospital"]
aconfig.ssl_rec_type = 'Acute Partner'
aconfig.service_lines = ['Hospital Medicine']
aconfig.num_ssl_records = 1
aconfig.pos_rec_type = 'CPA Standard'
aconfig.position_title = 'Physician Assistant'
aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':1}}
aconfig.is_hub = False
# aconfig.all_account_ids = ['0013F00000aMphtQAC']
# aconfig.all_ssl_ids = ['a1W3F000001RlZcUAK']
# aconfig.all_contact_ids = ['0033F00000QwkVAQAZ']
# # aconfig.all_position_ids = ['a083F0000037kmbQAA']

lg.log_results("\nINFO: \t Verification for matching rule on CANDIDATE's uniqueness fields." )
apih.start_script()
ah.begin_account_record()
cs.begin_contact_record()
record_header = ['Sr#', 'Candidate_status', 'Colleaggue_status', 'active_chbox', 'result', 'message']
m_dictnry, num_rows = rfile.read_file()
recs = []

file_name = lg.uniq_can_file_name
lg.log_results("INFO: \t The log file is - " + file_name)
uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)

with open(file_name, mode='w') as uc_file:
    uc_writer = csv.writer(uc_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    uc_writer.writerow(record_header)
    for index in range(num_rows):
        lg.log_results("\nINFO: \t Proccessing row number - " + str(index))
        aconfig.all_ssl_ids = []
        aconfig.all_position_ids = []
        ssl.begin_sound_service_line_record()
        pos_h.begin_position_records()
        recs, candidate_values = can.create_duplicate_candidate(m_dictnry, index)
        uc_writer.writerow(recs)
        lg.log_results("INFO: \t The Verification Result Is - " + str(recs))
        # Not running in PROD
        # sh.delete_current_record(candidate_values, aconfig.all_position_ids, aconfig.all_ssl_ids)










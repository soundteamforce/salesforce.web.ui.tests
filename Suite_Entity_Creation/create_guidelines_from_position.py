# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
import ui_config as uic

## INPUT NEEDED is POSITION NAME ##
# pos_name = 'EM.5251 AutoHospital XV (Staff)'
pos_name = 'EM.3098 AutoHPartner DD (Staff)'

uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
ret_position_ids =  sh.get_pos_id(pos_name)

lg.log_results("\nINFO: \t Begin Creating Guideline in test environment - " + sf_cc.test_env)
lg.log_results("\nINFO: \t Begin creating Agreement Term Guideline record for position " + pos_name)
# agreement term guidelines
# all_guideline_names = [
all_guideline_names = [
    'Compensation - ACP Bonus Eligibility',
                  'Compensation - Clinical Salary',
                  'Compensation - Commencement Bonus',
                  'Compensation - Education Loan Repayment',
                  'Compensation - Home Health Care Bonus Eligibility',
                  'Compensation - Hourly Pay',
                  'Compensation - Housing Allowance',
                  'Compensation - Moonlighting',
                  'Compensation - PTO or Sick',
                  'Compensation - Productivity Bonus Eligibility',
                  'Compensation - Promissory Note Amount',
                  'Compensation - Quality Bonus Eligibility',
                  'Compensation - Relocation Allowance',
                  'Compensation - Retention Bonus',
                  'Compensation - Shift Pay',
                  'Compensation - Stipend',
                  'Compensation - WRVU Pay Only',
                  'Compensation - Work Unit Pay',
                  'Additional - Workload & Schedule'
                ]
n = 0
for each_guideline_name in all_guideline_names:
    lg.log_results("\t \n The guideline getting created is " + each_guideline_name)
    n_string = str(n)
    ret_at_guideline_id = 'ret_at_guideline_id' + n_string
    ret_at_guideline_id, new_guideline_name = atgl_h.begin_at_guideline_record_per_name(ret_position_ids,
                                                                       each_guideline_name)
    n += 1











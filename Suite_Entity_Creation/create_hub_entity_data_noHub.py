# ------------------------56388 Support - QA Automation for creating Salesforce records
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_hub_entity_data_noHub.py env
# This is for Candidate Std when Hub is not created and positions and candidates are created.
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import contact_helper as cs
from Helpers import log_helper as lg
from Helpers import candidate_helper as can
from Helpers import position_helper as pos_h
import api_config as aconfig
from Helpers import api_helper as apih
from Resources import test_variables as tv
from Helpers import pa_api_helper as pah
from Helpers import at_api_helper as ath
import random
from datetime import date, datetime, timedelta as td
lg.log_results("\t START TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))

#--------------##--------------##--------------##--------------##--------------#

aconfig.account_rec_type = ['Hospital']
aconfig.ssl_rec_type = 'Partner Standard'
aconfig.service_lines = ['TeleMedicine']
aconfig.pos_rec_type = 'Standard Position'
aconfig.cand_rec_type = 'Candidate Standard'
aconfig.num_ssl_records = 1
aconfig.is_hub = True          # Is this hub related entity creation?
aconfig.create_hub = False      # Does hub needs to be created as a part of entity creation?

aconfig.position_details  = {0: {'position_title': 'Chief Hospitalist', 'suffix_str': 'ch-12-09', 'create_p':True},
                             1: {'position_title': 'Nurse Practitioner', 'suffix_str': 'np-xx', 'create_p':False},
                             2: {'position_title': 'Physician Assistant', 'suffix_str': 'pa-xx', 'create_p':False},
                             3: {'position_title': 'Locum Tenen', 'suffix_str': 'lcm-xx', 'create_p':False}}

aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':1},
                            1: {'con_rec_type': aconfig.contact_06_rec_type, 'num_contact_rec':0}}

all_at_types = [tv.HNIGHT, tv.SADMIN, tv.SDAY, tv.SSWING, tv.CHFST, tv.CLSAL, tv.ICU, tv.PTO, tv.PRDBN,
            tv.RELOC, tv.VISA, tv.CLINC, tv.SIGBN, tv.QUABN]
# all_at_types = [tv.TELE, tv.HWOCL, tv.SWOCL]
# #--------------# Start Of the Script #--------------#
apih.start_script()
# #--------------# Step 1 #--------------#
ah.begin_account_record()
ssl.begin_hub_ssl_records()
pos_h.begin_position_records()
# # #--------------# Step 2 #--------------#
# aconfig.all_account_ids = ['001P000001kGKr2IAG']
# aconfig.all_ssl_ids = ['a1WP00000021U78MAE', 'a1WP00000021U7DMAU']
# aconfig.all_position_ids = ['a08P0000009hcknIAA', 'a08P0000009hcksIAA']
# aconfig.all_contact_ids = ['003P000001RUB6XIAX']

cs.begin_contact_record()
for each_contact_id in aconfig.all_contact_ids:
    aconfig.contact_id = each_contact_id
    can.begin_candidate_record()

# #--------------# Step 3 #--------------#
# aconfig.all_candidate_names = ['C-103951', 'C-103952']
# aconfig.all_candidate_ids = ['a09P000000ApmmA', 'a09P000000Apmm5']

for each_can_id in aconfig.all_candidate_ids:
    aconfig.candidate_id = each_can_id
    aconfig.pa_id, aconfig.pa_name, new_pa = pah.begin_pa_creation('Provider Employment Agreement')
    if new_pa:
        at_types = random.sample(all_at_types, 3)   #selecting three random terms.
        all_at_ids, all_at_names = ath.create_agreement_term(at_types)
        lg.log_results("\t \t PASS :Terms " + str(all_at_names) + " Added on " + aconfig.pa_name )


lg.log_results("\n \t For Reference " )
lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
lg.log_results("aconfig.begin_hub_ssl_records = " + str(aconfig.all_ssl_ids))
lg.log_results("aconfig.all_position_ids = " + str(aconfig.all_position_ids))
lg.log_results("aconfig.all_contact_ids = " + str(aconfig.all_contact_ids) )
lg.log_results("aconfig.all_candidate_names = " + str(aconfig.all_candidate_names) )






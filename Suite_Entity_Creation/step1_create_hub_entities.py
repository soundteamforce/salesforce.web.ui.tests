# ------------------------56388 Support - QA Automation for creating Salesforce records
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_hub_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import contact_helper as cs
from Helpers import log_helper as lg
from Helpers import hub_helper as hub_h
from Helpers import hub_mngd_position_helper as hmpos_h
from Helpers import agreement_term_guideline_helper  as atg_h
import api_config as aconfig
from Helpers import api_helper as apih
from Helpers import hub_mngd_candidate_helper as hmc_h
from datetime import date, datetime, timedelta as td
from Resources import test_variables as tv


#--------------##--------------##--------------##--------------##--------------#
#Part 1 - Create Hub, Account, SSL and Positions.
#Part 2 - Create contact and Candidate records. Account value(s) need to be exists for Contact to create.
#--------------##--------------##--------------##--------------##--------------#
# The number of SSL is set at aconfig.num_ssl_records
# The number of hub_managed_position is decided by title + suffix + True/False
# The number of contacts is set at aconfig.contact_rec_type in dictionary
# The number of Hub candidates will be equal to combination of HubManagedPosition + Contact combination.
# The run Hub Candidate on the UI or the api call to the script '' will generate the candidates and positions.
#--------------##--------------##--------------##--------------##--------------#
#--------------# Reinitialized the values #--------------#

aconfig.account_rec_type = ['Hospital']
aconfig.ssl_rec_type = 'Partner Standard'
aconfig.service_lines = ['TeleMedicine']
aconfig.pos_rec_type = 'Standard Position'
aconfig.cand_rec_type = 'Candidate Standard'
aconfig.num_ssl_records = 6
aconfig.is_hub = True          # Is this hub related entity creation?
aconfig.create_hub = True      # Does hub needs to be created as a part of entity creation?

aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':0},
                            1: {'con_rec_type': aconfig.contact_06_rec_type, 'num_contact_rec':0}}

aconfig.hub_mangd_posn = {0: {'position_title': 'Telehospitalist', 'suffix_str': 'TLH-1210', 'create_hmp':False},
                          1: {'position_title': 'Nurse Practitioner', 'suffix_str': 'np-1210', 'create_hmp':False},
                          2: {'position_title': 'Medical Director, Telemedicine', 'suffix_str': 'MD-1210',
                              'create_hmp':False},
                          3: {'position_title': 'Physician Assistant', 'suffix_str': 'pa-1210', 'create_hmp':False}}

aconfig.hmg_review_status = 'Approved'      #'Pending'
# aconfig.hmg_to_create = [tv.ACPBN, tv.CHFST,  tv.CLSAL,  tv.EDLOAN,  tv.HHCBN,  tv.HNIGHT,  tv.HSALL,  tv.MLDAY,
#                         tv.PRDBN,  tv.PRMNT,  tv.PTO,  tv.QUABN,  tv.RELOC,  tv.RETBN,  tv.SDAY,  tv.SIGBN,
#                         tv.WLOAD,  tv.TREAD,  tv.WRVU]

#--------------# Start Of the Script #--------------#
apih.start_script()
#--------------# Step 1 #--------------#

hub_h.create_hub_record()
# ah.begin_account_record()
# ssl.begin_hub_ssl_records()
# hmpos_h.begin_hm_position_record()
#TODO add a case creation...
# atg_h.begin_at_guideline_record()

# cs.begin_contact_record()
# hmc_h.begin_hub_mnged_candidate_record()
# hub_h.update_hub_for_future_anchor_site()    #TODO update the anchor site at the end. after the terms are added on the
#
# lg.log_results("\n\t #--------------#  INFO: For reference #--------------# ")
# lg.log_results("aconfig.hub_id = '" + str(aconfig.hub_id) + "'")
# lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
# lg.log_results("aconfig.all_hmp_ids = " + str(aconfig.all_hmp_ids))
# lg.log_results("aconfig.all_contact_ids = " + str(aconfig.all_contact_ids) )
# lg.log_results("\n\t #--------------#  INFO: END #--------------# ")
# lg.log_results("\t \t Before executing 'Run Hub Update' manually on hub " + aconfig.hub_name)
# lg.log_results("\t \t Step 2 Needs following \n aconfig.hub_id = '" + aconfig.hub_id + "' \n aconfig.hub_name = '" +
#                aconfig.hub_name + "'")
lg.log_results("\t END TIME: \t " + datetime.now().strftime('%m-%d %H:%M:%S'))


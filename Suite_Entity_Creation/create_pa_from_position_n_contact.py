# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
from os import sys, path
import unittest
parent_dir_name = path.dirname(path.dirname(path.realpath(__file__)))
sys.path.append(parent_dir_name + "/salesforce.web.ui.tests")
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import pa_api_helper as pah
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import log_helper as lg
from Helpers import salesforce_helper as sh
import ui_config as uic


class create_pa(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        lg.log_results("\nINFO: \t Creating Provider Agreement in test environment - " + sf_cc.test_env)

    def setUp(self):
        print("autoqa")
        uic.position_name =  'TM.1089 AutoHospital ZA (TLH-Dec9)' #BUY
        uic.contact_name =  'AA03_0ZK AutoUserZK'
        # print("evrebels")
        # uic.position_name =  'EM.4469 AutoHospital JP (Staff)' #BUY
        # uic.contact_name =   'AAfTCFAR AALTCFAR' #

        uic.created_by_admin_id = sh.get_user_id(sf_cc.uname)
        uic.position_id =  sh.get_pos_id(uic.position_name)
        uic.contact_id = sh.get_contact_id(uic.contact_name)
        uic.candidate_id_1, uic.candidate_name_1, uic.candidate_id_2, uic.candidate_name_2 =  sh.get_recent_candidate(
            uic.contact_id)
        lg.log_results("\nINFO: \t Position is " + uic.position_name + "\n \t Contact is " + uic.contact_name)
        lg.log_results("\t The test candidate - " + uic.candidate_name_1 + "\t previous candidate - " +
                       uic.candidate_name_2)


    def tearDown(self):
        lg.log_results('')

    @classmethod
    def tearDownClass(cls):
        lg.log_results('\t End of TEST CASE.\n')

    @unittest.skip("Skipping")
    def test_01_create_pea(self):
        pa_type = 'Provider Employment Agreement'
        lg.log_results("\t \t \n The record type of provider agreement getting created is '" + pa_type + "' ")
        ret_pa, new_guideline_name = pah.begin_pa_creation(uic.position_id , pa_type)

    @unittest.skip("Skipping")
    def test_02_create_initial_cdl(self):
        pa_type = 'Initial CDL'
        lg.log_results("\t \t \n The record type of provider agreement getting created is '" + pa_type + "' ")
        ret_pa, new_guideline_name = pah.begin_pa_creation(uic.position_id , pa_type)

    # @unittest.skip("Skipping")
    def test_03_create_bol(self):
        pa_type = 'Buy Out Letter'
        lg.log_results("\t \t \n The record type of provider agreement getting created is '" + pa_type + "' ")
        ret_pa, new_guideline_name = pah.begin_pa_creation(uic.position_id , pa_type)

if __name__ == "__main__":
    unittest.main(module='__main__',argv=['test_env'])
    unittest.TestLoader.sortTestMethodsUsing = None
    runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import log_helper as lg


lg.log_results("\nINFO: \t Begin Creating Salesforce records in test environment - " + sf_cc.test_env)

lg.log_results("\nINFO: \t Begin creating ACCOUNT records.")
ret_account_ids, ret_account_names = ah.begin_account_record()
if ret_account_ids:
    lg.log_results("\n INFO: \t Account Ids are : " + str(ret_account_ids))
    lg.log_results("\nINFO: \t Begin creating SOUND SERVICE LINE records.")
    ret_sservice_line_ids = ssl.begin_sound_service_line_record(ret_account_ids)

    if ret_sservice_line_ids:
        lg.log_results("\n INFO: \t service_line Ids are : " + str(ret_sservice_line_ids))
        lg.log_results("\nINFO: \t Begin creating POSITION records.")
        ret_position_ids = pos_h.begin_position_records(ret_sservice_line_ids)

        if not ret_position_ids:
            lg.log_results("\nFAIL: \t Could not create Position data records.  ", "LIGHTRED_EX")
        else:
            lg.log_results("\n INFO: \t ret_position Ids are : " + str(ret_position_ids))
            #MCS - No need of agreement term guidelines
            lg.log_results("\nINFO: \t Begin creating Agreement Term Guideline record.")
            # agreement term guidelines
            all_guideline_names = ['Compensation - ACP Bonus Eligibility']
                              #      all_guideline_names = ['Compensation - ACP Bonus Eligibility',
                              # 'Compensation - Clinical Salary',
                              # 'Compensation - Commencement Bonus',
                              # 'Compensation - Education Loan Repayment',
                              # 'Compensation - Home Health Care Bonus Eligibility',
                              # 'Compensation - Hourly Pay',
                              # 'Compensation - Housing Allowance',
                              # 'Compensation - Moonlighting',
                              # 'Compensation - PTO or Sick',
                              # 'Compensation - Productivity Bonus Eligibility',
                              # 'Compensation - Promissory Note Amount',
                              # 'Compensation - Quality Bonus Eligibility',
                              # 'Compensation - Relocation Allowance',
                              # 'Compensation - Retention Bonus',
                              # 'Compensation - Shift Pay',
                              # 'Compensation - Stipend',
                              # 'Compensation - WRVU Pay Only',
                              # 'Compensation - Work Unit Pay'
                              # 'Additional - Workload & Schedule']
            n = 0
            for each_guideline_name in all_guideline_names:
                lg.log_results("\t \t The guideline getting created is " + each_guideline_name)
                n_string = str(n)
                ret_at_guideline_id = 'ret_at_guideline_id' + n_string
                ret_at_guideline_id, new_guideline_name = atgl_h.begin_at_guideline_record_per_name(ret_position_ids,
                                                                                   each_guideline_name)
                lg.log_results("Guideline created " + str(new_guideline_name) + " with id " + str(ret_at_guideline_id))
                n += 1
    else:
        lg.log_results("\nFAIL: \t Could not create Sound Service Line data records.  ", "LIGHTRED_EX")

    # Making sure that accounts are created before contactIds even if there is no dependencies as such.

    # lg.log_results("\nINFO: \t Begin creating CONTACT records ")
    # ret_contact_id, contact_name = cs.begin_contact_record()
    # if ret_contact_id:
    #     lg.log_results("\nINFO: \t Begin creating CANDIDATE record." )
    #     ret_candidate_id, candidate_name = can.begin_candidate_record(ret_contact_id, ret_account_ids)
    #     if not ret_candidate_id:
    #         lg.log_results("\nFAIL: \t Could not create CANDIDATE record.", "LIGHTRED_EX")
    # else:
    #     lg.log_results("\nFAIL: \t CONTACT record did not get created." , "LIGHTRED_EX")

else:
    lg.log_results("\nFAIL: \t Could not create Account data records.", "LIGHTRED_EX")


# ret_account_ids = ['0013F00000XCSkjQAH', '0013F00000XCSktQAH']
# lg.log_results("\nINFO: \t Begin creating CONTACT records ")
# ret_contact_id, contact_name = cs.begin_contact_record()
# if ret_contact_id:
#     lg.log_results("\nINFO: \t Begin creating CANDIDATE record." )
#     ret_candidate_id, candidate_name = can.begin_candidate_record(ret_contact_id, ret_account_ids)
#     if not ret_candidate_id:
#         lg.log_results("\nFAIL: \t Could not create CANDIDATE record.", "LIGHTRED_EX")
# else:
#     lg.log_results("\nFAIL: \t CONTACT record did not get created." , "LIGHTRED_EX")












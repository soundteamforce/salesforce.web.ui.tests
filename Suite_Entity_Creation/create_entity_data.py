# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Contact, Candidate, create test users like Test RDO, Test RVP, Test Support people etc.
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python create_entity_data.py env
# python create_entity_data.py evrebels
# ------------------------------------END----------------------------------------------------------------#
import sys
sys.path.append('../')
from Helpers import agreement_term_guideline_helper as atgl_h
from Helpers import log_helper as lg
import api_config as aconfig
from Helpers import api_helper as apih
from Resources import test_variables as tv
from Helpers import account_helper as ah
from Helpers import sound_service_line_helper as ssl
from Helpers import position_helper as pos_h
from Helpers import contact_helper as cs
from Helpers import candidate_helper as can
from Helpers import privileging_file_helper as pfh
from Helpers import salesforce_helper as sfh

aconfig.account_rec_type = ["Hospital"]
#aconfig.service_lines = ['Critical Care']
aconfig.service_lines = ['Hospital Medicine']
aconfig.ssl_rec_type = 'Partner Standard'
aconfig.num_ssl_records = 1
# For critical Care Service Line, the ssl_rec_type should be 'Acute Partner'
#aconfig.ssl_rec_type = 'Acute Partner'

# aconfig.service_lines = ['Telemedicine', 'TeleICU', 'EM','Hospital Medicine', 'Anesthesia' ]
# wizard regression script depends on this,# so to create 2 candidates. either two SSLs or two position_details
aconfig.pos_rec_type = 'CPA Standard'
# These are the only Candidate record types available now. Remaining are retired.
# ['Candidate Standard', 'External Locum', 'LT / 1099']
aconfig.cand_rec_type = ['Candidate Standard']
aconfig.position_details = {0: {'position_title': 'Chief Hospitalist', 'suffix_str': ' 0111(Chief)', 'create_p':True},
                      1: {'position_title': 'Nurse Practitioner', 'suffix_str': ' 0111(NP)', 'create_p':False},
                      2: {'position_title': 'Physician Assistant', 'suffix_str': ' (PA)', 'create_p':True},
                      3: {'position_title': 'Locum Tenen', 'suffix_str': ' (Locum)', 'create_p':False}}

aconfig.ag_review_status = 'Approved'      #'Pending'
aconfig.ag_to_create = [tv.SIGBN]            # wizard regression script depends on this.

aconfig.contact_rec_type = {0: {'con_rec_type': aconfig.contact_03_rec_type, 'num_contact_rec':1},
                            1: {'con_rec_type': aconfig.contact_06_rec_type, 'num_contact_rec':0},
                            2: {'con_rec_type': aconfig.contact_05_rec_type, 'num_contact_rec':0}}

aconfig.is_hub = False
apih.start_script()
ah.begin_account_record()
ssl.begin_new_combined_ssl_record()
pos_h.begin_position_records()
atgl_h.begin_at_guideline_record()

lg.log_results("\n\t #--------------#  INFO: Input For Next Steps Is #--------------# ")
lg.log_results("aconfig.all_account_ids = " + str(aconfig.all_account_ids))
lg.log_results("aconfig.all_ssl_ids = " + str(aconfig.all_ssl_ids))
lg.log_results("aconfig.all_position_ids = " + str(aconfig.all_position_ids))
lg.log_results("\n\t #--------------#  INFO: END #--------------# ")

cs.begin_contact_record()
can.begin_candidate_record()

lg.log_results("aconfig.all_contact_ids = " + str(aconfig.all_contact_ids))
lg.log_results("aconfig.all_candidate_names = " + str(aconfig.all_candidate_names))
lg.log_results("aconfig.all_candidate_ids = " + str(aconfig.all_candidate_ids))

#pfh.begin_creating_privileging_file(aconfig.all_candidate_ids)
#can.update_candidate_status()  # Added this to test Bug 88195
apih.end_script()



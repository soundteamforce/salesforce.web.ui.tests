#--------------Story #55597 Support - QA Automation for creating Salesforce records. ----------------#
# This is to create the following types of records of the entities.
# Account --> RecordType --> Hospital & Hospital Partner.
# Service Line --> RecordType --> Acute Service Line
# position --> EM  (serviceLine) + Emergency Medicine Physician (Pos Title),
#               Hospitalist (serviceLine) --> Chief Hospitalist (Position Title)
# -----------------------------------------------------------------------------------------------------#
# ------------------------56388 Support - QA Automation for creating Salesforce records - Contact, candidate & users--#
# Create test users like Test RDO, Test RVP, Test Support people etc.
# The required user types can be created  by passing the value to users list.
# users = ['Privileging Intake Coordinator', 'Privileging Consultant', 'Privileging Coordinator','ECHO Recruiting',
# 'Staffing Services Team']
# -----------------------------------------------------END----------------------------------------------#
# To Execute this program.
# python sf_create_entity_data.py env
# ------------------------------------END----------------------------------------------------------------#

import sys
sys.path.append('../')
import sf_connection_config as sf_cc
from Helpers import user_helper as user
from Helpers import log_helper as lg


lg.log_results("\nINFO: \t Begin Creating Salesforce records in test environment - " + sf_cc.test_env)

# # # Don't need to create the test user everytime.
lg.log_results("\nINFO: \t Begin creating USER record(s): ")
# users = ['Privileging Intake Coordinator', 'Privileging Consultant', 'Privileging Coordinator']
users = ['RDO']

#users = ['Privileging Intake Coordinator', 'Privileging Consultant', 'Privileging Coordinator', 'RDO']
ret_user_id, return_user_names = user.begin_user_record(users)
lg.assert_and_log(ret_user_id is None, "\nFAIL : \t USER record did NOT get created."
    , "\nPASS : \t USER record got created with Name: " + str(return_user_names))














# This config is needed to login to SF for comp_wizard_ui
# import warnings
# from urllib3.exceptions import InsecureRequestWarning
# warnings.simplefilter('ignore', InsecureRequestWarning)
import urllib3
urllib3.disable_warnings()

default_ui_test_env = 'evcreed'
wd = ""
web_wait = 20
web_longer_wait = 40
screen_num = 0
created_by_admin_id = ""
contact_name = ""
contact_id = ""
candidate_name_1 = ""
candidate_name_2 = ""
candidate_id_1 = ""
candidate_id_2 = ""
position_id = ""
position_name = ""
usr_rdo_id = ""
user_rdo_name = ""

new_pa_name = ""
new_pa_type = ""
new_pa2_name = ""
pa_apvr = ""
delete_existing_pa = 1
delete_existing_ats = 1
# enable_screenshot = True
enable_screenshot = True
error_msg = ""
# -----------------------------------------------------------------------------------------------------------------
# Target process resources
# CHANGE auite_name, run_target_process, and test_plan_run_id.
# -----------------------------------------------------------------------------------------------------------------
# Allows control over whether Target Process is updated as part of the run
# suite_name = 'Wizard MCS - Automation Tests - Functional Test Set'
# suite_name = 'Wizard MCS - Automation Tests - Regression Test Set'
# run_target_process = False
#'65844' # ORIGINAL Test Run : #TP - #65841
run_target_process = False
suite_name = 'Wizard Automation BVT'
test_plan_run_id = '79367'
tp_tag = 'sf_auto'
tp_test_case = False

child_test_plan_run_id = 0
errors = ""
passed_test_cases = 0
failed_test_cases = 0
total_test_cases = 0
test_case_name = None
test_cases = None
multiple_at_overlapping = False
first_at = True
login_count = 0
compliance_admin_user = 'Jennifer Hammonds'
# compliance_admin_user = 'Test Tester' #EMR compliance-profile, Business collegue - role
# compliance_admin_user = 'Test Compliance Admin'
privileging_admin = 'Fran Johnson'
# ce_collaborator = '' get some AA user
# ce_app_contact = '' get some other AA user
ce_intake_coordinator = ''
ce_privileging_coordinator = ''
ce_name = ''
is_ce_acquisition = False
ce_status_expected = ''
ce_recordtype_expected = ''
ce_id = ''
logoff_sf = 0

# suite_name = 'Wizard Test Automation'  #67246TC orphaned.
# Suite bvt test_plan_run_id = 67335  ... now for
# https://soundphysicians.tpondemand.com/entity/68873-test-automation-wizard-support new story 69227

pea_anesthesia_crna = 0
pea_anesthesia_physician = 0


from Helpers import log_helper as lg
from Helpers import login_as_user_with_search as lauws
import ui_config as uic
from Helpers import browser_utilities as bu
from Pages import comp_login as cl
from Helpers import ui_helper
from Resources import element_locator as e
from Helpers import salesforce_helper as sh

def create_privileging_file():
    lg.log_results("\t \t Creating Privileging file. ")
    bu.sleep_for(2)
    lauws.login_as_with_search(uic.privileging_admin, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')
    bu.sleep_for(2)
    bu.click_elmt(e.ce_new_privileging_btn_new)
    bu.send_keys_elmt_xpath(e.ce_new_privileging_record_type, 'Initial')  #recordtype = Initial
    bu.click_elmt(e.ce_new_privileging_continue_btn)


    bu.send_keys_elmt_xpath(e.ce_new_privileging_status, 'In Process')
    bu.send_keys_elmt_xpath(e.ce_new_privileging_file_flag_text, 'Green')

    bu.click_elmt_n_ss_b4_after(e.ce_new_privileging_file_save_btn)
    bu.sleep_for(2)
    ret_file = sh.get_pri_file()
    lg.assert_and_log(ret_file, "\t \t FAIL : Couldn't create privileging file successfully.",\
                      "\t \t PASS : Created privileging file successfully. " + str(ret_file))
    cl.log_off()
    return ret_file


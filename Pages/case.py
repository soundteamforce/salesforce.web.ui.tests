import sys
sys.path.append('../')
from selenium import webdriver
from Helpers import log_helper as lg
from Pages import login
from Helpers import utilities as u
from Helpers import case_helper as ch
from Resources import element_locator as e
import case_config as c
from Helpers import ignore_login_data as ild



def add_case_empt_event(record_type , case_req_catgry):
    d = c.wd
    try:
        lg.log_results("\nINFO: \t Entering values for New Case with Record Type '" + str(record_type) + "' ", "LIGHTBLACK_EX")
        lg.log_results("INFO: \t Request Category '" + str(case_req_catgry) + "' ", "LIGHTBLACK_EX")
        u.click_elmt(e.new_case_btn)
        u.select_val_visible_elmt(e.new_case_record_type, record_type)
        u.click_elmt(e.continue_btn)
        u.send_keys_elmt_xpath(e.new_case_subject_in, ch.case_subject)
        u.select_val_visible_elmt(e.new_case_req_catergory_drpdwn, case_req_catgry)
        u.send_keys_elmt_xpath(e.new_case_due_date, c.today_date)
        u.select_val_visible_elmt(e.new_case_status_drpdwn, ch.case_status)
        u.select_val_visible_elmt(e.new_case_priority_drpdwn, ch.case_priority)
        log_user_id = ch.get_user_id(ild.uname)
        case_contact_name = ch.get_contact_name(log_user_id)
        u.send_keys_elmt_xpath(e.new_case_contact_name, case_contact_name)
        u.send_keys_elmt_xpath(e.new_case_ini_description, ch.case_description)
        # Get the position field value from the salesforce DB/api
        log_user_id = ch.get_user_id(ild.uname)
        case_position = ch.get_position_number(log_user_id)
        # lg.log_results("Case_position is " + str(case_position), "YELLOW")
        u.send_keys_elmt_xpath(e.new_case_ini_position,  case_position)
        u.send_keys_elmt_xpath(e.new_case_ini_eff_date, c.today_date)
        u.click_elmt(e.new_case_save_btn)
    except TimeoutError as ex:
        lg.log_results("\nFAIL :\t\t Error on new case creation. " + str(ex), 'RED')
        d.quit()
        sys.exit(1)


def verify_case():
    d = c.wd
    lg.log_results("INFO: \t Verifying the case creation.", "LIGHTBLACK_EX")
    case_validate = {}
    case_validate["request_category"] = u.get_text(d, e.v_request_category_e)
    case_validate["record_type"] = u.get_text(d, e.v_record_type_e)
    case_validate["queue_owner"] = u.get_text(d, e.v_case_owner_e)
    case_validate["case_number"] = u.get_text(d, e.v_case_number_e)

    return case_validate


def add_case_support_case(record_type, case_req_catgry):
    d = c.wd
    try:
        lg.log_results("\nINFO: \t Entering values for New Case with Record Type '" + str(record_type) + "' ", "LIGHTBLACK_EX")
        lg.log_results("INFO: \t Request Category '" + str(case_req_catgry) + "' ", "LIGHTBLACK_EX")
        u.click_elmt(d, e.new_case_btn)
        u.select_val_visible_elmt(d, e.new_case_record_type, record_type)
        u.click_elmt(d, e.continue_btn)
        u.send_keys_elmt_xpath(d, e.new_case_subject_in, ch.case_subject)
        u.select_val_visible_elmt(d, e.new_case_req_catergory_drpdwn, case_req_catgry)
        u.send_keys_elmt_xpath(d, e.new_case_due_date, c.today_date)
        u.select_val_visible_elmt(d, e.new_case_status_drpdwn, ch.case_status)
        u.select_val_visible_elmt(d, e.new_case_priority_drpdwn, ch.case_priority)
        # Get the case_contact_name from the logged in user from DB/api.
        log_user_id = ch.get_user_id(ild.uname)
        case_contact_name = ch.get_contact_name(log_user_id)
        u.send_keys_elmt_xpath(d, e.new_case_contact_name, case_contact_name)
        u.send_keys_elmt_xpath(d, e.new_case_ini_description, ch.case_description)
        u.select_val_visible_elmt(d, e.case_change_type, ch.case_type)
        u.select_val_visible_elmt(d, e.case_pro_agreemt_needed, ch.case_pa_needed)
        u.select_val_visible_elmt(d, e.case_new_agrmt_trm_track, ch.case_nw_at_track)
        u.send_keys_elmt_xpath(d, e.case_sound_service_line, ch.case_SSL)
        u.click_elmt(d, e.new_case_save_btn)
    except TimeoutError as ex:
        lg.log_results("\nFAIL :\t\t Error on new case creation. " + str(ex), 'RED')
        d.quit()
        sys.exit(1)







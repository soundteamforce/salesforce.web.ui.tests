from Helpers import browser_utilities as bu
from Helpers import log_helper as lg
from Resources import element_locator as e
import ui_config as uic

def create_new_emp_req(emp_type):

    lg.log_results("\t \t New Employement Requirement of type - " + str(emp_type))

    bu.sleep_for(1)
    if 'cpa' in emp_type.casefold():
        bu.click_elmt(e.ce_cpa_new_effective_date_link)
        position_xpath_str = e.ce_cpa_new_er_pos_xpath_str
    elif 'qa' in emp_type.casefold():
        # effective date
        bu.click_elmt(e.ce_qa_new_effective_date_link)
        bu.send_keys_elmt_xpath(e.ce_qa_new_interval_input, '1')
        position_xpath_str = e.ce_qa_new_er_pos_xpath_str
    elif 'ratio' in emp_type.casefold():
        bu.click_elmt(e.ce_ratio_new_effective_date_link)
        bu.send_keys_elmt_xpath(e.ce_ratio_new_desc_input, 'Not Combined')
        bu.send_keys_elmt_xpath(e.ce_ratio_new_pri_ratio_num, '3')
        position_xpath_str = e.ce_ratio_new_er_pos_xpath_str

    bu.sleep_for(1)
    bu.click_elmt_n_ss_b4_after(e.ce_new_emp_req_save_btn)
    ret_val = check_errors()

    return (ret_val, position_xpath_str)


def check_errors():
    err_msg = "Error"
    element = bu.find_element_by_xpath("//div[@id='ep']")
    if err_msg.casefold() in element.text:
        return 'error'
    else:
        return 'success'








from Helpers import login_as_user_with_search as lauw_search
from Helpers import ui_helper
import ui_config as uic
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
from Pages import comp_login as cl
from Resources import element_locator as e
from Helpers import salesforce_helper as sh
from Helpers import login_as_user_with_search as lauws

def check_ce_acquisition():
    lg.log_results("\t \t Selecting the Acquisition option of Collaboration Engine '" + uic.ce_name + "'")
    bu.sleep_for(1)
    lauw_search.login_as_with_search(uic.compliance_admin_user, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')

    #     go to collab engine
    uic.ce_name = bu.get_text(e.cand_collab_engine_link)
    bu.click_elmt(e.cand_collab_engine_link)

    #  Temp comment
    bu.double_click(e.ce_acquisition_chbox_div)
    bu.click_elmt(e.ce_acquisition_chbox)
    bu.click_elmt_n_ss_b4_after(e.ce_inline_save_btn)

    uic.is_ce_acquisition = True
    verify_ce_acquisition()
    verify_docusign_status()

    cl.log_off()


def verify_ce_acquisition():
    bu.sleep_for(1)
    if uic.is_ce_acquisition is True:
        uic.ce_status_expected = 'Active'
        uic.ce_recordtype_expected = 'Collaboration Engine - Acquisition'

#   Check 3 employment requirements
    ce_dict_val = sh.get_collab_engine_details()

    ce_app_candidate_id_value = ce_dict_val['ce_app_candidate_id']
    ce_qa_er_id_value = ce_dict_val['ce_qa_er_id']
    ce_cpa_er_id_value = ce_dict_val['ce_cpa_er_id']
    ce_ratio_er_id_value = ce_dict_val['ce_ratio_er_id']
    ce_recordtype_id_value = ce_dict_val['ce_recordtype_id']
    ce_status_value = ce_dict_val['ce_status']
    uic.ce_id = ce_dict_val['ce_id']
    # ce_acquisition_value = ce_dict_val['ce_acquisition']
    # if ce_acquisition_value:
    #     ce_acquisition_value_str = 'True'
    # else:
    #     ce_acquisition_value_str = 'False'

    #  Values from DB
    cpa_er_name = sh.get_er_record(ce_cpa_er_id_value, 'CPA')
    qa_er_name = sh.get_er_record(ce_qa_er_id_value, 'QA')
    ratio_er_name = sh.get_er_record(ce_ratio_er_id_value, 'Combined')
    ce_recordtype_name = sh.get_ce_recordtype_name(ce_recordtype_id_value, 'Collaboration_Engine__c')
    ce_app_candidate_fullname_type = sh.get_candidate_from_id(ce_app_candidate_id_value)
    ce_app_candidate_fullname_type_name =  ce_app_candidate_fullname_type.replace(',', '').casefold()


    #  Values on Page
    ce_status_text = bu.find_element_by_xpath(e.ce_tble_xpath_str + "//tr[2]//td[4]").text
    cpa_row_text = bu.find_element_by_xpath(e.ce_tble_xpath_str + "//tr[6]").text
    qa_row_text = bu.find_element_by_xpath(e.ce_tble_xpath_str + "//tr[5]").text
    ratio_row_text = bu.find_element_by_xpath(e.ce_tble_xpath_str + "//tr[4]").text
    ce_recordtype_text = bu.find_element_by_xpath(e.ce_table_ce_recordtype_td).text
    ce_app_contact_text = bu.find_element_by_xpath(e.ce_tble_xpath_str + "//tr[2]//td[2]").text.casefold()

    contact_name_appeared = 1
    ce_app_contact_text_list = ce_app_contact_text.split()
    for each_word in ce_app_contact_text_list:
        if not each_word in ce_app_candidate_fullname_type_name:
            contact_name_appeared = 0

    lg.assert_and_log(contact_name_appeared == 1,
                      "\t \t Error in setting APP contact " + str(ce_app_contact_text), "\t \t APP Contact '" +
                      ce_app_contact_text + "' is set as expected")
    lg.assert_and_log(uic.ce_recordtype_expected in ce_recordtype_text, "\t \t CE record type is not as expected, "
                                                                     "it is '" + ce_recordtype_text + "'",
                      "\t \t Collaboration Engine recordtype as expected '" + ce_recordtype_text + "'")
    lg.assert_and_log(uic.ce_status_expected in ce_status_text, "\t \t The status value should be '" +
                      uic.ce_status_expected +"' not '" +
                      str(ce_status_text) + "'", "\t \t Collaboration Engine Status is '" + uic.ce_status_expected
                      + "' as Expected. ")
    lg.assert_and_log(cpa_er_name in cpa_row_text, "\t \t Error in CPA employement Requirements '" + str(
        cpa_er_name) + "'", "\t \t CPA Employment Requirement is '" + cpa_er_name + "' as expected")
    lg.assert_and_log(qa_er_name in qa_row_text, "\t \t Error in QA employement Requirements " + str(
        qa_er_name), "\t \t QA Employment Requirement is '" + qa_er_name + "' as expected")
    lg.assert_and_log(ratio_er_name in ratio_row_text, "\t \t Error in RATIO employement Requirements " + str(
        ratio_er_name), "\t \t RATIO Employment Requirement is '" + ratio_er_name + "' as expected")



def verify_docusign_status():

    lg.log_results("\t \t Verifying the docusign status of the supporting document.")
    docusign_name_status = sh.get_docusign_status()

    for i, each_dict in docusign_name_status.items():
        lg.assert_and_log(each_dict['ds_status'] is None,
            "\t \t Error in Supporting Document '" + str(each_dict['name']) + "' has status " + str(each_dict['ds_status'])
            , "\t \t Supporting Document '" + str(each_dict['name']) + "' has created successfully and hasn't sent a "
                                                                       "document for signature as expected.")

def mark_collab_engine_no_req():
    lg.log_results("\t \t Updating 'No Requirements' for Collaboration Engine to Checked.")
    bu.sleep_for(2)
    lauws.login_as_with_search(uic.compliance_admin_user, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')
    bu.click_elmt(e.cand_position_link)
    page_title = bu.find_element_by_xpath(e.position_title)
    page_title_text = page_title.text
    if ('Position' in page_title_text):
        bu.click_elmt(e.position_edit_btn)
        bu.click_elmt(e.position_no_req_checkbox)
        bu.click_elmt_n_ss_b4_after(e.position_save_btn)
        bu.sleep_for(2)
        no_req_checkbox = bu.find_element_by_xpath(e.position_no_req_chkbox_img)
        title_of_no_req_chbox = no_req_checkbox.get_attribute("title")
        lg.assert_and_log("Not Checked" not in title_of_no_req_chbox, "\t \t FAIL : Could not update the 'No "
                "Requirement' checkbox as expected.", "\t \t PASS :  Updated the 'No Requirement' checkbox as "
                                                      "expected.")


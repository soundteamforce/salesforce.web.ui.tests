import sys
import ui_config as cwuc
from Helpers import log_helper as lg
from Resources import element_locator as e
from Helpers import browser_utilities as bu
from Helpers import pa_wizard_helper as pawh
from Resources import test_variables as tv
from Pages import provider_agreement as pa_pg
from datetime import datetime, timedelta as td
from Helpers import salesforce_helper as sh

def add_agreement_guideline():
    lg.log_results("\t \t Starting to Add Agreement Guideline.")
    ret_text = bu.get_text(e.pac_pgdln_guide)
    if 'GUIDE-' not in ret_text:
        lg.log_results("\t \t There is no pre-approved Guideline to choose.", "BLUE")
        return False

    if not bu.is_visible_xpath(e.pac_pgdln_add_link):
        lg.log_results("\t \t  There is NO guideline for given position.", "BLUE")
        return False
    else:
        lg.log_results("\t \t Guideline for given position is " + cwuc.new_pa_name + " : " + cwuc.new_pa_type + "." )
        bu.click_elmt(e.pac_pgdln_add_link)
        bu.sleep_for(1)
        bu.send_keys_elmt_xpath(e.pa_agdln_start_date, tv.today_minus_10_days_mdyy)
        bu.sleep_for(1)
        bu.click_elmt_n_ss_b4_after(e.w_add_defaults_btn)
        bu.sleep_for(1)
        bu.click_elmt_n_ss_b4_after(e.w_execute_changes_btn)
        bu.sleep_for(10)  # This wait is needed to allow SF record get updated after approved guideline is added
        # pa_pg.pa_stage_validation(tv.pa_stage_approvd_value)
        pa_stage_val = sh.get_stage_for_pa(cwuc.new_pa_name)
        bu.sleep_for(2)
        expected_pa_stage_val = tv.pa_stage_approvd_value
        lg.assert_and_log(expected_pa_stage_val == pa_stage_val, '\t \t FAIL: Unexpected PA Stage value. Expected ' +
                          expected_pa_stage_val + " but got " +pa_stage_val, "\t \t PASS: Expected stage value of Provider " \
                                                                      "Agreement: " + pa_stage_val)
        return True


def update_agreement_guideline(pa_name, pa_type, fields_to_change):
    lg.log_results("\t \t Starting to Update Agreement Guideline. ")
    ret_text = bu.get_text(e.pac_pgdln_guide)
    if 'GUIDE-' not in ret_text:
        lg.log_results("\t \t There is no pre-approved Guideline to update.", "BLUE")
        return False
    if not bu.is_visible_xpath(e.pac_pgdln_add_link):
        lg.log_results("\t \t  There is NO guideline for given position.", "BLUE")
        return False
    else:
        lg.log_results("\t \t Guideline for given position is " + pa_name + " : " + pa_type + "." )
        bu.click_elmt(e.pac_pgdln_add_link)
        bu.sleep_for(1)
        for field_change in fields_to_change:
            if 'shift_hours' in field_change:
                if bu.is_visible_xpath(e.w_wizard_guideline_shift_hours_input):
                    lg.log_results("\t \t Updating the Guideline shift hours." + ".")
                    bu.clear_elmt_text(e.w_wizard_guideline_shift_hours_input)
                    bu.send_keys_elmt_xpath(e.w_wizard_guideline_shift_hours_input, "19")
            elif 'amount' in field_change:
                if bu.is_visible_xpath(e.w_wizard_guideline_shift_amount_input):
                    lg.log_results("\t \t Updating the Guideline shift Amount" + ".")
                    bu.clear_elmt_text(e.w_wizard_guideline_shift_amount_input)
                    bu.send_keys_elmt_xpath(e.w_wizard_guideline_shift_amount_input, "21300.98")
                    ele_amt = bu.find_element_by_xpath(e.w_wizard_guideline_shift_amount_input)
                    bu.execute_shift_tab(ele_amt)
                    bu.send_keys_elmt_xpath(e.pa_agdln_start_date, tv.today_minus_10_days_mdyy)
                    if bu.find_element_to_be_clickable(e.w_add_updated_btn):
                        bu.sleep_for(1)
                        bu.click_elmt_n_ss_b4_after(e.w_add_updated_btn)
                        bu.sleep_for(1)
                        bu.click_elmt_n_ss_b4_after(e.w_execute_changes_btn)
                        bu.sleep_for(3)
            elif 'end_date' in field_change:   # 93113
                if bu.is_not_visible_xpath(e.w_wizard_end_date_input):
                    lg.log_results("\t \t Updating the Guideline end date.")
                    bu.send_keys_elmt_xpath(e.pa_agdln_end_date, (datetime.now() + td(365)).strftime('%m/%d/%Y'))
                    ele_end_date = bu.find_element_by_xpath(e.pa_agdln_end_date)
                    bu.execute_tab(ele_end_date)
                    if bu.find_element_to_be_clickable(e.w_add_defaults_btn):
                        bu.sleep_for(1)
                        bu.click_elmt_n_ss_b4_after(e.w_add_defaults_btn)
                        bu.sleep_for(1)
                        bu.click_elmt_n_ss_b4_after(e.w_execute_changes_btn)
                        bu.sleep_for(3)
                        lg.log_results('\t \t Updated the guideline end_date.')


def add_agreement_term(at_arry, at_to_delete = None):
    lg.log_results("\t \t Starting to Add Agreement Term(s). ")
    n = 0
    for each_term in at_arry:
        bu.sleep_for(3)
        bu.click_elmt_n_ss_b4_after(e.w_add_unlisted_agreement_type)
       # bu.sleep_for(3)
        pawh.add_at_details(each_term)

    if tv.delete_at_line_item:
        delete_at_line_item(at_to_delete)

    if tv.exe_changes_btn_on:
        bu.click_elmt_n_ss_b4_after(e.w_execute_changes_btn)
        bu.sleep_for(1)
        error_flag = check_error()
        if error_flag:
            return
        else:
            bu.sleep_for(1)
            pa_stage_val = sh.get_stage_for_pa(cwuc.new_pa_name)
            if 'Buy Out' in each_term['at_type']:
                expected_pa_stage_val = tv.pa_stage_approvd_value
            else:
                expected_pa_stage_val = tv.pa_stage_app_reqtd_value

            lg.assert_and_log(expected_pa_stage_val == pa_stage_val, '\t \t FAIL: Unexpected PA Stage value ' +
                              pa_stage_val,
                              "\t \t PASS: Expected stage value of Provider Agreement: " + pa_stage_val)
            bu.sleep_for(5)


def change_candidate():
    lg.log_results("\t \t The changed candidate is " + cwuc.candidate_name_2)
    bu.clear_elmt_text(e.w_candidate_to_use_input)
    bu.send_keys_elmt_xpath(e.w_candidate_to_use_input, cwuc.candidate_name_2)
    bu.click_elmt(e.w_add_unlisted_agreement_type)    # adding this to be executed twice.


def check_error():
    bu.sleep_for(3)
    page_url = bu.get_current_url()
    is_error = False

    if 'ProviderAgreementCompilation' in page_url:
        error_text = bu.find_element_by_xpath("//div[@class='messageText']").text
        lg.assert_and_log(cwuc.error_msg in error_text, "\t \t FAIL: Failed", "\t \t PASS: Agreement Term creation failed as expected. " + error_text)
        bu.click_elmt(e.w_reset_btn)
        is_error = True

    return is_error

def validate_visibility_of_terms(test_terms, clause_type):
    lg.log_results("\t \t Verifying the terms being displayed with their Status value correctly. ")
    bu.sleep_for(2)
    expected_term_count = len(test_terms)
    pa_pg.edit_provider_agreement_terms()
    # Get the values from the page table
    table_e = bu.is_visible_xpath(e.w_left_at_table)
    text_in_table = table_e.text
    page_row_count = len(table_e.find_elements_by_xpath(".//tr"))

    page_index = int(page_row_count) - 1
    x = 1 # the dict will start with Index 0
    # Collect the values from the page
    page_term_details = {}
    while x <= page_index:
        page_term_details[x-1] = {}
        page_term_details[x-1]['at_type'] = bu.get_text(e.w_left_at_tr_str + str(x) + "]//td[4]//div")
        page_term_details[x-1]['status'] = bu.get_text(e.w_left_at_tr_str + str(x) + "]//td[3]//div")
        # page_term_details[x-1]['description'] = bu.get_text(e.w_left_at_tr_str + str(x) + "]//td[5]//div")
        page_term_details[x-1]['start_date'] = bu.get_text(e.w_left_at_tr_str + str(x) + "]//td[6]//div")
        page_term_details[x-1]['end_date'] = bu.get_text(e.w_left_at_tr_str + str(x) + "]//td[7]//div")
        temp_pa_name_el =  bu.find_element_by_xpath(e.w_left_at_tr_str + str(x) + "]//td[2]//div")
        pa_title = temp_pa_name_el.text
        if pa_title == None or pa_title == '':
            temp_pa_name = 'None'
        else:
            temp_pa_name = pa_title

        page_term_details[x-1]['pa_name'] = temp_pa_name
        page_term_details[x-1]['term_name'] = bu.get_text(e.w_left_at_tr_str + str(x) + "]//td[1]//div")
        x += 1

    if page_index == 0 and expected_term_count > 0:
        for each_term in test_terms:
            delta_between = tv.now - datetime.strptime(each_term['end_date'], '%m/%d/%Y')
            lg.assert_and_log((delta_between.days) > 45 and (each_term['at_type'] not in text_in_table),
               "\t \t FAIL: The Term of type " + each_term['at_type'] + " Should Not be displayed as End Date is > 45 "
                                                                      "Days",
                "\t \t PASS: The Term of Type " + each_term['at_type'] + " is Not Displayed as expected as End Date is "
                                                                       "> 45 Days")
    else:
        if 'Reject' in clause_type:
            for each_term in test_terms:                         # compare
                lg.assert_and_log(each_term['at_type'] in text_in_table,
                              "\t \t FAIL: The Term of type '" + each_term['at_type'] + "' Should be displayed.",
                              "\t \t PASS: The Term of Type '" + each_term['at_type'] + "' is Displayed as Expected "
                                                                                        "on Wizard Page")

        elif 'end_date' in clause_type:
            for each_term in test_terms:                         # compare
                days_in_past = (tv.now - datetime.strptime(each_term['end_date'], '%m/%d/%Y')).days
                if days_in_past > 45:
                    lg.assert_and_log(each_term['at_type'] not in text_in_table,
                        "\t \t FAIL: The Term of type '" + each_term['at_type'] + "' Should Not be displayed as "
                        "End Date is > 45 Days In Past",
                        "\t \t PASS: The Term of Type '" + each_term['at_type'] + "' is Not Displayed as expected  as "
                        "End Date is > 45 Days In Past")
                elif days_in_past <= 45:
                    for ii, each_page_term_detail in page_term_details.items():
                        if(each_term['at_type'] in each_page_term_detail['at_type']):
                            page_term_name = each_page_term_detail['term_name']
                            page_term_status = each_page_term_detail['status']
                            lg.assert_and_log('Inactive' in each_page_term_detail['status'],
                                "\t \t FAIL: Status of the term should not be " + page_term_status + " for '" +
                                              page_term_name + "'",
                                "\t \t PASS: Status of the term is '" + page_term_status + "' as expected for '" +
                                              page_term_name + "' with Term Type '" + each_term['at_type'] + "' as End "
                                                                                                             "Date is <= 45 Days In Past")
                        ii += 1

def check_visibility_of_atg(test_terms, exp_pa):
    lg.log_results("\t \t Verifying the guidelines are being displayed are not. ")
    bu.sleep_for(2)

    if bu.is_visible_xpath(e.w_atg_table_row):
        table_row = e.w_atg_table_row
        all_rows = bu.get_webelements(table_row)
        print(len(all_rows))

        for each_term in test_terms:
            i = 1
            found = False
            for row in all_rows:
                page_term_type = bu.get_text(e.w_atg_table_body + str("/tr[") + str(i) + str("]/td[2]"))
                if each_term['type__c'] in page_term_type:
                    found = True
                    page_term_name = bu.get_text(e.w_atg_table_body + str("/tr[") + str(i) + str("]/td[1]"))
                    lg.assert_and_log(cwuc.new_pa_type not in exp_pa,
                                      "\t \t FAIL: Agreement Term Guideline " + str(page_term_name) +" is not expected to show up",
                                      "\t \t PASS: Agreement "
                                      "Term Guideline " + str(page_term_name) +" of type " +each_term['type__c'] + " does show up in "  
                                                                                         "PA type of " + cwuc.new_pa_type)
                i += 1
            if found == False:
                lg.log_results("\t \t Agreement Term Guidelines of type " + str(each_term['type__c']) + " is not visible")



    else:
        lg.assert_and_log(cwuc.new_pa_type not in [tv.pa_type_APPCL, tv.pa_type_unsp],
                              "\t \t FAIL: Agreement Term Guidelines are not expected to show up",
                              "\t \t PASS: Agreement "
                              "Term Guidelines of " + test_terms[0]['type__c'] + " does not show up in "
                                                                                 "PA type of " + cwuc.new_pa_type)


def delete_at_line_item(at_del_list):
    for i, at in enumerate(at_del_list):
        deleted = 0
        row_elements = bu.get_webelements(e.w_line_item_rows)
        for j, row in enumerate(row_elements):
            current_desc = bu.get_text("//table[@class='slds-table slds-table_bordered slds-table_striped innerPadding']/tbody/tr["+str(j+1-deleted)+"]/td[6]")
            if at['at_description'] in current_desc:
                bu.click_elmt("//table[@class='slds-table slds-table_bordered slds-table_striped innerPadding']/tbody/tr["+str(j+1-deleted)+"]/td[14]/a")
                new_desc = bu.get_text(e.w_line_item_desc)
                if at['at_description'] not in new_desc:
                    deleted += 1
                    lg.log_results("\t \t PASS: The line item with description " + current_desc + " is deleted.")
                    break
            elif at['at_description'] not in current_desc and j == len(row_elements) - 1:
                lg.log_results("\t \t FAIL: Term not found.")

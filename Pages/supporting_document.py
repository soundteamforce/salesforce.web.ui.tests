import ui_config as uic
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
from Resources import element_locator as e
from Helpers import login_as_user_with_search as lauw_search
from Helpers import ui_helper
from Resources import test_variables as tv
from Helpers import salesforce_helper as sh
from datetime import datetime
from Resources import element_locator as e


def update_supporting_document_dates():
    lg.log_results("\t \t Updating the Supporting document Dates")

    # issue_date_str = str(tv.today_minus_30_days_mdyy)
    issue_date_str = str(tv.today_date_mdyy)
    expiration_date_str = str(tv.today_plus_30_days_mdyy)

    lauw_search.login_as_with_search(uic.compliance_admin_user, 'user')
    ui_helper.search_on_top(uic.candidate_name_1, 'Candidate')

    #     go to collab engine
    uic.ce_name = bu.get_text(e.cand_collab_engine_link)
    bu.click_elmt(e.cand_collab_engine_link)
    # Check if SD exists.
    # If yes,

    supporting_doc_name = bu.get_text(e.sd_name) # th element
    bu.click_elmt(e.sd_name) # th element
    lg.log_results("\t \t Updating Issue date to " + issue_date_str + " and "
            "expiration date to " + expiration_date_str)
    # Issue date is 2 months in past.
    bu.click_elmt(e.sd_edit_button)

    ele_issue_date = bu.find_element_by_xpath(e.sd_issue_date_div_1)
    bu.clear_elmt_text(e.sd_issue_date_div_1)
    bu.send_keys_elmt_xpath(e.sd_issue_date_div_1, issue_date_str)
    bu.execute_tab(ele_issue_date)

    bu.click_elmt_n_ss_b4_after(e.sd_save_button)
    bu.sleep_for(2)
    bu.click_elmt(e.sd_edit_button)
    ele_sd_exp_date_div_link = bu.find_element_by_xpath(e.sd_expiration_date_div_1)
    bu.clear_elmt_text(e.sd_expiration_date_div_1)
    bu.send_keys_elmt_xpath(e.sd_expiration_date_div_1, expiration_date_str)
    bu.sleep_for(2)     # Do not remove this sleep after expiration date editing.
    bu.execute_tab(ele_sd_exp_date_div_link)
    bu.sleep_for(2)     # Do not remove this sleep after expiration date editing.
    bu.click_elmt_n_ss_b4_after(e.sd_save_button)
    bu.sleep_for(2)
    verify_supporting_document(supporting_doc_name, issue_date_str, expiration_date_str)


def verify_supporting_document(spp_doc_name, issue_date, exp_date):

    lg.log_results("\t \t Verifying the supporting document updates.")
    sd_issue_date, sd_expiration_date, sd_next_sd_create_date = sh.get_supporting_doc(spp_doc_name)

    #Issue date
    page_issue_date = bu.get_text(e.page_issue_date)
    page_expiration_date = bu.get_text(e.page_expiration_date)
    page_sd_creates_date = bu.get_text(e.page_next_sd_create_date)
    next_sd_creates_date_1 = sd_next_sd_create_date.split("-")
    next_sd_creates_date_2 = next_sd_creates_date_1[1]  +"/" + next_sd_creates_date_1[2] + "/" + \
                                     next_sd_creates_date_1[0]
    next_sd_creates_date_dt = datetime.strptime(next_sd_creates_date_2 ,'%m/%d/%Y')
    next_sd_creates_date_formatted = next_sd_creates_date_dt.strftime('%#m/%#d/%Y')

    lg.assert_and_log(page_issue_date in str(issue_date), "\t \t Expected Issue date is " + str(
        issue_date) + " found " + page_issue_date , "\t \t The Issue date is updated "
        "correctly to " + page_issue_date)

    lg.assert_and_log(page_expiration_date in exp_date, "\t \t Expected Expiration date is " +
                      str(exp_date) + " , found " + page_expiration_date , "\t \t The Expiration date is updated " \
        "correctly to " + page_expiration_date)

    lg.assert_and_log(page_sd_creates_date in next_sd_creates_date_formatted, "\t \t The Next Supporting Document "
        "Creates On date is " + str(
      next_sd_creates_date_formatted) + " found " + page_sd_creates_date, "\t \t The Next Supporting Document "
         "Creates On Date updated correctly to " + page_sd_creates_date)



import sys
import sf_connection_config as sf_cc
import ui_config as cwuc
from selenium.common.exceptions import TimeoutException
from Helpers import log_helper as lg
from Resources import element_locator as e
from Helpers import browser_utilities as bu
from selenium import webdriver
from Resources import test_variables as tv

# check login pass or fail..

def login_sf():
    cwuc.wd = webdriver.Chrome()
    cwuc.wd.maximize_window()
    # wd = bu.get_web_driver()
    cwuc.wd.get(sf_cc.base_url)

    lg.log_results("\t \t Logging in to Salesforce.... ")
    lg.log_results("\t \t Launching Salesforce at " + sf_cc.base_url)
    lg.log_results("\t \t The test environment is " + sf_cc.test_env)
    # lg.log_results("INFO: \t The log file is " + lg.str_file_name, "LIGHTBLACK_EX")
    is_login = 0
    try:

        bu.send_keys_elmt_xpath(e.firstpg_login_uname, sf_cc.uname)
        bu.send_keys_elmt_xpath(e.firstpg_login_pswd, sf_cc.pswd)
        bu.click_elmt_n_ss_b4_after(e.firstpg_login_btn)
        if 'PROD' not in sf_cc.test_env:
            bu.is_visible_xpath(e.home_sandbox_lbl, cwuc.web_longer_wait)
            chk_sandbox = bu.is_visible_xpath(e.home_sandbox_lbl)

        # Switch the salesforce version to classic version
        switch_to_classic()

    except TimeoutException as ex:
        lg.log_results("FAIL: \t Login UN-Successful. Please check login information. ", 'LIGHTRED_EX')
        sys.exit(1)

    chk_user_full_name = bu.is_visible_xpath(e.home_usr_full_name)
    chk_env = bu.is_visible_xpath(e.home_env_lbl)
    # if chk_sandbox and chk_env and  chk_user_full_name:
    if chk_env and chk_user_full_name:
        is_login = 1

    lg.assert_and_log(is_login == 1, "\t \t FAIL : Could not login.", "\t \t PASS : Login Successful")

    return is_login


def log_off(logoff_sf = 0):

    bu.sleep_for(2)
    current_page = cwuc.wd.current_url
    page_user_fullname = bu.find_element_by_xpath("//a[@id='globalHeaderNameMink']//span[contains(text(),"
                                                  "login_as_uname)]").text
    if current_page.endswith("salesforce.com"):
        # lg.log_results("logging off " + "salesforce.com")
        return
    elif current_page.endswith("salesforce.com/"):
        # lg.log_results("logging off " + "salesforce.com/")
        return
    elif current_page.endswith(".salesforce.com/home/home.jsp"):
        if tv.test_admin_user not in page_user_fullname.casefold():
            bu.click_elmt(e.home_logout_popup)
            bu.click_elmt_n_ss_b4_after(e.home_logout_link)
            return
        else:
            if logoff_sf == 1:
                bu.click_elmt(e.home_logout_popup)
                bu.click_elmt_n_ss_b4_after(e.home_logout_link)
            else:
                return
    else:
        bu.click_elmt(e.home_logout_popup)
        bu.click_elmt_n_ss_b4_after(e.home_logout_link)
        # bu.click_elmt(e.home_logout_home)


def switch_to_classic():
    current_page = cwuc.wd.current_url
    if 'lightning.force.com' in current_page:
        # # switch_to_classic()
        classic_url_link = "https://soundphysicians--" + sf_cc.test_env + \
                           ".lightning.force.com/ltng/switcher?destination=classic&referrer=%2Flightning%2Fn%2Fzisf__zicHomeTab"
        bu.sleep_for(1)
        bu.goto_page(classic_url_link)
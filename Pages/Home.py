import sys
from Helpers import log_helper as lg
# Commented during Comp_wizard_bvt
# from Helpers import case_helper as ch
from Helpers import browser_utilities as bu
from Resources import element_locator as e
# import case_config as c
import ui_config as cwuc
from Helpers import salesforce_helper as sh
from Pages import comp_login as cl
from Helpers import login_as_user_with_search as lauw_search
import sf_connection_config as sf_cc
from Resources import test_variables as tv


def navigate_to_page(page_name):

# click on all_tab, cases.
    bu.click_elmt(e.all_tab_xpath)
    bu.click_elmt(e.cases_tab_xpath)



def login_admin():
    current_page = cwuc.wd.current_url
    if current_page.endswith("salesforce.com") or current_page.endswith("salesforce.com/"):
        cl.login_sf()
    else:
        chk_user_full_name = bu.is_visible_xpath(e.home_usr_full_name)
        if tv.test_admin_user.casefold() not in chk_user_full_name.text.casefold():
            cl.log_off()


def approve_reject_terms(exp_term_dictionary):
    lg.log_results("\t \t Starting to Approve or Reject Terms. ")
    at_db_dictry = {}
    i = 0
    x = 0
    bu.sleep_for(2)
    total_terms = len(exp_term_dictionary)
    if total_terms == 0:
        at_dictry = sh.get_term_details(cwuc.new_pa_name)
        bu.sleep_for(2)
        lg.assert_and_log(len(at_dictry) == total_terms, "\t \t FAIL: The number of terms that needs to be Approved "
            "or Rejected should be " + str(total_terms) + " not " + str(len(at_dictry)),
            "\t \t PASS: The number of terms that needs to be Approved "
                          "or Rejected are as expected " + str(len(at_dictry)))
    else:
        # get ALL the terms that needs to be approved or rejected.
        at_db_dictry = sh.get_term_details(cwuc.new_pa_name)
        cwuc.at_end_date = at_db_dictry[0]['term_end_date']    # adding this for bug 82449
        # this query at the variable level and not at the query level.
        bu.sleep_for(2)
        # Check if the terms sent is in the list of terms to be approved or rejected.
        primary_approvr = sh.get_primary_approver(cwuc.new_pa_name)
        lg.log_results("\t \t Primary approver is " + primary_approvr)
        cl.log_off(cwuc.logoff_sf)
        lauw_search.login_as_with_search(primary_approvr, 'user')
        for x, each_exp_term_dictionary in exp_term_dictionary.items():
            # Check for each dictionary in the nested one and execute the action needed ...................
            for i, each_db_dictnry in at_db_dictry.items():
                # lg.log_results("The expected dict is " + str(each_exp_term_dictionary))
                # lg.log_results("The DB dict is " + str(each_db_dictnry))
                if each_exp_term_dictionary['term_type_to_use'].casefold() in str(each_db_dictnry['description']).casefold():
                    # lg.log_results(str(each_db_dictnry['name']) + ' ' + str(each_db_dictnry['id']) + ' '
                    #     + str(each_db_dictnry['description']))
                    execute_action(str(each_db_dictnry['name']), each_exp_term_dictionary['action_taken'])
                i += 1
            x += 1
        cl.log_off()  # Logoff as approver


def execute_action(term_name, action_taken):
    lg.log_results("\t \t Starting to " + action_taken + " the term " + term_name)
    bu.sleep_for(2)
    row_element_xpath = bu.link_xpath_contains_text(term_name)
    row_element = bu.find_element_by_xpath(row_element_xpath)
    row_text = row_element.text
    if row_text in term_name:
        row_element.click()
        bu.click_elmt(e.appvr_items_to_appvr_rej_link)
        if action_taken in 'Reject':
            bu.send_keys_elmt_xpath(e.appvr_items_to_appvr_comment, 'Rejected By Automation Script. ')
            bu.click_elmt(e.appvr_items_to_reject_btn)
            expected_overall_status = 'Rejected'
        else:
            bu.send_keys_elmt_xpath(e.appvr_items_to_appvr_comment, 'Approved By Automation Script. ')
            bu.click_elmt(e.appvr_items_to_appvr_btn)
            expected_overall_status = 'Approved'

        bu.sleep_for(1)
        overall_status_text = bu.get_text(e.appvr_items_to_appvr_overall_status)
        bu.focus_to(e.appvr_items_to_appvr_overall_status)
        term_name = bu.get_text(e.appvr_items_to_appvr_description)

        lg.assert_and_log(expected_overall_status in overall_status_text, "\t \t FAIL: Could not " +
                          action_taken + "Term: " + term_name, "\t \t PASS: " + expected_overall_status
                          + " term " + term_name + " successfully.")
        # Go back to home page of the approver to check for next term.
        bu.click_elmt(e.appvr_items_to_appvr_home)


def approve_reject_pa(pa_name, action_taken):
    lg.log_results("\t\t Starting to Approve/Reject Provider Agreements.")

    # Get the name of Primary Clinical Approver
    pri_clinical_apprvr = sh.get_clinical_approver(cwuc.new_pa_name)
    lg.log_results("\t \t Primary approver is " + pri_clinical_apprvr)

    # Login as Primary Clinical Approver
    cl.log_off(cwuc.logoff_sf)
    lauw_search.login_as_with_search(pri_clinical_apprvr, 'user')

    lg.log_results("\t \t Starting to " + action_taken + " the PA " + pa_name)
    bu.sleep_for(2)
    row_element_xpath = bu.link_xpath_contains_text(pa_name)
    row_element = bu.find_element_by_xpath(row_element_xpath)
    row_text = row_element.text
    if row_text in pa_name:
        row_element.click()
        bu.click_elmt(e.appvr_items_to_appvr_rej_link)
        if action_taken in 'Reject':
            bu.send_keys_elmt_xpath(e.appvr_items_to_appvr_comment, 'Rejected By Automation Script. ')
            bu.click_elmt(e.appvr_items_to_reject_btn)
            expected_overall_status = 'Rejected'
        else:
            bu.send_keys_elmt_xpath(e.appvr_items_to_appvr_comment, 'Approved By Automation Script. ')
            bu.click_elmt(e.appvr_items_to_appvr_btn)
            expected_overall_status = 'Approved'

        bu.sleep_for(1)
        overall_status_text = bu.get_text(e.appvr_items_to_appvr_overall_status)
        bu.focus_to(e.appvr_items_to_appvr_overall_status)
        term_name = bu.get_text(e.appvr_items_to_appvr_description)

        lg.assert_and_log(expected_overall_status in overall_status_text, "\t \t FAIL: Could not " +
                          action_taken + "Provider Agreement: " + pa_name, "\t \t PASS: " + expected_overall_status
                          + " Provider Agreement " + pa_name + " successfully.")










# from selenium import webdriver
import sys
import case_config as c
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from sc_util import database_helper as db
from Helpers import log_helper as lg
from Helpers import utilities as u
from Resources import element_locator as e
from Helpers import ignore_login_data as ild

# check login pass or fail..

def login_sf(username, pswd, test_env):

    lg.log_results("INFO: \t Logging in to Salesforce.... ")

    d = u.get_web_driver()
    d.maximize_window()
    base_url = str(ild.base_url)
    d.get(base_url)


    lg.log_results("INFO: \t Launching Salesforce at " + base_url)
    lg.log_results("INFO: \t The test environment is " + ild.test_env)
    # lg.log_results("INFO: \t The log file is " + lg.str_file_name)

    try:
        u.send_keys_elmt_xpath(d, e.firstpg_login_uname, ild.uname)
        u.send_keys_elmt_xpath(d, e.firstpg_login_pswd,ild.pswd)
        u.click_elmt(d, e.firstpg_login_btn)

        element = WebDriverWait(d,10).until(EC.presence_of_element_located((By.XPATH, e.home_sandbox_lbl)))
    except TimeoutException as ex:
        lg.log_results("INFO: \t Login UN-Successful. Please check login information.", 'LIGHTRED_EX')
        d.quit()
        sys.exit(1)

    chk_sandbox = u.is_visible_xpath(d, e.home_sandbox_lbl)
    chk_env = u.is_visible_xpath(d, e.home_env_lbl)
    chk_user_full_name = u.is_visible_xpath(d, e.home_usr_full_name)

    if (chk_sandbox and chk_env and  chk_user_full_name):
        lg.log_results("INFO: \t Login Successful")
        is_login = 1
    else:
        lg.log_results("INFO: \t Login UN-Successful")
        is_login = 0

    return is_login, d









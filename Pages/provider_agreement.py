import sys
from Helpers import log_helper as lg
from Helpers import browser_utilities as bu
import ui_config as cwuc
from Resources import element_locator as e
from Helpers import salesforce_helper as sh
from Helpers import provider_agreement_helper as pah
from Helpers import comp_wizard_bvt_helper as w_bvt_hlpr
from Helpers import login_as_user_with_search as lauw_search
from Resources import test_variables as tv
from Resources import case_variables as cv
from Pages import comp_login as cl
import random as r
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By


def create_new_pa(pa_record_type, delete_pre_pa):

    agreement_type = ''
    create_second_pa = [tv.pa_rec_tp_amendment, tv.pa_rec_tp_compass, tv.pa_rec_tp_education, tv.pa_rec_tp_medical_corps]
    if delete_pre_pa:
        exsting_pa_id, exsting_pa_name = sh.get_existing_pa(cwuc.candidate_id_1)
        if exsting_pa_id and exsting_pa_name:
            cwuc.new_pa_name = exsting_pa_name

        sh.delete_pa(cwuc.delete_existing_pa, cwuc.delete_existing_ats)             # check if any orphaned terms in terms of NULL PA,
        # but attached to candidate.

    bu.click_elmt(e.cand_generate_pa_btn)

    pa_record = bu.find_element_by_xpath(e.pa_existing_pa_tbody)

    if pa_record.text and pa_record_type not in create_second_pa:
    # if pa_record.is_displayed() and pa_record_type != tv.pa_rec_tp_amendment:     #original
        lg.log_results("\t \t Provider Agreement already Exists. ")
        bu.click_elmt(e.pa_select_existing_pa_chbx)
        bu.click_elmt(e.pa_select_existing_pa_next_btn)
    #     Get the name of existing PEA
    else:
        lg.log_results("\t \t Creating new Provider Agreement of '" + pa_record_type + "' record type.")
        bu.click_elmt(e.pa_create_new_pa)
        new_pa_recordtype_element = e.pa_new_record_type_str + "'" + pa_record_type + "')]"

        bu.click_elmt(new_pa_recordtype_element)
        bu.click_elmt_n_ss_b4_after(e.pa_continue_btn)
        #     ON the NE Provider Agreement Page
        # bu.click_elmt(e.pa_agreement_type)
        if 'Initial CDL' in pa_record_type:
            agreement_type = "Initial CDL"
            bu.click_elmt(e.pa_cdl_return_date)
            bu.send_keys_elmt_xpath(e.pa_comments_text, "Automation Script Creation")
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif 'Compass Program Agreement' in pa_record_type:
            agreement_type = pa_record_type
            bu.click_elmt(e.pa_anticipated_start_date)
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif 'Education Stipend Agreement' in pa_record_type:
            agreement_type = pa_record_type
            bu.click_elmt(e.pa_anticipated_start_date)
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif 'Medical Corps Agreement' in pa_record_type:
            agreement_type = pa_record_type
            bu.click_elmt(e.pa_anticipated_start_date)
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif 'CDL' in pa_record_type:
            agreement_type = "CDL"
            bu.click_elmt(e.pa_cdl_return_date)
            bu.send_keys_elmt_xpath(e.pa_comments_text, "Automation Script Creation")
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)
            bu.send_keys_elmt_xpath(e.pa_agreement_stage, 'New')

        elif 'Buy Out Letter' in pa_record_type:
            lg.log_results("\t \t Primary Candidate '" + cwuc.candidate_name_1 + "' Previous candidate '" +
                           cwuc.candidate_name_2 + "'")
            bu.sleep_for(2)
            element = bu.find_element_by_xpath(e.pa_buy_previous_cand_input)
            bu.find_elmt_send_keys_xpath(e.pa_buy_previous_cand_input, cwuc.candidate_name_2)
            # agreement_type = "W2 Buy Out (1099/Echo Locums)"
            agreement_type = "Buy Out Letter, 3rd Party to Sound Perm"
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)
            bu.send_keys_elmt_xpath(e.pa_comments_text, "Automation Script Creation")

        elif 'Amendment' in pa_record_type:
            agreement_type = pa_record_type
            bu.click_elmt(e.pa_anticipated_amendment_start_date)
            bu.send_keys_elmt_xpath("//input[@id='CF00N0y000004pRew']", cv.case_number)
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif pa_record_type in ['Compensation Change Letter', 'Interim Leadership Position Letter', 'Position Removal Letter']:
            agreement_type = pa_record_type
            bu.send_keys_elmt_xpath("(//td[@class='dataCol']/span/input)[3]", cv.case_number)
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif 'Secondary Site Letter' in pa_record_type:
            agreement_type = pa_record_type
            bu.send_keys_elmt_xpath("//td[@class='last dataCol']/span/input", cv.case_number)
            bu.send_keys_elmt_xpath(e.pa_agreement_type, agreement_type)

        elif 'Independent Contractor Agreement' in pa_record_type:
            bu.send_keys_elmt_xpath(e.pa_ag_type_service_line, 'Hospital Medicine')   # added Hospital Medicine Service Line
            agreement_type = 'IC Agreement, Physician, Hospital Medicine (LLC)'       # new agreement type for HM
            bu.send_keys_elmt_xpath(e.pa_agreement_type_pea, agreement_type)
            bu.click_elmt(e.pa_anticipated_start_date_1)

        elif 'Clinical Ladder Tier' in pa_record_type:
            bu.select_val_visible_elmt("//td[@class ='dataCol col02']/div/span/select", 'Tier 3')
            bu.send_keys_elmt_xpath("//td[@class='dataCol']/span/input", cv.case_number)

        elif 'Unspecified' in pa_record_type:
            bu.click_elmt(e.pa_anticipated_start_date)


        elif 'Provider Employment Agreement' in pa_record_type:
            if cwuc.pea_anesthesia_crna == 1:
                bu.send_keys_elmt_xpath(e.pa_ag_type_service_line, 'Anesthesia')
                agreement_type = 'Employment Agreement, CRNA, Anesthesia'
            elif cwuc.pea_anesthesia_physician  == 1:
                bu.send_keys_elmt_xpath(e.pa_ag_type_service_line, 'Anesthesia')
                agreement_type = 'Employment Agreement, Physician, Anesthesia'
            else:
                bu.send_keys_elmt_xpath(e.pa_ag_type_service_line, 'Advisory Services')
                agreement_type = 'Employment Agreement, Physician, Advisory Services'

            bu.send_keys_elmt_xpath(e.pa_agreement_type_pea, agreement_type)
            bu.click_elmt(e.pa_anticipated_start_date_1)
            #bu.send_keys_elmt_xpath("//span[@class='dateInput dateOnlyInput']/input", "3/22/2022")

    bu.click_elmt_n_ss_b4_after(e.pa_save_btn)
    bu.sleep_for(5)
    # Get the name of the PROVIDER AGREEMENT HERE.
    h2_text = bu.get_text("//h2[@class='pageDescription']")
    if 'Provider Agreement Compilation' in h2_text:

        new_pa_elemt = bu.is_visible_xpath(e.w_pa_new_name, cwuc.web_longer_wait)
        new_pa_text = new_pa_elemt.text

        bu.take_screenshot()
        new_pa = new_pa_text.strip()
        new_pa_name = new_pa.split(',')[0]
        new_pa_type = new_pa.split(',')[1]
        new_pa_id = sh.get_pa_id(new_pa_name)
        bu.sleep_for(5)
        lg.assert_and_log(new_pa_name, "\t \t FAIL: Error while creating Provider Agreement.",
                          "\t \t PASS: Created Provider Agreement: " + new_pa_name + " of Type " + new_pa_type)

        pa_stage_val = sh.get_stage_for_pa(new_pa_name)
        lg.assert_and_log(tv.pa_stage_new_value == pa_stage_val, '\t \t FAIL: Unexpected PA Stage value ' +
                          pa_stage_val,
                          "\t \t PASS: Expected stage value of Provider Agreement: " + pa_stage_val)
        bu.sleep_for(5)

        return new_pa_name, new_pa_type, new_pa_id


def delete_terms(comp_at_number, addnl_at_number):
    lg.log_results("\t \t Starting to delete the terms. ")

    if comp_at_number > 0:
        row_link_var, index_val = pah.process_term_table('Compensation', comp_at_number)
        set_to_delete(row_link_var, index_val )

    if addnl_at_number > 0:
        row_link_var, index_val = pah.process_term_table('Additional', addnl_at_number)
        set_to_delete(row_link_var, index_val )


def set_to_delete(row_link, index):

    i = 2  # Always delete the Second row as header is first row
    while index > 1:
        #  HARDCODED PATH HERE
        element_xpath = row_link + str(i) + "]/th[1]/a"  # Always delete the Second row as header is first row

        term_name_element = bu.is_visible_xpath(element_xpath, cwuc.web_longer_wait)
        term_name = term_name_element.get_attribute('text')
        bu.click_elmt(element_xpath)
        #     delete it and it will go back to this page.
        delete_agreement_terms(term_name)
        index -= 1
    lg.log_results("\t \t Agreement Terms deleted successfully.")



def delete_agreement_terms(at_name):

    bu.sleep_for(1)
    bu.click_elmt_n_ss_b4_after(e.ct_delete_btn)
    bu.sleep_for(1)
    bu.click_elmt_n_ss_b4_after(e.ct_confirm_delete_btn)
    bu.sleep_for(2)

    at_deleted = sh.is_agreement_term_deleted(at_name)
    lg.assert_and_log(at_deleted is True, "\t \t FAIL : Agreement Term - " + at_name + " did NOT get deleted "
        "successfully", "\t \t PASS : Agreement Term - " + at_name + " got deleted successfully")


def edit_provider_agreement_terms():
    lg.log_results("\t \t Navigating To Wizard page.....")
    bu.sleep_for(3)
    bu.click_elmt_n_ss_b4_after(e.pa_edit_pa_terms_btn)


def navigate_to_provider_agreement_page():
    # This is for table elements underneath
    element_1 = bu.is_visible_xpath(e.home_searched_pa_link)
    bu.sleep_for(2)
    bu.click_elmt_with_sleep(e.home_searched_pa_link, 40)
    bu.take_screenshot()
    # lg.log_results("\t \t The stage value of PA after before adding the terms is " + str(bu.get_text(e.pa_stage)))


# def navigate_to_agreement_term_page():
#
#     element = bu.find_element_by_xpath(e.pa_agreement_term_name)
#     # element = bu.is_visible_xpath_wait_longer( e.pa_agreement_term_name)
#     element_text = str(element.text)
#     lg.log_results("\t \t Navigating to " + element_text + "." )
#     if element:
#         bu.click_elmt(e.pa_agreement_term_name)
#
#     return element_text


def pa_stage_validation(expected_stage_value):
    bu.sleep_for(5)
    pa_stage_text = bu.get_text(e.pa_stage_1)
    bu.scroll_to(e.pa_stage_1)
    bu.sleep_for(1)
    lg.assert_and_log(expected_stage_value == pa_stage_text, '\t \t FAIL: Expected stage is - ' +
                      expected_stage_value
                      + ', not ' + pa_stage_text, '\t \t PASS: Stage value of provider is as expected - ' +
                      expected_stage_value)

    bu.sleep_for(4)
    bu.take_screenshot()


def validate_term_status(login_as_uname, term_num, action_type):
    lg.log_results("\t \t Verifying the term Review Status and Status. ")
    expected_review_status = ''
    lauw_search.login_as_with_search(login_as_uname, 'user')
    w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
    navigate_to_provider_agreement_page()
    at_name_dictry, at_id_dictry = sh.get_at_ids_names(cwuc.new_pa_name, action_type)
    for each_term in at_name_dictry:
        if action_type in 'Reject':
            expected_review_status = 'Declined'
        elif action_type in 'Approve':
            expected_review_status = 'Approved'

        ret_rw_status, ret_stat = sh.get_rw_stat_and_stat(cwuc.new_pa_name, each_term)
        lg.assert_and_log((expected_review_status in ret_rw_status) and ('Pending Addition' in ret_stat),
                          "\t \t FAIL: Failed to " + action_type  + " the term " + each_term + ", has review status "
                        + ret_rw_status + " & Status " + ret_stat, "\t \t PASS: The term " + each_term + " has Review "
                        "Status - " + ret_rw_status + " & Status - " + ret_stat + " as expected. ")


def cancel_term(login_as_uname, comp_term_num, addnl_term_num):

    lg.log_results("\t \t Cancelling the terms.")
    logged_user = bu.find_element_by_xpath(e.home_usr_full_name).text
    if logged_user.casefold() not in login_as_uname.casefold():
        lauw_search.login_as_with_search(login_as_uname, 'user')
        w_bvt_hlpr.search_on_top(cwuc.new_pa_name,'Provider Agreement')
        navigate_to_provider_agreement_page()

    edit_provider_agreement_terms()
    table_e = bu.is_visible_xpath(e.w_left_at_table)
    row_count = len(table_e.find_elements_by_xpath(".//tr"))
    index = int(row_count) - 1
    while index >= 1:
        bu.click_elmt(e.w_left_at_tr_str + str(index) + "]//td[1]")
        bu.sleep_for(1)
        # right_cancellation_checkbox = bu.find_element_by_xpath("//input[@id='compilationPageId:compilationFormId:j_id116']")
        bu.click_elmt(e.w_left_at_cancellation_chbx)

        bu.sleep_for(1)
        bu.click_elmt_n_ss_b4_after(e.w_left_at_end_btn)
        bu.sleep_for(1)
        index -= 1

    bu.sleep_for(1)
    bu.focus_to(e.w_execute_changes_btn)
    bu.click_elmt(e.w_execute_changes_btn)
    bu.sleep_for(1)
    element = bu.find_element_by_xpath(e.w_pa_body_tbl)
    text_on_page = element.text
    lg.assert_and_log('Error' not in text_on_page, "\t \t FAIL : " + text_on_page, "\t \t PASS : Cancelled the "
                    "terms.")

    # Adding new cancellation validation
    at_name_dictry, at_id_dictry = sh.get_at_ids_names(cwuc.new_pa_name, 'cancel')
    for each_term in at_name_dictry:
        # 'Approved', 'Pending' in rw_stat or 'Pending Removal' in stat
        status_list = ['Approved', 'Pending', 'Declined']
        ret_rw_status, ret_stat = sh.get_rw_stat_and_stat(cwuc.new_pa_name, each_term)
        lg.assert_and_log(ret_rw_status in status_list or 'Pending Removal' in ret_stat,
        "\t \t FAIL: The term " + each_term + " did not cancelled correctly. Review Status is " + ret_rw_status +
        " & Status is " + ret_stat, "\t \t PASS: Verified Cancelled terms.")



def loop_thru_term_tables(term_type, at_number):
    bu.sleep_for(2)

    if 'Compensation' in term_type:
        table_element = bu.is_visible_xpath(e.compensation_term_link_tbody, cwuc.web_longer_wait)
    else:
        table_element = bu.is_visible_xpath(e.addtnal_term_link_tbody, cwuc.web_longer_wait)

    if 'Compensation' in term_type:
        row_link = e.comp_term_xpath
        if at_number > 5:
            go_to_list_element = bu.is_visible_xpath(e.pa_ct_terms_go_to_list, cwuc.web_longer_wait)
            row_count_value = go_to_list_element.get_attribute('text')
            row_count = row_count_value.split('(')[1].split(')')[0]
            index = int(row_count) + 1
            bu.click_elmt("//div[@id='a341k000000A42J_00N34000005l2K5_body']//div[@class='pShowMore'][contains(text(),'|')]//a[1]")
        else:
            row_count = len(table_element.find_elements_by_xpath(".//tr"))
            index = int(row_count)
    else:
        row_link = e.addtnal_term_xpath_row_link
        row_count = len(table_element.find_elements_by_xpath(".//tr"))
        index = int(row_count)

    i = 2  # Always start with the Second row as header is first row
    term_dictnry = {}
    table_row_num = 1
    while index > 1:
        #  HARDCODED PATH HERE
        element_xpath = row_link + str(i) + "]/th[1]/a"
        term_name_element = bu.is_visible_xpath(element_xpath, cwuc.web_longer_wait)
        term_name = term_name_element.get_attribute('text')

        if 'Compensation' in term_type:
            term_description_xpath = row_link + str(i) + "]//td[5]"
            revw_stat_xpath = row_link + str(i) + "]//td[9]"
            status_xpath = row_link + str(i) + "]//td[10]"
        else:
            term_description_xpath = row_link + str(i) + "]//td[5]"
            revw_stat_xpath = row_link + str(i) + "]//td[9]"
            status_xpath = row_link + str(i) + "]//td[10]"

        revw_stat_e = bu.find_element_by_xpath(revw_stat_xpath)
        review_stat_value = revw_stat_e.text

        status_e = bu.find_element_by_xpath(status_xpath)
        status_value = status_e.text

        description_e = bu.find_element_by_xpath(term_description_xpath)
        description_value = description_e.text

        term_dictnry[table_row_num] = {'term_name': term_name , 'review_status' : review_stat_value, 'status_value' :
            status_value, 'description': description_value}
        index -= 1
        i += 1
        table_row_num += 1

    return term_dictnry


def unlock_record():

    bu.click_elmt(e.pa_unlock_button)
    alert_obj = cwuc.wd.switch_to.alert
    alert_obj.accept()


def approve_reject_pa(pa_user, action):
    lg.log_results("\t\t Starting to " + action + " PA as " + pa_user)
    bu.sleep_for(5)
    lauw_search.login_as_with_search(pa_user, 'user')
    w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
    navigate_to_provider_agreement_page()
    bu.click_elmt("//div[@class='relatedProcessHistory']//table[@class='list']//a[contains(text(),'Approve / Reject')]")
    if action == 'Approve':
        bu.send_keys_elmt_xpath("//textarea[@id='Comments']", "Approved By Automation Script.")
        bu.click_elmt("//input[@name='goNext']")
    else:
        bu.send_keys_elmt_xpath("//textarea[@id='Comments']", "Rejected By Automation Script.")
        bu.click_elmt("//input[@name='Reject']")
    bu.take_screenshot()

def pa_recordtype_validation(expected_rec_type, expected_pa_stage_val):
    pa_stage_val = sh.get_stage_for_pa(cwuc.new_pa_name)
    #expected_pa_stage_val = tv.pa_stage_value_da
    lg.assert_and_log(expected_pa_stage_val == pa_stage_val, '\t \t FAIL: Unexpected PA Stage value ' +
                      pa_stage_val, "\t \t PASS: Expected stage value of Provider Agreement: " + pa_stage_val)
    bu.sleep_for(2)
    rec_type_on_pg = bu.get_text("//*[@id='ep']/div[2]/div[5]/table/tbody/tr[2]/td[2]")
    bu.take_screenshot()
    # Amendment - DDP [Change]
    rtype_on_page_stripped = rec_type_on_pg.replace(' [Change]', '').strip()
    lg.assert_and_log(rtype_on_page_stripped == expected_rec_type,
                      '\t \t FAIL: Expected record type is - ' + expected_rec_type + ', not ' + rtype_on_page_stripped,
                      '\t \t PASS: Record Type value of Provider Agreement is as expected - ' + rtype_on_page_stripped)


def get_pa_approver(pa_name):
    apvr_set_correctly = False
    apvr_list = []
    w_bvt_hlpr.search_on_top(cwuc.new_pa_name, 'Provider Agreement')
    navigate_to_provider_agreement_page()
    bu.sleep_for(3)
    bu.scroll_to(e.pa_approver_name)
    bu.take_screenshot()
    cwuc.pa_apvr = bu.get_text(e.pa_approver_name)      # gets the approver name from PA page
    ssl_name = sh.get_ssl_name(cwuc.candidate_name_1)   # gets the SSL name from candidate
    apvr_list, apvr_dict = sh.get_approvers_ssl(ssl_name)          # gets the approvers on SSL

    if cwuc.pa_apvr in apvr_list:                       # checks if approver is on the list and level of approval on SSL
        apvr_id = sh.get_user_id_name(cwuc.pa_apvr)
        approver_field_name = get_approval_level(apvr_id, apvr_dict)
        apvr_set_correctly = True
        if approver_field_name == 'Primary_Clinical_Approver__c':
            lg.log_results("\t \t " + cwuc.pa_apvr + " is set as Primary Clinical Approver on the SSL")

        elif approver_field_name == 'Secondary_Clinical_Approver__c':
            lg.log_results("\t \t " + cwuc.pa_apvr + " is set as Secondary Clinical Approver on the SSL")

        elif approver_field_name == 'Primary_Compensation_Approver__c':
            lg.log_results("\t \t " + cwuc.pa_apvr + " is set as Primary Compensation Approver on the SSL")

        elif approver_field_name == 'Secondary_Compensation_Approver__c':
            lg.log_results("\t \t " + cwuc.pa_apvr + " is set as Secondary Compensation Approver on the SSL")

        elif approver_field_name == 'Primary_CRNA_Approver__c':
            lg.log_results("\t \t " + cwuc.pa_apvr + " is set as Primary CRNA Approver on the SSL")

        elif approver_field_name == 'Secondary_CRNA_Approver__c':
            lg.log_results("\t \t " + cwuc.pa_apvr + " is set as Secondary CRNA Approver on the SSL")

    lg.assert_and_log(apvr_set_correctly is True, "\t\t FAIL: Approver is not set correctly.",
                      "\t\t PASS: Approver is set correctly.")


def get_approval_level(apvr_id, apvr_dict):
    for key, value in apvr_dict.items():
        if value == apvr_id:
            return key


def check_app_wc_email(pa_name):
    exp_sub = 'Email: Welcome to Sound Physicians!'
    pa = bu.get_text("//h2[@class='pageDescription']")
    bu.sleep_for(5)
    if pa_name in pa:
        app_email_sent_date = bu.get_text(e.pa_app_email_sent_date)
        bu.take_screenshot()
        lg.log_results("\t \t APP Welcome notification Email is sent on " + app_email_sent_date)
        bu.click_elmt(e.pa_act_htry_link)
        # get text of the email subject to verify
        email_sub = bu.get_text(e.pa_app_wc_email_sub)
        if email_sub:
            bu.click_elmt("(//div[@class='listRelatedObject taskBlock'])[2]/div/div[2]/table/tbody/tr[2]/th/a")
            sender = bu.get_text("//div[@id='FromName_ileinner']")
            receiver_email = bu.get_text("//div[@id='ToAddress_ileinner']")
            bu.take_screenshot()
            lg.assert_and_log(email_sub in exp_sub, "\t \t FAIL: The APP Welcome notification Email subject is expected to be "
                              + exp_sub + " but is " + email_sub, "\t \t PASS: The APP Welcome Notification Email is sent on "+
                              app_email_sent_date + " with subject " + email_sub)
            lg.log_results("\t \t The APP Welcome notification email is sent from " + sender + " to " + receiver_email)
        else:
            lg.log_results("\t \t APP Welcome notification email not sent.")